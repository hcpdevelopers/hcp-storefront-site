<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Order Connectome in a Box | Human Connectome Project</title>
    <meta http-equiv="Description" content="Public access to HCP data. Currently, Phase 1 Pilot Data is available." />
    <meta http-equiv="Keywords" content="Public data release, Human Connectome Project, pilot data, brain connectivity data" />
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/header-includes.php'); ?>

</head>

<body>

<!-- site container -->
<div id="site_container">

    <div id="NIH_header2">
        <span><a href="http://www.neuroscienceblueprint.nih.gov/connectome/" title="NIH Human Connectome Project" style="color:#b4cfff; text-decoration:none;">NIH Blueprint: The Human Connectome Project</a></span>
    </div>

    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/storefront-topnav.php'); ?>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/db-login.php'); ?>

    <?php
    $q = "SELECT status,DATE_FORMAT(last_modified,'%M %D, %Y') as date FROM storefront_status ORDER BY last_modified DESC LIMIT 1;";
    $r = mysqli_query($db,$q) or die ($q);
    $cinab_status = mysqli_fetch_assoc($r);

    $q = "SELECT status,DATE_FORMAT(last_modified,'%M %D, %Y') as date FROM registration_status ORDER BY last_modified DESC LIMIT 1;";
    $r = mysqli_query($db,$q) or die ($q);
    $course_status = mysqli_fetch_assoc($r);

    $q = "SELECT count(release_id) as num FROM releases WHERE status = 'current';";
    $r = mysqli_query($db,$q) or die ($q);
    $current_releases = mysqli_fetch_assoc($r);

    $q = "SELECT count(release_id) as num FROM releases WHERE status = 'coming soon';";
    $r = mysqli_query($db,$q) or die ($q);
    $pending_releases = mysqli_fetch_assoc($r);

    $q = "SELECT count(course_id) as num FROM courses WHERE status = 'current';";
    $r = mysqli_query($db,$q) or die ($q);
    $current_courses = mysqli_fetch_assoc($r);

    $q = "SELECT count(course_id) as num FROM courses WHERE status = 'archived';";
    $r = mysqli_query($db,$q) or die ($q);
    $old_courses = mysqli_fetch_assoc($r);
    ?>

    <!-- "Middle" area contains all content between the top navigation and the footer -->
    <div id="middle">
        <div id="content-wide">
            <?php if ($_GET['dataMessage']) : ?>

                <?php if (strpos($_GET['dataUrl'] ,"data-use-terms") > 0) : ?>
                    <script>
                        window.location.assign('https://www.humanconnectome.org/study/hcp-young-adult/document/wu-minn-hcp-consortium-open-access-data-use-terms')
                    </script>
                <?php else : ?>

                    <div class="data-message alertBox">
                        <p><strong><?php echo str_replace("_"," ",$_GET['dataMessage']) ?></strong></p>
                        <p>The HCP Data storefront is permanently closed. The content at <strong><?php echo $_GET['dataUrl'] ?></strong> is no longer available. </p>
                    </div>
                <?php endif; ?>
            <?php endif; ?>


            <div class="column-third centered">
                <h2>Sign Up For HCP Courses</h2>
                <p><a href="/courses"><img src="/img/icon-HCP-course.png" alt="course icon" /></a></p>
                <?php if ($course_status['status'] !== 'closed') : ?>
                    <?php
                    if ($current_courses['num'] > 0) :
                        echo "<p><a href='/courses/course-registration.php'><button>Register Now</button></a>";
                        echo "<p>Course registration is now open! <a href='/courses'>Browse Courses</a></p>";
                    elseif ($old_courses['num'] > 0) :
                        echo "<p><a href=\"/courses\"><button>Browse Courses</button></a></p>";
                        echo "<p></p><p>Browse and download materials for past courses</p>";
                    endif;
                    ?>
                <?php else : ?>
                    <p><a href="/courses"><button>Browse Courses</button></a></p>
                    <p>Browse and download materials for past courses</p>
                <?php endif; ?>
            </div>
            <div class="column-third centered">
                <h2>Need Help With An Order?</h2>
                <p><a href="/support"><img src="/img/icon-HCP-support.png" alt="support icon" /></a></p>
                <p><a href="/support"><button>Get Support</button></a></p>
                <p>We will be happy to assist you. </p>
            </div>
            <div class="column-third centered disabled" style="opacity: 0.4">
                <h2>Order HCP Data</h2>
                <p><img src="/img/icon-HCP-data.png" alt="data icon" /></p>
                <p>The Data storefront is permanently closed, as of <?php echo $cinab_status['date'] ?></p>
            </div>



        </div>
        <!-- /#Content -->

    </div> <!-- middle -->

    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/footer.php'); ?>

</div>
<!-- end container -->

<script src="//www.google.com/jsapi?key=AIzaSyA5m1Nc8ws2BbmPRwKu5gFradvD_hgq6G0" type="text/javascript"></script>
<script type="text/javascript">
    /*
     *  How to restrict a search to a Custom Search Engine
     *  http://www.google.com/cse/
     */

    google.load('search', '1');

    function OnLoad() {
        // Create a search control
        var searchControl = new google.search.SearchControl();

        // Add in a WebSearch
        var webSearch = new google.search.WebSearch();

        // Restrict our search to pages from our CSE
        webSearch.setSiteRestriction('000073750279216385221:vnupk6whx-c');

        // Add the searcher to the SearchControl
        searchControl.addSearcher(webSearch);

        // tell the searcher to draw itself and tell it where to attach
        searchControl.draw(document.getElementById("search"));

        // execute an inital search
        // searchControl.execute('');
    }

    google.setOnLoadCallback(OnLoad);

</script>


<!-- google analytics -->
<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-18630139-1']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

</script>
</body>
</html>
