<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>HCP Storefront Support | Human Connectome Project</title>
    <meta http-equiv="Description" content="HCP Order Support -- order lookup" />
    <meta http-equiv="Keywords" content="Public data release, Human Connectome Project, pilot data, brain connectivity data" />
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/header-includes.php'); ?>

</head>

<body>

<!-- site container -->
<div id="site_container">

    <div id="NIH_header2">
        <span><a href="http://www.neuroscienceblueprint.nih.gov/connectome/" title="NIH Human Connectome Project" style="color:#b4cfff; text-decoration:none;">NIH Blueprint: The Human Connectome Project</a></span>
    </div>

    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/storefront-topnav.php'); ?>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/db-login.php'); ?>

    <!-- Breadcrumbs -->
    <div id="breadcrumbs">
        <p>&raquo; <a href="https://www.humanconnectome.org">HCP Home</a> &gt; <a href="/">Storefront</a> &gt; Support</p>
    </div>
    <!-- end breadcrumbs -->

    <!-- "Middle" area contains all content between the top navigation and the footer -->
    <div id="middle">
        <div id="content-wide">
            <h1>HCP Order Support</h1>

            <?php
            if ($_POST['id']) :
                $order = $_POST;

                $q = "SELECT * FROM receipt WHERE order_id='".$order['id']."'";
                $r = mysqli_query($db,$q) or die ($q);
                $receipt = mysqli_fetch_array($r);

                ?>

                <div class="hcpOrderSummary">
                    <h3>Order <?php echo $order['id'] ?></h3>

                    <!-- order summary -->
                    <div class="column-third">
                        <p><strong>Order Summary</strong></p>
                        <?php if ($_POST['order_type'] === 'data') : ?>
                        <div class="well">
                            <?php
                            $q = "SELECT * FROM releases WHERE release_id='".$order['data_ordered']."';";
                            $r = mysqli_query($db,$q) or die($q);
                            $releaseInfo = mysqli_fetch_assoc($r);
                            ?>
                            <ul>
                                <li style="font-weight: bold;">Data Ordered: <?php echo $releaseInfo['title'] ?></li>
                                <li>Order Manifest: <?php echo $releaseInfo['data_size'] ?> total data on <?php echo $releaseInfo['drive_qty'] ?> hard drives</li>
                                <li>Order Status: <?php echo $order['status']; if ($order['delivered']) echo " - delivered"; ?></li>
                                <li>Date Ordered: <?php echo print_date($order['order_date']) ?></li>
                                <?php if ($order['status'] === 'shipped') : ?>
                                    <li>Date Shipped: <?php echo print_date($order['date_shipped']) ?></li>
                                    <li>Fedex Tracking ID: <a href="https://www.fedex.com/apps/fedextrack/?action=track&tracknumbers=<?php echo $order['tracking_code'] ?>&locale=en_US&cntry_code=us" target="_blank"><?php echo $order['tracking_code'] ?></a></li>
                                <?php endif; ?>
                                <?php if ($order['status'] === 'refund') : ?>
                                    <li>Date Refunded: <?php echo print_date($order['date_refunded']) ?></li>
                                    <li>Amount Refunded: $<?php echo number_format($order['amount_refunded'],2); ?></li>
                                    <li>Refund Transaction ID: <?php echo $order['refund_tx'] ?></li>
                                <?php endif; ?>
                            </ul>
                        </div>
                        <p><strong>Drives In Your Order</strong></p>
                        <div class="well">
                            <ul>
                            <?php
                            $q = "SELECT inv.serial, inv.release_id, inv.drive_type, inv.drive_size FROM drives_ordered as ord,drive_inventory as inv WHERE ord.order_id='".$order['id']."' AND ord.serial=inv.serial AND inv.drive_status='shipped' ORDER BY inv.release_id ASC;";
                            $r = mysqli_query($db,$q) or die($q);
                            $driveManifest = $r;
                            ?>
                            <?php while ($drive = mysqli_fetch_assoc($driveManifest)) :  ?>
                                <li>Serial: <strong><?php echo $drive['serial'] ?></strong> - <?php echo $drive['release_id'] ?></li>
                            <?php endwhile; ?>
                            </ul>
                        </div>
                        <?php elseif ($_POST['order_type'] === 'preorder') : ?>
                        <div class="well">
                            <?php
                            $q = "SELECT * FROM releases WHERE release_id='".$order['data_ordered']."';";
                            $r = mysqli_query($db,$q) or die($q);
                            $releaseInfo = mysqli_fetch_assoc($r);

                            $q = "SELECT * FROM preorders WHERE id='".$order['preorder_id']."';";
                            $r = mysqli_query($db,$q) or die($q);
                            $preorderInfo = mysqli_fetch_assoc($r);
                            ?>
                            <ul>
                                <li style="font-weight: bold;">Data Preordered: <?php echo $releaseInfo['title'] ?></li>
                                <li>Date Ordered: <?php echo print_date($order['order_date']) ?></li>
                                <li>Order Manifest: <?php echo $releaseInfo['data_size'] ?> total data on <?php echo $releaseInfo['drive_qty'] ?> hard drives</li>
                                <li>Preorder Code: <?php echo $preorderInfo['id'] ?></li>
                                <li>Preorder Status: <?php echo $preorderInfo['status']; ?></li>
                                <?php if ($preorderInfo['status'] === 'applied') : ?>
                                    <li>Preorder amount applied to Order <?php echo $preorderInfo['order_completion_id'] ?></li>
                                <?php endif; ?>
                            </ul>
                        </div>

                        <?php elseif ($_POST['order_type'] === 'course') : ?>
                        <div class="well">
                            <?php
                            $q = "SELECT * FROM courses WHERE course_id='".$order['course_registered']."';";
                            $r = mysqli_query($db,$q) or die($q);
                            $courseInfo = mysqli_fetch_assoc($r);
                            ?>
                            <ul>
                                <li style="font-weight: bold">Course Registration: <?php echo $courseInfo['course_title']; ?></li>
                                <li>Registration Date: <?php echo print_date($order['order_date']) ?></li>
                                <li>Location: <?php echo $courseInfo['location'] ?></li>
                                <li>Course Dates: <?php echo print_date($courseInfo['course_start_date']) ?> &ndash; <?php echo print_date($courseInfo['course_end_date']) ?></li>
                                <li>Registration Status: <?php echo ($order['status'] === 'open') ? 'Registered' : $order['status'] ?></li>
                                <?php if ($order['status'] === 'refund') : ?>
                                    <li>Date Refunded: <?php echo $order['date_refunded'] ?></li>
                                    <li>Amount Refunded: $<?php echo number_format($order['amount_refunded'],2); ?></li>
                                    <li>Refund Transaction ID: <?php echo $order['refund_tx'] ?></li>
                                <?php endif; ?>
                            </ul>
                        </div>

                        <?php endif; ?>
                    </div>

                    <!-- customer info -->
                    <div class="column-third">
                        <?php if ($_POST['order_type'] === 'data' || $_POST['order_type'] === 'preorder') : ?>
                            <p><strong>Customer Info</strong></p>
                            <div class="well">
                                <ul>
                                    <?php
                                    $addressKeys = array('customer_name','customer_institution','shipping_address','shipping_address_2','shipping_city','shipping_state','shipping_postal_code','shipping_country');
                                    foreach ($addressKeys as $v) {
                                        if ($order[$v]) echo "<li>".$order[$v]."</li>";
                                    }
                                    ?>
                                </ul>
                            </div>
                            <p><strong>Contact Info</strong></p>
                            <div class="well">
                                <ul>
                                    <?php
                                    $addressKeys = array('Phone' => 'shipping_phone','Email' => 'customer_email');
                                    foreach ($addressKeys as $k => $v) {
                                        if ($order[$v]) echo "<li><strong>".$k.":</strong> ".$order[$v]."</li>";
                                    }
                                    ?>
                                </ul>
                            </div>

                        <?php elseif ($_POST['order_type'] === 'course') : ?>
                            <p><strong>Attendee Information</strong></p>
                            <div class="well">
                                <ul>
                                    <?php
                                    $participantKeys = array(
                                        'Name' => 'customer_name',
                                        'Contact Email' => 'customer_email',
                                        'Contact Phone'=>'shipping_phone',
                                        'Research PI'=>'customer_pi'
                                    );
                                    foreach( $participantKeys as $k => $v):
                                        if ($order[$v]) echo "<li>".$k.": ".$order[$v]."</li>";
                                    endforeach;
                                    ?>
                                </ul>
                            </div>
                            <p><strong>Billing Information</strong> </p>
                            <div class="well">
                                <ul>
                                    <?php
                                    $billingKeys = array('billing_contact','billing_address','billing_address_2','billing_city','billing_state','billing_postal_code','billing_country');
                                    foreach ($billingKeys as $v) {
                                        if ($order[$v]) echo "<li>".$order[$v]."</li>";
                                    }
                                    ?>
                                </ul>
                            </div>
                        <?php endif; ?>
                    </div>


                    <!-- receipt info -->
                    <div class="column-third">
                        <p><strong>Order Receipt</strong></p>

                        <?php if ($_POST['order_type'] === 'data') : ?>
                            <div class="well">
                                <ul>
                                    <li>Transaction ID: <?php echo $order['transaction_id']; ?></li>
                                    <?php
                                    $receiptKeys = array(
                                        'Total Drive Cost' => 'total_drive_cost',
                                        'Enclosure Cost' => 'enclosure_cost',
                                        'Shipping Cost' => 'shipping_cost',
                                        'Sales Tax' => 'sales_tax',
                                        'CashNet Fee' => 'cashnet_fee'
                                    );
                                    foreach ($receiptKeys as $k => $v):
                                        if ($receipt[$v]) echo "<li>".$k.": $".number_format($receipt[$v],2)."</li>";
                                    endforeach;
                                    ?>
                                    <?php if ($order['preorder_id']) : ?>
                                    <li>Preorder ID applied: <?php echo $order['preorder_id'] ?></li>
                                    <?php endif; ?>
                                    <li>Receipt Notes: <?php echo $receipt['receipt_notes'] ?></li>
                                </ul>
                            </div>
                        <?php elseif ($_POST['order_type'] === 'preorder') : ?>
                            <div class="well">
                                <ul>
                                    <li>Transaction ID: <?php echo $order['transaction_id']; ?></li>
                                    <li>Preorder Deposit Amount: $<?php echo number_format($receipt['preorder_amount'],2); ?></li>
                                </ul>
                            </div>

                        <?php elseif ($_POST['order_type'] === 'course') : ?>
                            <div class="well">
                                <ul>
                                    <li>Transaction ID: <?php echo $order['transaction_id']; ?></li>
                                    <li>Registration Rate: <?php echo $order['registration_type'] ?></li>
                                    <?php
                                    $receiptKeys = array(
                                        'Total Registration Cost' => 'registration_cost',
                                        'Sales Tax' => 'sales_tax',
                                        'CashNet Fee' => 'cashnet_fee'
                                    );
                                    foreach ($receiptKeys as $k => $v):
                                        if ($receipt[$v]) echo "<li>".$k.": $".number_format($receipt[$v],2)."</li>";
                                    endforeach;
                                    ?>
                                    <?php
                                    if ($receipt['other_cost']) :
                                        $otherCost = json_decode($receipt['other_cost'],true);
                                        echo "<li>".$otherCost['charge_title'].": $".number_format(floatval($otherCost['charge_amount']),2)."</li>";
                                    endif; ?>

                                    <li>Receipt Notes: <?php echo $receipt['receipt_notes'] ?></li>
                                    <li>Registration Notes: <?php echo $order['notes'] ?></li>
                                </ul>
                            </div>

                        <?php endif; ?>

                    </div>
                    <p><strong>See any errors in this order information? Please <a href="mailto:orders@humanconnectome.org">Contact Order Support</a>.</strong></p>
                </div>

                <p>Look up another order?</p>

            <?php elseif ($_POST['error']) : ?>
                <p class="error"><strong><?php echo $_POST['error'] ?></strong></p>
                <p>Look up another order?</p>

            <?php else : ?>
                <p>Need to confirm an order or check on its status? You can look up its information here. </p>
            <?php endif; ?>

            <form action="/support/process-order-lookup.php" method="post" class="hcpForm">
                <div class="hcpForm-wrapper" id="product-info" style="overflow:auto;">
                    <div class="hidden error-messages error-message-product">
                        <p><strong>Errors Found:</strong></p>
                        <ul class="error-list">
                            <li>Things fall apart, the centre cannot hold.</li>
                        </ul>
                    </div>

                    <p>Please provide order verification information: </p>
                    <div>
                        <label for="order_id">HCP Order ID or Transaction ID</label>
                        <input type="text" name="order_id" class="required" title="Order ID" />
                    </div>
                    <div>
                        <label for="customer_email">Customer Email Address Associated With Order</label>
                        <input type="text" name="customer_email" class="required email" title="Customer Email Address" />
                    </div>

                    <div>
                        <input type="submit" value="Look Up Order" />
                    </div>

                </div>
            </form>



        </div>
        <!-- /#Content -->

    </div> <!-- middle -->

    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/footer.php'); ?>

</div>
<!-- end container -->

<script src="//www.google.com/jsapi?key=AIzaSyA5m1Nc8ws2BbmPRwKu5gFradvD_hgq6G0" type="text/javascript"></script>
<script type="text/javascript">
    /*
     *  How to restrict a search to a Custom Search Engine
     *  http://www.google.com/cse/
     */

    google.load('search', '1');

    function OnLoad() {
        // Create a search control
        var searchControl = new google.search.SearchControl();

        // Add in a WebSearch
        var webSearch = new google.search.WebSearch();

        // Restrict our search to pages from our CSE
        webSearch.setSiteRestriction('000073750279216385221:vnupk6whx-c');

        // Add the searcher to the SearchControl
        searchControl.addSearcher(webSearch);

        // tell the searcher to draw itself and tell it where to attach
        searchControl.draw(document.getElementById("search"));

        // execute an inital search
        // searchControl.execute('');
    }

    google.setOnLoadCallback(OnLoad);

</script>


<!-- google analytics -->
<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-18630139-1']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

</script>
</body>
</html>