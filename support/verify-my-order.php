<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Order Connectome in a Box | Human Connectome Project</title>
    <meta http-equiv="Description" content="Public access to HCP data. Currently, Phase 1 Pilot Data is available." />
    <meta http-equiv="Keywords" content="Public data release, Human Connectome Project, pilot data, brain connectivity data" />
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/header-includes.php'); ?>

</head>

<body>

<!-- site container -->
<div id="site_container">

    <div id="NIH_header2">
        <span><a href="http://www.neuroscienceblueprint.nih.gov/connectome/" title="NIH Human Connectome Project" style="color:#b4cfff; text-decoration:none;">NIH Blueprint: The Human Connectome Project</a></span>
    </div>

    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/storefront-topnav.php'); ?>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/db-login.php'); ?>

    <!-- Breadcrumbs -->
    <div id="breadcrumbs">
        <p>&raquo; <a href="https://www.humanconnectome.org">HCP Home</a> &gt; <a href="/">Storefront</a> &gt; Support</p></div>
    <!-- end breadcrumbs -->

    <?php
    $q = "SELECT *,DATE_FORMAT(last_modified,'%M %D, %Y') as date FROM storefront_status ORDER BY last_modified DESC LIMIT 1;";
    $r = mysqli_query($db,$q) or die ($q);
    $cinab_status = mysqli_fetch_assoc($r);

    $q = "SELECT *,DATE_FORMAT(last_modified,'%M %D, %Y') as date FROM registration_status ORDER BY last_modified DESC LIMIT 1;";
    $r = mysqli_query($db,$q) or die ($q);
    $course_status = mysqli_fetch_assoc($r);

    $notifications = array();

    if ($cinab_status['status'] === 'closed'):
        $notifications[] = array('text' => $cinab_status['message_text'], 'date' => $cinab_status['date']);
    endif;

    if ($course_status['status'] === 'closed'):
        $notifications[] = array('text' => $course_status['message_text'], 'date' => $course_status['date']);
    endif;

    ?>

    <!-- "Middle" area contains all content between the top navigation and the footer -->
    <div id="middle">
        <div id="content-wide">
            <h1>HCP Drive Exchange Program</h1>
            <h2>Order and Drive Verification</h2>
            <p>This is the first step of the process of entering the HCP Drive Exchange program. On this page, you will verify your previous order of the HCP 900 Subjects Data Release, which is required to qualify for the exchange program. When this order information is verified, you will have the opportunity to generate a custom code which you can use for a limited time to place a preorder for the HCP-1200 data release hard drives. </p>
            <p>If you have any questions about this program or this process, email <strong><a href="mailto:orders@humanconnectome.org">orders@humanconnectome.org</a></strong> and we will assist you. </p>

            <h2>Order Lookup</h2>
            <p>Enter your order information in this form to proceed. </p>

            <form action="/support/process-order-lookup.php" method="post" class="hcpForm">
                <input type="hidden" name="config" value="exchange-verify" />
                <div class="hcpForm-wrapper" id="product-info" style="overflow:auto;">
                    <div class="hidden error-messages error-message-product">
                        <p><strong>Errors Found:</strong></p>
                        <ul class="error-list">
                            <li>To err is human, to report on errors is divine.</li>
                        </ul>
                    </div>

                    <p>Please provide order verification information: </p>
                    <div>
                        <label for="order_id">HCP Order ID or Transaction ID</label>
                        <input type="text" name="order_id" class="required" title="Order ID" />
                    </div>
                    <div>
                        <label for="customer_email">Customer Email Address Associated With Order</label>
                        <input type="text" name="customer_email" class="required email" title="Customer Email Address" />
                    </div>

                    <div>
                        <input type="submit" value="Look Up Order" />
                    </div>

                </div>
            </form>

        </div>
        <!-- /#Content -->

    </div> <!-- middle -->

    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/footer.php'); ?>

</div>
<!-- end container -->

<script src="//www.google.com/jsapi?key=AIzaSyA5m1Nc8ws2BbmPRwKu5gFradvD_hgq6G0" type="text/javascript"></script>
<script type="text/javascript">
    /*
     *  How to restrict a search to a Custom Search Engine
     *  http://www.google.com/cse/
     */

    google.load('search', '1');

    function OnLoad() {
        // Create a search control
        var searchControl = new google.search.SearchControl();

        // Add in a WebSearch
        var webSearch = new google.search.WebSearch();

        // Restrict our search to pages from our CSE
        webSearch.setSiteRestriction('000073750279216385221:vnupk6whx-c');

        // Add the searcher to the SearchControl
        searchControl.addSearcher(webSearch);

        // tell the searcher to draw itself and tell it where to attach
        searchControl.draw(document.getElementById("search"));

        // execute an inital search
        // searchControl.execute('');
    }

    google.setOnLoadCallback(OnLoad);

</script>


<!-- google analytics -->
<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-18630139-1']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

</script>
</body>
</html>
