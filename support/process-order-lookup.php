<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Order Connectome in a Box | Human Connectome Project</title>
    <meta http-equiv="Description" content="Public access to HCP data. Currently, Phase 1 Pilot Data is available." />
    <meta http-equiv="Keywords" content="Public data release, Human Connectome Project, pilot data, brain connectivity data" />
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/header-includes.php'); ?>

</head>

<body>

<!-- site container -->
<div id="site_container">

    <div id="NIH_header2">
        <span><a href="http://www.neuroscienceblueprint.nih.gov/connectome/" title="NIH Human Connectome Project" style="color:#b4cfff; text-decoration:none;">NIH Blueprint: The Human Connectome Project</a></span>
    </div>

    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/storefront-topnav.php'); ?>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/db-login.php'); ?>

    <!-- Breadcrumbs -->
    <div id="breadcrumbs">
        <p>&raquo; <a href="https://www.humanconnectome.org">HCP Home</a> &gt; <a href="/">Storefront</a> &gt; Support</p></div>
    <!-- end breadcrumbs -->

    <!-- "Middle" area contains all content between the top navigation and the footer -->
    <div id="middle">
        <div id="content">
            <h1>HCP Order Support</h1>
            <?php
            if ($_POST['order_id'] && $_POST['customer_email'] && !$_POST['customer_name']) :
                
                $q = "SELECT *,date_format(timestamp,'%M %D, %Y') as order_date FROM orders WHERE (id='".$_POST['order_id']."' AND customer_email LIKE '%".$_POST['customer_email']."%') OR (transaction_id='".$_POST['order_id']."' AND customer_email LIKE '%".$_POST['customer_email']."%') LIMIT 1;";
                $r = mysqli_query($db,$q) or die('<p>Sorry, there was an error with this query. Please contact site support.</p><p>'.$q.'</p>');
                
                $formAction = ($_POST['config'] == 'exchange-verify') ? 'order-verification.php' : 'order-lookup.php';
                ?>
                <form method="POST" action="<?php echo $formAction ?>">
                <?php
                if (mysqli_num_rows($r) > 0) :
                    $orderInfo = mysqli_fetch_assoc($r);

                    foreach($orderInfo as $k => $v) :
                        ?>
                        <input type="hidden" name="<?php echo $k ?>" value="<?php echo $v ?>" />
                        <?php
                    endforeach;
                    ?>
                <?php else : ?>
                    <input type="hidden" name="error" value="Sorry, no matches were returned from the order ID and email address you provided" />
                    <input type="hidden" name="debug" value="<?php echo $q ?>" />
                <?php endif; ?>
                    <!-- <input type="submit" value="Show order info" /> -->
                </form>

                <script type="application/javascript">

                    $(document).ready(function(){
                        $('form').submit();
                    })

                </script>
            <?php



            else :
                die('<p>Sorry, there was an error with your request. Please try again.</p>');
            endif;
            ?>

        </div>
    </div>

    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/footer.php'); ?>

</div>
<!-- end container -->

</body>
</html>
