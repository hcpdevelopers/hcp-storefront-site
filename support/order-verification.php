<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Order Connectome in a Box | Human Connectome Project</title>
    <meta http-equiv="Description" content="Public access to HCP data. Currently, Phase 1 Pilot Data is available." />
    <meta http-equiv="Keywords" content="Public data release, Human Connectome Project, pilot data, brain connectivity data" />
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/header-includes.php'); ?>

</head>

<body>

<!-- site container -->
<div id="site_container">

    <div id="NIH_header2">
        <span><a href="http://www.neuroscienceblueprint.nih.gov/connectome/" title="NIH Human Connectome Project" style="color:#b4cfff; text-decoration:none;">NIH Blueprint: The Human Connectome Project</a></span>
    </div>

    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/storefront-topnav.php'); ?>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/db-login.php'); ?>

    <!-- Breadcrumbs -->
    <div id="breadcrumbs">
        <p>&raquo; <a href="https://www.humanconnectome.org">HCP Home</a> &gt; <a href="/">Storefront</a> &gt; Support</p>
    </div>
    <!-- end breadcrumbs -->

    <!-- "Middle" area contains all content between the top navigation and the footer -->
    <div id="middle">
        <div id="content-wide">
            <h1>HCP Drive Exchange Program</h1>

            <?php
            if ($_POST['id'] && ($_POST['order_type'] == 'data') && in_array(strtolower($_POST['status']), ['shipped','hand-delivery']) ) :
                if (strpos($_POST['data_ordered'],'900') === false) :
                    die ('<p>Sorry, this offer is only available to orderers of the 900 Subjects Data Release. If you feel you have received this message in error, please contact orders@humanconnectome.org.</p>');
                endif;
                $order = $_POST;

                $q = "SELECT * FROM receipt WHERE order_id='".$order['id']."'";
                $r = mysqli_query($db,$q) or die ($q);
                $receipt = mysqli_fetch_array($r);

                function new_exchange_id($customer_id,$customer_email,$drives){
                    global $db, $order;
                    if (!$customer_id || !$customer_email || count($drives) == 0) return false;

                    $exchange_id = base36converter();
                    $drive_list = json_encode($drives);

                    $q = "INSERT INTO exchange_program (exchange_id, customer_id, customer_email, original_order_id, drive_ids, status) VALUES ('".$exchange_id."', '".$customer_id."', '".$customer_email."', '".$order['id']."', '".$drive_list."', 'open');";
                    $r = mysqli_query($db,$q) or die($q);

                    return $exchange_id;
                }

                function existing_exchange_id($customer_id){
                    global $db;

                    $q = "SELECT *, exchange_id as id FROM exchange_program WHERE customer_id = '".$customer_id."' LIMIT 1;";
                    $r = mysqli_query($db,$q);
                    if (mysqli_num_rows($r) > 0) {
                        return mysqli_fetch_assoc($r);
                    } else {
                        return false;
                    }
                }

                ?>
                <h3>Order Information Verified</h3>

                <div class="hcpOrderSummary">
                    <h3>Order <?php echo $order['id'] ?></h3>

                    <!-- order summary -->
                    <div class="column-third">
                        <p><strong>Order Summary</strong></p>
                        <div class="well">
                            <?php
                            $q = "SELECT * FROM releases WHERE release_id='".$order['data_ordered']."';";
                            $r = mysqli_query($db,$q) or die($q);
                            $releaseInfo = mysqli_fetch_assoc($r);
                            ?>
                            <ul>
                                <li style="font-weight: bold;">Data Ordered: <?php echo $releaseInfo['title'] ?></li>
                                <li>Order Manifest: <?php echo $releaseInfo['data_size'] ?> total data on <?php echo $releaseInfo['drive_qty'] ?> hard drives</li>
                                <li>Order Status: <?php echo $order['status']; if ($order['delivered']) echo " - delivered"; ?></li>
                                <li>Date Ordered: <?php echo print_date($order['order_date']) ?></li>
                                <?php if ($order['status'] === 'shipped') : ?>
                                    <li>Date Shipped: <?php echo print_date($order['date_shipped']) ?></li>
                                    <li>Fedex Tracking ID: <a href="https://www.fedex.com/apps/fedextrack/?action=track&tracknumbers=<?php echo $order['tracking_code'] ?>&locale=en_US&cntry_code=us" target="_blank"><?php echo $order['tracking_code'] ?></a></li>
                                <?php endif; ?>
                                <?php if ($order['status'] === 'refund') : ?>
                                    <li>Date Refunded: <?php echo print_date($order['date_refunded']) ?></li>
                                    <li>Amount Refunded: $<?php echo number_format($order['amount_refunded'],2); ?></li>
                                    <li>Refund Transaction ID: <?php echo $order['refund_tx'] ?></li>
                                <?php endif; ?>
                            </ul>
                        </div>
                        <p><strong>Drives In Your Order</strong></p>
                        <div class="well">
                            <ul>
                                <?php
                                $q = "SELECT inv.serial, inv.release_id, inv.drive_type, inv.drive_size FROM drives_ordered as ord,drive_inventory as inv WHERE ord.order_id='".$order['id']."' AND ord.serial=inv.serial AND inv.drive_status='shipped' ORDER BY inv.release_id ASC;";
                                $r = mysqli_query($db,$q) or die($q);
                                $driveManifest = $r;
                                ?>
                                <?php while ($drive = mysqli_fetch_assoc($driveManifest)) :  ?>
                                    <li>Serial: <strong><?php echo $drive['serial'] ?></strong> - <?php echo $drive['release_id'] ?></li>
                                <?php endwhile; ?>
                            </ul>
                        </div>
                    </div>

                    <!-- customer info -->
                    <div class="column-third">
                        <p><strong>Customer Info</strong></p>
                        <div class="well">
                            <ul>
                                <?php
                                $addressKeys = array('customer_name','customer_institution','shipping_address','shipping_address_2','shipping_city','shipping_state','shipping_postal_code','shipping_country');
                                foreach ($addressKeys as $v) {
                                    if ($order[$v]) echo "<li>".$order[$v]."</li>";
                                }
                                ?>
                            </ul>
                        </div>
                        <p><strong>Contact Info</strong></p>
                        <div class="well">
                            <ul>
                                <?php
                                $addressKeys = array('Phone' => 'shipping_phone','Email' => 'customer_email');
                                foreach ($addressKeys as $k => $v) {
                                    if ($order[$v]) echo "<li><strong>".$k.":</strong> ".$order[$v]."</li>";
                                }
                                ?>
                            </ul>
                        </div>
                    </div>


                    <!-- receipt info -->
                    <div class="column-third">
                        <p><strong>Order Receipt</strong></p>
                        <div class="well">
                            <ul>
                                <li>Transaction ID: <?php echo $order['transaction_id']; ?></li>
                                <?php
                                $receiptKeys = array(
                                    'Total Drive Cost' => 'total_drive_cost',
                                    'Enclosure Cost' => 'enclosure_cost',
                                    'Shipping Cost' => 'shipping_cost',
                                    'Sales Tax' => 'sales_tax',
                                    'CashNet Fee' => 'cashnet_fee'
                                );
                                foreach ($receiptKeys as $k => $v):
                                    if ($receipt[$v]) echo "<li>".$k.": $".number_format($receipt[$v],2)."</li>";
                                endforeach;
                                ?>
                                <?php if ($order['preorder_id']) : ?>
                                    <li>Preorder ID applied: <?php echo $order['preorder_id'] ?></li>
                                <?php endif; ?>
                                <li>Receipt Notes: <?php echo $receipt['receipt_notes'] ?></li>
                            </ul>
                        </div>
                    </div>
                    <p><strong>See any errors in this order information? Please <a href="mailto:orders@humanconnectome.org">Contact Order Support</a>.</strong></p>
                </div>

                <?php
                /* look up or find exchange program ID */
                if (existing_exchange_id($order['customer_id'])) :
                    $exchange_id_details = existing_exchange_id($order['customer_id']);
                    $exchange_id = $exchange_id_details['id'];
                else :
                    // generate a formatted list of drives, then create a new exchange program ID
                    $driveList = [];
                    mysqli_data_seek($driveManifest,0);
                    while ($drive = mysqli_fetch_assoc($driveManifest)) :
                        $driveList[$drive['release_id']] = $drive['serial'];
                    endwhile;
                    $exchange_id = new_exchange_id($order['customer_id'], $order['customer_email'], $driveList);
                    $exchange_id_details['status'] = 'open';
                endif;
                ?>
                <h3>Drive Exchange Details</h3>
                <p><strong>Exchange ID:</strong> <?php echo $exchange_id ?> (Status: <?php echo $exchange_id_details['status'] ?>)</p>

                <?php if ($exchange_id_details['status'] == 'open') : ?>
                    <p><a href="/data?hcp_exchange_code=<?php echo $exchange_id ?>"><button>Use this ID to start your order</button></a></p>

                <?php elseif ($exchange_id_details['status'] == 'applied') : ?>
                    <?php
                    $q = "SELECT id,customer_email, DATE(timestamp) as order_date FROM orders WHERE drive_recycle='".$exchange_id."' LIMIT 1;";
                    $r = mysqli_query($db,$q) or die($q);
                    $new_order = mysqli_fetch_assoc($r);
                    ?>
                    <p>According to our records, this program ID has been applied to a new order by <?php echo $new_order['customer_email'] ?> on <?php echo $new_order['order_date'] ?>. If you think this is incorrect, please contact <a href="mailto:orders@humanconnectome.org">orders@humanconnectome.org</a>.</p>

                <?php endif; ?>

            <?php elseif ($_POST['error']) : ?>
                <p class="error"><strong><?php echo $_POST['error'] ?></strong></p>
                <p>Look up another order?</p>

                <form action="/support/process-order-lookup.php" method="post" class="hcpForm">
                    <div class="hcpForm-wrapper" id="product-info" style="overflow:auto;">
                        <div class="hidden error-messages error-message-product">
                            <p><strong>Errors Found:</strong></p>
                            <ul class="error-list">
                                <li>Things fall apart, the centre cannot hold.</li>
                            </ul>
                        </div>

                        <p>Please provide order verification information: </p>
                        <div>
                            <label for="order_id">HCP Order ID or Transaction ID</label>
                            <input type="text" name="order_id" class="required" title="Order ID" />
                        </div>
                        <div>
                            <label for="customer_email">Customer Email Address Associated With Order</label>
                            <input type="text" name="customer_email" class="required email" title="Customer Email Address" />
                        </div>

                        <div>
                            <input type="submit" value="Look Up Order" />
                        </div>

                    </div>
                </form>

            <?php else :
                die('<p>Sorry, we could not find a qualifying order with your information. Please contact the site administrator for assistance.</p>')
                ?>
            <?php endif; ?>


        </div>
        <!-- /#Content -->

    </div> <!-- middle -->

    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/footer.php'); ?>

</div>
<!-- end container -->

<script src="//www.google.com/jsapi?key=AIzaSyA5m1Nc8ws2BbmPRwKu5gFradvD_hgq6G0" type="text/javascript"></script>
<script type="text/javascript">
    /*
     *  How to restrict a search to a Custom Search Engine
     *  http://www.google.com/cse/
     */

    google.load('search', '1');

    function OnLoad() {
        // Create a search control
        var searchControl = new google.search.SearchControl();

        // Add in a WebSearch
        var webSearch = new google.search.WebSearch();

        // Restrict our search to pages from our CSE
        webSearch.setSiteRestriction('000073750279216385221:vnupk6whx-c');

        // Add the searcher to the SearchControl
        searchControl.addSearcher(webSearch);

        // tell the searcher to draw itself and tell it where to attach
        searchControl.draw(document.getElementById("search"));

        // execute an inital search
        // searchControl.execute('');
    }

    google.setOnLoadCallback(OnLoad);

</script>


<!-- google analytics -->
<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-18630139-1']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

</script>
</body>
</html>