<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Data | Human Connectome Project</title>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/header-includes.php'); ?>

</head>

<body>
<!-- site container -->
<div id="site_container">

    <div id="NIH_header2">
<span>
<a href="http://www.neuroscienceblueprint.nih.gov/connectome/" title="NIH Human Connectome Project" style="color:#b4cfff; text-decoration:none;">NIH Blueprint: The Human Connectome Project</a></span>
    </div>

    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/storefront-topnav.php'); ?>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/db-login.php'); ?>

    <link rel="stylesheet" href="/css/google-cse-customizations.css" />
    <!-- #EndLibraryItem --><!-- End Nav -->

    <!-- Breadcrumbs -->
    <div id="breadcrumbs">
        <p>&raquo; <a href="/">HCP Storefront</a> > custom charge confirmation</p></div>
    <!-- end breadcrumbs -->

    <!-- "Middle" area contains all content between the top navigation and the footer -->
    <div id="middle">
        <div id="content">

            <?php
            $orderId = $_POST['HCP-ORDERID'];
            if ($orderId) {
                ?>

                <?php

                /* check for successful transaction */
                $success = $_POST['respmessage'];
                if ($success == 'SUCCESS') {

                    ?>

                    <h2>HCP Storefront: Successfully Processed Charge</h2>
                    <p>Thank you for your payment. Your receipt ID is #<?php echo $orderId; ?> and your payment transaction ID is #<?php echo $_POST['tx'] ?>. Please save these for your records.</p>
                    <p>If any of this is incorrect, please contact us at <a href="mailto:orders@humanconnectome.org"><strong>orders@humanconnectome.org</strong></a> to correct it.</p>
                    <h3>Charge Summary</h3>
                    <?php
                    // set variables
                    $items = $_POST['itemcode'];

                    /* update order status in DB -- set status according to shipping country */
                    $orderStatus = "complete";

                    $q = "UPDATE orders SET status='".$orderStatus."',transaction_id='".$_POST['tx']."' WHERE id='".$orderId."';";
                    $r = mysqli_query($db,$q) or die($q);

                    $q = "SELECT * FROM receipt,orders WHERE receipt.order_id='".$orderId."' AND orders.id='".$orderId."';";
                    $r = mysqli_query ($db,$q) or die($q);
                    $order = mysqli_fetch_array($r);

                    // update custom charge code so it can't be used again.
                    $q = "UPDATE custom_orders SET status='applied',applied_to_order_id='".$orderId."' WHERE id='".$_POST['ref3val1']."' LIMIT 1;";
                    $r = mysqli_query ($db,$q) or die($q);

                    $q = "SELECT * FROM custom_orders WHERE id='".$_POST['ref3val1']."' LIMIT 1;";
                    $r = mysqli_query ($db,$q) or die($q);
                    $customChargeInfo = mysqli_fetch_assoc($r);

                    ?>
                    <!-- report to customer -->
                    <table cellspacing="1" cellpadding="3" width="100%" class="sortable">
                        <thead>
                        <tr><th style="background-color:#06c;">Order ID: <?php echo $orderId ?></th>
                            <th>Description</th></tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <strong>Custom Charge: </strong><?php echo $customChargeInfo['charge_title'] ?><br>
                                <strong>Charge ID: </strong><?php echo $customChargeInfo['id'] ?>
                            </td>
                            <td><?php echo $customChargeInfo['charge_justification'] ?></td>
                        </tr>
                        <tr><th colspan = 2>Item costs</th></tr>
                        <tr><td>Charge Cost:</td><td align="right">$<?php echo number_format($customChargeInfo['amount'],2) ?></td></tr>
                        <tr><td>Transaction Fee: </td><td align="right">$<?php echo number_format($order['cashnet_fee'],2); ?></td></tr>
                        <tr><td colspan="2" align="right" style="padding-top:10px;"><p><strong>Total cost: $<?php echo number_format($customChargeInfo['amount'] + $order['cashnet_fee'],2); ?></strong></p></td></tr>
                        </tbody>
                    </table>


                    <?php
                    /* if order was unsuccessful */

                } else {

                    /* update order DB */
                    $q = "UPDATE orders SET status='failed',transaction_id='".$_POST['tx']."' WHERE id='".$orderId."';";
                    $r = mysqli_query($db,$q) or die($q);
                    ?>
                    <h2>HCP Storefront: Problem With Your Transaction</h2>
                    <p>There was a problem with your transaction. Please contact <a href="mailto:orders@humanconnectome.org"><strong>orders@humanconnectome.org</strong></a> and inquire about order ID #<?php echo $_POST['HCP-ORDERID'] ?> to rectify it.</p>
                    <p><strong>Error Code <?php echo $_POST['result'] ?>:</strong> <span class="error"><?php echo $_POST['respmessage'] ?> </span></p>
                    <?php
                }
                ?>

                <div style="background-color:#f0f0f0; padding:6px; display:none;">
                    <?php

                    function printArray($array,$title){
                        echo "<h3>".$title."</h3>";
                        echo "<ul>";
                        foreach ($array as $key => $value){
                            echo "<li>$key => $value</li>";
                            if(is_array($value)){ //If $value is an array, print it as well!
                                printArray($value);
                            }
                        }
                        echo "</ul>";
                    }
                    printArray($order,"Data in Order table");
                    echo '<hr />';
                    printArray($_POST,"POST data received through form");

                    ?>
                </div>

                <p style="padding-bottom:6px; border-bottom: 1px #ccc solid">&nbsp;</p>
                <?php
                mysqli_close($db);
                ?>

                <?php
            } else {
                ?>
                <h2>Error</h2>
                <p>No order information. If you think you got to this page in error, please contact orders@humanconnectome.org</p>

                <div style="background-color:#f0f0f0; padding:6px; display:none;">
                    <?php

                    function printArray($array){
                        echo "<ul>";
                        foreach ($array as $key => $value){
                            echo "<li>$key => $value</li>";
                            if(is_array($value)){ //If $value is an array, print it as well!
                                printArray($value);
                            }
                        }
                    }
                    printArray($order);
                    echo '<hr />';
                    printArray($_POST);

                    ?>
                </div>
                <?php
            }
            ?>
        </div>
        <!-- /#Content -->

        <div id="sidebar-rt">

            <div id="sidebar-content">
                <div class="sidebox databox">
                    <h2>HCP Course Details</h2>
                    <p><img src="/img/courses/hcp-course-2016-boston.jpg" alt="Exploring the Human Connectome" /></p>
                    <p>The 2016 HCP Course is designed for investigators who are interested in:</p>
                    <ul>
                        <li>using data being collected and distributed by HCP</li>
                        <li>acquiring and analyzing HCP-style imaging and behavioral data at your own institution</li>
                        <li>processing your own non-HCP data using HCP pipelines and methods</li>
                        <li>learning to use Connectome Workbench tools and the CIFTI connectivity data format</li>
                        <li>learning HCP multi-modal neuroimaging analysis methods, including those that combine MEG and MRI data</li>
                    </ul>
                    <p>&nbsp;</p>
                    <p>For more details, see the <a href="/courses/2016/exploring-the-human-connectome.php">HCP Course Website</a></p>
                </div>
                <!-- /sidebox -->
            </div>
        </div>
        <!-- /sidebar -->


    </div> <!-- middle -->

    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/footer.php'); ?>

</div>
<!-- end container -->

<script src="//www.google.com/jsapi?key=AIzaSyA5m1Nc8ws2BbmPRwKu5gFradvD_hgq6G0" type="text/javascript"></script>
<script type="text/javascript">
    /*
     *  How to restrict a search to a Custom Search Engine
     *  http://www.google.com/cse/
     */

    google.load('search', '1');

    function OnLoad() {
        // Create a search control
        var searchControl = new google.search.SearchControl();

        // Add in a WebSearch
        var webSearch = new google.search.WebSearch();

        // Restrict our search to pages from our CSE
        webSearch.setSiteRestriction('000073750279216385221:vnupk6whx-c');

        // Add the searcher to the SearchControl
        searchControl.addSearcher(webSearch);

        // tell the searcher to draw itself and tell it where to attach
        searchControl.draw(document.getElementById("search"));

        // execute an inital search
        // searchControl.execute('');
    }

    google.setOnLoadCallback(OnLoad);

</script>


<!-- google analytics -->
<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-18630139-1']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

</script>
</body>
</html>
