<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Order Connectome in a Box | Human Connectome Project</title>
    <meta http-equiv="Description" content="Public access to HCP data. Currently, Phase 1 Pilot Data is available." />
    <meta http-equiv="Keywords" content="Public data release, Human Connectome Project, pilot data, brain connectivity data" />
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/header-includes.php'); ?>

</head>

<body>

<!-- site container -->
<div id="site_container">

    <div id="NIH_header2">
        <span><a href="http://www.neuroscienceblueprint.nih.gov/connectome/" title="NIH Human Connectome Project" style="color:#b4cfff; text-decoration:none;">NIH Blueprint: The Human Connectome Project</a></span>
    </div>

    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/storefront-topnav.php'); ?>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/db-login.php'); ?>

    <!-- Breadcrumbs -->
    <div id="breadcrumbs">
        <p>&raquo; <a href="/data/">Get HCP Data</a> &gt; connectome in a box &gt; order form</p></div>
    <!-- end breadcrumbs -->

    <!-- "Middle" area contains all content between the top navigation and the footer -->
    <div id="middle">
        <div id="content">
            <noscript>
                <p style="color:#c00; clear:both;"><strong>Alert:</strong> You appear to have JavaScript turned off. Registration and access for ConnectomeDB requires JavaScript. Please enable it in your browser before proceeding.</p>
            </noscript>

            <h2 id="storeHead">Connectome in a Box Order Form</h2>
            <div style="background: url('/img/icon-alert-48.png') 10px center no-repeat #ffd; border:1px solid #ccc; border-radius:5px; padding:10px 10px 10px 70px; margin-bottom: 1em;" class="hidden">
                <p style="margin-bottom: 6px;"><strong>NOTICE: </strong><br />
                    The rate of incoming orders has outpaced our rate of acquiring hard drives for our inventory. Normal delivery would take place within 30 days of your order. Current delivery times may take longer, until we have a fresh stock of hard drives in house.</p>
            </div>

            <?php
            $hcp_exchange_code = ($_GET['hcp_exchange_code']) ? $_GET['hcp_exchange_code'] : false;

            $q = "SELECT * FROM storefront_status ORDER BY last_modified DESC LIMIT 1;"; // get the most recent storefront status only
            $r = mysqli_query($db,$q) or die("<p>Storefront is currently undergoing maintenance.</p>");
            $storefront = mysqli_fetch_array($r);

            if ($storefront['status'] == 'closed' ) die ('<p>'.$storefront['message_text'].'</p>');

            if ($storefront['status'] !== 'open' && !$hcp_exchange_code) :
                ?>
                <script type="text/javascript">

                    $(document).ready(function(){
                        $('select').prop('disabled',true);
                        $('input').prop('disabled','disabled');
                        $('textarea').prop('disabled','disabled');
                        $('.add-drive').hide();
                        $('#cinab-order').removeProp('action');
                        $('#storeHead').append(' - <a href="javascript:showModal(\'coming-soon\')">Coming Soon</a>');
                    });

                    // hide the drive recycling option
                    $(document).ready(function(){
                        $('#recycle').addClass('hidden');

                        // show the "new data coming soon" modal
                        showModal('coming-soon');
                    });

                </script>
            <?php endif; ?>


            <form id="cinab-order" class="hcpForm" method="post" action="/data/confirm.php">
                <?php if ($hcp_exchange_code) :
                $q = "SELECT * FROM orders WHERE id IN ( SELECT original_order_id FROM exchange_program WHERE exchange_id = '".$hcp_exchange_code."' AND status='open') LIMIT 1;";
                $r = mysqli_query($db,$q) or die ($q);

                if (mysqli_num_rows($r) == 0) :
                    die('<p class="error">You are attempting to use an invalid or expired program code. Please contact orders@humanconnectome.org for assistance if you feel you are receiving this message in error.</p>');
                else:
                    $this_order = mysqli_fetch_assoc($r);
                endif;
                ?>
                    <div class="hcpForm-wrapper" style="background-color: #ffee99">
                        <p>Welcome back, <?php echo $this_order['customer_name'] ?>. Thanks for participating in the HCP Drive Exchange Program. You will receive instructions on how to exchange your drives and receive your refund after you complete your order. </p>
                    </div>
                <?php endif; ?>

                <h3>1: Shipping Location</h3>
                <div class="hcpForm-wrapper">
                    <div class="hidden error-messages" id="error-message-country">
                        <p><strong>Errors Found:</strong></p>
                        <ul class="error-list">
                            <li>You must select a country.</li>
                        </ul>
                    </div>
                    <div>
                        <label for="HCP-COUNTRY">Country</label>
                        <select name="HCP-COUNTRY" id="country-select">
                            <option value=""></option>
                            <option value="United States">United States</option>
                            <option value="Afghanistan">Afghanistan</option>
                            <option value="Albania">Albania</option>
                            <option value="Algeria">Algeria</option>
                            <option value="American Samoa">American Samoa</option>
                            <option value="Andorra">Andorra</option>
                            <option value="Anguilla">Anguilla</option>
                            <option value="Antarctica">Antarctica</option>
                            <option value="Antigua And Barbuda">Antigua And Barbuda</option>
                            <option value="Argentina">Argentina</option>
                            <option value="Armenia">Armenia</option>
                            <option value="Aruba">Aruba</option>
                            <option value="Australia">Australia</option>
                            <option value="Austria">Austria</option>
                            <option value="Azerbaijan">Azerbaijan</option>
                            <option value="Bahamas">Bahamas</option>
                            <option value="Bahrain">Bahrain</option>
                            <option value="Bangladesh">Bangladesh</option>
                            <option value="Barbados">Barbados</option>
                            <option value="Belarus">Belarus</option>
                            <option value="Belgium">Belgium</option>
                            <option value="Belize">Belize</option>
                            <option value="Benin">Benin</option>
                            <option value="Bermuda">Bermuda</option>
                            <option value="Bhutan">Bhutan</option>
                            <option value="Bolivia">Bolivia</option>
                            <option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
                            <option value="Botswana">Botswana</option>
                            <option value="Bouvet Island">Bouvet Island</option>
                            <option value="Brazil">Brazil</option>
                            <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
                            <option value="Brunei Darussalam">Brunei Darussalam</option>
                            <option value="Bulgaria">Bulgaria</option>
                            <option value="Burkina Faso">Burkina Faso</option>
                            <option value="Burundi">Burundi</option>
                            <option value="Cambodia">Cambodia</option>
                            <option value="Cameroon">Cameroon</option>
                            <option value="Canada">Canada</option>
                            <option value="Cape Verde">Cape Verde</option>
                            <option value="Cayman Islands">Cayman Islands</option>
                            <option value="Central African Republic">Central African Republic</option>
                            <option value="Chad">Chad</option>
                            <option value="Chile">Chile</option>
                            <option value="China">China</option>
                            <option value="Christmas Island">Christmas Island</option>
                            <option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
                            <option value="Colombia">Colombia</option>
                            <option value="Comoros">Comoros</option>
                            <option value="Congo">Congo</option>
                            <option value="Congo, the Democratic Republic of the">Congo, the Democratic Republic of the</option>
                            <option value="Cook Islands">Cook Islands</option>
                            <option value="Costa Rica">Costa Rica</option>
                            <option value="Cote d'Ivoire">Cote d'Ivoire</option>
                            <option value="Croatia">Croatia</option>
                            <option value="Cyprus">Cyprus</option>
                            <option value="Czech Republic">Czech Republic</option>
                            <option value="Denmark">Denmark</option>
                            <option value="Djibouti">Djibouti</option>
                            <option value="Dominica">Dominica</option>
                            <option value="Dominican Republic">Dominican Republic</option>
                            <option value="East Timor">East Timor</option>
                            <option value="Ecuador">Ecuador</option>
                            <option value="Egypt">Egypt</option>
                            <option value="El Salvador">El Salvador</option>
                            <option value="England">England</option>
                            <option value="Equatorial Guinea">Equatorial Guinea</option>
                            <option value="Eritrea">Eritrea</option>
                            <option value="Espana">Espana</option>
                            <option value="Estonia">Estonia</option>
                            <option value="Ethiopia">Ethiopia</option>
                            <option value="Falkland Islands">Falkland Islands</option>
                            <option value="Faroe Islands">Faroe Islands</option>
                            <option value="Fiji">Fiji</option>
                            <option value="Finland">Finland</option>
                            <option value="France">France</option>
                            <option value="French Guiana">French Guiana</option>
                            <option value="French Polynesia">French Polynesia</option>
                            <option value="French Southern Territories">French Southern Territories</option>
                            <option value="Gabon">Gabon</option>
                            <option value="Gambia">Gambia</option>
                            <option value="Georgia">Georgia</option>
                            <option value="Germany">Germany</option>
                            <option value="Ghana">Ghana</option>
                            <option value="Gibraltar">Gibraltar</option>
                            <option value="Great Britain">Great Britain</option>
                            <option value="Greece">Greece</option>
                            <option value="Greenland">Greenland</option>
                            <option value="Grenada">Grenada</option>
                            <option value="Guadeloupe">Guadeloupe</option>
                            <option value="Guam">Guam</option>
                            <option value="Guatemala">Guatemala</option>
                            <option value="Guinea">Guinea</option>
                            <option value="Guinea-Bissau">Guinea-Bissau</option>
                            <option value="Guyana">Guyana</option>
                            <option value="Haiti">Haiti</option>
                            <option value="Heard and Mc Donald Islands">Heard and Mc Donald Islands</option>
                            <option value="Honduras">Honduras</option>
                            <option value="Hong Kong">Hong Kong</option>
                            <option value="Hungary">Hungary</option>
                            <option value="Iceland">Iceland</option>
                            <option value="India">India</option>
                            <option value="Indonesia">Indonesia</option>
                            <option value="IRAN">IRAN</option>
                            <option value="Iraq">Iraq</option>
                            <option value="Ireland">Ireland</option>
                            <option value="Israel">Israel</option>
                            <option value="Italy">Italy</option>
                            <option value="Jamaica">Jamaica</option>
                            <option value="Japan">Japan</option>
                            <option value="Jordan">Jordan</option>
                            <option value="Kazakhstan">Kazakhstan</option>
                            <option value="Kenya">Kenya</option>
                            <option value="Kiribati">Kiribati</option>
                            <option value="KOREA, DEMOCRATIC PEOPLE'S REPUBLIC OF">KOREA, DEMOCRATIC PEOPLE'S REPUBLIC OF</option>
                            <option value="Korea, Republic of">Korea, Republic of</option>
                            <option value="Kuwait">Kuwait</option>
                            <option value="Kyrgyzstan">Kyrgyzstan</option>
                            <option value="Lao People's Democratic Republic">Lao People's Democratic Republic</option>
                            <option value="Latvia">Latvia</option>
                            <option value="Lebanon">Lebanon</option>
                            <option value="Lesotho">Lesotho</option>
                            <option value="Liberia">Liberia</option>
                            <option value="Libya">Libya</option>
                            <option value="Liechtenstein">Liechtenstein</option>
                            <option value="Lithuania">Lithuania</option>
                            <option value="Luxembourg">Luxembourg</option>
                            <option value="MACAO">MACAO</option>
                            <option value="Macedonia">Macedonia</option>
                            <option value="Madagascar">Madagascar</option>
                            <option value="Malawi">Malawi</option>
                            <option value="Malaysia">Malaysia</option>
                            <option value="Maldives">Maldives</option>
                            <option value="Mali">Mali</option>
                            <option value="Malta">Malta</option>
                            <option value="Marshall Islands">Marshall Islands</option>
                            <option value="Martinique">Martinique</option>
                            <option value="Mauritania">Mauritania</option>
                            <option value="Mauritius">Mauritius</option>
                            <option value="Mayotte">Mayotte</option>
                            <option value="Mexico">Mexico</option>
                            <option value="Micronesia, Federated States of">Micronesia, Federated States of</option>
                            <option value="Moldova, Republic of">Moldova, Republic of</option>
                            <option value="Monaco">Monaco</option>
                            <option value="Mongolia">Mongolia</option>
                            <option value="Montserrat">Montserrat</option>
                            <option value="Morocco">Morocco</option>
                            <option value="Mozambique">Mozambique</option>
                            <option value="Myanmar">Myanmar</option>
                            <option value="Namibia">Namibia</option>
                            <option value="Nauru">Nauru</option>
                            <option value="Nepal">Nepal</option>
                            <option value="Netherlands">Netherlands</option>
                            <option value="Netherlands Antilles">Netherlands Antilles</option>
                            <option value="New Caledonia">New Caledonia</option>
                            <option value="New Zealand">New Zealand</option>
                            <option value="Nicaragua">Nicaragua</option>
                            <option value="Niger">Niger</option>
                            <option value="Nigeria">Nigeria</option>
                            <option value="Niue">Niue</option>
                            <option value="Norfolk Island">Norfolk Island</option>
                            <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                            <option value="Norway">Norway</option>
                            <option value="Oman">Oman</option>
                            <option value="Pakistan">Pakistan</option>
                            <option value="Palau">Palau</option>
                            <option value="Panama">Panama</option>
                            <option value="Papua New Guinea">Papua New Guinea</option>
                            <option value="Paraguay">Paraguay</option>
                            <option value="Peru">Peru</option>
                            <option value="Philippines">Philippines</option>
                            <option value="Pitcairn">Pitcairn</option>
                            <option value="Poland">Poland</option>
                            <option value="Portugal">Portugal</option>
                            <option value="Puerto Rico">Puerto Rico</option>
                            <option value="Qatar">Qatar</option>
                            <option value="Reunion">Reunion</option>
                            <option value="Romania">Romania</option>
                            <option value="Russia">Russia</option>
                            <option value="Russian Federation">Russian Federation</option>
                            <option value="Rwanda">Rwanda</option>
                            <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                            <option value="Saint Lucia">Saint Lucia</option>
                            <option value="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option>
                            <option value="Samoa (Independent)">Samoa (Independent)</option>
                            <option value="San Marino">San Marino</option>
                            <option value="Sao Tome and Principe">Sao Tome and Principe</option>
                            <option value="Saudi Arabia">Saudi Arabia</option>
                            <option value="Scotland">Scotland</option>
                            <option value="Senegal">Senegal</option>
                            <option value="Serbia and Montenegro">Serbia and Montenegro</option>
                            <option value="Seychelles">Seychelles</option>
                            <option value="Sierra Leone">Sierra Leone</option>
                            <option value="Singapore">Singapore</option>
                            <option value="Slovakia">Slovakia</option>
                            <option value="Slovenia">Slovenia</option>
                            <option value="Solomon Islands">Solomon Islands</option>
                            <option value="Somalia">Somalia</option>
                            <option value="South Africa">South Africa</option>
                            <option value="South Georgia and the South Sandwich Islands">South Georgia and the South Sandwich Islands</option>
                            <option value="Spain">Spain</option>
                            <option value="Sri Lanka">Sri Lanka</option>
                            <option value="St. Helena">St. Helena</option>
                            <option value="St. Pierre and Miquelon">St. Pierre and Miquelon</option>
                            <option value="Suriname">Suriname</option>
                            <option value="Svalbard and Jan Mayen Islands">Svalbard and Jan Mayen Islands</option>
                            <option value="Swaziland">Swaziland</option>
                            <option value="Sweden">Sweden</option>
                            <option value="Switzerland">Switzerland</option>
                            <option value="Taiwan">Taiwan</option>
                            <option value="Tajikistan">Tajikistan</option>
                            <option value="Tanzania">Tanzania</option>
                            <option value="Thailand">Thailand</option>
                            <option value="Togo">Togo</option>
                            <option value="Tokelau">Tokelau</option>
                            <option value="Tonga">Tonga</option>
                            <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                            <option value="Tunisia">Tunisia</option>
                            <option value="Turkey">Turkey</option>
                            <option value="Turkmenistan">Turkmenistan</option>
                            <option value="Turks and Caicos Islands">Turks and Caicos Islands</option>
                            <option value="Tuvalu">Tuvalu</option>
                            <option value="Uganda">Uganda</option>
                            <option value="Ukraine">Ukraine</option>
                            <option value="United Arab Emirates">United Arab Emirates</option>
                            <option value="United Kingdom">United Kingdom</option>
                            <option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
                            <option value="Uruguay">Uruguay</option>
                            <option value="Uzbekistan">Uzbekistan</option>
                            <option value="Vanuatu">Vanuatu</option>
                            <option value="Vatican City State (Holy See)">Vatican City State (Holy See)</option>
                            <option value="Venezuela">Venezuela</option>
                            <option value="Viet Nam">Viet Nam</option>
                            <option value="Virgin Islands (British)">Virgin Islands (British)</option>
                            <option value="Virgin Islands (U.S.)">Virgin Islands (U.S.)</option>
                            <option value="Wales">Wales</option>
                            <option value="Wallis and Futuna Islands">Wallis and Futuna Islands</option>
                            <option value="Western Sahara">Western Sahara</option>
                            <option value="Yemen">Yemen</option>
                            <option value="Zambia">Zambia</option>
                            <option value="Zimbabwe">Zimbabwe</option>
                        </select>
                    </div>
                    <p>Connectome data is shipped from Washington University in Saint Louis. Shipping cost will vary by order quantity and country of residence.</p>
                </div>

                <script type="text/javascript" src="/js/order-validation.js"></script>

                <?php
                /* DEFINE FUNCTION TO HANDLE PRODUCT DISPLAY.
                 * Requires an array that has single product information
                 */
                function displayProducts($release, $orderLimit, $itemStatus) {
                    ?>
                    <div class="product-box <?php echo $itemStatus ?>" id="<?php echo $release['release_id']; ?>">
                        <input type="hidden" name="drive-qty[]" class="drive-qty item-info" value="<?php echo $release['drive_qty'] ?>" disabled="disabled" />
                        <input type="hidden" class="qty item-info" name="<?php echo $release['release_id'].'qty'; ?>" value="0" disabled="disabled" />
                        <input type="hidden" name="itemcode[]" value="<?php echo $release['release_id'] ?>" class="itemcode item-info" disabled="disabled" />
                        <input type="hidden" name="amount[]" value="<?php echo $release['cost']; ?>" class="itemamount item-info" disabled="disabled" />
                        <?php if ($itemStatus == "coming-soon") : ?>
                            <input type="hidden" name="order-type" class="item-info" value="preorder" disabled="disabled" />
                        <?php else : ?>
                            <input type="hidden" name="order-type" class="item-info" value="data" disabled="disabled" />
                        <?php endif; ?>
                        <div class="product-description">
                            <h3><?php if ($itemStatus == "coming-soon") echo "Coming Soon: " ?><?php echo $release['title']; ?>
                                <?php if ($release['release_date']) : ?>
                                    <br>
                                    <span style="font-size: 12px; font-weight:normal;">Release version: <?php echo $release['release_date']; ?></span>
                                <?php endif; ?></h3>
                            <?php echo $release['description']; ?>
                            <?php
                            $drive_type = $release['drive_type'];

                            ?>
                            <p>Cost: <?php echo '$'.number_format($release['cost'],2).' for '.$release['drive_qty'].' '. $release['drive_type'].' ';
                                echo ($release['drive_qty']>1) ? ' drives' : ' drive'; ?>, plus shipping.</p>
                            <?php if ($itemStatus == "coming-soon") : ?>
                                <p><strong>Preorder Deposit: $100.00</strong>. <a href="javascript:showModal('preorder')" title="How do preorders work?">How do preorders work?</a> </p>
                            <?php endif; ?>
                            <div class="order-panel">
                                <div class="add-item drive add-drive <?php if ($itemStatus == "coming-soon") echo "preorder"; ?>"></div>
                            </div>
                        </div> <!-- /product description -->

                        <?php
                        for ($i=1; $i<=$orderLimit; $i++) {
                            ?>
                            <div class="format-box hidden">
                                <div class="format-box-count">
                                    <span class="item-count"><?php echo $release['drive_qty'] ?> drive(s)</span>
                                </div>
                                <?php if ($release['drive_formats'] == 'All') : ?>
                                    <div class="format-box-select">
                                        Format:
                                        <select name="drive-format[]" class="item-info" disabled>
                                            <option value="Mac">Mac</option>
                                            <option value="Windows">Windows</option>
                                            <option value="Linux" selected="selected">Linux (Default)</option>
                                        </select>
                                    </div>
                                <?php else : ?>
                                    <div class="format-box-select">
                                        <label>
                                            Format:
                                            <input name="drive-format[]" class="item-info" type="hidden" value="Linux" disabled="disabled" />
                                            <input type="text" value="Linux" disabled="disabled" style="width:100px" />
                                            <a href="javascript:showModal('linux-format');">Why only Linux?</a>
                                        </label>
                                    </div>
                                <?php endif; ?>
                            </div>

                        <?php
                        } // end format box loop
                        ?>

                        <?php if ($release['enclosure']=='Y') : ?>

                            <div class="format-box hidden enclosure">
                                <div class="format-box-count">
                                    Enclosure:
                                </div>
                                <div class="format-box-select">
                                    <p style="margin:0; padding: 10px 0;"><input type="checkbox" class="enclosureQty" name="enclosureQty" value="1" style="margin: -6px 0 0 -3px;" /> Optional: Ship drive in a Drive Enclosure? (Cost: $39)</p>
                                </div>
                            </div>
                        <?php elseif ($release['drive_qty'] == '1') : ?>
                            <div class="format-box hidden enclosure">
                                <div class="format-box-count">

                                </div>
                                <div class="format-box-select">
                                    <p style="margin:0; padding: 10px 0;"><em><a href="javascript:" onclick="showModal('enclosure')">Can I connect this drive via USB?</a></em></p>
                                </div>
                            </div>
                        <?php endif; ?>

                    </div>

                <?
                }
                /* end PRODUCT DISPLAY function
                */
                ?>

                <h3>2: Product Specification</h3>
                <p>Please select only one set of Connectome Data to order or pre-order.</p>

                <input type="hidden" name="itemcnt" id="itemcnt" value="0" />
                <input type="hidden" name="drivecnt" id="drivecnt" value="0" />

                <div class="hcpForm-wrapper" id="product-info" style="overflow:auto;">
                    <div class="hidden error-messages" id="error-message-product">
                        <p><strong>Errors Found:</strong></p>
                        <ul class="error-list">
                            <li>Things fall apart, the centre cannot hold.</li>
                        </ul>
                    </div>
                    <p style="padding-bottom:6px; border-bottom: 1px #ccc solid" id="product-toggle">CURRENT DATA RELEASES <!-- -  <a href="javascript:dataToggle('archive')">Show Archived Data Releases</a> --></p>


                    <?php
                    // get all CURRENT releases from DB and create a product picker for each
                    $releases = mysqli_query($db,'SELECT * FROM releases WHERE status="current" ORDER BY title ASC;');
                    $orderLimit = 1; // could be set in DB if desired
                    while ($release = mysqli_fetch_array($releases)) {
                        displayProducts($release, $orderLimit, "current");
                    } // end product loop for current releases
                    mysqli_free_result($releases);
                    ?>

                    <p><em>Note: all drives will be Linux EXT3-formatted.</em></p>

                </div> <!-- /wrapper -->

                <div class="hcpForm-wrapper" id="product-info" style="overflow:auto;">
                    <p style="padding-bottom:6px; border-bottom: 1px #ccc solid" id="product-toggle">UPCOMING DATA RELEASES <!-- -  <a href="javascript:dataToggle('archive')">Show Archived Data Releases</a> --></p>
                    <p>Though these releases are not ready to ship yet, you can place a pre-order and be guaranteed an early delivery. <a href="javascript:showModal('preorder')" title="How do preorders work?">How do preorders work?</a></p>

                    <?php
                    // get all ARCHIVED releases from DB and create a product picker for each
                    $releases = mysqli_query($db,'SELECT * FROM releases WHERE status="coming soon" ORDER BY release_date DESC;');
                    $orderLimit = 1; // could be set in DB if desired
                    while ($release = mysqli_fetch_array($releases)) {
                        displayProducts($release, $orderLimit, "coming-soon");
                    } // end product loop for current releases
                    mysqli_free_result($releases);
                    ?>

                    <p><em>Note: all drives will be Linux EXT3-formatted.</em></p>

                </div> <!-- /wrapper -->

                <div class="hcpForm-wrapper" id="optional-products" style="overflow:auto; display:none;">
                    <div class="hidden error error-messages error-message-options"></div>
                    <p style="padding-bottom:6px; border-bottom: 1px #ccc solid">OPTIONAL ADD-ONS</p>
                    <div class="product-box" id="<?php echo $release['release_id']; ?>">
                        <input type="hidden" name="enclosurePrice" class="itemprice" value="39" />
                        <div class="product-description">
                            <h3>USB Drive Enclosure</h3>
                            <p>HCP data is shipped on a standard 3.5" SATA hard drive. We can provide the <a href="http://www.newegg.com/Product/Product.aspx?Item=N82E16817392048" target="_blank">VANTEC USB 3.0 &amp; eSATA enclosure</a> (or its equivalent). Alternatives, such as <strong><a href="http://www.newegg.com/Product/Product.aspx?Item=17-392-051&amp;Tpk=N82E16817392051">a multi-bay enclosure</a></strong>, can be purchased directly from other suppliers. </p>
                            <p>Cost per enclosure: $39</p>
                            <div class="order-panel">
                                <div class="add-item enclosure add-enclosure"></div>

                            </div>
                        </div> <!-- /product description -->

                    </div>
                </div>

                <h3>3: Shipping Address</h3>

                <div class="hcpForm-wrapper" id="shipping-info">
                    <div class="hidden error-messages" id="error-message-address">
                        <p><strong>Errors Found:</strong></p>
                        <ul class="error-list">
                            <li>Things fall apart, the centre cannot hold.</li>
                        </ul>
                    </div>

                    <div>
                        <p><input type="checkbox" name="billing" value="yes" checked="checked" onclick="javascript:toggleBillingAddress(this)" /> Use this same address for billing. <span style="color:red
                    ; font-weight: bold; display: none" id="billing_address_warning"><br />You will be prompted to fill out your billing address on the next page.</span></p>
                    </div>
                    <div>
                        <label for="customer_name">Name</label>
                        <input type="text" name="customer_name" style="width:485px" class="required">
                    </div>
                    <div>
                        <label for="customer_institution">Institution</label>
                        <input type="text" name="customer_institution" style="width:485px" >
                    </div>
                    <div>
                        <label for="customer_email">Email Address</label>
                        <input type="text" name="customer_email" style="width:485px" class="required email" />
                    </div>
                    <div>
                        <label for="shipping_address">Street Address</label>
                        <input type="text" name="shipping_address" style="width:485px" class="required" />
                    </div>
                    <div>
                        <label for="shipping_office">Lab/Office/Department</label>
                        <textarea name="shipping_office" style="width:485px" rows="3"></textarea>
                    </div>
                    <div>
                        <label for="shipping_phone">Phone Number (required for FedEx shipping)</label>
                        <input type="text" name="shipping_phone" style="width:485px" class="required" />
                    </div>
                    <div>
                        <label for="shipping_city">City</label>
                        <input type="text" name="shipping_city" style="width:290px" class="required" />
                    </div>
                    <div class="state-US">
                        <label for="shipping_state">State</label>
                        <select name="shipping_state" class="required">
                            <option selected="selected"></option>
                            <option value="AL">Alabama</option>
                            <option value="AK">Alaska</option>
                            <option value="AZ">Arizona</option>
                            <option value="AR">Arkansas</option>
                            <option value="CA">California</option>
                            <option value="CO">Colorado</option>
                            <option value="CT">Connecticut</option>
                            <option value="DC">District Of Columbia</option>
                            <option value="DE">Delaware</option>
                            <option value="FL">Florida</option>
                            <option value="GA">Georgia</option>
                            <option value="HI">Hawaii</option>
                            <option value="ID">Idaho</option>
                            <option value="IL">Illinois</option>
                            <option value="IN">Indiana</option>
                            <option value="IA">Iowa</option>
                            <option value="KS">Kansas</option>
                            <option value="KY">Kentucky</option>
                            <option value="LA">Louisiana</option>
                            <option value="ME">Maine</option>
                            <option value="MD">Maryland</option>
                            <option value="MA">Massachusetts</option>
                            <option value="MI">Michigan</option>
                            <option value="MN">Minnesota</option>
                            <option value="MS">Mississippi</option>
                            <option value="MO">Missouri</option>
                            <option value="MT">Montana</option>
                            <option value="NE">Nebraska</option>
                            <option value="NV">Nevada</option>
                            <option value="NH">New Hampshire</option>
                            <option value="NJ">New Jersey</option>
                            <option value="NM">New Mexico</option>
                            <option value="NY">New York</option>
                            <option value="NC">North Carolina</option>
                            <option value="ND">North Dakota</option>
                            <option value="OH">Ohio</option>
                            <option value="OK">Oklahoma</option>
                            <option value="OR">Oregon</option>
                            <option value="PA">Pennsylvania</option>
                            <option value="RI">Rhode Island</option>
                            <option value="SC">South Carolina</option>
                            <option value="SD">South Dakota</option>
                            <option value="TN">Tennessee</option>
                            <option value="TX">Texas</option>
                            <option value="UT">Utah</option>
                            <option value="VT">Vermont</option>
                            <option value="VA">Virginia</option>
                            <option value="WA">Washington</option>
                            <option value="WV">West Virginia</option>
                            <option value="WI">Wisconsin</option>
                            <option value="WY">Wyoming</option>
                        </select>

                    </div>
                    <div class="state-int hidden">
                        <label for="shipping_state">State / Province / Region</label>
                        <input type="text" name="shipping_state" style="width:150px" disabled="disabled" />
                    </div>
                    <div>
                        <label for="shipping_postal_code">ZIP/Postal Code</label>
                        <input type="text" name="shipping_postal_code" style="width:90px" class="required" />
                    </div>
                </div>

                <?php
                if (!isset($_GET['taxexempt'])) {
                    ?>

                    <div class="hidden" id="salestax" style="border-bottom: 3px #f1f1f1 solid; margin-bottom:18px; padding-bottom:6px;">
                        <h3>Sales Tax / Exemption</h3>
                        <p style="font: 13px/18px Arial, Helvetica, sans-serif !important;">Residents of Missouri and Indiana are required to pay sales tax, unless they work for an institution that claims tax exemption. To claim tax exemption, please email us at <strong><a href="mailto:orders@humanconnectome.org?subject=tax exemption">orders@humanconnectome.org</a></strong>.</p>
                    </div>

                <?php
                } else {
                    ?>
                    <input type="hidden" name="taxexempt" value="exempt" />
                <?php
                }
                ?>

                <h3>4: Payment Information</h3>
                <ul>
                    <li>The Human Connectome Project only requires payment to cover material and shipping cost. There is no cost for service provided, nor any profit taken. </li>
                    <li>Payments are securely processed by an eTransact storefront set up by Washington University in St Louis. The Human Connectome Project website does not store or track credit card information in its database. </li>
                    <?php
                    if (!isset($_GET['taxexempt'])) {
                        ?>
                        <li>Residents of Missouri and Indiana are required to pay sales tax, unless they work for an institution that claims tax exemption. To claim tax exemption, please email us at <a href="mailto:orders@humanconnectome.org">orders@humanconnectome.org</a> and we will send further instructions on placing your order.</li>
                    <?php
                    } else {
                        ?>
                        <li style="color:blue">You are using a tax-exempt version of this order form. No sales taxes will be charged.</li>
                        <input type="hidden" name="taxexempt" value="TRUE" />
                        <input type="hidden" name="justification" value="User provided proof of tax exemption." />
                    <?php
                    }
                    ?>
                </ul>

                <div class="hcpForm-wrapper" id="order-cost">
                    <div class="g-recaptcha" data-sitekey="6LcXMzEUAAAAAHwC1l4GRLw-AsCp2q-_PAQtzZyh"></div>
                    <br>
                    <?php if (!$_GET['noValidation']) : ?>
                        <!-- validates order -->
                        <p style="margin-bottom:0;"><input type="button" class="highlight" value="Proceed to Checkout" onclick="validateOrders()" /></p>
                    <?php else : ?>
                        <p style="margin-bottom:0;"><input type="submit" class="highlight" value="Proceed to Checkout" /></p>
                        <input type="hidden" name="dut" value="true" />
                    <?php endif; ?>
                </div>
                
                <input type="hidden" name="dut" id="dut-accepted" value="v. 2013-Apr-26" disabled="disabled" />
                <input type="hidden" name="hcp_exchange_code" value="<?php echo $hcp_exchange_code ?>" />

            </form>

            <?php
            if ($hcp_exchange_code) : ?>
            <script type="text/javascript">
                // transfer DB vals from PHP to JS.
                var suppliedVals = {};
                <?php
                $fields = [ 'billing_country','customer_name','customer_institution','customer_email','shipping_address','shipping_phone','shipping_city','shipping_state','shipping_postal_code' ];
                foreach ($fields as $field) : ?>
                    suppliedVals['<?php echo $field ?>'] = '<?php echo $this_order[$field] ?>';
                <?php endforeach; ?>

                // mysql field name, form element name
                var selects = [
                        { 'billing_country':'HCP-COUNTRY' },
                        { 'shipping_state':'shipping_state' }
                    ],
                    inputs = [
                        { 'customer_name':'customer_name' },
                        { 'customer_institution': 'customer_institution' },
                        { 'customer_email': 'customer_email' },
                        { 'shipping_address': 'shipping_address' },
                        { 'shipping_phone': 'shipping_phone' },
                        { 'shipping_city': 'shipping_city' },
                        { 'shipping_state': 'shipping_state' },
                        { 'shipping_postal_code': 'shipping_postal_code' }
                    ];
                selects.forEach(function(select){
                    for (var k in select) {
                        $('select[name='+select[k]+']').find('option').each(function(){
                            if ($(this).val() == suppliedVals[k]) {
                                $(this).prop('selected','selected');
                                return true;
                            }
                        });
                    }
                });
                inputs.forEach(function(input){
                    for (var k in input) {
                        $('input[name='+input[k]+']').val( suppliedVals[k] );
                    }
                });
                if (suppliedVals['billing_country'] !== 'United States') {
                    $('.state-US').addClass('hidden')
                        .find('select').removeClass('required').prop('disabled','disabled');
                    $('.state-int').removeClass('hidden')
                        .find('input').prop('disabled',false);
                }

            </script>
            <?php
            endif;
            ?>

            <?php
            mysqli_close($db);
            ?>
        </div>
        <!-- /#Content -->

        <div id="sidebar-rt">

            <div id="sidebar-content">
                <div class="sidebox databox">
                    <h2>SATA Drive Details</h2>
                    <p><img src="/img/icon-SATA-drive.png" width="256" height="256" alt="SATA Drive" style="margin-left:-10px" /></p>
                    <p>HCP  data releases can be delivered on a standard SATA hard drive. These drives are supported by all tower computers. Laptop users may want to purchase a drive enclosure with a USB connector, from us or from another source.</p>
                    <h3>A note on pricing:</h3>
                    <p>All costs for drives, materials and shipping reflect the material costs that we incur for sourcing. Any difference between our pricing and currently available commodity prices reflect the price that we paid to stock these items. The service of creating the Connectome In a Box is fully funded by grants from the NIH, and we do not charge any additional amount.</p>

                </div>
                <!-- /sidebox -->
            </div>
        </div>
        <!-- /sidebar -->

        <!-- modal overlays -->
        <div id="page-mask" class="pm" onclick="javascript:modalClose();"></div>
        <div id="dut-Phase2OpenAccess" class="modal">
            <p class="modal-title">Complete Order: Accept HCP Data Use Terms</p>
            <div class="dutWrapper">
                <div class="dutContents">
                    <p><strong>WU-Minn HCP Consortium Open Access Data Use Terms </strong></p>
                    <p>I request access to data collected by the Washington University - University of Minnesota Consortium of the Human Connectome Project (WU-Minn HCP), and I agree to the following: </p>
                    <ol>
                        <li>I will not attempt to establish the identity of or attempt to contact any of the included human subjects. </li>
                        <li>I understand that under no circumstances will the code that would link these data to Protected Health Information be given to me, nor will any additional information about individual human subjects be released to me under these Open Access Data Use Terms. </li>
                        <li>I will comply with all relevant rules and regulations imposed by my institution. This may mean that I need my research to be approved or declared exempt by a committee that oversees research on human subjects, e.g. my IRB or Ethics Committee. The released HCP data are not considered de-identified, insofar as certain combinations of HCP Restricted Data (available through a separate process) might allow identification of individuals. Different committees operate under different national, state and local laws and may interpret regulations differently, so it is important to ask about this. If needed and upon request, the HCP will provide a certificate stating that you have accepted the HCP Open Access Data Use Terms. </li>
                        <li>I may redistribute original WU-Minn HCP Open Access data and any derived data as long as the data are redistributed under these same Data Use Terms. </li>
                        <li>I will acknowledge the use of WU-Minn HCP data and data derived from WU-Minn HCP data when publicly presenting any results or algorithms that benefitted from their use. </li>
                        <ol>
                            <li>Papers, book chapters, books, posters, oral presentations, and all other printed and digital presentations of results derived from HCP data should contain the following wording in the acknowledgments section:  "Data were provided [in part] by the Human Connectome Project, WU-Minn Consortium (Principal Investigators: David Van Essen and Kamil Ugurbil; 1U54MH091657) funded by the 16 NIH Institutes and Centers that support the NIH Blueprint for Neuroscience Research; and by the McDonnell Center for Systems Neuroscience at Washington University." </li>
                            <li>Authors of publications or presentations using WU-Minn HCP data should cite relevant publications describing the methods used by the HCP to acquire and process the data. The specific publications that are appropriate to cite in any given study will depend on what HCP data were used and for what purposes.  An annotated and appropriately up-to-date list of publications that may warrant consideration is available at <a href="http://www.humanconnectome.org/about/acknowledgehcp.html" target="_blank">http://www.humanconnectome.org/about/acknowledgehcp.html</a></li>
                            <li>The WU-Minn HCP Consortium as a whole should not be included as an author of publications or presentations if this authorship would be based solely on the use of WU-Minn HCP data.</li>
                        </ol>
                        <li>Failure to abide by these guidelines will result in termination of my privileges to access WU-Minn HCP data.</li>
                    </ol>
                    <p id="dut-version">v. 2013-Apr-26</p>
                </div>
            </div>
            <div style="padding: 0 10px 10px;">
                <span class="dut-reminder">Please read through the terms completely.</span>
                <span class="dut-accept-message" style="display: none;"><input type="checkbox" id="p2-agree-check" name="p2-agree-check" onchange="agreeChange(this,'p2-btn-agree')"><label style="display:inline-block; font-size:14px !important;">Please accept each of the terms of use.</label></span>
                <span class="dut-accept"><a href="javascript:modalClose('dut-Phase2OpenAccess');" style="display: inline-block; font-size:12px !important; height: 24px; margin-right:30px;">Cancel</a> <input id="p2-btn-agree" type="button" value="Proceed to Checkout" class="form-submit" style="display:none" /></span>
            </div>

        </div> <!-- end DUT modal -->

        <div class="modal" id="coming-soon" style="min-height:0px !important;">
            <div class="modal-title">Storefront Temporarily Closed. <span class="modal-close" onclick="modalClose()"><img src="/img/close-dark.png" /></span></div>
            <div class="modal_content">
                <h1><img src="/img/icon-HCP-data-drive-large.png" align="right" style="margin:0 0 0 30px" /><?php echo $storefront['message_title'] ?></h1>
                <?php echo $storefront['message_text']; ?>
                <p><button onclick="javascript:modalClose()">Okay Thanks</button> </p>
            </div>
        </div> <!-- end "coming soon" modal -->

        <div class="modal" id="linux-format">
            <div class="modal-title">HCP Drive Formats <span class="modal-close" onclick="modalClose()"><img src="/img/close-dark.png" /></span></div>
            <div class="modal_content">
                <h1><img src="/img/icon-HCP-data-drive-large.png" align="right" style="margin:0 0 0 30px" />Why are Connectome in a Box drives only available in Linux?</h1>
                <p>Working with datasets that spans multiple hard drives demands more complex thinking about logistics, for our users dealing with storage and access as well as for our own ability to fulfill orders. We have made a decision to distribute our full datasets as Linux NTFS-formatted drives. This is the common standard for large-scale storage solutions, and has the best chance of long-term usability. </p>
                <p><strong>Q: What about Mac users?</strong></p>
                <p>There is free 3rd-party software that allows <a href="http://blog.applegrew.com/2011/12/access-ext3ext2-file-system-on-mac-osx-lion-10-7/" target="_blank" title="ACCESS EXT3/EXT2 FILE SYSTEM ON MAC OSX LION (10.7)">Mac users to mount and get data from Linux-formatted drives</a>. We have tested this implementation and it appears to work.</p>
                <p><strong>Q: What about Windows users?</strong></p>
                <p>There is free 3rd-party software for Windows users that allows them to <a href="http://www.diskinternals.com/linux-reader/" target="_blank" title="Freeware Linux Reader for Windows Get access to any files from Windows!">copy data from Linux-formatted drives</a>, but it does not allow users to mount the drive directly. We have tested this solution, and while far from ideal, it does provide access to data.</p>
                <p><strong>Q: Concerned about this change?</strong></p>
                <p>Please let us know by contacting us at <a href="mailto:info@humanconnectome.org">info@humanconnectome.org</a>.</p>
                <p><button onclick="javascript:modalClose()">Okay Thanks</button> </p>
            </div>
        </div> <!-- end "linux format" modal -->

        <div class="modal" id="enclosure" style="min-height: 300px;">
            <div class="modal-title">Using Drive Enclosures <span class="modal-close" onclick="modalClose()"><img src="/img/close-dark.png" /></span></div>
            <div class="modal_content">
                <h1>How can I connect this hard drive via USB?</h1>
                <p>Since we began making Connectome data available on hard drives, we have attempted to support as wide an audience of users as possible, regardless of whether you have a network storage cluster and a full IT team to support it, or just a laptop and a USB port.</p>
                <p>However, our recent switch to 8-TB hard drives appears to have outstripped the capabilities of most USB enclosures or USB drive docks, which commonly only list support up to 4-TB drives. We recommend checking <a href="http://newegg.com/" title="Newegg.com">Newegg.com</a> for USB drive docks, and reading the descriptions carefully. </p>
                <p><button onclick="javascript:modalClose()">Okay Thanks</button> </p>
            </div>
        </div>

        <div class="modal" id="preorder">
            <div class="modal-title">HCP Data Preorders <span class="modal-close" onclick="modalClose()"><img src="/img/close-dark.png" /></span></div>
            <div class="modal_content">
                <h1><img src="/img/icon-HCP-data-drive-large.png" align="right" style="margin:0 0 0 30px" />How Do Preorders Work?</h1>
                <p>The diversity and amount of data that the Human Connectome Project can distribute has grown, but our bottleneck is in the production of verified hard drives for each data release. For data that we plan to release shortly, we are allowing customers to place a pre-order. This preorder allows you to get first priority for delivery, and allows us to gauge the level of interest in each upcoming release. </p>
                <p>Your preorder requires an initial deposit of $100.00, so that we can separate passive interest from active buying interest. This deposit will be fully applied to the eventual purchase cost, and is fully refundable at any time if you change your mind about ordering.</p>
                <p>When you complete your preorder, you will receive a coupon code via email. When the data you ordered has been released, you will receive a second email with a custom link to this storefront with your coupon code included. By using this coupon code, your initial deposit will be automatically applied to the cost of the data release.</p>
                <p>If you have any questions about this process, please contact us at <a href="mailto:info@humanconnectome.org">info@humanconnectome.org</a>.</p>
                <p><button onclick="javascript:modalClose()">Okay Thanks</button> </p>
            </div>
        </div> <!-- end "linux format" modal -->


    </div> <!-- middle -->

    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/footer.php'); ?>

</div>
<!-- end container -->

<script src="//www.google.com/jsapi?key=AIzaSyA5m1Nc8ws2BbmPRwKu5gFradvD_hgq6G0" type="text/javascript"></script>
<script type="text/javascript">
    /*
     *  How to restrict a search to a Custom Search Engine
     *  http://www.google.com/cse/
     */

    google.load('search', '1');

    function OnLoad() {
        // Create a search control
        var searchControl = new google.search.SearchControl();

        // Add in a WebSearch
        var webSearch = new google.search.WebSearch();

        // Restrict our search to pages from our CSE
        webSearch.setSiteRestriction('000073750279216385221:vnupk6whx-c');

        // Add the searcher to the SearchControl
        searchControl.addSearcher(webSearch);

        // tell the searcher to draw itself and tell it where to attach
        searchControl.draw(document.getElementById("search"));

        // execute an inital search
        // searchControl.execute('');
    }

    google.setOnLoadCallback(OnLoad);

</script>

<!-- google analytics -->
<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-18630139-1']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

</script>
</body>
</html>
