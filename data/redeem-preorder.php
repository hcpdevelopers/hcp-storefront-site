<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Complete HCP Data Preorder | Human Connectome Project</title>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/header-includes.php'); ?>

</head>

<body>


<!-- site container -->
<div id="site_container">

    <div id="NIH_header2">
        <span><a href="http://www.neuroscienceblueprint.nih.gov/connectome/" title="NIH Human Connectome Project" style="color:#b4cfff; text-decoration:none;">NIH Blueprint: The Human Connectome Project</a></span>
    </div>

    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/storefront-topnav.php'); ?>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/db-login.php'); ?>

    <script type="text/javascript">
        /* confirm acceptance of DUT on page open. */
        $(document).ready(function(){
            showModal('#dut-Phase2OpenAccess');
        });


    </script>

    <!-- Breadcrumbs -->
    <div id="breadcrumbs">
        <p>&raquo; data</p>
    </div>
    <!-- end breadcrumbs -->

    <!-- "Middle" area contains all content between the top navigation and the footer -->
    <div id="middle">
        <div id="content">
            <noscript>
                <p style="color:#c00; clear:both;"><strong>Alert:</strong> You appear to have JavaScript turned off. Registration and access for ConnectomeDB requires JavaScript. Please enable it in your browser before proceeding.</p>
            </noscript>



            <h2>Connectome In A Box: Redeem Preorder</h2>
            <?php
            /* base 36 converter - used to create unique preorder IDs */
            function base36converter() {
                $input = time();
                $base = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                $output = '';

                while ($input > 0) {
                    $output = $base[$input%36] . $output;
                    $input = floor($input / 36);
                }

                return $output;
            }

            ?>

            <?php // prevent spam form submissions
            if (empty($_POST['g-recaptcha-response']) || $_POST['g-recaptcha-response'] == '') die("<p><strong>Sorry, this form is restricted to humans.</strong></p>");
            ?>

            <?php if ($_GET['preorder_code']) : ?>
            <?php
            // check for valid code
            $q = "SELECT * FROM preorders WHERE id='".$_GET['preorder_code']."' AND status='open' LIMIT 1";
            $r = mysqli_query($db,$q) or die ($q);
            $preorderInfo = mysqli_fetch_assoc($r);

            if (mysqli_num_rows($r) > 0) :
                echo '<p>You are redeeming preorder code '.$preorderInfo['id'].' for the '.$preorderInfo['release_id'].' dataset.</p>';
            else :
                echo '<p>'.$q.'</p>';
                die('<p>I\'m sorry, you are trying to redeem an improper or expired preorder code. Please contact orders@humanconnectome.org if you need help with this process.</p>');
            endif;

            $q = "SELECT * FROM orders WHERE preorder_id ='".$_GET['preorder_code']."' AND order_type='preorder' LIMIT 1";
            $r = mysqli_query($db,$q) or die ($q);
            $orderInfo = mysqli_fetch_assoc($r);

            if (mysqli_num_rows($r) > 0) :
                // echo '<p>Hi, '.$orderInfo['customer_name'].'!</p>';
            else :
                echo '<p>'.$q.'</p>';
                die('<p>I\'m sorry, there was an error associating your preorder code with an active order in our database. Please contact orders@humanconnectome.org if you need help with this process.</p>' );
            endif;
            ?>

            <?php
            $orderId = $orderInfo['id'];
            $data_ordered_array = explode(',',$orderInfo['data_ordered']);
            $data_ordered = $data_ordered_array[0];

            // create a new order for the order completion transaction
            $q = mysqli_query($db,'INSERT INTO orders (timestamp,status) VALUES (NOW(),\'incomplete\')');
            $newOrder = mysqli_fetch_array(mysqli_query($db,'SELECT id FROM orders ORDER BY id DESC LIMIT 1'));
            $newOrderId = $newOrder['id'];
            ?>

            <?php
            /* look up customer by email address and either match, or create customer ID */
            $customerID = $orderInfo['customer_id'];
            ?>

            <?php
            /* get release info from DB */
            $q = "SELECT * FROM releases WHERE release_id='".$data_ordered."' AND status='current';";
            $r = mysqli_query($db,$q) or die ($q);
            $releaseInfo = mysqli_fetch_assoc($r);

            if (mysqli_num_rows($r) > 0) :
                // nuffink
            else :
                echo '<p>'.$q.'</p>';
                die ('<p>I\'m sorry, you are trying to complete a preorder for a dataset that is not currently available to purchase. Please contact orders@humanconnectome.org if you need help with this process.</p>');
            endif;
            ?>

            <?php
            /* copy order info from the old order into the new order */
            $orderType = ('data');
            $q = "UPDATE orders SET customer_name='".$orderInfo['customer_name']."',
                    customer_id='".$customerID."',
                    customer_email='".$orderInfo['customer_email']."',
                    customer_institution='".$orderInfo['customer_institution']."',
                    dut_accepted='".$orderInfo['dut']."',
                    shipping_address='".$orderInfo['shipping_address']."',
                    shipping_address_2='".$orderInfo['shipping_address_2']."',
                    shipping_city='".$orderInfo['shipping_city']."',
                    shipping_state='".$orderInfo['shipping_state']."',
                    shipping_postal_code='".$orderInfo['shipping_postal_code']."',
                    shipping_country='".$orderInfo['shipping_country']."',
                    shipping_phone='".$orderInfo['shipping_phone']."',
                    data_ordered='".$data_ordered."',
                    drives_ordered='".$orderInfo['drives_ordered']."',
                    enclosures_ordered='".$orderInfo['enclosures_ordered']."',
                    preorder_id='".$orderInfo['preorder_id']."',
                    order_type='data' WHERE id='".$newOrderId."';";
            $r = mysqli_query($db,$q);

            ?>

            <?php
            /* set array variables for product orders */
            $items = array($data_ordered);
            $amounts = array($releaseInfo['cost']);
            $driveFormat = $releaseInfo['drive_formats'];
            $itemcount = 1;
            $drivecount = $releaseInfo['drive_qty'];
            $discount = $preorderInfo['amount'];
            $enclosures = $orderInfo['enclosures_ordered'];
            $subtotal = array_sum($amounts) +(39 * $enclosures); // + $discount
            $salesTax = 0; // will be set in a moment
            $shipping = array();
            $state = $orderInfo['shipping_state'];

            if ($state == 'HI') :
                echo "<p>Hawaii residents are charged international shipping rates by Fedex, when shipping from St Louis, MO.</p>";
            endif;

            /* Calculate sales tax, if necessary. Only tax drive cost. */
            $taxRates = array(
                "MO" => 0.08679,
                "IN" => 0.07
            );
            if (!isset($_GET['taxexempt'])) {
                $salesTax = $taxRates[$state] * $subtotal;
            }

            /* Get shipping cost from release info. Include a 2.25% Cashnet fee that gets added on top. */
            $country = $orderInfo['shipping_country'];

            /* get shipping costs from release info */

            function calculateShipping($shippingJson) {
                global $country, $customShipping, $state;

                // convert JSON string of shipping costs to PHP array
                $shippingCosts = json_decode($shippingJson,true);

                if ($customShipping) :
                    $s = $customShipping;
                else :
                    switch($country) {
                        case "United States":
                            if ($state == 'HI') {
                                $s = $shippingCosts["Canada"];
                            } else {
                                $s = $shippingCosts["United States"];
                            }
                            break;
                        case "Canada":
                            $s = $shippingCosts["Canada"];
                            break;
                        case "Mexico":
                            $s = $shippingCosts["Mexico"];
                            break;
                        default:
                            $s = $shippingCosts["International"];
                    }
                endif;

                return $s;
            }

            ?>

            <!-- report to customer -->
            <p><strong>Order ID: <?php echo $newOrderId; ?></strong> (Completion of Preorder ID <?php echo $orderInfo['id'] ?>)</p>
            <table cellspacing="1" cellpadding="3" width="100%" class="sortable" style="margin-bottom: 17px;">
                <thead>
                <tr><th style="background-color:#06c;">Customer ID: <?php echo $customerID ?></th><th>Format </th><th>Drives Shipped</th><th>Cost</th></tr>
                </thead>
                <tbody>

                <?php
                if ($items) :
                    foreach ($items as $key => $value) {

                        if ($value == "HCP-CUSTOM") :
                            $release['title'] =  $_POST['custom-drive-id'][$key];
                        endif;
                        $driveQty = $releaseInfo['drive_qty'];

                        $orderToQuery = $value;
                        $driveToQuery = explode(',',$releaseInfo['contains_drives']); // get first drive from list of drives
                        $driveToQuery = $driveToQuery[0];

                        $shipping[$key] = calculateShipping($releaseInfo['shipping_rate']);
                        ?>

                        <tr>
                            <td><?php echo $releaseInfo['title']; ?></td>
                            <td>Linux</td>
                            <td><?php echo $driveQty.' '.$releaseInfo['drive_type'].' drive(s)' ?></td>
                            <td align="right">$<?php echo number_format($amounts[$key],2) ?></td>
                        </tr>

                    <?php
                    } // end item listing
                endif;

                if ($enclosures > 0) {
                    ?>
                    <tr><td colspan="3">Drive Enclosures: QTY (<?php echo $enclosures ?>)</td><td align="right">$<?php echo number_format(39*$enclosures,2) ?></td></tr>
                <?php
                }

                if ($salesTax > 0) {
                    ?>
                    <tr><td colspan="3">Sales Tax</td><td align="right">$<?php echo number_format($salesTax,2); ?></td></tr>
                <?php
                }

                $totalShipping = array_sum($shipping);
                // calculate CashNet fee as 2.25% of every dollar processed as payment, and add it to the S&H.
                $cashNet = ($subtotal + $salestax + $totalShipping)*(0.0225);
                ?>

                <tr>
                    <td colspan="3">Shipping Cost <?php if (($country != "United States") && ($country != "Canada") && ($country != "Mexico")) : echo " (includes $40 international duty)"; endif; ?></td>
                    <td align="right">$<?php echo number_format($totalShipping,2); ?></td>
                </tr>
                <tr>
                    <td colspan="3">Transaction Fee (bundled into final "Shipping &amp; Handling" charge)</td><td align="right">$<?php echo number_format($cashNet,2); ?></td>
                </tr>

                <tr>
                    <td colspan="3">
                        <strong>Total Purchase Cost:</strong></td><td align="right"><strong>$<?php echo number_format($subtotal + $salesTax + $totalShipping + $cashNet,2) ; ?></strong></td>
                </tr>
                <tr style="background-color: #fffcc9">
                    <td colspan="3"><strong>Preorder Deposit Amount: </strong></td><td align="right"><strong>- $<?php echo number_format($preorderInfo['amount'],2); ?></strong></td>
                </tr>
                <tr>
                    <td colspan="3"><strong>Remaining Cost:</strong></td><td align="right"><strong>$<?php echo number_format($subtotal + $salesTax + $totalShipping + $cashNet-$preorderInfo['amount'],2) ; ?></strong></td>
                </tr>



                </tbody>
            </table>


            <!-- ADDRESS CONFIRM / CHANGE -->
            <p>&nbsp;</p>
            <h3>Please Confirm Your Shipping Information</h3>
            <p><em>You will be prompted to enter your billing information on the next screen.</em></p>
            <div style="border: 1px solid #ccc; margin: 1em 0;">
                <iframe frameborder="0" width="528" height="400" src="address-change.php?orderId=<?php echo $orderId ?>"></iframe>
            </div>
            

            <!-- BEGIN ORDER FORM -->

            <?php if ($_SERVER['HTTP_HOST'] == 'dev.humanconnectome.org') : ?>
            <!-- test storefront - no live transactions -->
            <form class="hcpForm" action="https://commerce.cashnet.com/404handler/pageredirpost.aspx?Virtual=HCPtest" method="post" style="position:relative">
                <p style="position: absolute; color: red; bottom: 0px; left: 0px;">Test Storefront - no transactions will be processed.</p>
            <?php else : ?>
            <!-- LIVE storefront! -->
            <form class="hcpForm" action="https://commerce.cashnet.com/404handler/pageredirpost.aspx?Virtual=HCP" method="post">
            <?php endif; ?>

                <input type="hidden" name="HCP-NAME_G" value="<?php echo $orderInfo['customer_name'] ?>" />
                <input type="hidden" name="HCP-EMAIL_G" value="<?php echo $orderInfo['customer_email'] ?>" />
                <input type="hidden" name="HCP-INSTITUTION" value="<?php echo ($orderInfo['customer_institution'] ? $orderInfo['customer_institution'] : '') ?>" />
                <input type="hidden" name="HCP-ADDR_G" value="<?php echo $orderInfo['billing_address']." ".$orderInfo['billing_address_2'] ?>" />
                <input type="hidden" name="HCP-CITY_G" value="<?php echo $orderInfo['billing_city'] ?>" />
                <input type="hidden" name="HCP-STATE_G" value="<?php echo $orderInfo['billing_state'] ?>" />
                <input type="hidden" name="HCP-ZIP_G" value="<?php echo $orderInfo['billing_postal_code'] ?>" />
                <input type="hidden" name="HCP-COUNTRY" value="<?php echo $orderInfo['billing_country'] ?>" />



                <?php
                /*
                 * CONVERT INHERITED FORM VALUES FROM 'POST' TO CASHNET FORMAT
                 */
                ?>

                <input type="hidden" name="itemcnt" value="1" />
                <input type="hidden" name="orderId" value="<?php echo $newOrderId ?>" />
                <input type="hidden" name="HCP-ORDERID" value="<?php echo $newOrderId ?>" />



                    <?php
                    $item=0;
                    if ($items) :
                        foreach($items as $key => $value) {
                            $item = $key+1;

                                ?>
                                <input type="hidden" name="<?php echo 'itemcode'.$item ?>" value="<?php echo $value ?>" />
                                <input type="hidden" name="<?php echo 'amount'.$item ?>" value="<?php echo number_format($amounts[$key] - $preorderInfo['amount'],2) ?>" />
                                <input type="hidden" name="<?php echo 'qty'.$item ?>" value="1" />
                                <input type="hidden" name="<?php echo 'ref1type'.$item ?>" value="HCP-DRIVE FORMAT" />
                                <input type="hidden" name="<?php echo 'ref1val'.$item ?>" value="<?php echo $driveFormat ?>" />

                            <!-- extra form fields for final receipt -->
                            <input type="hidden" name="drive-format[]" value="<?php echo $driveFormat[$key] ?>" />
                        <?php
                        } // end item (drive) entry
                    endif;

                    $q = "INSERT INTO receipt (order_id,total_drive_cost) VALUES ('".$newOrderId."','".array_sum($amounts)."');";
                    $r = mysqli_query($db,$q) or die($q);

                    // store justification for price change in receipt notes.
                    $q = "UPDATE receipt SET receipt_notes = 'Preorder ".$preorderInfo['id']." redeemed' WHERE order_id='".$newOrderId."';";
                    $r = mysqli_query($db,$q) or die($q);

                    /* add drive enclosure if ordered */
                    if ($_POST['enclosureQty'] > 0) :
                        $item++;
                        $enclosureCost = $_POST['enclosureQty'] *39;

                        $q = "UPDATE receipt SET enclosure_cost='".$enclosureCost."' WHERE order_id='".$newOrderId."';";
                        $r = mysqli_query($db,$q) or die($q);

                        ?>
                        <input type="hidden" name="<?php echo 'itemcode'.$item ?>" value="HCP-CABLE" />
                        <input type="hidden" name="<?php echo 'amount'.$item ?>" value="<?php echo number_format($enclosureCost,2); ?>" />
                        <input type="hidden" name="<?php echo 'qty'.$item ?>" value="<?php echo $orderInfo['enclosures_ordered'] ?>" />

                    <?php
                    endif; // enclosure

                    /* add sales tax as a new line-item on the invoice if necessary */
                    if (isset($salesTax)) :
                        $item++;

                        $q = "UPDATE receipt SET sales_tax='".number_format($salesTax,2)."' WHERE order_id='".$newOrderId."';";
                        $r = mysqli_query($db,$q) or die($q);

                        ?>
                        <input type="hidden" name="<?php echo 'itemcode'.$item ?>" value="HCP-SALESTAX" />
                        <input type="hidden" name="<?php echo 'amount'.$item ?>" value="<?php echo $salesTax ?>" />
                    <?php
                    endif; // sales tax

                        /* Add shipping cost as a new line item on the invoice */
                        $item++;

                        $q = "UPDATE receipt SET shipping_cost='".number_format($totalShipping,2)."', cashnet_fee='".number_format($cashNet,2)."' WHERE order_id='".$newOrderId."';";
                        $r = mysqli_query($db,$q) or die($q);
                    ?>
                    <input type="hidden" name="<?php echo 'itemcode'.$item ?>" value="HCP-SHIPPING" />
                    <input type="hidden" name="<?php echo 'amount'.$item ?>" value="<?php echo $totalShipping + $cashNet ?>" />

                   <p align="right" style="margin-bottom:0;"><input type="submit" class="highlight" value="Proceed to Checkout" /></p>

                </form>
                <p></p>
                <p>Credit card information will be gathered at checkout. Forms of payment we accept:</p>
                <p><img src="/img/icons/payment-American-Express.png" alt="AMEX" /> <img src="/img/icons/payment-Discover.png" alt="Discover" /> <img src="/img/icons/payment-Mastercard.png" alt="Mastercard" /> <img src="/img/icons/payment-Visa.png" alt="Visa" /> <img src="/img/icons/payment-JCB.png" alt="JCB" /></p>

                <!-- condition if DUT was not accepted -->
                <?php else : ?>
                    <h3>Order form error</h3>
                    <p>This form can only be used to complete payment for a valid preorder. Please contact the site administrator if you need help with this process.</p>

                <?php endif; ?>


        </div>
        <!-- /#Content -->

        <div id="sidebar-rt">

            <div id="sidebar-content">
                <?php if ($release['status'] == 'current') : ?>
                <div class="sidebox databox">
                    <?php
                    $q = "SELECT count(drive_status) as drives from drive_inventory WHERE release_id='".$driveToQuery."' AND (drive_status='NEW' or drive_status='READY TO SHIP');";
                    $r = mysqli_query($db,$q);
                    $inv = mysqli_fetch_array($r);

                    $q= "SELECT count(id) as orders from orders WHERE data_ordered LIKE '%".$orderToQuery."%' AND status='open';";
                    $r = mysqli_query($db,$q);
                    $ord = mysqli_fetch_array($r);
                    ?>
                    <h2>Order Availability</h2>
                    <p><strong>Items In Stock:</strong></p>
                    <div class="storefront-counter stock"><?php echo intval($inv['drives'])+3 ?></div>
                    <p><strong>Orders Already In Queue:</strong></p>
                    <div class="storefront-counter orders"><?php echo intval($ord['orders']) ?></div>
                    <p><strong>Projected Ship Window:</strong>
                        <?php if ( (intval($inv['drives'])+9) > intval($ord['orders']) ) :
                                echo "Within 30 Days";
                        else :
                                echo "Within 45 Days";
                        endif; ?>
                        </p>
                </div>
                <?php endif; ?>

                <div class="sidebox databox">
                    <h2>Shipping &amp; Fulfillment Details</h2>
                    <p><img src="/img/icon-shipping-Fedex.png" width="256" height="256" alt="SATA Drive" style="margin-left:-10px" /></p>
                    <p>Please note: we batch-process orders for efficiency. <strong>The fulfillment process may take up to thirty days from the time of your order</strong>. </p>
                    <p>All Connectome Data is shipped from Washington University in Saint Louis via FedEx. When the order is shipped, you will be contacted and provided with a FedEx tracking number. Your order may be shipped in multiple packages, depending on order size.</p>
                    <p>Feel free to contact us at any time in the process with your order number at <a href="mailto:orders@humanconnectome.org">orders@humanconnectome.org</a>.</p>
                </div>
                <!-- /sidebox -->
            </div>
        </div>
        <!-- /sidebar -->


    </div> <!-- middle -->


    <?php
    mysqli_close($db);
    ?>

    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/footer.php'); ?>

</div>
<!-- end container -->

<script src="//www.google.com/jsapi?key=AIzaSyA5m1Nc8ws2BbmPRwKu5gFradvD_hgq6G0" type="text/javascript"></script>
<script type="text/javascript">
    /*
     *  How to restrict a search to a Custom Search Engine
     *  http://www.google.com/cse/
     */

    google.load('search', '1');

    function OnLoad() {
        // Create a search control
        var searchControl = new google.search.SearchControl();

        // Add in a WebSearch
        var webSearch = new google.search.WebSearch();

        // Restrict our search to pages from our CSE
        webSearch.setSiteRestriction('000073750279216385221:vnupk6whx-c');

        // Add the searcher to the SearchControl
        searchControl.addSearcher(webSearch);

        // tell the searcher to draw itself and tell it where to attach
        searchControl.draw(document.getElementById("search"));

        // execute an inital search
        // searchControl.execute('');
    }

    google.setOnLoadCallback(OnLoad);

</script>


<!-- google analytics -->
<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-18630139-1']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

</script>
</body>
</html>
