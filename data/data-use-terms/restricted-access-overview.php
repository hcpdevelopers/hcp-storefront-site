<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Restricted Data Usage Overview | Human Connectome Project</title>
    <meta http-equiv="Description"
          content="Overview of the restrictions on using sensitive HCP data, and guidance on proper usage."/>
    <meta http-equiv="Keywords"
          content="Restricted Access data, data use terms, family data, Public data release, Human Connectome Project"/>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/header-includes.php'); ?>

</head>

<body>
<!-- site container -->
<div id="site_container">

    <div id="NIH_header2">
<span>
<a href="http://www.neuroscienceblueprint.nih.gov/connectome/" title="NIH Human Connectome Project"
   style="color:#b4cfff; text-decoration:none;">NIH Blueprint: The Human Connectome Project</a></span>
    </div>

    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/storefront-topnav.php'); ?>

    <!-- Breadcrumbs -->
    <div id="breadcrumbs">
        <p>&raquo; <a href="/data/">HCP data</a> &gt;<a href="/data/data-use-terms/">HCP Data Use Terms</a>
            &gt; Restricted Data usage overview</p>
    </div>
    <!-- end breadcrumbs -->

    <!-- "Middle" area contains all content between the top navigation and the footer -->
    <div id="middle">
        <div id="content">

            <h1>Restricted Data Usage</h1>

            <!-- /#data-sets -->
            <p>Acceptance of&nbsp;HCP Open Access Data Use Terms&nbsp;is required for access to all HCP&nbsp;data, and
                will&nbsp;grant the user&nbsp;access to HCP imaging and most behavioral data.&nbsp;Users will be
                prompted to accept the Data Use Terms upon initial login&nbsp;to ConnectomeDB.</p>
            <p> Some data elements, including family structure (e.g. whether a participant is a twin or a non-twin
                sibling), exact age, handedness, ethnicity and race&nbsp;(see Restricted Data application form for the
                full list of elements), are available only to qualified investigators who agree to HCP’s Restricted Data
                Use Terms.  Applications for access to Restricted Data must be&nbsp;submitted by every investigator who
                will view and use the data (i.e. each member of a research group using the data must apply, from PI to
                trainees), will be&nbsp;processed&nbsp;individually, and approval is on a case-by-case basis.</p>
            <p>An investigator who uses Restricted Data takes on a serious responsibility to protect the privacy of HCP
                subjects. Specifically, if you are given access to these data,&nbsp;<strong>you MUST&nbsp;abide by a
                    prohibition against publishing certain types of individual data&nbsp; in combination</strong>&nbsp;that&nbsp;could
                make a person individually recognizable, or that could harm and embarrass someone who was inadvertently
                identified. The specifics of how Restricted Data can be published are detailed in the&nbsp;<a
                        href="/data/data-use-terms/restricted-access.php">Restricted Data Use Terms and application</a>
                <em>(Updated Jan 26, 2016)</em>, which qualified investigators must fill out, sign and submit to the
                HCP.</p>
            <p>To&nbsp;provide guidance to&nbsp;investigators, we have developed the following examples to illustrate
                types of information that can and cannot be published under the conditions that are delineated in the
                Restricted Data Use Terms.&nbsp; These examples also illustrate how our data use policy&nbsp;aims to
                protect subject identity without substantially impeding scientific communication.&nbsp; If you have
                either general or specific questions, or if you have additional use case scenarios that would be
                instructive to consider, please communicate these to Dr. Sandra Curtiss (<a
                        href="mailto:scurtiss@brainvis.wustl.edu">scurtiss@brainvis.wustl.edu</a>, HCP Project Manager),
                who will respond directly and potentially add appropriate use cases to this website.</p>
            <h3>Example 1. Heritability of cortical folding patterns. &nbsp;</h3>
            <p>Dr. X, an investigator who qualifies for restricted HCP data access,&nbsp;compares cortical folding
                patterns in HCP identical twins, non-identical twins, and non-twin sibs. &nbsp;She discovers that
                folding patterns are more heritable in some regions than others. &nbsp;To make her case, she illustrates
                the findings using two figures, with Figure 1 showing group-average analyses and data, and Figure 2
                showing folding patterns for individual subjects for exemplar identical twin pairs vs non-sib pairs.
                &nbsp;Figure 2 would necessarily include information about family structure. Under the Restricted Data
                Use Terms, Dr. X:</p>
            <ul>
                <li>Could publish Figure 2 showing cortical folding patterns and family structure of selected
                    individuals;
                </li>
            </ul>
            <ul>
                <li>Could not report the handedness, exact age, race/ethnicity,&nbsp;body weight, or other restricted
                    access elements of those individuals.
                </li>
            </ul>
            <ul>
                <li>Would identify the individuals as Subjects A, B, C, D, E and F in Figure 2.</li>
            </ul>
            <ul>
                <li>Would not report any HCP subject IDs, either for these individuals or for the subjects included in
                    the group average data, in either the figures or the Methods section of her paper.
                </li>
            </ul>
            <ul>
                <li>Would post the HCP subject IDs for Subjects A, B, C, D, E and F and for the subjects included in
                    group averages on the HCP Restricted Access website&nbsp;(through the <a
                            href="https://humanconnectome.org/documentation/subject-key/index.html">submit subject key site</a>), so that other
                    qualified investigators could evaluate her results.
                </li>
            </ul>
            <ul>
                <li>Could include handedness, exact age, race/ethnicity, or body weight information in Figure 1, as long
                    as the group averages were computed from at least three individuals.
                </li>
            </ul>
            <h3>Example 2.&nbsp;Exact Age.<em>&nbsp;</em></h3>
            <p>Qualified Investigator Dr. Y analyzes working memory responses (Z-statistics within a prefrontal ROI) in
                Task-fMRI scans as a function of age (by year). &nbsp;He finds a significant correlation and publishes a
                scatter-plot figure showing this correlation for the ~70 subjects from the HCP Q1 dataset.&nbsp; He is
                allowed to publish this scatter plot because, in the plot,&nbsp;age cannot be linked to individual
                subjects even though the values are plotted as Z-statistics by year for each individual.&nbsp;&nbsp; In
                another figure, Dr. Y shows individual-subject surface maps of working memory responses in four of these
                subjects. &nbsp; He may not report the age of&nbsp;individual&nbsp;subjects in the figure legend, or
                even the prefrontal Z-stat score for individual subjects in the legend&nbsp;(because this information
                would enable someone to look at the working memory scores of the individual subjects and infer,&nbsp;with
                high probability, their age by year). Under the Restricted Data Use Terms, Dr. Y:</p>
            <ul>
                <li>Could publish the desired figures, including the HCP subject IDs since no family structure
                    information is provided.
                </li>
            </ul>
            <ul>
                <li>Could not report the twin/non-twin status or ethnicity/race of individual subjects in this paper.
                </li>
            </ul>
        </div>
        <!-- /#Content -->

        <div id="sidebar-rt">
            <div id="sidebar-content"><!-- /sidebox -->
                <?php include_once($_SERVER['DOCUMENT_ROOT'].'/incl/sidebar-restricted-access.php'); ?>
            </div>
        </div>


    </div> <!-- middle -->

    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/footer.php'); ?>

</div>
<!-- end container -->

<script src="//www.google.com/jsapi?key=AIzaSyA5m1Nc8ws2BbmPRwKu5gFradvD_hgq6G0" type="text/javascript"></script>
<script type="text/javascript">
    /*
     *  How to restrict a search to a Custom Search Engine
     *  http://www.google.com/cse/
     */

    google.load('search', '1');

    function OnLoad() {
        // Create a search control
        var searchControl = new google.search.SearchControl();

        // Add in a WebSearch
        var webSearch = new google.search.WebSearch();

        // Restrict our search to pages from our CSE
        webSearch.setSiteRestriction('000073750279216385221:vnupk6whx-c');

        // Add the searcher to the SearchControl
        searchControl.addSearcher(webSearch);

        // tell the searcher to draw itself and tell it where to attach
        searchControl.draw(document.getElementById("search"));

        // execute an inital search
        // searchControl.execute('');
    }

    google.setOnLoadCallback(OnLoad);

</script>


<!-- google analytics -->
<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-18630139-1']);
    _gaq.push(['_trackPageview']);

    (function () {
        var ga = document.createElement('script');
        ga.type = 'text/javascript';
        ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(ga, s);
    })();

</script>
</body>
</html>
