<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Restricted Access Data | Human Connectome Project</title>
    <meta http-equiv="Description" content="Preview the HCP Open Access Data Use Terms before signing." />
    <meta http-equiv="Keywords" content="Open Access data, data use terms, behavioral data, Public data release, Human Connectome Project" />
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/header-includes.php'); ?>

</head>

<body>
<!-- site container -->
<div id="site_container">

    <div id="NIH_header2">
<span>
<a href="http://www.neuroscienceblueprint.nih.gov/connectome/" title="NIH Human Connectome Project" style="color:#b4cfff; text-decoration:none;">NIH Blueprint: The Human Connectome Project</a></span>
    </div>

    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/storefront-topnav.php'); ?>

    <!-- Breadcrumbs -->
    <div id="breadcrumbs">
        <p>&raquo;  <a href="/data/">HCP data</a> &gt; <a href="/data/data-use-terms/index.php">HCP Data Use Terms</a> &gt; Open Access DUT</p></div>
    <!-- end breadcrumbs -->

    <!-- "Middle" area contains all content between the top navigation and the footer -->
    <div id="middle">
        <div id="content">

            <h1>WU-Minn HCP Consortium Open Access Data Use Terms</h1>
            <p><em>Last updated: Apr 26, 2013.</em></p>
            <p>I request access to data collected by the Washington University - University of Minnesota Consortium of the Human Connectome Project (WU-Minn HCP), and I agree to the following:</p>
            <ol>
                <li>I will not  attempt to establish the identity of or attempt to contact any of the included  human subjects.</li>
                <li>I  understand that under no circumstances will the code that would link these data  to Protected Health Information be given to me, nor will any additional  information about individual human subjects be released to me under these Open  Access Data Use Terms.</li>
                <li>I  will comply with all relevant rules and regulations imposed by my institution. This  may mean that I need my research to be approved or declared exempt by a  committee that oversees research on human subjects, e.g. my IRB or Ethics Committee.&nbsp;The released HCP data  are not considered de-identified, insofar as&nbsp;certain combinations of HCP  Restricted Data&nbsp;(available through a separate process) might allow  identification of individuals. &nbsp;Different committees operate under  different national, state and local laws and may interpret regulations  differently, so it is important to ask about this. If needed and upon request,  the HCP will provide a certificate stating that you have accepted the HCP Open  Access Data Use Terms.</li>
                <li>I may  redistribute original WU-Minn HCP Open Access data and any derived data as long  as the data are redistributed under these same Data Use Terms.</li>
                <li>I will  acknowledge the use of WU-Minn HCP data and data derived from WU-Minn HCP data  when publicly presenting any results or algorithms that benefitted from their  use.</li>
                <ol>
                    <li>Papers,  book chapters, books, posters, oral presentations, and all other printed and  digital presentations of results derived from HCP data should contain the  following wording in the acknowledgments section: &quot;Data were provided [in  part] by the Human Connectome Project, WU-Minn Consortium (Principal  Investigators: David Van Essen and Kamil Ugurbil; 1U54MH091657) funded by the  16 NIH Institutes and Centers that support the NIH Blueprint for Neuroscience  Research; and by the McDonnell Center for Systems Neuroscience at Washington  University.&quot; </li>
                    <li>Authors of  publications or presentations using WU-Minn HCP data should cite relevant  publications describing the methods used by the HCP to acquire and process the  data. The specific publications that are appropriate to cite in any given study  will depend on what HCP data were used and for what purposes. An annotated and  appropriately up-to-date list of publications that may warrant consideration is  available at <a href="http://www.humanconnectome.org/about/acknowledgehcp.html" target="_blank">http://www.humanconnectome.org/about/acknowledgehcp.html</a></li>
                    <li>The WU-Minn  HCP Consortium as a whole should not be included as an author of publications  or presentations if this authorship would be based solely on the use of WU-Minn  HCP data.</li>
                </ol>
                <li>Failure to abide by  these guidelines will result in termination of my privileges to access WU-Minn  HCP data.  </li>
            </ol>
        </div>
        <!-- /#Content -->

        <div id="sidebar-rt">

            <div id="search">
                <!--   If Javascript is not enabled, nothing will display here -->
            </div> <!-- Search box -->

            <div id="sidebar-content"><!-- #BeginLibraryItem "/Library/DUT-Open Access.lbi" --><div class="sidebox databox">
                <h2>Sign up for Open Access Data</h2>
                <p><img src="/img/icon-signature.png" width="256" height="205" alt="Sign terms" style="margin-left:-10px" /></p>
                <p> Create a free account at <strong><a href="https://db.humanconnectome.org">db.humanconnectome.org</a></strong></p>
                <p style="font-size:11px !important">Terms updated Apr 26, 2013. <br />
                    <a href="/data/data-use-terms/open-access.php">View online</a> | <a href="/data/data-use-terms/DataUseTerms-HCP-Open-Access-26Apr2013.pdf">Download PDF</a></p>
            </div><!-- #EndLibraryItem --></div>
        </div>




    </div> <!-- middle -->

    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/footer.php'); ?>

</div>
<!-- end container -->

<script src="//www.google.com/jsapi?key=AIzaSyA5m1Nc8ws2BbmPRwKu5gFradvD_hgq6G0" type="text/javascript"></script>
<script type="text/javascript">
    /*
     *  How to restrict a search to a Custom Search Engine
     *  http://www.google.com/cse/
     */

    google.load('search', '1');

    function OnLoad() {
        // Create a search control
        var searchControl = new google.search.SearchControl();

        // Add in a WebSearch
        var webSearch = new google.search.WebSearch();

        // Restrict our search to pages from our CSE
        webSearch.setSiteRestriction('000073750279216385221:vnupk6whx-c');

        // Add the searcher to the SearchControl
        searchControl.addSearcher(webSearch);

        // tell the searcher to draw itself and tell it where to attach
        searchControl.draw(document.getElementById("search"));

        // execute an inital search
        // searchControl.execute('');
    }

    google.setOnLoadCallback(OnLoad);

</script>


<!-- google analytics -->
<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-18630139-1']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

</script>
</body>
</html>
