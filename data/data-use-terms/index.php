<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Data Use Terms | Human Connectome Project</title>
    <meta http-equiv="Description" content="To protect the privacy of our participants, the HCP has implemented provisions for handling Open Access data and Restricted Data." />
    <meta http-equiv="Keywords" content="Public data release, Human Connectome Project, pilot data, brain connectivity data" />
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/header-includes.php'); ?>

</head>

<body>
<!-- site container -->
<div id="site_container">

    <div id="NIH_header2">
<span>
<a href="http://www.neuroscienceblueprint.nih.gov/connectome/" title="NIH Human Connectome Project" style="color:#b4cfff; text-decoration:none;">NIH Blueprint: The Human Connectome Project</a></span>
    </div>

    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/storefront-topnav.php'); ?>

    <!-- Breadcrumbs -->
    <div id="breadcrumbs">
        <p>&raquo;  <a href="/data/">HCP data</a> &gt; HCP Data Use Terms</p></div>
    <!-- end breadcrumbs -->

    <!-- "Middle" area contains all content between the top navigation and the footer -->
    <div id="middle">
        <div id="content">

            <h1>HCP Data Use Terms: Important!</h1>

            <!-- /#data-sets -->
            <p>The  HCP provides imaging, behavioral, and demographic data from a large population  of healthy adults. This poses special challenges for protecting the privacy of  participants, especially because it is a family study including twins and their  siblings. Unless these data are properly managed, there is a risk that participants  might be recognizable to family members and others. In addition, some of the  data elements collected might harm or embarrass participants if they were to be  inadvertently identified.</p>
            <p>To  protect the privacy of our participants, the HCP has implemented a two-tiered  plan for data sharing, with different provisions for handling Open Access data  and Restricted Data.</p>
            <h2>Open Access Data</h2>
            <p>Open  Access Data (all imaging data and most of the behavioral data) is available to  those who register an account at ConnectomeDB and agree to the Open Access Data  Use Terms. This includes agreement to comply with institutional rules and  regulations. </p>
            <p><strong>This  means you may need the approval of your IRB or Ethics Committee to use the  data. </strong>The released HCP data are  not considered de-identified, since&nbsp;certain combinations of HCP Restricted  Data&nbsp;(available through a separate process) might allow identification of  individuals. &nbsp;Different national, state and local laws may apply and be interpreted differently,  so<strong> it is important that you consult with your IRB  or Ethics Committee before beginning your research.  </strong>If needed and upon request, the HCP will provide  a certificate stating that you have accepted the HCP Open Access Data Use Terms.</p>
            <p>Please  note that everyone who works with HCP open access data must review and agree to  these terms, including those who are accessing shared copies of this data. If  you are sharing HCP Open Access data, please advise your co-researchers that  they must register with ConnectomeDB and agree to these terms. </p>
            <ul>
                <p><strong>&raquo; <a href="https://db.humanconnectome.org/">Register and sign the Open Access Data  Use Terms at ConnectomeDB</a>.</strong>        </p>
            </ul>
            <h2>Restricted  Data</h2>
            <p>Restricted  Data Elements include a number of categories, such as family structure (twin or  non-twin status), age by year, and handedness. </p>
            <p><strong>Each  qualified investigator wanting to use Restricted Data must agree to the  Restricted Data Use Terms.</strong> These terms explain how Restricted Data may and may not be used and shared, and  they reiterate the need for compliance with institutional requirements. <strong>They  include major limitations on how Restricted Data can be incorporated into  publications and public presentations</strong><strong>.</strong>&nbsp;&nbsp;</p>
            <p><strong>You  must comply with your institutional rules and regulations regarding research on  human subjects. </strong>The released HCP data are  not considered de-identified, since&nbsp;certain combinations of HCP Restricted  Data&nbsp;might allow identification of individuals. &nbsp;Different national,  state and local laws may apply and be interpreted differently, so<strong> it is important that you consult with your IRB  or Ethics Committee before beginning your research.  </strong>If needed and upon request, the HCP will provide  a certificate stating that you have accepted the HCP Open and Restricted Access  Data Use Terms.</p>
            <ul>
                <p><strong>&raquo; <a href="/data/data-use-terms/restricted-access-overview.php">Learn more about  obtaining and using HCP Restricted  Data</a>.</strong>    </p>
            </ul>
        </div>
        <!-- /#Content -->

        <div id="sidebar-rt">

            <div id="sidebar-content">
                <?php include_once($_SERVER['DOCUMENT_ROOT'].'/incl/sidebar-open-access.php'); ?>
                <?php include_once($_SERVER['DOCUMENT_ROOT'].'/incl/sidebar-restricted-access.php'); ?>
            </div>
        </div>




    </div> <!-- middle -->

    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/footer.php'); ?>

</div>
<!-- end container -->

<script src="//www.google.com/jsapi?key=AIzaSyA5m1Nc8ws2BbmPRwKu5gFradvD_hgq6G0" type="text/javascript"></script>
<script type="text/javascript">
    /*
     *  How to restrict a search to a Custom Search Engine
     *  http://www.google.com/cse/
     */

    google.load('search', '1');

    function OnLoad() {
        // Create a search control
        var searchControl = new google.search.SearchControl();

        // Add in a WebSearch
        var webSearch = new google.search.WebSearch();

        // Restrict our search to pages from our CSE
        webSearch.setSiteRestriction('000073750279216385221:vnupk6whx-c');

        // Add the searcher to the SearchControl
        searchControl.addSearcher(webSearch);

        // tell the searcher to draw itself and tell it where to attach
        searchControl.draw(document.getElementById("search"));

        // execute an inital search
        // searchControl.execute('');
    }

    google.setOnLoadCallback(OnLoad);

</script>


<!-- google analytics -->
<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-18630139-1']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

</script>
</body>
</html>
