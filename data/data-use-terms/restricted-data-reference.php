<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Quick Reference: Data Use Terms - Human Connectome Project</title>
    <meta http-equiv="description" content="Welcome to the website for the Human Connectome Project, whose purpose is to map the neural pathways that make up the human brain. This project is funded by a grant from the National Institutes for Health." />
    <meta http-equiv="keywords" content="Human Connectome Project, Connectome, HCP, neural pathways, brain mapping, brain connectivity, brain circuitry, alzheimer's research, psychiatric disorders, neuroimaging, functional MRI, PETT scan, NIH, National Institutes for Health" />
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/header-includes.php'); ?>

</head>

<body>
<!-- site container -->
<div id="site_container">

    <div id="NIH_header2">
        <span><a href="http://www.neuroscienceblueprint.nih.gov/connectome/" title="NIH Human Connectome Project" style="color:#b4cfff; text-decoration:none;">NIH Blueprint: The Human Connectome Project</a></span>
    </div>

    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/storefront-topnav.php'); ?>

    <!-- Breadcrumbs -->
    <div id="breadcrumbs">
        <p>&raquo; <!-- InstanceBeginEditable name="Breadcrumbs" --><a href="/data/">data</a> &gt; <a href="/data/data-use-terms/">data use terms</a> &gt; quick reference<!-- InstanceEndEditable --></p></div>
    <!-- end breadcrumbs -->

    <!-- "Middle" area contains all content between the top navigation and the footer -->
    <div id="middle">
        <div id="content-wide" class="editable">
            <!-- InstanceBeginEditable name="Main Content" -->
            <!--    <div id="cse" style="width:100%; display:none;"></div> -->
            <h1>Quick Reference: Open Access vs Restricted Data</h1>
            <table class="docs-table">
                <tr>
                    <th>OPEN ACCESS DATA</th>
                    <th class="restricted">RESTRICTED ACCESS DATA</th>
                </tr>
                <tr>
                    <td><p>HCP Open Access data include brain imaging data (after defacing to remove identifying features) plus non-sensitive behavioral data. Investigators can view and use Open Access data by registering at db.humanconnectome.org and accepting the Open Access Data Use Terms, which includes an agreement to comply with institutional rules and regulations. </p>
                        <p><strong><a href="/data/data-use-terms/open-access.php">Open Access Data Use Terms</a></strong></td>
                    <td><p>Access to data that could potentially be used to identify HCP participants is strictly controlled. Only investigators that are deemed to have a legitimate research interest in this data, and who agree to the Restricted Access Data Use Terms, will be granted access. </p>
                        <p>There are two tiers of restricted data, and therefore two tiers of restricted access.</p>
                        <ul>
                            <li><strong>Tier 1 Restricted Data </strong>refers to potentially identifying but non-sensitive information, such as Age (in years), Race/Ethnicity, and twin status.</li>
                            <p><strong><a href="/data/data-use-terms/restricted-access.php">Tier 1 Restricted Data Use Terms</a></strong></p>
                            <li><strong>Tier 2 Restricted Data </strong>refers to data that has a greater potential factor for allowing subjects to be identified, such as color vision and physiological data. Tier 2 also contains data that could be damaging to a subject if it became publicly known. This includes items such as drug/alcohol use and family psychological history.</li>
                        </ul></td>
                </tr>
                <tr>
                    <th>Image Data</th>
                    <th class="restricted">RESTRICTED Image Data</th>
                </tr>
                <tr>
                    <td><p>All image data has been defaced and de-eared, and is considered open access data. </p>
                        <ul>
                            <li>MR Session data</li>
                            <li>Freesurfer Summary Stats</li>
                            <li>Freesurfer Volume Segmentation</li>
                            <li>Freesurfer Surface Area</li>
                            <li>Freesurfer Surface Thickness</li>
                        </ul></td>
                    <td>N/A</td>
                </tr>
                <tr>
                    <th>Participant Demographic Data</th>
                    <th class="restricted">RESTRICTED Participant Demographic Data</th>
                </tr>
                <tr>
                    <td><ul>
                            <li>Age group within a 5-year span (e.g. 21-25, 26-30)</li>
                            <li>Gender</li>
                        </ul></td>
                    <td><p><strong>These elements are Tier 1 Restricted</strong></p>
                        <ul>
                            <li>Age by year</li>
                            <li>Zygosity (twin status and twin type)</li>
                            <li>Family unit membership</li>
                            <li>Ethnicity</li>
                            <li>Race</li>
                            <li>Handedness</li>
                        </ul>
                        <p><strong>These elements are Tier 2 Restricted</strong></p><ul>
                            <li>Color vision and visual correction measures (TIER 2)</li>
                            <li>Body weight / BMI (TIER 2)</li>
                            <li>Height (TIER 2)</li>
                        </ul></td>
                </tr>
                <tr>
                    <th>Participant Psychiatric, Substance Abuse, Life Function Data</th>
                    <th class="restricted">RESTRICTED Participant Screening Data</th>
                </tr>
                <tr>
                    <td><p>N/A</p></td>
                    <td><p><strong>These elements are Tier 1 restricted</strong></p>
                        <ul>
                            <li>Life Function (Achenbach adult self-report)</li>
                            <li>DSM-oriented Scale</li>
                        </ul>
                        <p><strong>Each of these elements are Tier 2 restricted data.</strong></p>
                        <ul>
                            <li>Urine drug screen</li>
                            <li>HbA1c and TSH results</li>
                            <li>Psychiatric clinical symptoms (SSAGA interview)</li>
                            <li>Psychiatric or neurological illnesses of participants' parents</li>
                            <li>Substance use (self-report)</li>
                            <li>Nicotine dependence (Fagerstrom)<br />
                            </li>
                        </ul></td>
                </tr>
                <tr>
                    <th>Participant Sensory and Physical Function</th>
                    <th class="restricted">RESTRICTED Participant Physical Function</th>
                </tr>
                <tr>
                    <td><p><strong>Sensory</strong></p>
                        <ul>
                            <li>Audition (words in noise)</li>
                            <li>Olfaction (odor identification)</li>
                            <li>Pain (pain intensity &amp; interference)</li>
                            <li>Taste (taste intensity)</li>
                            <li>Visual acuity (not available until Q3)</li>
                            <li>Visual contrast sensitivity (Mars contrast sensitivity)</li>
                        </ul>
                        <strong>Physical Function</strong>          <ul>
                            <li>Hematocrit levels</li>
                        </ul></td>
                    <td><p><strong>Each of these elements are Tier 2 restricted data.</strong></p>
                        <ul>
                            <li>Information on endocrine disorders and age of onset</li>
                            <li>Glucose levels (Hemoglobin A1c)</li>
                            <li>Menstrual cycle and hormonal status</li>
                            <li>Thyroid function</li>
                        </ul></td>
                </tr>
                <tr>
                    <th>Participant Behavioral Data</th>
                    <th class="restricted">RESTRICTED Participant Behavioral Data</th>
                </tr>
                <tr>
                    <td><p><strong>Alertness</strong></p>
                        <ul>
                            <li>Cognitive Status (MMSE)</li>
                            <li>Pittsburgh Sleep Questionnaire (PSQI)</li>
                        </ul>
                        <p><strong>Cognition</strong></p>
                        <ul>
                            <li>Episodic Memory (Picture Sequence Memory)</li>
                            <li>Executive Function/Cognitive Flexibility (Dimensional Change Card Sort)</li>
                            <li>Executive Function/Inhibition (Flanker Task)</li>
                            <li>Fluid Intelligence (Penn Progressive Matrices)</li>
                            <li>Language/Reading Decoding (Oral Reading Recognition)</li>
                            <li>Language/Vocabulary Comprehension (Picture Vocabulary)</li>
                            <li>Processing Speed (Pattern Completion Processing Speed)</li>
                            <li>Self-regulation/Impulsivity (Delay Discounting)</li>
                            <li>Spatial Orientation (Variable Short Penn Line Orientation Test)</li>
                            <li>Sustained Attention (Short Penn Continuous Performance Test)</li>
                            <li>Verbal Episodic Memory (Penn Word Memory Test)</li>
                            <li>Working Memory (List Sorting)†</li>
                        </ul>
                        <p><strong>Emotion</strong></p>
                        <ul>
                            <li>Emotion Recognition (Penn Emotion Recognition Test)</li>
                            <li>Negative Affect (Sadness, Fear, Anger)</li>
                            <li>Psychological Well-being (Positive Affect, Life Satisfaction, Meaning and Purpose)</li>
                            <li>Social Relationships (Social Support, Companionship, Social Distress, Positive Social Development)</li>
                            <li>Stress and Self Efficacy (Perceived Stress, Self-Efficacy)</li>
                        </ul>
                        <p><strong>Motor</strong></p>
                        <ul>
                            <li>Endurance (2 minute walk test)</li>
                            <li>Locomotion (4-meter walk test)</li>
                            <li>Motor Dexterity (9-hole Pegboard)</li>
                            <li>Strength (Grip Strength Dynamometry)</li>
                        </ul>
                        <p><strong>Personality</strong></p><ul>
                            <li>Five Factor Model (NEO-FFI)</li>
                        </ul></td>
                    <td><p><strong>Visual Processing</strong></p>
                        <ul>
                            <li>Color Vision (Farnsworth Test)</li>
                        </ul></td>
                </tr>
            </table>
            <p>&nbsp;</p>
            <!-- InstanceEndEditable -->
        </div>

        <!-- Primary page Content -->


    </div> <!-- middle -->

    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/footer.php'); ?>

</div>
<!-- end container -->

<script src="//www.google.com/jsapi?key=AIzaSyA5m1Nc8ws2BbmPRwKu5gFradvD_hgq6G0" type="text/javascript"></script>
<script type="text/javascript">
    /*
     *  How to restrict a search to a Custom Search Engine
     *  http://www.google.com/cse/
     */

    google.load('search', '1');

    function OnLoad() {
        // Create a search control
        var searchControl = new google.search.SearchControl();

        // Add in a WebSearch
        var webSearch = new google.search.WebSearch();

        // Restrict our search to pages from our CSE
        webSearch.setSiteRestriction('000073750279216385221:vnupk6whx-c');

        // Add the searcher to the SearchControl
        searchControl.addSearcher(webSearch);

        // tell the searcher to draw itself and tell it where to attach
        searchControl.draw(document.getElementById("search"));

        // execute an inital search
        // searchControl.execute('');
    }

    google.setOnLoadCallback(OnLoad);

</script>


<!-- google analytics -->
<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-18630139-1']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

</script>
</body>
<!-- InstanceEnd --></html>
