<!DOCTYPE html>
<html>
<head>
    <link href="/css/hcpv3.css" rel="stylesheet" type="text/css" />
    <link href="/css/sortable.css" rel="stylesheet" type="text/css" />
    <link href="/css/data.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="//code.jquery.com/jquery-1.8.3.min.js"></script></head>
<body>
<?php
/**
 * Created by PhpStorm.
 * User: whorto01
 * Date: 7/6/2016
 * Time: 10:55 AM
 */

if (!$_GET['orderId']) die ('<p>Sorry, an error has occurred. Please contact the site administrator.</p>');

/* mysql connection */
$dbname = ($_SERVER['HTTP_HOST']=='dev.humanconnectome.org') ? 'devadmin_orders_dev' : 'devadmin_orders';
$dbuser = 'devadmin_drives';
$dbpass = '1IXQe6NUdnVB';
$dbhost = $_ENV{DATABASE_SERVER};
$db = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname);

if (!$db) {
    die('Connect Error (' . mysqli_connect_errno() . ') '
        . mysqli_connect_error());
}
mysqli_set_charset($db,"utf8");

$orderId = $_GET['orderId'];
if ($_POST) :
// take post values and update order info
    $q = "UPDATE orders SET customer_name='".$_POST['customer_name']."', 
                            customer_email='".$_POST['customer_email']."',
                            customer_institution='".$_POST['customer_institution']."',
                            shipping_address='".$_POST['shipping_address']."',
                            shipping_address_2='".$_POST['shipping_address_2']."',
                            shipping_city='".$_POST['shipping_city']."',
                            shipping_state='".$_POST['shipping_state']."',
                            shipping_postal_code='".$_POST['shipping_postal_code']."',
                            shipping_country='".$_POST['shipping_country']."',
                            shipping_phone='".$_POST['shipping_phone']."'
                        WHERE id='".$orderId."' LIMIT 1";
    // die($q);
    $r = mysqli_query($db,$q) or die($q);
endif;

$q = "SELECT * FROM orders WHERE id='".$orderId."' LIMIT 1";
$r = mysqli_query($db,$q);
$orderInfo = mysqli_fetch_assoc($r);
?>
<div id="middle" style="width: inherit; padding: 0;">
    <div id="content" style="padding: 0; min-height: 400px;">
        <div id="display" class="hcpForm-wrapper address-panel">
            <p><?php echo $orderInfo["customer_name"]?><br />
                <?php echo ($orderInfo["customer_institution"]) ? $orderInfo["customer_institution"]."<br />" : "" ?>
                <?php echo $orderInfo["customer_email"]?><br />
            <?php echo $orderInfo["shipping_address"]?><br />
            <?php echo ($orderInfo["shipping_address_2"]) ? $orderInfo["shipping_address_2"]."<br />" : "" ?>
            <?php echo $orderInfo["shipping_city"]?>, <?php echo $orderInfo["shipping_state"]?><br />
            <?php echo $orderInfo["shipping_postal_code"]?><br />
            <?php echo $orderInfo["shipping_country"]?><br />
            <?php echo $orderInfo["shipping_phone"]?></p>
            <p><a href="javascript:void" onclick="$('.address-panel').toggle();">Edit</a></p>
        </div>
        <form method="post" action="address-change.php?orderId=<?php echo $orderId ?>" class="hcpForm">
            <div id="edit" class="hcpForm-wrapper address-panel" style="display:none;">
                <div class="hidden error-messages" id="error-message-address">
                    <p><strong>Errors Found:</strong></p>
                    <ul class="error-list">
                        <li>You're crazy.</li>
                    </ul>
                </div>
                <div>
                    <label for="customer_name">Name</label>
                    <input type="text" name="customer_name" style="width:485px" class="required" value="<?php echo $orderInfo["customer_name"]?>">
                </div>
                <div>
                    <label for="customer_institution">Institution</label>
                    <input type="text" name="customer_institution" style="width:485px" value="<?php echo ($orderInfo["customer_institution"]) ? $orderInfo["customer_institution"] : "" ?>">
                </div>
                <div>
                    <label for="customer_email">Email Address</label>
                    <input type="text" name="customer_email" style="width:485px" class="required email" value="<?php echo $orderInfo["customer_email"]?>" />
                </div>
                <div>
                    <label for="shipping_address">Street Address</label>
                    <input type="text" name="shipping_address" style="width:485px" class="required" value="<?php echo $orderInfo["shipping_address"]?>"/>
                </div>
                <div>
                    <label for="shipping_address_2">Lab/Office/Department</label>
                    <textarea name="shipping_address_2" style="width:485px" rows="3"><?php echo ($orderInfo["shipping_address_2"]) ? $orderInfo["shipping_address_2"] : "" ?></textarea>
                </div>
                <div>
                    <label for="shipping_city">City</label>
                    <input type="text" name="shipping_city" style="width:290px" class="required" value="<?php echo $orderInfo["shipping_city"]?>" />
                </div>
                <div class="state-US">
                    <label for="shipping_state">State</label>
                    <select name="shipping_state" class="required">
                        <option selected="selected"></option>
                        <option value="AL">Alabama</option>
                        <option value="AK">Alaska</option>
                        <option value="AZ">Arizona</option>
                        <option value="AR">Arkansas</option>
                        <option value="CA">California</option>
                        <option value="CO">Colorado</option>
                        <option value="CT">Connecticut</option>
                        <option value="DC">District Of Columbia</option>
                        <option value="DE">Delaware</option>
                        <option value="FL">Florida</option>
                        <option value="GA">Georgia</option>
                        <option value="HI">Hawaii</option>
                        <option value="ID">Idaho</option>
                        <option value="IL">Illinois</option>
                        <option value="IN">Indiana</option>
                        <option value="IA">Iowa</option>
                        <option value="KS">Kansas</option>
                        <option value="KY">Kentucky</option>
                        <option value="LA">Louisiana</option>
                        <option value="ME">Maine</option>
                        <option value="MD">Maryland</option>
                        <option value="MA">Massachusetts</option>
                        <option value="MI">Michigan</option>
                        <option value="MN">Minnesota</option>
                        <option value="MS">Mississippi</option>
                        <option value="MO">Missouri</option>
                        <option value="MT">Montana</option>
                        <option value="NE">Nebraska</option>
                        <option value="NV">Nevada</option>
                        <option value="NH">New Hampshire</option>
                        <option value="NJ">New Jersey</option>
                        <option value="NM">New Mexico</option>
                        <option value="NY">New York</option>
                        <option value="NC">North Carolina</option>
                        <option value="ND">North Dakota</option>
                        <option value="OH">Ohio</option>
                        <option value="OK">Oklahoma</option>
                        <option value="OR">Oregon</option>
                        <option value="PA">Pennsylvania</option>
                        <option value="RI">Rhode Island</option>
                        <option value="SC">South Carolina</option>
                        <option value="SD">South Dakota</option>
                        <option value="TN">Tennessee</option>
                        <option value="TX">Texas</option>
                        <option value="UT">Utah</option>
                        <option value="VT">Vermont</option>
                        <option value="VA">Virginia</option>
                        <option value="WA">Washington</option>
                        <option value="WV">West Virginia</option>
                        <option value="WI">Wisconsin</option>
                        <option value="WY">Wyoming</option>
                    </select>

                </div>
                <div class="state-int hidden">
                    <label for="shipping_state">State / Province / Region</label>
                    <input type="text" name="shipping_state" style="width:150px" disabled="disabled" />
                </div>
                <div>
                    <label for="shipping_postal_code">ZIP/Postal Code</label>
                    <input type="text" name="shipping_postal_code" style="width:90px" class="required" value="<?php echo $orderInfo["shipping_postal_code"] ?>" />
                </div>
                <div>
                    <label for="shipping_country">Country</label>
                    <select name="shipping_country" id="country-select">
                        <option value=""></option>
                        <option value="United States">United States</option>
                        <option value="Afghanistan">Afghanistan</option>
                        <option value="Albania">Albania</option>
                        <option value="Algeria">Algeria</option>
                        <option value="American Samoa">American Samoa</option>
                        <option value="Andorra">Andorra</option>
                        <option value="Anguilla">Anguilla</option>
                        <option value="Antarctica">Antarctica</option>
                        <option value="Antigua And Barbuda">Antigua And Barbuda</option>
                        <option value="Argentina">Argentina</option>
                        <option value="Armenia">Armenia</option>
                        <option value="Aruba">Aruba</option>
                        <option value="Australia">Australia</option>
                        <option value="Austria">Austria</option>
                        <option value="Azerbaijan">Azerbaijan</option>
                        <option value="Bahamas">Bahamas</option>
                        <option value="Bahrain">Bahrain</option>
                        <option value="Bangladesh">Bangladesh</option>
                        <option value="Barbados">Barbados</option>
                        <option value="Belarus">Belarus</option>
                        <option value="Belgium">Belgium</option>
                        <option value="Belize">Belize</option>
                        <option value="Benin">Benin</option>
                        <option value="Bermuda">Bermuda</option>
                        <option value="Bhutan">Bhutan</option>
                        <option value="Bolivia">Bolivia</option>
                        <option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
                        <option value="Botswana">Botswana</option>
                        <option value="Bouvet Island">Bouvet Island</option>
                        <option value="Brazil">Brazil</option>
                        <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
                        <option value="Brunei Darussalam">Brunei Darussalam</option>
                        <option value="Bulgaria">Bulgaria</option>
                        <option value="Burkina Faso">Burkina Faso</option>
                        <option value="Burundi">Burundi</option>
                        <option value="Cambodia">Cambodia</option>
                        <option value="Cameroon">Cameroon</option>
                        <option value="Canada">Canada</option>
                        <option value="Cape Verde">Cape Verde</option>
                        <option value="Cayman Islands">Cayman Islands</option>
                        <option value="Central African Republic">Central African Republic</option>
                        <option value="Chad">Chad</option>
                        <option value="Chile">Chile</option>
                        <option value="China">China</option>
                        <option value="Christmas Island">Christmas Island</option>
                        <option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
                        <option value="Colombia">Colombia</option>
                        <option value="Comoros">Comoros</option>
                        <option value="Congo">Congo</option>
                        <option value="Congo, the Democratic Republic of the">Congo, the Democratic Republic of the</option>
                        <option value="Cook Islands">Cook Islands</option>
                        <option value="Costa Rica">Costa Rica</option>
                        <option value="Cote d'Ivoire">Cote d'Ivoire</option>
                        <option value="Croatia">Croatia</option>
                        <option value="Cyprus">Cyprus</option>
                        <option value="Czech Republic">Czech Republic</option>
                        <option value="Denmark">Denmark</option>
                        <option value="Djibouti">Djibouti</option>
                        <option value="Dominica">Dominica</option>
                        <option value="Dominican Republic">Dominican Republic</option>
                        <option value="East Timor">East Timor</option>
                        <option value="Ecuador">Ecuador</option>
                        <option value="Egypt">Egypt</option>
                        <option value="El Salvador">El Salvador</option>
                        <option value="England">England</option>
                        <option value="Equatorial Guinea">Equatorial Guinea</option>
                        <option value="Eritrea">Eritrea</option>
                        <option value="Espana">Espana</option>
                        <option value="Estonia">Estonia</option>
                        <option value="Ethiopia">Ethiopia</option>
                        <option value="Falkland Islands">Falkland Islands</option>
                        <option value="Faroe Islands">Faroe Islands</option>
                        <option value="Fiji">Fiji</option>
                        <option value="Finland">Finland</option>
                        <option value="France">France</option>
                        <option value="French Guiana">French Guiana</option>
                        <option value="French Polynesia">French Polynesia</option>
                        <option value="French Southern Territories">French Southern Territories</option>
                        <option value="Gabon">Gabon</option>
                        <option value="Gambia">Gambia</option>
                        <option value="Georgia">Georgia</option>
                        <option value="Germany">Germany</option>
                        <option value="Ghana">Ghana</option>
                        <option value="Gibraltar">Gibraltar</option>
                        <option value="Great Britain">Great Britain</option>
                        <option value="Greece">Greece</option>
                        <option value="Greenland">Greenland</option>
                        <option value="Grenada">Grenada</option>
                        <option value="Guadeloupe">Guadeloupe</option>
                        <option value="Guam">Guam</option>
                        <option value="Guatemala">Guatemala</option>
                        <option value="Guinea">Guinea</option>
                        <option value="Guinea-Bissau">Guinea-Bissau</option>
                        <option value="Guyana">Guyana</option>
                        <option value="Haiti">Haiti</option>
                        <option value="Heard and Mc Donald Islands">Heard and Mc Donald Islands</option>
                        <option value="Honduras">Honduras</option>
                        <option value="Hong Kong">Hong Kong</option>
                        <option value="Hungary">Hungary</option>
                        <option value="Iceland">Iceland</option>
                        <option value="India">India</option>
                        <option value="Indonesia">Indonesia</option>
                        <option value="IRAN">IRAN</option>
                        <option value="Iraq">Iraq</option>
                        <option value="Ireland">Ireland</option>
                        <option value="Israel">Israel</option>
                        <option value="Italy">Italy</option>
                        <option value="Jamaica">Jamaica</option>
                        <option value="Japan">Japan</option>
                        <option value="Jordan">Jordan</option>
                        <option value="Kazakhstan">Kazakhstan</option>
                        <option value="Kenya">Kenya</option>
                        <option value="Kiribati">Kiribati</option>
                        <option value="KOREA, DEMOCRATIC PEOPLE'S REPUBLIC OF">KOREA, DEMOCRATIC PEOPLE'S REPUBLIC OF</option>
                        <option value="Korea, Republic of">Korea, Republic of</option>
                        <option value="Kuwait">Kuwait</option>
                        <option value="Kyrgyzstan">Kyrgyzstan</option>
                        <option value="Lao People's Democratic Republic">Lao People's Democratic Republic</option>
                        <option value="Latvia">Latvia</option>
                        <option value="Lebanon">Lebanon</option>
                        <option value="Lesotho">Lesotho</option>
                        <option value="Liberia">Liberia</option>
                        <option value="Libya">Libya</option>
                        <option value="Liechtenstein">Liechtenstein</option>
                        <option value="Lithuania">Lithuania</option>
                        <option value="Luxembourg">Luxembourg</option>
                        <option value="MACAO">MACAO</option>
                        <option value="Macedonia">Macedonia</option>
                        <option value="Madagascar">Madagascar</option>
                        <option value="Malawi">Malawi</option>
                        <option value="Malaysia">Malaysia</option>
                        <option value="Maldives">Maldives</option>
                        <option value="Mali">Mali</option>
                        <option value="Malta">Malta</option>
                        <option value="Marshall Islands">Marshall Islands</option>
                        <option value="Martinique">Martinique</option>
                        <option value="Mauritania">Mauritania</option>
                        <option value="Mauritius">Mauritius</option>
                        <option value="Mayotte">Mayotte</option>
                        <option value="Mexico">Mexico</option>
                        <option value="Micronesia, Federated States of">Micronesia, Federated States of</option>
                        <option value="Moldova, Republic of">Moldova, Republic of</option>
                        <option value="Monaco">Monaco</option>
                        <option value="Mongolia">Mongolia</option>
                        <option value="Montserrat">Montserrat</option>
                        <option value="Morocco">Morocco</option>
                        <option value="Mozambique">Mozambique</option>
                        <option value="Myanmar">Myanmar</option>
                        <option value="Namibia">Namibia</option>
                        <option value="Nauru">Nauru</option>
                        <option value="Nepal">Nepal</option>
                        <option value="Netherlands">Netherlands</option>
                        <option value="Netherlands Antilles">Netherlands Antilles</option>
                        <option value="New Caledonia">New Caledonia</option>
                        <option value="New Zealand">New Zealand</option>
                        <option value="Nicaragua">Nicaragua</option>
                        <option value="Niger">Niger</option>
                        <option value="Nigeria">Nigeria</option>
                        <option value="Niue">Niue</option>
                        <option value="Norfolk Island">Norfolk Island</option>
                        <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                        <option value="Norway">Norway</option>
                        <option value="Oman">Oman</option>
                        <option value="Pakistan">Pakistan</option>
                        <option value="Palau">Palau</option>
                        <option value="Panama">Panama</option>
                        <option value="Papua New Guinea">Papua New Guinea</option>
                        <option value="Paraguay">Paraguay</option>
                        <option value="Peru">Peru</option>
                        <option value="Philippines">Philippines</option>
                        <option value="Pitcairn">Pitcairn</option>
                        <option value="Poland">Poland</option>
                        <option value="Portugal">Portugal</option>
                        <option value="Puerto Rico">Puerto Rico</option>
                        <option value="Qatar">Qatar</option>
                        <option value="Reunion">Reunion</option>
                        <option value="Romania">Romania</option>
                        <option value="Russia">Russia</option>
                        <option value="Russian Federation">Russian Federation</option>
                        <option value="Rwanda">Rwanda</option>
                        <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                        <option value="Saint Lucia">Saint Lucia</option>
                        <option value="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option>
                        <option value="Samoa (Independent)">Samoa (Independent)</option>
                        <option value="San Marino">San Marino</option>
                        <option value="Sao Tome and Principe">Sao Tome and Principe</option>
                        <option value="Saudi Arabia">Saudi Arabia</option>
                        <option value="Scotland">Scotland</option>
                        <option value="Senegal">Senegal</option>
                        <option value="Serbia and Montenegro">Serbia and Montenegro</option>
                        <option value="Seychelles">Seychelles</option>
                        <option value="Sierra Leone">Sierra Leone</option>
                        <option value="Singapore">Singapore</option>
                        <option value="Slovakia">Slovakia</option>
                        <option value="Slovenia">Slovenia</option>
                        <option value="Solomon Islands">Solomon Islands</option>
                        <option value="Somalia">Somalia</option>
                        <option value="South Africa">South Africa</option>
                        <option value="South Georgia and the South Sandwich Islands">South Georgia and the South Sandwich Islands</option>
                        <option value="Spain">Spain</option>
                        <option value="Sri Lanka">Sri Lanka</option>
                        <option value="St. Helena">St. Helena</option>
                        <option value="St. Pierre and Miquelon">St. Pierre and Miquelon</option>
                        <option value="Suriname">Suriname</option>
                        <option value="Svalbard and Jan Mayen Islands">Svalbard and Jan Mayen Islands</option>
                        <option value="Swaziland">Swaziland</option>
                        <option value="Sweden">Sweden</option>
                        <option value="Switzerland">Switzerland</option>
                        <option value="Taiwan">Taiwan</option>
                        <option value="Tajikistan">Tajikistan</option>
                        <option value="Tanzania">Tanzania</option>
                        <option value="Thailand">Thailand</option>
                        <option value="Togo">Togo</option>
                        <option value="Tokelau">Tokelau</option>
                        <option value="Tonga">Tonga</option>
                        <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                        <option value="Tunisia">Tunisia</option>
                        <option value="Turkey">Turkey</option>
                        <option value="Turkmenistan">Turkmenistan</option>
                        <option value="Turks and Caicos Islands">Turks and Caicos Islands</option>
                        <option value="Tuvalu">Tuvalu</option>
                        <option value="Uganda">Uganda</option>
                        <option value="Ukraine">Ukraine</option>
                        <option value="United Arab Emirates">United Arab Emirates</option>
                        <option value="United Kingdom">United Kingdom</option>
                        <option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
                        <option value="Uruguay">Uruguay</option>
                        <option value="Uzbekistan">Uzbekistan</option>
                        <option value="Vanuatu">Vanuatu</option>
                        <option value="Vatican City State (Holy See)">Vatican City State (Holy See)</option>
                        <option value="Venezuela">Venezuela</option>
                        <option value="Viet Nam">Viet Nam</option>
                        <option value="Virgin Islands (British)">Virgin Islands (British)</option>
                        <option value="Virgin Islands (U.S.)">Virgin Islands (U.S.)</option>
                        <option value="Wales">Wales</option>
                        <option value="Wallis and Futuna Islands">Wallis and Futuna Islands</option>
                        <option value="Western Sahara">Western Sahara</option>
                        <option value="Yemen">Yemen</option>
                        <option value="Zambia">Zambia</option>
                        <option value="Zimbabwe">Zimbabwe</option>
                    </select>
                </div>
                <div>
                    <label for="shipping_phone">Phone Number (required for FedEx shipping)</label>
                    <input type="text" name="shipping_phone" style="width:485px" class="required" value="<?php echo $orderInfo["shipping_phone"]?>"/>
                </div>
                <div>
                    <!-- <input type="button" class="highlight" value="Update Address" onclick="validateMe()" /><br /> -->
                    <input type="submit" class="highlight" value="Update Shipping Address" /><br />
                    <a href="javascript:void" onclick="$('.address-panel').toggle();">Cancel</a>
                </div>
            </div>
            <input type="hidden" id="itemcnt" value="1" />
        </form>
    </div>
</div>
<script type="text/javascript" src="/js/order-validation.js"></script>
<script>
    // set country
    $('#country-select').find('option').prop('selected',false)
        .each(function(){
            if ('<?php echo $orderInfo['shipping_country'] ?>' == $(this).val()) {
                $(this).prop('selected','selected');
                return;
            }
    });
    // set state
    if ('<?php echo $orderInfo['shipping_country'] ?>' == 'United States') {
        $('.state-US').find('option').prop('selected',false)
            .each(function(){
                if ('<?php echo $orderInfo['shipping_state'] ?>' == $(this).val()) {
                    $(this).prop('selected','selected');
                    return;
                }
        });
    } else {
        $('.state-US').addClass('hidden');
        $('.state-int').removeClass('hidden')
            .find('input').val('<?php echo $orderInfo['shipping_state'] ?>');
    }
</script>

<?php
mysqli_close($db);
?>
</body>
</html>