<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Important Notice to Connectome In A Box Data Users | Human Connectome Project</title>
    <meta http-equiv="Description" content="Connectome In A Box is a way to get ALL the data. We will format a hard drive and send it anywhere in the world." />
    <meta http-equiv="Keywords" content="Connectome Data, Connectivity data, human brain mapping, Human Connectome Project" />
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/header-includes.php'); ?>
</head>

<body>

<div id="site_container">

    <div id="NIH_header2">
        <span><a href="http://www.neuroscienceblueprint.nih.gov/connectome/" title="NIH Human Connectome Project" style="color:#b4cfff; text-decoration:none;">NIH Blueprint: The Human Connectome Project</a></span>
    </div>

    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/storefront-topnav.php'); ?>


    <!-- Breadcrumbs -->
    <div id="breadcrumbs">
        <p>&raquo; <a href="/data/">Get HCP Data</a> &gt; <a href="/data/cinab/connectome-in-a-box.php">Connectome In A Box</a> &gt; Notice</p></div>
    <!-- end breadcrumbs -->

    <!-- "Middle" area contains all content between the top navigation and the footer -->
    <div id="middle"><!-- content-wide -->
        <div id="content">
            <h1>Important Notice to Recipients and System Administrators of HCP Connectome In A Box Hard Drives</h1>
            <p>Thank you for acquiring a Connectome-in-a-Box that  contains HCP image data.   This provides an easy and efficient way to transfer large HCP datasets  to other labs and institutions wanting to process lots of data, especially when  multiple investigators are involved. With it comes a need to insure compliance  with HCP’s Data Use Terms as well as any institutional requirements.</p>
            <p>Before accessing the HCP Connectome-in-a-Box data, it  is required that <strong>each </strong>investigator to&nbsp;<a href="http://www.humanconnectome.org/data/data-use-terms/">register</a>&nbsp;and agree  to the&nbsp;<a href="http://www.humanconnectome.org/data/data-use-terms/open-access.html">Open Access Data Use Terms</a>.  This may entail getting local IRB approval to  use the data.  While this is the  responsibility of each individual investigator, those involved in helping  distribute the data by way of Connectome-in-a-Box have a responsibility  to facilitate this process.  </p>
            <p>1) Access to Connectome-in-a-Box data should be  provided <strong>only</strong> to investigators who  have agreed to the HCP Open Access Data Use Terms.  Appropriate recommended methods of access  restriction include password protection, Unix security group controls, and  physically securing the portable drive under lock and key.  HCP will provide a service via ConnectomeDB  that allows local administrators to ascertain whether a particular individual  has agreed to these terms. </p>
            <p>Administrators  who have an HCP account can use the following curl command to obtain a comma-separated list of  groups a given user belongs to (once a valid email is entered in the string).&nbsp;</p>
            <div style="background: #f7f7f7; padding: 20px; border-radius: 5px; width: 520px; margin-bottom: 1em; overflow-x:auto;">
                <code style="font-size: 12px; font-family: Consolas,Courier,monospace;">curl  https://db.humanconnectome.org/data/services/verifyaccount?email=&lt;email-address&gt;</code>
            </div>
            <p>  If a list is returned that contains &ldquo;<strong>Phase2OpenUsers</strong>&rdquo;, the individual has  accepted the data use terms for access to HCP Phase II data. </p>
            <p>2) The message on the next page should be communicated  to all individuals at your institution who express interest in using HCP data  from Connectome-in-a-Box.</p>
            <p>Sincerely,<br />
                David C. Van Essen (PI), for the WU-Minn HCP  Consortium</p>
            <p>May 1, 2013 </p>
            <hr />
            <h2>IMPORTANT NOTICE to investigators wanting to  use HCP datasets available on Connectome-in-a-Box hard drives.</h2>
            <p>HCP’s Connectome-in-a-Box provides imaging data from  the Open Access dataset. Before using any of these data for research, you and all  other investigators using the data are required&nbsp;to&nbsp;<a href="/data/data-use-terms/">register</a>&nbsp;and agree  to the&nbsp;<a href="/data/data-use-terms/open-access.php">Open Access Data Use Terms</a>.<strong><em>&nbsp;</em></strong><strong>This includes agreeing to  comply with institutional rules and regulations.  </strong>This  may mean that you need your research to be approved or declared exempt by a  committee that oversees research on human subjects (e.g., your IRB or Ethics  Committee).  The  released HCP data are not considered de-identified, insofar as&nbsp;certain  combinations of HCP Restricted Data&nbsp;(available through a separate process)  might allow identification of individuals. &nbsp;Different committees  operate under different national, state and local laws and may interpret  regulations differently, so it is important to ask about this. If needed and  upon request, the HCP will provide a certificate stating that you have accepted  the HCP Open Access Data Use Terms.</p>
            <p>Sincerely,<br />
                David C. Van Essen (PI), for the WU-Minn HCP  Consortium</p>
            <p>    May 1, 2013</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <h3>&nbsp;</h3>
        </div>

        <!-- Primary page Content -->

        <div id="sidebar-rt">

            <div id="sidebar-content">
                <div class="sidebox databox">
                    <h2>Sign up for Open Access Data</h2>
                    <p><img src="/img/icon-signature.png" width="256" height="205" alt="Sign terms" style="margin-left:-10px" /></p>
                    <p>Create a free account at <strong><a href="https://db.humanconnectome.org">db.humanconnectome.org</a></strong></p>
                    <p style="font-size:11px !important">Terms updated Apr 26, 2013. <br />
                        <a href="https://www.humanconnectome.org/data/data-use-terms/open-access.html">View online</a> | <a href="https://www.humanconnectome.org/data/data-use-terms/DataUseTerms-HCP-Open-Access-26Apr2013.pdf">Download PDF</a></p>
                </div>
                <div class="sidebox">
                    <h4>Questions?</h4>
                    <p>Questions about working with Connectome Data? Consider asking them in our HCP Data Users email list. </p>
                    <p>&nbsp;</p>
                    <p><strong><a href="https://www.humanconnectome.org/contact/#subscribe">HCP Mailing list signup form</a></strong></p>
                </div>
            </div>

        </div> <!-- sidebar -->

    </div> <!-- middle -->

    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/footer.php'); ?>

</div>
<!-- end container -->

<script src="https://www.google.com/jsapi?key=AIzaSyA5m1Nc8ws2BbmPRwKu5gFradvD_hgq6G0" type="text/javascript"></script>

<script type="text/javascript">
    /*
     *  How to restrict a search to a Custom Search Engine
     *  http://www.google.com/cse/
     */

    google.load('search', '1');

    function OnLoad() {
        // Create a search control
        var searchControl = new google.search.SearchControl();

        // Add in a WebSearch
        var webSearch = new google.search.WebSearch();

        // Restrict our search to pages from our CSE
        webSearch.setSiteRestriction('000073750279216385221:vnupk6whx-c');

        // Add the searcher to the SearchControl
        searchControl.addSearcher(webSearch);

        // tell the searcher to draw itself and tell it where to attach
        searchControl.draw(document.getElementById("search"));

        // execute an inital search
        // searchControl.execute('');
    }

    google.setOnLoadCallback(OnLoad);

</script>


<!-- google analytics -->
<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-18630139-1']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

</script>
</body>
</html>
