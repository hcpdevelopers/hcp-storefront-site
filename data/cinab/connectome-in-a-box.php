<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Connectome In A Box | Human Connectome Project</title>
    <meta http-equiv="Description" content="Connectome In A Box is a way to get ALL the data. We will format a hard drive and send it anywhere in the world." />
    <meta http-equiv="Keywords" content="Connectome Data, Connectivity data, human brain mapping, Human Connectome Project" />
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/header-includes.php'); ?>

</head>

<body>

<div id="page-mask" onclick="modalClose()"></div>


<!-- site container -->
<div id="site_container">

    <div id="NIH_header2">
        <span><a href="http://www.neuroscienceblueprint.nih.gov/connectome/" title="NIH Human Connectome Project" style="color:#b4cfff; text-decoration:none;">NIH Blueprint: The Human Connectome Project</a></span>
    </div>

    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/storefront-topnav.php'); ?>

    <!-- Breadcrumbs -->
    <div id="breadcrumbs">
        <p>&raquo; <a href="/data/">Get HCP Data</a> &gt; Connectome In A Box</p></div>
    <!-- end breadcrumbs -->

    <!-- "Middle" area contains all content between the top navigation and the footer -->
    <div id="middle">
        <div id="content-wide" style="min-height: inherit; padding-bottom: 0; ">
            <div id="search" style="position: absolute; right: 7px; top: 41px;">
                <!--   If Javascript is not enabled, nothing will display here -->
            </div> <!-- Search box -->
            <h1>Connectome In A Box</h1>

            <!--  <div style="border: 1px solid #e0e0e0; box-shadow: 1px 2px 9px #e0e0e0; background-color: #fff; margin-botton: 30px; padding: 15px; position: relative;"> -->
            <script>
                $(document).ready(function(){
                    $( "#tabs" ).tabs({
                        collapsible: true,
                        active: true,
                        show: { effect: "fadeIn", duration: 200 }
                        /*	heightStyle: "auto" */
                    });
                    $( "#tabs" ).show();

                    // show "coming soon" modal
                    // showModal('coming-soon');
                });

                function openTab(n) {
                    $("#tabs").tabs("option","active",n)
                }
            </script>
            <div id="tabs">
                <ul style="padding-left: 20px !important;">
                    <li><a href="#order_CINAB">Order Connectome In A Box</a></li>
                    <li><a href="#CINAB_Q2">What's New?</a></li>
                    <li><a href="#meg">What About MEG data?</a></li>
                </ul>

                <div id="order_CINAB" style="clear:both">
                    <a href="/data/" title="Order Connectome in a Box"><img src="/img/icon-HCP-data-drive-order.png" style="border: none; float:right; margin-left: 20px;" /></a>
                    <h2>Order Connectome in a Box</h2>
                    <p>The Human Connectome Project has gathered data on young, healthy adult brains at a scale and level of quality never before attempted. For example, we are scanning using accelerated multi-band fMRI techniques that can capture up to 1,200 frames in a single resting-state session.</p>
                    <p>The resulting dataset is, in a word, gargantuan.  Our currently released data is close to <strong>eighty-seven terabytes for 1100+ subjects</strong>; we are prepared to release data for up to 1,200 subjects before the project is complete. We are  committed to data sharing throughout the life of the project, which presents a  number of challenges: How can this much data be moved across the world to  anyone who wants it? Where will researchers store the data once they have it?</p>
                    <p>Connectome In A Box is our expedient solution for those that want large portions of the data: a series of hard drives prepopulated with open access image data made available by the HCP.  These hard drives can be ordered through the HCP and shipped anywhere in the world. </p>
                </div>
                <div id="CINAB_Q2" style="clear:both">
                    <a href="/data/" title="Order Connectome in a Box"><img src="/img/icon-HCP-data-drive-order.png" style="border: none; float:right; margin-left: 20px;" /></a>
                    <h2>What's New With The 1200 Subjects Release?</h2>
                    <p>There have been several updates of note in June 2017 for current users of Connectome In A Box. Here are a few key points: </p>
                    <ul>
                        <li>
                            Data distributed on the drives as .zip archives per subject/MRI modality/processing level, identical to packages available for download on ConnectomeDB
                        </li>
                        <li>
                            Hard drives will need to be mounted and packages unzipped to external storage, so have your IT support plan for having ~100 TB of local storage space to unzip the entire dataset; <a href="https://wiki.humanconnectome.org/display/PublicData/Connectome+In+A+Box+Notice+for+the+1200+Subjects+Data+Release" target="_blank" title="View note on HCP Wiki">see this note for Connectome In A Box purchasers</a> for details
                        </li>
                        <li>
                            Includes unprocessed and processed 3T image data for 1113 subjects
                        </li>
                        <li>
                            Includes newly reprocessed 3T diffusion data for all subjects with diffusion data
                        </li>
                        <li>
                            Image QC measures are now available on ConnectomeDB
                        </li>
                        <li>As with the 900 Subjects data, we are distributing data on 8-TB drives. </li>
                        <li><strong>IRB compliance guidance: </strong>All Connectome data administrators should read <a href="/data/cinab/connectome-in-a-box-notice.php"><strong>this notice on data usage and compliance</strong></a> with your institutions restrictions on human subject research </li>
                    </ul>
                    <p>This data release does NOT include the following:</p>
                    <ul>
                        <li>
                            MEG session data; these data are available for download on ConnectomeDB or for purchase on a separate hard drive; the contents of this drive are unchanged from the 900 Subjects data release
                        </li>
                        <li>
                            7T MR session data; we are taking preorders for 7T hard drives to be available at a later date (<a href="javascript:showModal('preorder')"><strong>How do preorders work?</strong></a>)
                        </li>
                        <li>
                            3T Retest data for 45 subjects who are part of the S1200 dataset; these can be downloaded from ConnectomeDB
                        </li>
                        <li>
                            Group data; Extensively processed group data is available on the S1200 project page, click "Open Dataset" in ConnectomeDB
                        </li>
                        <li>
                            Individual subject behavioral data; these can be downloaded as .csv spreadsheets from ConnectomeDB
                        </li>
                    </ul>
                </div>
                <div id="meg" style="clear:both; overflow: auto;">
                    <a href="/data/" title="Order Connectome in a Box"><img src="/img/icon-HCP-data-drive-order.png" style="border: none; float:right; margin-left: 20px;" /></a>
                    <h2>MEG Subjects Data Release: Now Available</h2>
                    <p>We have released a dataset of all MEG and MR data for the 95 subjects who have MEG data. This data is available for purchase now. </p>
                </div>
            </div>
        </div> <!-- content-wide -->
        <div id="content">
            <a name="more"></a>
            <div class="alertBox">
                <p><strong>IMPORTANT: </strong><br />
                    Before you begin to use HCP Data, please review the set of available <a href="https://www.humanconnectome.org/data/data-use-terms/index.html">HCP Data Use Terms</a>, and follow the steps to accept the terms that apply to your research.
                </p>
            </div>
            <h2>What is Connectome in a Box?</h2>
            <h3>When can I expect shipment? </h3>
            <p>We process orders in the order that they are received. Most standard orders will ship within 30 days of their initial order. However, a high order volume and low drive inventory may affect this turnaround time. We will display current inventory levels and order volume during the order process, prior to payment. </p>
            <h3>It's free, right? </h3>
            <p>Unfortunately, no. Researchers are required to pay for  the drive as well as shipping and processing costs.  The HCP makes no profit from the sales of Connectome  data.  We have worked to minimize the  cost to the research community and will adjust pricing as market rates drop for  high capacity drives.  Currently, the  total cost to investigators for <span style="text-decoration:underline">each drive of data</span> is approximately $200, including shipping costs. (Non-US customers can expect to pay more.) </p>
            <h3>What if I want to &quot;try before I buy&quot;? </h3>
            <p>We highly recommend interested HCP data users <a href="https://db.humanconnectome.org/">visit ConnectomeDB</a>&nbsp;and download packages of  data from one of our pre-selected subject groups. </p>
            <h3>What  formats does Connectome in a Box come in? Can I get it formatted for Mac?  Linux?</h3>
            <p>As of July 2013, we are only shipping drives formatted for Linux (EXT3). Mac and Windows users may be able to use workarounds, but these are not guaranteed to work on all versions of these operating systems. Please see this <a href="/data/cinab/connectome-in-a-box-format.php">updated note on drive formats</a> for more information. </p>
            <h3>How do I verify that the data on my drive is complete?</h3>
            <p>We strongly recommend verifying your data integrity using MD5 checksums that are included with each subject. </p>
            <p>Every package file, on the Connectome in a Box drive or in a downloaded set of subject data, contains a series of checksum strings in a series of JSON files. These files are located in a hidden directory named ".xdlm" within each subject folder. (Example: 100307/.xdlm) Each checksum can be checked with md5sum to verify that the contents are the same in your version of the files as they were when they were created.</p>
            <a name="recycle"></a>
            <h3>What if I get a bad hard drive? Can I return it? </h3>
            <p>Prior to shipping, the accuracy and completeness of  the data on each drive is digitally verified.   However, it’s possible that a drive may become defective after  shipping.  If your drive stops working  within 30 days of receipt, you may ship it back for a replacement at no additional  cost.  Please contact <a href="mailto:orders@humanconnectome.org">orders@humanconnectome.org</a> for  more information.</p>
            <h3>How does the HCP create the drives?</h3>
            <p>The HCP informatics team has assembled a high throughput drive duplicator, referred to by its builders as the &ldquo;duplicatinator.&rdquo; Once the copies are completed, we use a checksum to verify data integrity. </p>
            <h3>How do I order? </h3>
            <p>Go to <a href="https://store.humanconnectome.org/"><strong>https://store.humanconnectome.org/</strong></a> to get started. </p>

            <!-- MODALS ---->
            <div class="modal" id="coming-soon" style="min-height:0px !important;">
                <div class="modal-title">Connectome Data Order Form Coming Soon <span class="modal-close" onclick="modalClose()"><img src="/img/close-dark.png" /></span></div>
                <div class="modal_content">
                    <h1><img src="/img/icon-HCP-data.png" align="right" style="margin:0 0 0 30px" />Coming Soon!</h1>
                    <p>Thank you for your interest in ordering HCP Data. The storefront will be relaunched soon to accept orders for Q3 data, as well as reprocessed data from Q1 and Q2. In the meantime, feel free to download individual packages of data from <a href="https://db.humanconnectome.org/">ConnectomeDB</a>. </p>
                    <p><strong>Current users:</strong> There will be a patch available shortly on ConnectomeDB to update Q1 and Q2 data.</p>
                    <p>If you would like to receive an update when the storefront is ready for orders, please sign up to our <a href="https://www.humanconnectome.org/contact/#subscribe">HCP Announce mailing list</a>.</p>
                </div>
            </div> <!-- end "coming soon" modal -->

            <div class="modal" id="preorder">
                <div class="modal-title">HCP Data Preorders <span class="modal-close" onclick="modalClose()"><img src="/img/close-dark.png" /></span></div>
                <div class="modal_content">
                    <h1><img src="/img/icon-HCP-data.png" align="right" style="margin:0 0 0 30px" />How Do Preorders Work?</h1>
                    <p>The diversity and amount of data that the Human Connectome Project can distribute has grown, but our bottleneck is in the production of verified hard drives for each data release. For data that we plan to release shortly, we are allowing customers to place a pre-order. This preorder allows you to get first priority for delivery, and allows us to gauge the level of interest in each upcoming release. </p>
                    <p>Your preorder requires an initial deposit of $100.00, so that we can separate passive interest from active buying interest. This deposit will be fully applied to the eventual purchase cost, and is fully refundable at any time if you change your mind about ordering.</p>
                    <p>When you complete your preorder, you will receive a coupon code via email. When the data you ordered has been released, you will receive a second email with a custom link to this storefront with your coupon code included. By using this coupon code, your initial deposit will be automatically applied to the cost of the data release.</p>
                    <p>If you have any questions about this process, please contact us at <a href="mailto:info@humanconnectome.org">info@humanconnectome.org</a>.</p>
                    <p><button onclick="javascript:modalClose()">Okay Thanks</button> </p>
                </div>
            </div> <!-- end "linux format" modal -->


        </div>

        <!-- Primary page Content -->

        <div id="sidebar-rt">

            <div id="sidebar-content"><!-- #BeginLibraryItem "/Library/Sidebar - Explore ConnectomeDB.lbi" --><div class="sidebox">
                    <h2 align="center"><a href="https://db.humanconnectome.org">"Let me explore the dataset"</a></h2>
                    <p><a href="https://db.humanconnectome.org" target="_blank"><button class="big-button" title="Download from ConnectomeDB">
                                <img src="/img/button-connectomedb-225px.png" />
                            </button></a></p>
                    <p>All HCP data can be downloaded from ConnectomeDB, including&nbsp;a  variety of convenient packaged downloads for groups of unrelated subjects. You can explore the full protocol before downloading with our online data archive.</p>
                    <h3><a href="https://db.humanconnectome.org" target="_blank">Log in to ConnectomeDB</a></h3>
                </div><!-- #EndLibraryItem --><p>&nbsp;</p>
            </div>

            <!-- editable sidebar content region -->
        </div> <!-- sidebar -->

    </div> <!-- middle -->

    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/footer.php'); ?>

</div>
<!-- end container -->

<script src="https://www.google.com/jsapi?key=AIzaSyA5m1Nc8ws2BbmPRwKu5gFradvD_hgq6G0" type="text/javascript"></script>

<script type="text/javascript">
    /*
     *  How to restrict a search to a Custom Search Engine
     *  http://www.google.com/cse/
     */

    google.load('search', '1');

    function OnLoad() {
        // Create a search control
        var searchControl = new google.search.SearchControl();

        // Add in a WebSearch
        var webSearch = new google.search.WebSearch();

        // Restrict our search to pages from our CSE
        webSearch.setSiteRestriction('000073750279216385221:vnupk6whx-c');

        // Add the searcher to the SearchControl
        searchControl.addSearcher(webSearch);

        // tell the searcher to draw itself and tell it where to attach
        searchControl.draw(document.getElementById("search"));

        // execute an inital search
        // searchControl.execute('');
    }

    google.setOnLoadCallback(OnLoad);

</script>


<!-- google analytics -->
<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-18630139-1']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

</script>
</body>
</html>
