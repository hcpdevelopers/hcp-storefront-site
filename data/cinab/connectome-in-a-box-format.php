<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Connectome In A Box Note on Drive Formats | Human Connectome Project</title>
    <meta http-equiv="Description" content="Connectome In A Box is a way to get ALL the data. We will format a hard drive and send it anywhere in the world." />
    <meta http-equiv="Keywords" content="Connectome Data, Connectivity data, human brain mapping, Human Connectome Project" />
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/header-includes.php'); ?>
</head>

<body>

<!-- site container -->
<div id="site_container">

    <div id="NIH_header2">
        <span><a href="http://www.neuroscienceblueprint.nih.gov/connectome/" title="NIH Human Connectome Project" style="color:#b4cfff; text-decoration:none;">NIH Blueprint: The Human Connectome Project</a></span>
    </div>

    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/storefront-topnav.php'); ?>

    <!-- Breadcrumbs -->
    <div id="breadcrumbs">
        <p>&raquo; <a href="/data/">Get HCP Data</a> &gt; <a href="/data/cinab/connectome-in-a-box.php">Connectome In A Box</a> &gt; Formats</p></div>
    <!-- end breadcrumbs -->

    <!-- "Middle" area contains all content between the top navigation and the footer -->
    <div id="middle"><!-- content-wide -->
        <div id="content">
            <h1>Why are Connectome in a Box drives only available in Linux?</h1>
            <p>Working with datasets that span multiple hard drives demands more complex thinking about logistics, for our users dealing with storage and access as well as for our own ability to fulfill orders. We have made a decision to distribute our full datasets as Linux EXT3-formatted drives. This is the common standard for large-scale storage solutions, and has the best chance of long-term usability. </p>
            <p>The primary purpose of these drives is to serve as a method of transport, where an ideal outcome would be that your local IT department loads the data from these drives onto an enterprise redundant storage solution. However, we recognize that not all researchers have access to these resources, and will need workarounds. </p>
            <p><strong>Simply put, we cannot guarantee full read-write access to Connectome In A Box hard drives for non-Linux users. </strong></p>
            <h2>Advice for Mac users?</h2>
            <p><a href="http://www.cnet.com/how-to/how-to-manage-ext2ext3-disks-in-os-x/">This CNET article</a> describes three possible approaches to mounting a Linux drive on Mac OS. However, it should be noted that as of the release of Mac OSX 10.10 (Yosemite), these methods are becoming increasingly fragile and should not be depended on.</p>
            <p>There is also <a href="http://www.paragon-software.com/home/extfs-mac/" target="_blank">a commercial software package by Paragon Software</a> that professes to allow Mac OSX to read Linux drives natively. However, in our limited testing on Mac OSX 10.10.2, the software (version 9.6) did not work.</p>
            <p><strong>HCP Wiki: <a href="https://wiki.humanconnectome.org/display/PublicData/How+to+mount+a+Connectome+In+A+Box+Hard+Drive+in+Mac+OSX">How to mount a Connectome hard drive in Mac OS</a> </strong></p>
            <h2>Advice for Windows users?</h2>
            <p>Setting up a virtual machine that runs Linux Ubuntu appears to work fairly reliably in getting read-only access to data. <a href="http://www.instructables.com/id/Introduction-38/" target="_blank" title="How to install a Linux virtual machine on Windows 7">This tutorial</a> is a good place to start. Please note that you may also have to install USB drivers if you are using an enclosure to mount your drive.</p>
            <p>There is also free 3rd-party software that allows <a href="http://sourceforge.net/projects/ext2fsd/" title="Download Ext2Fsd v0.51" target="_blank">Windows users to mount and get data from Linux EXT3-formatted drives</a>. We have tested this implementation on Windows 7 and it appears to work.</a>
                (Note: download v0.51, and be sure to &quot;enable writing support for ext3 volumes&quot; when installing.)</p>
            <p><strong>HCP Wiki: <a href="https://wiki.humanconnectome.org/display/PublicData/How+to+Mount+a+Connectome+In+A+Box+drive+in+Windows">How to mount a Connectome hard drive in Windows</a></strong></p>
        </div>

        <!-- Primary page Content -->

        <div id="sidebar-rt">

            <div id="sidebar-content">
                <div class="sidebox databox">
                    <h2>Sign up for Open Access Data</h2>
                    <p><img src="/img/icon-signature.png" width="256" height="205" alt="Sign terms" style="margin-left:-10px" /></p>
                    <p> Create a free account at <strong><a href="https://db.humanconnectome.org">db.humanconnectome.org</a></strong></p>
                    <p style="font-size:11px !important">Terms updated Apr 26, 2013. <br />
                        <a href="https://www.humanconnectome.org/data/data-use-terms/open-access.html">View online</a> | <a href="https://www.humanconnectome.org/data/data-use-terms/DataUseTerms-HCP-Open-Access-26Apr2013.pdf">Download PDF</a></p>
                </div>
                <div class="sidebox">
                    <h4>Questions?</h4>
                    <p>Questions about working with Connectome Data? Consider asking them in our HCP Data Users email list. </p>
                    <p>&nbsp;</p>
                    <p><strong><a href="https://www.humanconnectome.org/contact/#subscribe">HCP Mailing list signup form</a></strong></p>
                </div>
            </div>

        </div> <!-- sidebar -->

    </div> <!-- middle -->

    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/footer.php'); ?>

</div>
<!-- end container -->

<script src="https://www.google.com/jsapi?key=AIzaSyA5m1Nc8ws2BbmPRwKu5gFradvD_hgq6G0" type="text/javascript"></script>

<script type="text/javascript">
    /*
     *  How to restrict a search to a Custom Search Engine
     *  http://www.google.com/cse/
     */

    google.load('search', '1');

    function OnLoad() {
        // Create a search control
        var searchControl = new google.search.SearchControl();

        // Add in a WebSearch
        var webSearch = new google.search.WebSearch();

        // Restrict our search to pages from our CSE
        webSearch.setSiteRestriction('000073750279216385221:vnupk6whx-c');

        // Add the searcher to the SearchControl
        searchControl.addSearcher(webSearch);

        // tell the searcher to draw itself and tell it where to attach
        searchControl.draw(document.getElementById("search"));

        // execute an inital search
        // searchControl.execute('');
    }

    google.setOnLoadCallback(OnLoad);

</script>


<!-- google analytics -->
<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-18630139-1']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

</script>
</body>
</html>
