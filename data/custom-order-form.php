<?php
if ($_SERVER['HTTP_HOST'] == 'dev.humanconnectome.org') :
// nothing 
else :
    $auth_realm = 'Custom Order Form';
    require_once 'auth.php';
endif;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Custom Data Order Form | Admin Only | Human Connectome Project</title>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/header-includes.php'); ?>

</head>

<body>
<!-- site container -->
<div id="site_container">

    <div id="NIH_header2">
<span>
<a href="http://www.neuroscienceblueprint.nih.gov/connectome/" title="NIH Human Connectome Project" style="color:#b4cfff; text-decoration:none;">NIH Blueprint: The Human Connectome Project</a></span>
    </div>

    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/storefront-topnav.php'); ?>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/db-login.php'); ?>

    <!-- Breadcrumbs -->
    <div id="breadcrumbs">
        <p>&raquo; home</p></div>
    <!-- end breadcrumbs -->

    <!-- "Middle" area contains all content between the top navigation and the footer -->
    <div id="middle">
        <div id="content-wide" class="editable">

            <h1>Custom Order Form</h1>
            <p>Access to this form is only allowable by administrative users. Do not misuse.</p>

            <form class="hcpForm" action="/data/confirm.php" method="post">

                <h3>Customer Shipping Info</h3>
                <div class="hcpForm-wrapper" id="shipping-info">
                    <div class="hidden error-messages" id="error-message-address">
                        <p><strong>Errors Found:</strong></p>
                        <ul class="error-list">
                            <li>You're crazy.</li>
                        </ul>
                    </div>

                    <div>
                        <p><input type="checkbox" name="billing" value="yes" checked="checked" onclick="javascript:toggleBillingAddress(this)" /> Use this same address for billing. <span style="color:red
                    ; font-weight: bold; display: none" id="billing_address_warning"><br />You will be prompted to fill out your billing address on the next page.</span></p>
                    </div>
                    <div>
                        <label for="HCP-COUNTRY">Country</label>
                        <select name="HCP-COUNTRY" id="country-select">
                            <option value=""></option>
                            <option value="United States">United States</option>
                            <option value="Afghanistan">Afghanistan</option>
                            <option value="Albania">Albania</option>
                            <option value="Algeria">Algeria</option>
                            <option value="American Samoa">American Samoa</option>
                            <option value="Andorra">Andorra</option>
                            <option value="Anguilla">Anguilla</option>
                            <option value="Antarctica">Antarctica</option>
                            <option value="Antigua And Barbuda">Antigua And Barbuda</option>
                            <option value="Argentina">Argentina</option>
                            <option value="Armenia">Armenia</option>
                            <option value="Aruba">Aruba</option>
                            <option value="Australia">Australia</option>
                            <option value="Austria">Austria</option>
                            <option value="Azerbaijan">Azerbaijan</option>
                            <option value="Bahamas">Bahamas</option>
                            <option value="Bahrain">Bahrain</option>
                            <option value="Bangladesh">Bangladesh</option>
                            <option value="Barbados">Barbados</option>
                            <option value="Belarus">Belarus</option>
                            <option value="Belgium">Belgium</option>
                            <option value="Belize">Belize</option>
                            <option value="Benin">Benin</option>
                            <option value="Bermuda">Bermuda</option>
                            <option value="Bhutan">Bhutan</option>
                            <option value="Bolivia">Bolivia</option>
                            <option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
                            <option value="Botswana">Botswana</option>
                            <option value="Bouvet Island">Bouvet Island</option>
                            <option value="Brazil">Brazil</option>
                            <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
                            <option value="Brunei Darussalam">Brunei Darussalam</option>
                            <option value="Bulgaria">Bulgaria</option>
                            <option value="Burkina Faso">Burkina Faso</option>
                            <option value="Burundi">Burundi</option>
                            <option value="Cambodia">Cambodia</option>
                            <option value="Cameroon">Cameroon</option>
                            <option value="Canada">Canada</option>
                            <option value="Cape Verde">Cape Verde</option>
                            <option value="Cayman Islands">Cayman Islands</option>
                            <option value="Central African Republic">Central African Republic</option>
                            <option value="Chad">Chad</option>
                            <option value="Chile">Chile</option>
                            <option value="China">China</option>
                            <option value="Christmas Island">Christmas Island</option>
                            <option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
                            <option value="Colombia">Colombia</option>
                            <option value="Comoros">Comoros</option>
                            <option value="Congo">Congo</option>
                            <option value="Congo, the Democratic Republic of the">Congo, the Democratic Republic of the</option>
                            <option value="Cook Islands">Cook Islands</option>
                            <option value="Costa Rica">Costa Rica</option>
                            <option value="Cote d'Ivoire">Cote d'Ivoire</option>
                            <option value="Croatia">Croatia</option>
                            <option value="Cyprus">Cyprus</option>
                            <option value="Czech Republic">Czech Republic</option>
                            <option value="Denmark">Denmark</option>
                            <option value="Djibouti">Djibouti</option>
                            <option value="Dominica">Dominica</option>
                            <option value="Dominican Republic">Dominican Republic</option>
                            <option value="East Timor">East Timor</option>
                            <option value="Ecuador">Ecuador</option>
                            <option value="Egypt">Egypt</option>
                            <option value="El Salvador">El Salvador</option>
                            <option value="England">England</option>
                            <option value="Equatorial Guinea">Equatorial Guinea</option>
                            <option value="Eritrea">Eritrea</option>
                            <option value="Espana">Espana</option>
                            <option value="Estonia">Estonia</option>
                            <option value="Ethiopia">Ethiopia</option>
                            <option value="Falkland Islands">Falkland Islands</option>
                            <option value="Faroe Islands">Faroe Islands</option>
                            <option value="Fiji">Fiji</option>
                            <option value="Finland">Finland</option>
                            <option value="France">France</option>
                            <option value="French Guiana">French Guiana</option>
                            <option value="French Polynesia">French Polynesia</option>
                            <option value="French Southern Territories">French Southern Territories</option>
                            <option value="Gabon">Gabon</option>
                            <option value="Gambia">Gambia</option>
                            <option value="Georgia">Georgia</option>
                            <option value="Germany">Germany</option>
                            <option value="Ghana">Ghana</option>
                            <option value="Gibraltar">Gibraltar</option>
                            <option value="Great Britain">Great Britain</option>
                            <option value="Greece">Greece</option>
                            <option value="Greenland">Greenland</option>
                            <option value="Grenada">Grenada</option>
                            <option value="Guadeloupe">Guadeloupe</option>
                            <option value="Guam">Guam</option>
                            <option value="Guatemala">Guatemala</option>
                            <option value="Guinea">Guinea</option>
                            <option value="Guinea-Bissau">Guinea-Bissau</option>
                            <option value="Guyana">Guyana</option>
                            <option value="Haiti">Haiti</option>
                            <option value="Heard and Mc Donald Islands">Heard and Mc Donald Islands</option>
                            <option value="Honduras">Honduras</option>
                            <option value="Hong Kong">Hong Kong</option>
                            <option value="Hungary">Hungary</option>
                            <option value="Iceland">Iceland</option>
                            <option value="India">India</option>
                            <option value="Indonesia">Indonesia</option>
                            <option value="IRAN">IRAN</option>
                            <option value="Iraq">Iraq</option>
                            <option value="Ireland">Ireland</option>
                            <option value="Israel">Israel</option>
                            <option value="Italy">Italy</option>
                            <option value="Jamaica">Jamaica</option>
                            <option value="Japan">Japan</option>
                            <option value="Jordan">Jordan</option>
                            <option value="Kazakhstan">Kazakhstan</option>
                            <option value="Kenya">Kenya</option>
                            <option value="Kiribati">Kiribati</option>
                            <option value="KOREA, DEMOCRATIC PEOPLE'S REPUBLIC OF">KOREA, DEMOCRATIC PEOPLE'S REPUBLIC OF</option>
                            <option value="Korea, Republic of">Korea, Republic of</option>
                            <option value="Kuwait">Kuwait</option>
                            <option value="Kyrgyzstan">Kyrgyzstan</option>
                            <option value="Lao People's Democratic Republic">Lao People's Democratic Republic</option>
                            <option value="Latvia">Latvia</option>
                            <option value="Lebanon">Lebanon</option>
                            <option value="Lesotho">Lesotho</option>
                            <option value="Liberia">Liberia</option>
                            <option value="Libya">Libya</option>
                            <option value="Liechtenstein">Liechtenstein</option>
                            <option value="Lithuania">Lithuania</option>
                            <option value="Luxembourg">Luxembourg</option>
                            <option value="MACAO">MACAO</option>
                            <option value="Macedonia">Macedonia</option>
                            <option value="Madagascar">Madagascar</option>
                            <option value="Malawi">Malawi</option>
                            <option value="Malaysia">Malaysia</option>
                            <option value="Maldives">Maldives</option>
                            <option value="Mali">Mali</option>
                            <option value="Malta">Malta</option>
                            <option value="Marshall Islands">Marshall Islands</option>
                            <option value="Martinique">Martinique</option>
                            <option value="Mauritania">Mauritania</option>
                            <option value="Mauritius">Mauritius</option>
                            <option value="Mayotte">Mayotte</option>
                            <option value="Mexico">Mexico</option>
                            <option value="Micronesia, Federated States of">Micronesia, Federated States of</option>
                            <option value="Moldova, Republic of">Moldova, Republic of</option>
                            <option value="Monaco">Monaco</option>
                            <option value="Mongolia">Mongolia</option>
                            <option value="Montserrat">Montserrat</option>
                            <option value="Morocco">Morocco</option>
                            <option value="Mozambique">Mozambique</option>
                            <option value="Myanmar">Myanmar</option>
                            <option value="Namibia">Namibia</option>
                            <option value="Nauru">Nauru</option>
                            <option value="Nepal">Nepal</option>
                            <option value="Netherlands">Netherlands</option>
                            <option value="Netherlands Antilles">Netherlands Antilles</option>
                            <option value="New Caledonia">New Caledonia</option>
                            <option value="New Zealand">New Zealand</option>
                            <option value="Nicaragua">Nicaragua</option>
                            <option value="Niger">Niger</option>
                            <option value="Nigeria">Nigeria</option>
                            <option value="Niue">Niue</option>
                            <option value="Norfolk Island">Norfolk Island</option>
                            <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                            <option value="Norway">Norway</option>
                            <option value="Oman">Oman</option>
                            <option value="Pakistan">Pakistan</option>
                            <option value="Palau">Palau</option>
                            <option value="Panama">Panama</option>
                            <option value="Papua New Guinea">Papua New Guinea</option>
                            <option value="Paraguay">Paraguay</option>
                            <option value="Peru">Peru</option>
                            <option value="Philippines">Philippines</option>
                            <option value="Pitcairn">Pitcairn</option>
                            <option value="Poland">Poland</option>
                            <option value="Portugal">Portugal</option>
                            <option value="Puerto Rico">Puerto Rico</option>
                            <option value="Qatar">Qatar</option>
                            <option value="Reunion">Reunion</option>
                            <option value="Romania">Romania</option>
                            <option value="Russia">Russia</option>
                            <option value="Russian Federation">Russian Federation</option>
                            <option value="Rwanda">Rwanda</option>
                            <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                            <option value="Saint Lucia">Saint Lucia</option>
                            <option value="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option>
                            <option value="Samoa (Independent)">Samoa (Independent)</option>
                            <option value="San Marino">San Marino</option>
                            <option value="Sao Tome and Principe">Sao Tome and Principe</option>
                            <option value="Saudi Arabia">Saudi Arabia</option>
                            <option value="Scotland">Scotland</option>
                            <option value="Senegal">Senegal</option>
                            <option value="Serbia and Montenegro">Serbia and Montenegro</option>
                            <option value="Seychelles">Seychelles</option>
                            <option value="Sierra Leone">Sierra Leone</option>
                            <option value="Singapore">Singapore</option>
                            <option value="Slovakia">Slovakia</option>
                            <option value="Slovenia">Slovenia</option>
                            <option value="Solomon Islands">Solomon Islands</option>
                            <option value="Somalia">Somalia</option>
                            <option value="South Africa">South Africa</option>
                            <option value="South Georgia and the South Sandwich Islands">South Georgia and the South Sandwich Islands</option>
                            <option value="Spain">Spain</option>
                            <option value="Sri Lanka">Sri Lanka</option>
                            <option value="St. Helena">St. Helena</option>
                            <option value="St. Pierre and Miquelon">St. Pierre and Miquelon</option>
                            <option value="Suriname">Suriname</option>
                            <option value="Svalbard and Jan Mayen Islands">Svalbard and Jan Mayen Islands</option>
                            <option value="Swaziland">Swaziland</option>
                            <option value="Sweden">Sweden</option>
                            <option value="Switzerland">Switzerland</option>
                            <option value="Taiwan">Taiwan</option>
                            <option value="Tajikistan">Tajikistan</option>
                            <option value="Tanzania">Tanzania</option>
                            <option value="Thailand">Thailand</option>
                            <option value="Togo">Togo</option>
                            <option value="Tokelau">Tokelau</option>
                            <option value="Tonga">Tonga</option>
                            <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                            <option value="Tunisia">Tunisia</option>
                            <option value="Turkey">Turkey</option>
                            <option value="Turkmenistan">Turkmenistan</option>
                            <option value="Turks and Caicos Islands">Turks and Caicos Islands</option>
                            <option value="Tuvalu">Tuvalu</option>
                            <option value="Uganda">Uganda</option>
                            <option value="Ukraine">Ukraine</option>
                            <option value="United Arab Emirates">United Arab Emirates</option>
                            <option value="United Kingdom">United Kingdom</option>
                            <option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
                            <option value="Uruguay">Uruguay</option>
                            <option value="Uzbekistan">Uzbekistan</option>
                            <option value="Vanuatu">Vanuatu</option>
                            <option value="Vatican City State (Holy See)">Vatican City State (Holy See)</option>
                            <option value="Venezuela">Venezuela</option>
                            <option value="Viet Nam">Viet Nam</option>
                            <option value="Virgin Islands (British)">Virgin Islands (British)</option>
                            <option value="Virgin Islands (U.S.)">Virgin Islands (U.S.)</option>
                            <option value="Wales">Wales</option>
                            <option value="Wallis and Futuna Islands">Wallis and Futuna Islands</option>
                            <option value="Western Sahara">Western Sahara</option>
                            <option value="Yemen">Yemen</option>
                            <option value="Zambia">Zambia</option>
                            <option value="Zimbabwe">Zimbabwe</option>
                        </select>
                    </div>

                    <div>
                        <label for="customer_name">Name</label>
                        <input type="text" name="customer_name" style="width:485px" class="required">
                    </div>
                    <div>
                        <label for="customer_institution">Institution</label>
                        <input type="text" name="customer_institution" style="width:485px" >
                    </div>
                    <div>
                        <label for="customer_email">Email Address</label>
                        <input type="text" name="customer_email" style="width:485px" class="required email" />
                    </div>
                    <div>
                        <label for="shipping_address">Street Address</label>
                        <input type="text" name="shipping_address" style="width:485px" class="required" />
                    </div>
                    <div>
                        <label for="shipping_address_2">Street Address 2</label>
                        <textarea name="shipping_address_2" style="width:485px" rows="3"></textarea>
                    </div>
                    <div>
                        <label for="shipping_city">City</label>
                        <input type="text" name="shipping_city" style="width:290px" class="required" />
                    </div>
                    <div class="state-US">
                        <label for="shipping_state">State</label>
                        <select name="shipping_state" class="required">
                            <option selected="selected"></option>
                            <option value="AL">Alabama</option>
                            <option value="AK">Alaska</option>
                            <option value="AZ">Arizona</option>
                            <option value="AR">Arkansas</option>
                            <option value="CA">California</option>
                            <option value="CO">Colorado</option>
                            <option value="CT">Connecticut</option>
                            <option value="DC">District Of Columbia</option>
                            <option value="DE">Delaware</option>
                            <option value="FL">Florida</option>
                            <option value="GA">Georgia</option>
                            <option value="HI">Hawaii</option>
                            <option value="ID">Idaho</option>
                            <option value="IL">Illinois</option>
                            <option value="IN">Indiana</option>
                            <option value="IA">Iowa</option>
                            <option value="KS">Kansas</option>
                            <option value="KY">Kentucky</option>
                            <option value="LA">Louisiana</option>
                            <option value="ME">Maine</option>
                            <option value="MD">Maryland</option>
                            <option value="MA">Massachusetts</option>
                            <option value="MI">Michigan</option>
                            <option value="MN">Minnesota</option>
                            <option value="MS">Mississippi</option>
                            <option value="MO">Missouri</option>
                            <option value="MT">Montana</option>
                            <option value="NE">Nebraska</option>
                            <option value="NV">Nevada</option>
                            <option value="NH">New Hampshire</option>
                            <option value="NJ">New Jersey</option>
                            <option value="NM">New Mexico</option>
                            <option value="NY">New York</option>
                            <option value="NC">North Carolina</option>
                            <option value="ND">North Dakota</option>
                            <option value="OH">Ohio</option>
                            <option value="OK">Oklahoma</option>
                            <option value="OR">Oregon</option>
                            <option value="PA">Pennsylvania</option>
                            <option value="RI">Rhode Island</option>
                            <option value="SC">South Carolina</option>
                            <option value="SD">South Dakota</option>
                            <option value="TN">Tennessee</option>
                            <option value="TX">Texas</option>
                            <option value="UT">Utah</option>
                            <option value="VT">Vermont</option>
                            <option value="VA">Virginia</option>
                            <option value="WA">Washington</option>
                            <option value="WV">West Virginia</option>
                            <option value="WI">Wisconsin</option>
                            <option value="WY">Wyoming</option>
                        </select>

                    </div>
                    <div id="state-int" class="hidden">
                        <label for="shipping_state">State / Province / Region</label>
                        <input type="text" name="shipping_state" style="width:150px" disabled="disabled" />
                    </div>
                    <div>
                        <label for="shipping_postal_code">ZIP/Postal Code</label>
                        <input type="text" name="shipping_postal_code" style="width:90px" class="required" />
                    </div>
                    <div>
                        <label for="shipping_phone">Phone Number (required for FedEx shipping)</label>
                        <input type="text" name="shipping_phone" style="width:485px" class="required" />
                    </div>
                </div>

                <h1>Product Order Form</h1>
                <script>
                    $(document).ready(function(){

                        // every time a user enters a quantity on the order form...
                        $('.qty').on('keyup',function(){ calcAmount(); });
                        $('.itemprice').on('keyup',function(){ calcAmount(); });

                        $('#country-select').on('change',function(){
                            // every time a user sets their country ...
                            var cnt=$(this).val();
                            if (cnt != 'United States') {

                                // change address form field
                                $('#state-US').addClass('hidden')
                                    .find('select').prop('disabled',true).removeClass('required');
                                $('#state-int').removeClass('hidden')
                                    .find('input').removeProp('disabled');

                                // hide sales tax warning
                                $('#salestax').not('hidden').addClass('hidden');

                            } else {

                                // change address form field and reset state selection
                                $('#state-US').removeClass('hidden')
                                    .find('select').prop('disabled',false).addClass('required')
                                    .find('option:selected').prop('selected',false);
                                $('#state-int').addClass('hidden')
                                    .find('input').prop('disabled','disabled');

                            }
                        });
                    });

                    function calcAmount(){
                        var qty=0, itemcnt=0, amount=0, driveQty=0;
                        // recalculate total item count and drive count
                        $('.release').each(function(){
                            var thisQty=Number($(this).find('.qty').val());
                            var theseDrives=Number($(this).find('.drive-qty').val());
                            driveQty+= thisQty * theseDrives;
                            itemcnt+= thisQty;

                            // recalculate amount to be charged, and whether to charge. Activate itemcode.
                            var amt=$(this).find('.itemamount');
                            var itemcode = $(this).find('.itemcode');
                            var itemid = $(this).find('.itemid');

                            if (thisQty>0) {
                                // amount
                                $(amt).prop('disabled',false);
                                var price= Number($(this).find('.itemprice').val());
                                $(amt).val(price * thisQty);

                                // itemcode
                                $(itemcode).prop('disabled',false);

                                // item ID
                                $(itemid).prop('disabled',false);
                            } else {
                                $(amt).prop('disabled','disabled').val('');
                                $(itemcode).prop('disabled','disabled');
                                $(itemid).prop('disabled','disabled');
                            }
                        });
                        $('#drivecnt').val(driveQty);
                        $('#itemcnt').val(itemcnt);
                    }

                    function toggleBillingAddress(el) {
                        $('#billing_address_warning').toggle();
                        if ($(el).prop("checked")) {
                            // user intends to use same address for shipping and billing; disable billing address form fields
                            $('#billing-info').find('input').each(function(){
                                $(this).prop("disabled","disabled");
                            });
                            $('#billing-info').find('textarea').prop("disabled","disabled");
                            $('#billing-info').find('select').prop("disabled","disabled");
                            $('#billing-info').addClass('hidden');
                        } else {
                            // user intends to use different addresses for shipping and billing; enable billing address form fields
                            $('#billing-info').find('input').each(function(){
                                $(this).prop("disabled",false);
                            });
                            $('#billing-info').find('textarea').prop("disabled",false);
                            $('#billing-info').find('select').prop("disabled",false);
                            $('#billing-info').removeClass('hidden');
                        }
                    }
                </script>
                <input type="hidden" name="itemcnt" id="itemcnt" value="0" />
                <input type="hidden" name="drivecnt" id="drivecnt" value="0" />
                <h2>Individual Drives</h2>
                <p><em>Note: All data is shipped on Linux-formatted drives.</em></p>
                <?php
                if ($_SESSION['username'] == 'maiello@sdn-napoli.it') :
                    $releases = mysqli_query($db,'SELECT * FROM releases WHERE release_id="HCP-UR100"');
                else :
                    $releases = mysqli_query($db,'SELECT * FROM releases WHERE status="current" ORDER BY release_date DESC;');
                endif;
                $orderLimit = 1; // could be set in DB if desired
                while ($release = mysqli_fetch_array($releases)) :
                    $cost = ($_SESSION['username'] == 'maiello@sdn-napoli.it') ? 0 : $release['cost'];
                    ?>

                    <div style="border: 1px solid #ccc; border-radius: 3px; margin-bottom: 1em; padding: 10px;" class="release" id="release-<?php echo $release['release_id']; ?>">
                        <div style="background-color: #f0f0f0; float:right; margin-left: 30px; padding: 10px; width: 25%;">
                            <label>Custom Price per unit?</label>
                            <input type="number" name="itemprice[]" class="itemprice" value="<?php echo $cost; ?>" />
                        </div>
                        <div style="background-color: #f0f0f0; float:right; margin-left: 30px; padding: 10px; width: 25%;">
                            <label>Quantity Ordered:</label>
                            <input type="text" style="width: 60px; font-size: 16px;" class="qty" name="<?php echo $release['release_id'].'qty'; ?>" value="0" />
                        </div>
                        <input type="hidden" name="drive-qty[]" class="drive-qty" value="1" />
                        <input type="hidden" name="amount[]" value="" style="width:60px;" class="itemamount item-info" disabled="disabled" />
                        <input type="hidden" name="itemcode[]" value="<?php echo $release['release_id']; ?>" class="itemcode item-info" disabled="disabled" />
                        <input type="hidden" name="drive-format[]" value="Linux" />
                        <h3><?php echo $release['title'] ?> <span style="font-size: 11px; font-weight:normal; padding-left:10px;">Release date: <?php echo $release['release_date']; ?></span></h3>
                        <?php echo $release['description']; ?>
                        <?php
                        $drive_type = $release['drive_type'];
                        ?>
                        <p>Cost: <?php echo '$'.$cost.', plus shipping.'; ?></p>
                    </div>
                    <?php
                    $drives = explode(",",$release['contains_drives']);
                    if (count($drives) > 1) :
                        ?>
                        <div style="padding-left: 30px;"> <!-- inset custom drives -->
                            <h3>Custom Drive Orders: Select Single Drive</h3>
                            <?php
                            foreach ($drives as $key => $drive):
                                ?>
                                <div style="border: 1px solid #ccc; border-radius: 3px; margin-bottom: 1em; padding: 10px;" class="release">
                                    <div style="background-color: #f0f0f0; float:right; margin-left: 30px; padding: 10px; width: 25%;">
                                        <label>Custom Price per unit?</label>
                                        <input type="number" name="itemprice[]" class="itemprice" value="<?php echo $release['cost']/count($drives); ?>" />
                                    </div>
                                    <div style="background-color: #f0f0f0; float:right; margin-left: 30px; padding: 10px; width: 25%;">
                                        <label>Quantity Ordered:</label>
                                        <input type="text" style="width: 60px; font-size: 16px;" class="qty" name="HCP-CUSTOMqty" value="0" />
                                    </div>
                                    <input type="hidden" name="drive-qty[]" class="drive-qty" value="1" />
                                    <input type="hidden" name="amount[]" value="" style="width:60px;" class="itemamount item-info" disabled="disabled" />
                                    <input type="hidden" name="itemcode[]" value="HCP-CUSTOM" class="itemcode item-info" disabled="disabled" />
                                    <input type="hidden" name="custom-drive-id[]" value="<?php echo $drive ?>" class="itemid item-info" disabled="disabled" />
                                    <input type="hidden" name="drive-format[]" value="Linux" />
                                    <h3><?php echo "Drive ID: ".$drive ?><br>
                                        <span style="font-size: 11px; font-weight:normal;">Release: <?php  echo $release['title'] ?><br>
                                            Release date: <?php echo $release['release_date']; ?></span></h3>
                                    <?php echo $release['description']; ?>
                                    <?php
                                    $drive_type = $release['drive_type'];
                                    ?>
                                    <p>Cost: <?php echo '$'. $release['cost']/count($drives); ?></p>
                                </div>
                            <?php
                            endforeach;
                            ?>
                        </div><!-- end inset div -->
                    <?php
                    endif;
                endwhile;
                ?>

                <?php if ($_SESSION['username'] == 'adminUser') : ?>
                    <h1>Pricing Options</h1>
                    <div class="hidden" id="salestax" style="border-bottom: 3px #f1f1f1 solid; margin-bottom:18px; padding-bottom:6px;">
                        <h3>Sales Tax / Exemption</h3>
                        <p style="font: 13px/18px Arial, Helvetica, sans-serif !important;">Residents of Missouri and Indiana are required to pay sales tax, unless they work for an institution that claims tax exemption. To claim tax exemption, please email us at <strong><a href="mailto:orders@humanconnectome.org?subject=tax exemption">orders@humanconnectome.org</a></strong>.</p>
                        <div class="hcpForm-wrapper hidden">
                            <label for="taxexempt">Tax Exemption</label>
                            <p><input type="checkbox" name="taxexempt" value="yes" disabled="disabled" /> I certify that my institution is tax exempt and I have attached a copy of our tax exempt letter. (PDF files only.)</p>
                            <p><input type="file" name="taxexempt-letter" accept="application/pdf" style="width:485px;" disabled="disabled" /></p>
                        </div>
                    </div>
                    <p><label for="taxexempt"><input type="checkbox" name="taxexempt" /> Tax Exempt Status?</label></p>

                    <p><label for="customShipping">Custom Shipping Cost? (i.e. will be hand-delivered, or bundled with another order)</label>
                        <input type="text" name="customShipping" style="width:60px" placeholder="0.00" /> </p>
                    <p><label for="justification">Justification (required if either pricing option is selected)</label>
                        <textarea name="justification" style="width: 540px;"></textarea></p>
                <?php endif; ?>

                <h1>Data Use Terms</h1>
                <p><strong>WU-Minn HCP Consortium Open Access Data Use Terms </strong></p>
                <p>I request access to data collected by the Washington University - University of Minnesota Consortium of the Human Connectome Project (WU-Minn HCP), and I agree to the following: </p>
                <ol>
                    <li>I will not attempt to establish the identity of or attempt to contact any of the included human subjects. </li>
                    <li>I understand that under no circumstances will the code that would link these data to Protected Health Information be given to me, nor will any additional information about individual human subjects be released to me under these Open Access Data Use Terms. </li>
                    <li>I will comply with all relevant rules and regulations imposed by my institution. This may mean that I need my research to be approved or declared exempt by a committee that oversees research on human subjects, e.g. my IRB or Ethics Committee. The released HCP data are not considered de-identified, insofar as certain combinations of HCP Restricted Data (available through a separate process) might allow identification of individuals. Different committees operate under different national, state and local laws and may interpret regulations differently, so it is important to ask about this. If needed and upon request, the HCP will provide a certificate stating that you have accepted the HCP Open Access Data Use Terms. </li>
                    <li>I may redistribute original WU-Minn HCP Open Access data and any derived data as long as the data are redistributed under these same Data Use Terms. </li>
                    <li>I will acknowledge the use of WU-Minn HCP data and data derived from WU-Minn HCP data when publicly presenting any results or algorithms that benefitted from their use. </li>
                    <ol style="list-style:lower-alpha">
                        <li>Papers, book chapters, books, posters, oral presentations, and all other printed and digital presentations of results derived from HCP data should contain the following wording in the acknowledgments section:  "Data were provided [in part] by the Human Connectome Project, WU-Minn Consortium (Principal Investigators: David Van Essen and Kamil Ugurbil; 1U54MH091657) funded by the 16 NIH Institutes and Centers that support the NIH Blueprint for Neuroscience Research; and by the McDonnell Center for Systems Neuroscience at Washington University." </li>
                        <li>Authors of publications or presentations using WU-Minn HCP data should cite relevant publications describing the methods used by the HCP to acquire and process the data. The specific publications that are appropriate to cite in any given study will depend on what HCP data were used and for what purposes.  An annotated and appropriately up-to-date list of publications that may warrant consideration is available at <a href="http://www.humanconnectome.org/about/acknowledgehcp.html" target="_blank">http://www.humanconnectome.org/about/acknowledgehcp.html</a></li>
                        <li>The WU-Minn HCP Consortium as a whole should not be included as an author of publications or presentations if this authorship would be based solely on the use of WU-Minn HCP data.</li>
                    </ol>
                    <li>Failure to abide by these guidelines will result in termination of my privileges to access WU-Minn HCP data.</li>
                </ol>
                <p id="dut-version">v. 2013-Apr-26</p>
                <p><label for="dut"><input type="checkbox" name="dut" value="true" /> I have read and accept these data use terms</label></p>

                <div class="g-recaptcha" data-sitekey="6LcXMzEUAAAAAHwC1l4GRLw-AsCp2q-_PAQtzZyh" style="margin: 2em 0"></div>

                <p align="right" style="margin-bottom:0;"><input type="submit" value="Proceed to Checkout" /></p>

            </form>

            <?php
            mysqli_close($db);
            ?>
        </div>

        <!-- Primary page Content -->


    </div> <!-- middle -->

    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/footer.php'); ?>


</div>
<!-- end container -->

<script src="//www.google.com/jsapi?key=AIzaSyA5m1Nc8ws2BbmPRwKu5gFradvD_hgq6G0" type="text/javascript"></script>
<script type="text/javascript">
    /*
     *  How to restrict a search to a Custom Search Engine
     *  http://www.google.com/cse/
     */

    google.load('search', '1');

    function OnLoad() {
        // Create a search control
        var searchControl = new google.search.SearchControl();

        // Add in a WebSearch
        var webSearch = new google.search.WebSearch();

        // Restrict our search to pages from our CSE
        webSearch.setSiteRestriction('000073750279216385221:vnupk6whx-c');

        // Add the searcher to the SearchControl
        searchControl.addSearcher(webSearch);

        // tell the searcher to draw itself and tell it where to attach
        searchControl.draw(document.getElementById("search"));

        // execute an inital search
        // searchControl.execute('');
    }

    google.setOnLoadCallback(OnLoad);

</script>


<!-- google analytics -->
<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-18630139-1']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

</script>
</body>
</html>
