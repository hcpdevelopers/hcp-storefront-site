// JavaScript Document

$(document).ready(function(){
    // unset all the order variables
    $('.qty').each(function(){
        $(this).val(0);
    });
    // $('#itemcnt').val(0);
    $('#drivecnt').val(0);
    /*
     $('#billing-info').find('input[type="text"]').val('');
     $('#billing-info').find('textarea').val('');
     $('#billing-info').find('option:selected').prop('selected',false);
     $('#billing-info').find('input[type="checkbox"]').prop('checked',false);
     $('#country-select').find('option:selected').prop('selected',false);
     */
    $('.modal').find('input[type="checkbox"]').prop('checked',false);

    /* Watch for user inputs that are geared toward fixing validation errors */
    $('input').change(function(){
        if ($(this).parents().hasClass('error') ) {
            $(this).parents().removeClass('error');
        }
    });
    $('select').change(function() {
        if ($(this).parents().hasClass('error') ) {
            $(this).parents().removeClass('error');
        }
    });

    // validate country and update shipping rate
    $('#country-select').change(function(){
        var cnt=$(this).val();
        if (cnt != 'United States') {

            // change address form field
            $('.state-US').addClass('hidden')
                .find('select').prop('disabled',true).removeClass('required');
            $('.state-int').removeClass('hidden')
                .find('input').removeProp('disabled');

            // hide sales tax warning
            $('#salestax').not('hidden').addClass('hidden');

            /*
             // disable form fields
             $('#salestax').find('input').prop('disabled','disabled');
             */
        } else {

            // change address form field and reset state selection
            $('.state-US').removeClass('hidden')
                .find('select').prop('disabled',false).addClass('required')
                .find('option:selected').prop('selected',false);
            $('.state-int').addClass('hidden')
                .find('input').prop('disabled','disabled');

        }
    });

    // add or remove course to order when user selects a registration type
    $('.item-price-select').on('change',function(){
        var product = $(this).parents('.product-box');
        var price = $(this).val();
        var regtype = $(this).find('option:selected').data('regtype'); // get registration type from the data- attribute on the selected option
        if (price > 0) {
            addToCart(product,regtype);
        } else {
            removeFromCart(product);
        }

        // add condition for student rate
        if ($(this).find('option:selected').data('regtype') == 'student') {
            $("input[name=customer_pi]").addClass("required").prev("label").html("Name of PI / Faculty Mentor (required for student rate registration)");
        } else {
            $("input[name=customer_pi]").removeClass("required").prev("label").html("Name of PI / Faculty Mentor (Optional)");
        }
    });

});

function toggleBillingAddress(el) {
    $('#billing_address_warning').toggle();
    if ($(el).prop("checked")) {
        // user intends to use same address for shipping and billing; disable billing address form fields
        $('#billing-info').find('input').each(function(){
            $(this).prop("disabled","disabled");
        });
        $('#billing-info').find('textarea').prop("disabled","disabled");
        $('#billing-info').find('select').prop("disabled","disabled");
        $('#billing-info').addClass('hidden');
    } else {
        // user intends to use different addresses for shipping and billing; enable billing address form fields
        $('#billing-info').find('input').each(function(){
            $(this).prop("disabled",false);
        });
        $('#billing-info').find('textarea').prop("disabled",false);
        $('#billing-info').find('select').prop("disabled",false);
        $('#billing-info').removeClass('hidden');
    }
}

function addToCart(product,regtypeval) {
// add drive order from tab that has been clicked on. Pass a product container as an object through to the function
    $(product).addClass('in-cart');

    $(product).find('.add-item').addClass('drive-added').removeClass('add-drive');

    // increment order qty
    var qtyField = $(product).find('.qty');
    // var qty =  $(qtyField).val();
    $(qtyField).val(1);

    // increment item count (the number of Cashnet products ordered)
    var itemcnt = $('#itemcnt').val(); itemcnt++;
    $('#itemcnt').val(itemcnt);

    // store registration type
    var regtype = $(product).find('.regtype');
    $(regtype).val(regtypeval);

    // reveal hidden format boxes to show format and enclosure options,
    // enable disabled form fields (once more for select)
    $(product).find('.item-info').each(function(i){ $(this).prop('disabled',false); });
    // $(product).find('select[class="item-info"]').prop('disabled',false);
}

function removeFromCart(product){
// remove drive order from tab that has been clicked away. pass a product container as an object through to the function.
    $(product).removeClass('in-cart');

    $(product).find('.add-item').removeClass('drive-added').addClass('add-drive');

    // set order qty to zero
    var qtyField = $(product).find('.qty');
    $(qtyField).val(0);

    // decrement item count (the number of Cashnet products ordered)
    var itemcnt = $('#itemcnt').val();
    if (itemcnt > 0) {
        itemcnt--;
        $('#itemcnt').val(itemcnt);
    }

    // remove stored registration type
    var regtype = $(product).find('.regtype');
    $(regtype).val('');

    // hide options
    // disable all form fields for this product
    $(product).find('.item-info').each(function(i){ $(this).prop('disabled','disabled'); });
    // $(product).find('select[class="item-info"]').prop('disabled','disabled');
}

function dataToggle(productType) {
    var unProduct;
    if (productType === 'archive') {
        $('.current').hide();
        $('.archive').slideDown();
        $('#product-toggle').html('ARCHIVED DATA RELEASES -  <a href="javascript:dataToggle(\'bundle\')">Show Current Data Releases</a>');
        unProduct = '.current';
    } else {
        $('.archive').hide();
        $('.current').slideDown();
        $('#product-toggle').html('CURRENT DATA RELEASES -  <a href="javascript:dataToggle(\'archive\')">Show Archived Data Releases</a>');
        unProduct = '.archive';
    }


}


function validateMe(formId, validateProducts=true, validateAddress=true, validateEmail=true) {
    // initialize
    var items=[], address=[], country=[], payment=[], email=[], general=[], scrollTop;
    $(formId).find('.error-message-country').addClass('hidden').find('.error-list').empty();
    $(formId).find('.error-message-address').addClass('hidden').find('.error-list').empty();
    $(formId).find('.error-message-product').addClass('hidden').find('.error-list').empty();
    $(formId).find('.error-message-payment').addClass('hidden').find('.error-list').empty();

    if (validateProducts) {
        // validate item selection
        if ($(formId).find('#itemcnt').val() < 1) {
            items.push("You haven't selected a course registration.");
        }
        /* $('.format-box').not('.hidden').not('.enclosure').each(function(){
         if ($(this).find('select').val().length<1) {
         items.push("You must specify a drive format for each drive ordered.");
         $(this).addClass('error');
         }
         });
         /* no longer offering multiple formats
         */
        if ($(formId).find(items).length > 0) {
            $(formId).find('.error-message-product').removeClass('hidden');
            // write an error to the product specification area for each error found
            for (i = 0; i < items.length; i++) {
                var msg = '<li>' + items[i] + '</li>';
                $(formId).find('.error-message-product').find('.error-list').append(msg);
            }
            scrollTop = 640;
        }
    }

    if (validateAddress) {
        // validate country selection and address
        if ($(formId).find('#country-select').val().length < 1) {
            $(formId).find('.error-message-country').removeClass('hidden')
                .find('.error-list').append('<li>You must select a country.</li>');
            country.push('You must select a country.');
            scrollTop = 450;
        }

        if (address.length > 0) {
            $(formId).find('.error-message-address').removeClass('hidden');
            for (i=0; i<address.length; i++) {
                var msg = '<li>'+address[i]+'</li>';
                $(formId).find('.error-message-address').find('.error-list').append(msg);
            }
            scrollTop = (scrollTop > 0 ) ? scrollTop : 1200;
        }
    }

    if (validateEmail) {
        // email validation
        $(formId).find('.email').each(function() {
            var string = $(this).val().toLowerCase();
            var label = $(this).prev('label').html();
            if ((string) && ( !string.match(/^([a-z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-z0-9](?:[.a-z0-9-]{0,61}[a-z0-9])?(?:\.[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?))$/i) )) {
                email.push(label + ' is not valid.');
                $(this).parent('div').addClass('error');
            }
        });

        if (email.length > 0) {
            $(formId).find('.error-message-address').removeClass('hidden');
            for (i=0; i<email.length; i++) {
                var msg = '<li>'+email[i]+'</li>';
                $(formId).find('.error-message-address').find('.error-list').append(msg);
            }
            scrollTop = (scrollTop > 0 ) ? scrollTop : 1200;
        }
    }

    // always validate required fields in general
    $(formId).find('.required').each(function () {
        if ($(this).val().length < 1) {
            general.push($(this).prev('label').html() + ' is required.');
            $(this).parent('div').addClass('error');
        }
    });
    
    // always look for honeypot clicks
    if ($(formId).find('input[name=dont_click_me]').prop('checked')) {
        general.push("You must be a robot. Try again as a human.");
    }

    if (general.length > 0) {
        $(formId).find('.error-message-general').removeClass('hidden');
        for (i=0; i<general.length; i++) {
            var msg = '<li>'+general[i]+'</li>';
            $(formId).find('.error-message-general').find('.error-list').append(msg);
        }
        scrollTop = (scrollTop > 0 ) ? scrollTop : 1200;
    }

    errors = items.length + address.length + country.length + payment.length + email.length + general.length;
    if (errors > 0) {
        $(document).scrollTop(scrollTop);
        return false;
    } else {
        // disable any select inputs where no course registration has been selected
        $(formId).find('.item-price-select').each(function(el){ if (!$(this).val()) $(this).prop('disabled','disabled') });

        $(formId).submit();
    }

}

/* final validation step: confirm user acceptance of DUT before allowing them to go forward */
function confirmDUT() {
    alert("You have accepted the HCP Data Use Terms.");
    return true;
}
