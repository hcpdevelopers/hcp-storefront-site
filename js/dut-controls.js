// JavaScript Document


function showRegDiv() {
	showModal('reg-div');
	validateRegForm();
}
function closeConfirmation() {
	if ($('#reg-confirm-msg').text().match(/Thank/)) {
		modalClose();
	} else {
		modalClose('reg-confirm-div');
		modalClose('reg-processing-div');
	}
}

/* 
 * --- 
 * Data Use Terms controls 
 * ---
 */


function scrollFunc(ele,check,button) {
	if (ele.clientHeight + ele.scrollTop >= ele.scrollHeight) {
		$("#" + check).prop('disabled',false);
		$('.dut-reminder').hide();
		$('.dut-accept-message').show();
	}
}


function forgotLoginRequest() {
	params='account=' + $('#forgotPassword').val() + '&email=' + $('#forgotUsername').val() + '&XNAT_CSRF=' + window.csrfToken;
	//$('#forgot-pass-div').addClass("hidden").hide();
	$('#reg-processing-div').removeClass("hidden").show();
	YAHOO.util.Connect.asyncRequest('GET',serverRoot +'/REST/services/forgotlogin?'+params,{ success:forgotGetSuccess,failure:forgotGetFailure });
}


$(document).ready(function(){
	$('.dutWrapper').on('scroll',function(){

		if ( ($(this).scrollTop() + $(this).height()) > $('.dutContents').height() ) {

			$('.dut-reminder').hide();
			$('.dut-accept-message').show();
			$('.form-submit').show();
			$('#dut-accepted').prop('disabled',false);
		}
	});

	$('#p2-btn-agree').on('click',function(){
		$('#cinab-order').submit();
	})

});


