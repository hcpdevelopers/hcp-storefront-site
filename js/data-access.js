// JavaScript Document

/*
 * ---
 * general modal controls
 * ---
 */


function showRegDiv() {
	showModal('reg-div');
	validateRegForm();
}

function showModal(id) {
	$('#page-mask').show();
	$('#'+id).show();
	$('.dutWrapper').scrollTop(0);
	$('.dut-reminder').show();
	$('.dut-accept').hide();
}

function showModal(id,showOpts) {
	if (showOpts) {
		$('div[name="interact-div"]').show();
		$('div[name="viewdut-div"]').hide();
	} else {
		$('div[name="interact-div"]').hide();
		$('div[name="viewdut-div"]').show();
	}
	$('#page-mask').show();
	$('#'+id).show();
	$('.dut-reminder').show();
	$('.dut-accept-message').hide();
	$('.form-submit').hide();
	$('.dutWrapper').scrollTop(0);
}
function modalClose(id) {
	$('#page-mask').hide();
	$('#reg-div div[name="interact-div"]').show();
	if (id) {
		$('#'+id).hide();
	} else {
		$('.modal').hide();
	}
	$('.modal').find('input[type="checkbox"]').prop('checked',false).prop('disabled',false);
	$('#dut-accepted').prop('disabled','disabled'); /* remove hidden input that verifies signature */
}
function closeConfirmation() {
	if ($('#reg-confirm-msg').text().match(/Thank/)) {
		modalClose();
	} else {
		modalClose('reg-confirm-div');
		modalClose('reg-processing-div');
	}
}
/* 
 * --- 
 * E-mail/Password Modification
 * ---
 */
function ConfirmEmail() {
	if(YUIDOM.get('new_email').value == YUIDOM.get('confirm_email').value)
	{
		if(YUIDOM.get('new_email')!="" && YUIDOM.get('new_email').value.indexOf('@')>-1){
			return true;
		}else{
			alert("Please enter a valid email address");
			return false;
		}
	}else{
		alert('Values do not match');
		YUIDOM.get('new_email').value='';
		YUIDOM.get('confirm_email').value='';
		return false;
	}
}

function ConfirmPassword() {
	if (document.getElementById)
	{
		if(document.getElementById('new_password').value == document.getElementById('confirm_password').value)
		{
			return true;
		}else{
			alert('Values do not match');
			document.getElementById('new_password').value='';
			document.getElementById('confirm_password').value='';
			return false;
		}
	}
	else if (document.all)
	{
		if(document.all['new_password'].value == document.all['confirm_password'].value)
		{
			return true;
		}else{
			alert('Values do not match');
			document.all['new_password'].value='';
			document.all['confirm_password'].value='';
			return false;
		}
	}
}

/* 
 * --- 
 * Registration form checker 
 * ---
 */
$(document).ready(function(){
	// adds highlight to focused field, and highlights help text 
	$('.inputWrapper input').focusin( function(){
		$(this).parent().addClass('highlighted');
		$(this).next().removeClass('hidden');
	});
	// removes highlight and hides help text
	$('.inputWrapper input').focusout( function(){
		$(this).parent().removeClass('highlighted');
		$(this).next().addClass('hidden');
	});
	// validates username input 
	$('#reg-div input[name="username"]').focusout( function(){
		$(this).val($(this).val().toLowerCase());
	});
	// validates password complexity -- NEEDS FIXING
	$('#reg-div input[name="pw"]').focusout( function(){
		var string = $(this).val();
		//if ((string) && (string.match(/[a-z]/)) && (string.match(/[A-Z]/)) && (string.match(/(\d|\W)/)) && (string.length >= 8)) {
		if ((string) && (string.replace(/^[ ]*/,'').replace(/[ ]*$/,'').length >= 5)) {
			$('#password-simple').addClass('hidden');
			$(this).parent().removeClass('error');
		} else {
			$('#password-simple').removeClass('hidden');
			$(this).parent().addClass('error');
		}
		var matchedString = $('#reg-div input[name="pwc"]').val();
		if (matchedString.length>0) {
			if (string === matchedString) {
				$('#password-mismatch').addClass('hidden');
				$('#reg-div input[name="pwc"]').parent().removeClass('error');
			} else {
				$('#password-mismatch').removeClass('hidden');
				$('#reg-div input[name="pwc"]').parent().addClass('error');
			}
		}
	});
	// validates password match
	$('#reg-div input[name="pwc"]').focusout( function(){
		var string = $(this).val();
		var matchedString = $('#reg-div input[name="pw"]').val();
		if (string === matchedString) {
			$('#password-mismatch').addClass('hidden');
			$(this).parent().removeClass('error');
		} else {
			$('#password-mismatch').removeClass('hidden');
			$(this).parent().addClass('error');
		}
	});

	$('#new_password').focusout( function(){
		var string = $(this).val();
		if (string.length<1 || string.replace(/^[ ]*/,'').replace(/[ ]*$/,'').length >= 5) {
			$('#password-simple').addClass('hidden');
			if (string.length>1) {
				$('#pw_submit').removeProp('disabled');
			}
		} else {
			if ($('#password-simple').hasClass('hidden')) {
				$(this).focus();
			}
			$('#password-simple').removeClass('hidden');
			$('#pw_submit').prop('disabled','disabled');
		}
	});
			
	// validates email input
	$('#new_email').focusout( function(){
		var string=$(this).val().toLowerCase();
		if ((string) && ( !string.match(/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/) )) {
			if ($('#email-error').hasClass('hidden')) {
				$(this).focus();
			}
			$(this).addClass('error');
			$('#email-error').removeClass('hidden');
			$('#email_submit').prop('disabled','disabled');
		} else {
			$('#email-error').addClass('hidden');
			if (string.match(/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/)) {
				$('#email_submit').removeProp('disabled');
			}
		}
	});
			
	// validates email input
	$('#reg-div input.email').focusout( function(){
		var string=$(this).val().toLowerCase();
		if ((string) && ( !string.match(/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/) )) {
			$(this).parent().addClass('error');
			$('#email-error').removeClass('hidden');
		} else {
			$('#email-error').addClass('hidden');
			$(this).parent().removeClass('error');
		}
	});
	
	// validates form completion -- NEEDS FIXING
	$('.inputWrapper input').blur( function() {
		validateRegForm();
	}); 
});

function validateRegForm() {
	var formComplete = true;
	$('.inputWrapper input.required').each(function() {
		var string=$(this).val();
		if (!string) {
			formComplete = false;
			return false;
		}
	});
	if (formComplete && !$('#reg-div div').is('.error')) {
		$('#regSubmit').removeProp('disabled');
	} else {
		$('#regSubmit').prop('disabled','disabled');
	}
}

/* 
 * --- 
 * Registration thank you and confirmation control 
 * ---
 */

function showThanks() {
	var username = $('#reg-div input[name="username"]').val();
	var email = ($('#reg-div input.email').val()) ? $('#reg-div input.email').val() : 'empty';
	
	$('#reg-thanks-msg').text('You have created an account with the username "'+ username +'." Please check your email at '+ email +' to complete your registration.');
	$('#reg-div').addClass("hidden");
	$('#reg-processing-div').addClass("hidden").hide();
	$('#reg-thanks-div .modal-title').text("Please confirm your registration");
	$('#reg-thanks-div').removeClass("hidden").show();
	return false;
}
function showConfirm() {
	var username = $('#reg-div input[name="username"]').val();
	var email = ($('#reg-div input.email').val()) ? $('#reg-div input.email').val() : 'empty';
	
	$('#reg-confirm-msg').text('Thank you for registering an HCP account with the email address ' + email + '. You have created an account with the username "'+ username +'".');
	$('#reg-div').addClass("hidden");
	$('#reg-confirm-div').removeClass("hidden").show();
	return false;
}
function showFailed(txt) {
	$('#reg-confirm-msg').text("").append(txt);
	$('#reg-div').addClass("hidden");
	$('#reg-confirm-div').removeClass("hidden").show();
	return false;
}
function dbRegisterCall(connectURI) {
	// Submit registration
	//$('#reg-div').addClass("hidden");
	$('#reg-processing-div').removeClass("hidden").show();
	$('#regSubmit').prop("disabled","disabled");
	var formObject = document.getElementById('regForm');
	YAHOO.util.Connect.setForm(formObject,false);
	YAHOO.util.Connect.asyncRequest('POST',connectURI,dbRegCallback,null);

}
dbRegCallback={
	success:function(o){
		dbRegSuccess(o);
			},
	failure:function(o){
		dbRegFailed(o);
	},
	scope:this
};
function dbRegSuccess(o) {
	showConfirm();
	// Submit request to join hcp-announce list
	if ($('#hcp-announce').is(":checked")) {
	   $('#hcpAnnounceListEmail').val( $('#regFormEmail').val() );
	   $form = $('#hcpAnnounceListForm');
	   $form.submit();
	}
	// Submit request to join hcp-users list
	if ($('#hcp-users').is(":checked")) {
	   $('#dataUsersListEmail').val( $('#regFormEmail').val() );
	   $form = $('#dataUsersListForm');
	   $form.submit();
	}
	$('.inputWrapper input').val("");
}
function dbRegFailed(o) {
	if (o.responseText != null && o.responseText.length<100) {
		showFailed(o.responseText);
	} else {
		showFailed("The server could not process the registration");
	}
}

/* 
 * --- 
 * validate username / password reset form
 * currently set to disable form and highlight errors
 * ---
 */
 
function forgotValidation() {
	var mailUser=$("#forgotUsername").val(); 
	var resetPass=$("#forgotPassword").val();
	if (!mailUser && !resetPass) {
		alert("Please enter values");
		return false;
	}
/*
	if (mailUser) {
		$("#pass-reminder-error")
			.empty()
			.append("<strong>Error: </strong>The email address you entered cannot be found in the HCP database.")
			.removeClass("hidden");
		$("#forgotUsername")
			.focus();
	}
	if (resetPass) {
		$("#pass-reminder-error")
			.empty()
			.append("<strong>Error: </strong>The username you entered cannot be found in the HCP database.")
			.removeClass("hidden");
		$("#forgotPassword")
			.focus();
	}
	return false;
*/
	return true;
}

/* 
 * --- 
 * Data Use Terms controls 
 * ---
 */

 // TO DO: write script that enables checkbox when user scrolls down (i.e. "reads") the terms of use. 

function dutConfirm(ele) {
	/* 
	 * Change the page according to their acceptance of terms.
	 * Would this require a page refresh in XNAT?
	 */
	  
	$.cookie(ele,"true");
	$('#dut-' + ele).addClass("hidden");
	$('#dut-processing-div').addClass("hidden").hide();
	$('#dut-thanks-div').removeClass("hidden").show();
	$('#reg-thanks-msg').text("You have accepted the Data Use Terms for this data set, and now have permission to download this data. Please consult documentation on how to download HCP data if you have any questions.");
	
	// expose content that would be hidden before DUT acceptance
	$('#list-' + ele).find('.permission-controlled')
		.removeClass("hidden");
		//.css('background-color','yellow');
	
	$('#list-' + ele).find('input.request')
		.removeClass("request")
		.removeClass("hidden").show()
		.addClass("accepted")
		.prop("disabled","disabled")
		.val("Agreed");
		//.css('outline','3px solid yellow');
	$('#div-' + ele).addClass("hidden").hide();
	$('#div2-' + ele).removeClass("hidden").show();
	$('#btn-' + ele).removeClass("hidden").show();
	
	// highlight acceptance of terms
	$('#list-' + ele + ' .dut')
		.addClass("accepted")
		.text("Terms of Use Accepted");
		//.css('background-color','yellow');

}
 
function agreeChange(checkbox,id) {
	// if user checks box, don't let them uncheck. Turn on the form submit and guide them to it. 
	$(checkbox).prop('disabled','disabled');
	$('#'+id).removeProp('disabled').focus();
	$('.modal').append('<input type="hidden" name="dut" id="accept-dut" value="v. 15-Feb-2013" />');
}

function dutError(ele,responseText) {
	/* 
	 * Change the page according to their acceptance of terms.
	 * Would this require a page refresh in XNAT?
	$('#dut-' + ele).addClass("hidden");
	$('#dut-thanks-div').removeClass("hidden").show();
	$('#reg-thanks-msg').text("").append("Could not register data use acceptance" + responseText );
	 */
	
}

function dutSubmit(terms) {
	
	$('#dut-processing-div').removeClass("hidden").show();
	if (terms == "Phase1") {
		$('#p1-btn-agree').prop("disabled","disabled");
		arg = "Phase1PilotData" ;
		params = 'terms=Phase1&acceptTerms=true&XNAT_CSRF='+window.csrfToken;
		YAHOO.util.Connect.asyncRequest('POST',serverRoot +'/REST/services/datause?'+params,{ success:duPostSuccess,failure:duPostFailure,argument:arg });
	} else if (terms == "Phase2") {
		$('#p2-btn-agree').prop("disabled","disabled");
		arg = "Phase2OpenAccess" ;
		params = 'terms=Phase2&acceptTerms=true&XNAT_CSRF='+window.csrfToken;
		YAHOO.util.Connect.asyncRequest('POST',serverRoot +'/REST/services/datause?'+params,{ success:duPostSuccess,failure:duPostFailure,argument:arg });
	} else if (terms == "WorkbenchData") {
		$('#wb-btn-agree').prop("disabled","disabled");
		arg = "WorkbenchData" ;
		params = 'terms=WB&acceptTerms=true&XNAT_CSRF='+window.csrfToken;
		YAHOO.util.Connect.asyncRequest('POST',serverRoot +'/REST/services/datause?'+params,{ success:duPostSuccess,failure:duPostFailure,argument:arg });
	}

}

function duPostSuccess(o) {
	if (o.responseText.toUpperCase() == "TRUE") {
		dutConfirm(o.argument);
	} else {
		if (o.argument == "Phase1") {
			$('#p1-btn-agree').removeProp("disabled");
		} else if (o.argument == "Phase2") {
			$('#p1-btn-agree').removeProp("disabled");
		} else if (o.argument == "WorkbenchData") {
			$('#p1-btn-agree').removeProp("disabled");
		}
		dutNotYet(o.argument,o.responseText);
	}
}

function duPostFailure(o) {
	if (o.argument == "Phase1") {
		$('#p1-btn-agree').removeProp("disabled");
	} else if (o.argument == "Phase2") {
		$('#p1-btn-agree').removeProp("disabled");
	} else if (o.argument == "WorkbenchData") {
		$('#p1-btn-agree').removeProp("disabled");
	}
	dutError(o.argument,o.responseText);
}

function duGetSuccess(o) {
	if (o.responseText.toUpperCase() == "TRUE") {
		dutAlready(o.argument);
	} else {
		dutNotYet(o.argument,false);
	}
}

function duGetFailure(o) {
	if (!($.cookie(ele) == "true" && $.cookie("button_user") == dutUser)) {
		dutNotYet(o.argument,false);
	}
}

function dutCheck() {

	// Check data use acceptance status
	var arg;
	var params;

	arg = "WorkbenchData" ;
	params = 'terms=WB&XNAT_CSRF='+window.csrfToken;
	YAHOO.util.Connect.asyncRequest('GET',serverRoot +'/REST/services/datause?'+params,{ success:duGetSuccess,failure:duGetFailure,argument:arg });

	arg = "Phase1PilotData" ;
	params = 'terms=Phase1&XNAT_CSRF='+window.csrfToken;
	YAHOO.util.Connect.asyncRequest('GET',serverRoot +'/REST/services/datause?'+params,{ success:duGetSuccess,failure:duGetFailure,argument:arg });

	arg = "Phase2OpenAccess";
	params = 'terms=Phase2&XNAT_CSRF='+window.csrfToken;
	YAHOO.util.Connect.asyncRequest('GET',serverRoot +'/REST/services/datause?'+params,{ success:duGetSuccess,failure:duGetFailure,argument:arg });

}

function dutAlready(ele) {
	// DUT accepted - show agreed button
	$.cookie(ele,"true");
	$('#list-' + ele).find('.permission-controlled')
		.removeClass("hidden")
	
	$('#dut-processing-div').addClass("hidden").hide();
	$('#list-' + ele).find('input.request')
		.removeClass("hidden").show()
		.removeClass("request")
		.addClass("accepted")
		.prop("disabled","disabled")
		.val("Agreed")
	$('#div-' + ele).addClass("hidden").hide();
	$('#div2-' + ele).removeClass("hidden").show();
	$('#btn-' + ele).removeClass("hidden").show();
	
	// highlight acceptance of terms
	$('#list-' + ele + ' .dut')
		.addClass("accepted")
		.text("Terms of Use Accepted")
}

var dutUser;

function dutNotYet(ele,force) {
	// DUT not yet accepted - show request button
	if (force || !($.cookie(ele) == "true" && $.cookie("button_user") == dutUser)) {
		$.cookie(ele,"false");
	} 
	$('#dut-processing-div').addClass("hidden").hide();
	$('#list-' + ele).find('input.request')
		.removeClass("hidden")
	$('#div-' + ele).removeClass("hidden").show()
	$('#div2-' + ele).addClass("hidden").hide()
	$('#btn-' + ele).addClass("hidden").hide()
	
	// highlight acceptance of terms
	$('#list-' + ele + ' .dut')
		.removeClass("accepted")
		.text("Terms of Use")
}

function initIndexWindow() {
	//alert("Vers 1");
	dutCheck();
}

function doInitButtons(user) {
	// Speed up setting up of buttons using values from cookies
	dutUser = $.cookie("button_user");
	if ( $.cookie("button_user") == user) {
		dutCookieSet("Phase1PilotData");
		dutCookieSet("Phase2OpenAccess");
		dutCookieSet("WorkbenchData");
	}
	$.cookie("button_user",user);
}

function dutCookieSet(ele) {
	if ($.cookie(ele) == "true" ) {
		dutAlready(ele);
	} else {
		dutNotYet(ele,false);
	}
}

function scrollFunc(ele,check,button) {
	if (ele.clientHeight + ele.scrollTop >= ele.scrollHeight) {
		$("#" + check).removeProp('disabled');
		$('.dut-reminder').hide();
		$('.dut-accept').show();
	}
}

function forgotGetSuccess(o) {
	$('#reg-processing-div').addClass("hidden").hide();
	$('#reg-thanks-div .modal-title').text("Forgot Username/Password Service");
	$('#reg-thanks-div').removeClass("hidden").show();
	$('#reg-thanks-msg').text(o.responseText);
}

function forgotGetFailure(o) {
	forgotGetSuccess(o);
}

function forgotLoginRequest() {
	params='account=' + $('#forgotPassword').val() + '&email=' + $('#forgotUsername').val() + '&XNAT_CSRF=' + window.csrfToken;
	//$('#forgot-pass-div').addClass("hidden").hide();
	$('#reg-processing-div').removeClass("hidden").show();
	YAHOO.util.Connect.asyncRequest('GET',serverRoot +'/REST/services/forgotlogin?'+params,{ success:forgotGetSuccess,failure:forgotGetFailure });
}

