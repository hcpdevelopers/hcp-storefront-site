<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Confirm HCP Data Order | Human Connectome Project</title>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/header-includes.php'); ?>

</head>

<body>


<!-- site container -->
<div id="site_container">

    <div id="NIH_header2">
        <span><a href="http://www.neuroscienceblueprint.nih.gov/connectome/" title="NIH Human Connectome Project" style="color:#b4cfff; text-decoration:none;">NIH Blueprint: The Human Connectome Project</a></span>
    </div>

    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/storefront-topnav.php'); ?>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/db-login.php'); ?>

    <!-- Breadcrumbs -->
    <div id="breadcrumbs">
        <p>&raquo; <a href="/courses/">HCP Courses</a> &gt; confirm registration details</p>
    </div>
    <!-- end breadcrumbs -->

    <!-- "Middle" area contains all content between the top navigation and the footer -->
    <div id="middle">
        <div id="content">
            <noscript>
                <p style="color:#c00; clear:both;"><strong>Alert:</strong> You appear to have JavaScript turned off. Registration and access for ConnectomeDB requires JavaScript. Please enable it in your browser before proceeding.</p>
            </noscript>

            <?php
            // prevent spam form submissions
            if (empty($_POST['g-recaptcha-response']) || $_POST['g-recaptcha-response'] == '') die("<p><strong>Sorry, this registration form is restricted to humans.</strong> If you are not a bot, please go back and fill out the form again, including a valid ReCaptcha entry.</p>");
            ?>

            <h2>HCP Storefront: Confirm Order</h2>
            <p>Please review the following order information. If any of this is incorrect, please <a href="javascript:window.history.back();">go back to the order form</a> to correct it.</p>

            <?php
            /* create entry in 'orders' database and capture its just-created ID field */
            $neworder = mysqli_query($db,'INSERT INTO orders (timestamp,status) VALUES (NOW(),\'incomplete\')');
            $orderIds = mysqli_fetch_array(mysqli_query($db,'SELECT id FROM orders ORDER BY id DESC LIMIT 1'));
            $orderId = $orderIds[0];
            $courses_ordered = '';
            $cnum = 0;
            if (!$_POST['itemcode']) :
                die("Sorry, no item code was found. Please go back and try again, or contact the site administrator.");
            endif;
            ?>

            <?php
            /* look up customer by email address and either match, or create customer ID */
            $q = "SELECT COUNT(*) as rows,customer_id as id,shipping_country FROM orders WHERE customer_email = '".$_POST['customer_email']."';";
            $customer_record = mysqli_fetch_array(mysqli_query($db,$q));
            if (!$customer_record['id']) :
                // generate country code
                $q = "SELECT * FROM country_codes WHERE name = '".$_POST['HCP-COUNTRY']."';";
                $r = mysqli_query($db,$q) or die($q);
                $countryinfo = mysqli_fetch_array($r);
                ?><p><?php implode(",",$countryinfo); ?></p><?php
                $str1 = $countryinfo['dial_code'];
                while (strlen($str1) < 4) :
                    $str1 = "0".$str1;
                endwhile;
                $str2 = $orderId;
                while (strlen($str2) < 4) :
                    $str2 = "0".$str2;
                endwhile;
                $customerID = "H".$str1.$str2;
            else :
                $customerID = $customer_record['id'];
            endif;
            ?>

            <?php
            /* enter the order info into the order DB */
            $q = "UPDATE orders SET order_type='custom',
                        customer_name='".addslashes($_POST['customer_name'])."',
						customer_id='".$customerID."',
						customer_email='".$_POST['customer_email']."', 
						customer_institution='".addslashes($_POST['customer_institution'])."',
						shipping_country='".$_POST['HCP-COUNTRY']."', 
						shipping_phone='".$_POST['shipping_phone']."',
						notes='".addslashes($_POST['notes'])."',
						custom_order_id='".$_POST['customChargeId']."'
						WHERE id='".$orderId."';";
            $r = mysqli_query($db,$q) or die($q);

            if ($_POST['customer_pi']) mysqli_query($db,"UPDATE orders SET customer_pi='".$_POST['customer_pi']."' WHERE id='".$orderId."';");
            ?>

            <?php
            /* set array variables for product orders */
            $items = $_POST['courseid'];
            $amounts = $_POST['item-price'];
            $itemcount = $_POST['itemcnt'];
            $subtotal = $amounts; // + $discount
            $chargeId = $_POST['customChargeId'];

            // get custom charge info
            $q = "SELECT * FROM custom_orders WHERE id='".$chargeId."' LIMIT 1;";
            $r = mysqli_query($db,$q) or die ($q);
            $customChargeInfo = mysqli_fetch_assoc($r);

            // calculate CashNet fee as 2.25% of every dollar processed as payment, and add it to the S&H.
            $cashNet = $subtotal*(0.0225);
            // echo "<p>You were charged $".number_format($cashNet,2)." in CashNet fees</p>";

            ?>

            <!-- raw post contents -->
            <div style="background-color:#f0f0f0; padding:6px;" class="hidden">
                <?php
                printArray($_POST);

                function printArray($array){
                    echo "<ul>";
                    foreach ($array as $key => $value){
                        echo "<li>$key => $value</li>";
                        if(is_array($value)){ //If $value is an array, print it as well!
                            printArray($value);
                        }
                    }
                    echo "</ul>";
                }

                ?>
            </div>

            <!-- report to customer -->
            <p><strong>Order ID: <?php echo $orderId; ?></strong></p>
            <table cellspacing="1" cellpadding="3" width="100%" class="sortable" style="margin-bottom: 17px;">
                <thead>
                <tr><th style="background-color:#06c;">Customer ID: <?php echo $customerID ?></th><th>Charge Description</th><th>Cost</th></tr>
                </thead>
                <tbody>

                <tr><td><strong>Custom Charge:</strong> <?php echo $customChargeInfo['charge_title']; ?></td><td><?php echo $customChargeInfo['charge_justification'] ?></td><td align="right">$<?php echo number_format(floatval($amounts),2) ?></td></tr>
                <tr><td colspan="2">Transaction Fee</td><td align="right">$<?php echo number_format($cashNet,2); ?></td></tr>
                <tr><td colspan="2"><strong>Total cost:</strong></td><td align="right"><strong>$<?php echo number_format($subtotal + $cashNet,2) ; ?></strong></td></tr>
                </tbody>
            </table>



            <!-- BEGIN ORDER FORM -->

            <?php if ($_SERVER['HTTP_HOST'] == 'dev.humanconnectome.org') : ?>
            <!-- test storefront - no live transactions -->
            <form class="hcpForm" action="https://train.cashnet.com/404handler/pageredirpost.aspx?Virtual=HCPtest" method="post" style="position:relative">
                <p style="position: absolute; color: red; bottom: 0px; left: 0px;">Test Storefront - no transactions will be processed.</p>
                <?php else : ?>
                <!-- LIVE storefront! -->
                <form class="hcpForm" action="https://commerce.cashnet.com/404handler/pageredirpost.aspx?Virtual=HCP" method="post">
                    <?php endif; ?>

                    <h3>Registrant Information</h3>
                    <p><?php echo $_POST["customer_name"]?><br />
                        <?php echo ($_POST["customer_institution"]) ? $_POST["customer_institution"]."<br />" : "" ?>
                        <?php echo $_POST["HCP-COUNTRY"]?><br />
                        <?php echo $_POST["shipping_phone"]?><br />
                        <?php echo $_POST["customer_email"]?></p>

                    <h3>Billing Information</h3>

                    <p><?php echo $_POST["HCP-NAME_G"]?><br />
                        <?php echo ($_POST["HCP-INSTITUTION"]) ? $_POST["HCP-INSTITUTION"]."<br />" : "" ?>
                        <?php echo $_POST["HCP-ADDR_G"]?><br />
                        <?php echo ($_POST["billing_office"]) ? $_POST["billing_office"]."<br />" : "" ?>
                        <?php echo $_POST["HCP-CITY_G"]?>, <?php echo $_POST["billing_state"]?><br />
                        <?php echo $_POST["HCP-ZIP_G"]?><br />
                        <?php echo $_POST["HCP-COUNTRY"]?><br />
                        <?php echo $_POST["HCP-PHONE"]?><br />
                        <?php echo $_POST["HCP-EMAIL_G"]?></p>
                    <input type="hidden" name="HCP-NAME_G" value="<?php echo $_POST['HCP-NAME_G'] ?>" />
                    <input type="hidden" name="HCP-EMAIL_G" value="<?php echo $_POST['HCP-EMAIL_G'] ?>" />
                    <input type="hidden" name="HCP-INSTITUTION" value="<?php echo ($b ? $_POST['HCP-INSTITUTION'] : '') ?>" />
                    <input type="hidden" name="HCP-ADDR_G" value="<?php echo $_POST['HCP-ADDR_G']." ".$_POST['billing_office'] ?>" />
                    <input type="hidden" name="HCP-CITY_G" value="<?php echo $_POST['HCP-CITY_G'] ?>" />
                    <input type="hidden" name="HCP-STATE_G" value="<?php echo $_POST['billing_state'] ?>" />
                    <input type="hidden" name="HCP-ZIP_G" value="<?php echo $_POST['HCP-ZIP_G'] ?>" />
                    <input type="hidden" name="HCP-COUNTRY" value="<?php echo $_POST['HCP-COUNTRY'] ?>" />
                    <?php
                    $q = "UPDATE orders SET billing_contact='".addslashes($_POST['HCP-NAME_G'])."',
                            billing_address='".$_POST['HCP-ADDR_G']." ".$_POST['billing_office']."',
                            billing_city='".$_POST['HCP-CITY_G']."',
                            billing_state='".$_POST['billing_state']."',
                            billing_postal_code='".$_POST['HCP-ZIP_G']."',
                            billing_country='".$_POST['HCP-COUNTRY']."' WHERE id='".$orderId."';";
                    $r = mysqli_query($db,$q);
                    ?>



                    <?php
                    /*
                     * CONVERT INHERITED FORM VALUES FROM 'POST' TO CASHNET FORMAT
                     */
                    ?>

                    <input type="hidden" name="itemcnt" value="1" />
                    <input type="hidden" name="orderId" value="<?php echo $orderId ?>" />
                    <input type="hidden" name="HCP-ORDERID" value="<?php echo $orderId ?>" />

                    <input type="hidden" name="itemcode1" value="HCP-ADDON" />
                    <input type="hidden" name="amount1" value="<?php echo number_format($amounts,2) ?>" />
                    <input type="hidden" name="qty1" value="1" />
                    <input type="hidden" name="ref1type1" value="HCP-ADDON_TYPE" />
                    <input type="hidden" name="ref1val1" value="<?php echo $customChargeInfo['charge_title'] ?>" />
                    <input type="hidden" name="ref2type1" value="HCP-ADDON_DESC" />
                    <input type="hidden" name="ref2val1" value="<?php echo $customChargeInfo['charge_justification'] ?>" />
                    <input type="hidden" name="ref3type1" value="HCP-ADDON_ID" />
                    <input type="hidden" name="ref3val1" value="<?php echo $customChargeInfo['id'] ?>" />

                    <input type="hidden" name="itemcode2" value="HCP-FEE" />
                    <input type="hidden" name="amount2" value="<?php echo number_format($cashNet,2) ?>" />
                    <?
                    $formatted_charge = json_encode(array('charge_title' => $customChargeInfo['charge_title'], 'charge_amount' => $amounts ));

                    $q = "INSERT INTO receipt (order_id,other_cost,cashnet_fee,receipt_notes) VALUES ('".$orderId."','".$amounts."','".number_format($cashNet,2)."','Custom Charge ID: ".$customChargeInfo['id']."');";
                    $r = mysqli_query($db,$q) or die($q);

                    ?>

                    <p align="right" style="margin-bottom:0;"><input type="submit" value="Proceed to Checkout" /></p>

                </form>
                <p></p>
                <p>Credit card information will be gathered at checkout. Forms of payment we accept:</p>
                <p><img src="/img/icons/payment-American-Express.png" alt="AMEX" /> <img src="/img/icons/payment-Discover.png" alt="Discover" /> <img src="/img/icons/payment-Mastercard.png" alt="Mastercard" /> <img src="/img/icons/payment-Visa.png" alt="Visa" /> <img src="/img/icons/payment-JCB.png" alt="JCB" /></p>


                <?php
                mysqli_close($db);
                ?>



        </div>
        <!-- /#Content -->

        <div id="sidebar-rt">

            <div id="sidebar-content">
                <div class="sidebox databox">
                    <h2>HCP Course Details</h2>
                    <p><img src="/img/courses/hcp-course-2016-boston.jpg" alt="Exploring the Human Connectome" /></p>
                    <p>The 2016 HCP Course is designed for investigators who are interested in:</p>
                    <ul>
                        <li>using data being collected and distributed by HCP</li>
                        <li>acquiring and analyzing HCP-style imaging and behavioral data at your own institution</li>
                        <li>processing your own non-HCP data using HCP pipelines and methods</li>
                        <li>learning to use Connectome Workbench tools and the CIFTI connectivity data format</li>
                        <li>learning HCP multi-modal neuroimaging analysis methods, including those that combine MEG and MRI data</li>
                    </ul>
                    <p>&nbsp;</p>
                    <p>For more details, see the <a href="/courses/2016/exploring-the-human-connectome.php">HCP Course Website</a></p>
                </div>
                <!-- /sidebox -->
            </div>
        </div>
        <!-- /sidebar -->


    </div> <!-- middle -->

    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/footer.php'); ?>

</div>
<!-- end container -->

<script src="//www.google.com/jsapi?key=AIzaSyA5m1Nc8ws2BbmPRwKu5gFradvD_hgq6G0" type="text/javascript"></script>
<script type="text/javascript">
    /*
     *  How to restrict a search to a Custom Search Engine
     *  http://www.google.com/cse/
     */

    google.load('search', '1');

    function OnLoad() {
        // Create a search control
        var searchControl = new google.search.SearchControl();

        // Add in a WebSearch
        var webSearch = new google.search.WebSearch();

        // Restrict our search to pages from our CSE
        webSearch.setSiteRestriction('000073750279216385221:vnupk6whx-c');

        // Add the searcher to the SearchControl
        searchControl.addSearcher(webSearch);

        // tell the searcher to draw itself and tell it where to attach
        searchControl.draw(document.getElementById("search"));

        // execute an inital search
        // searchControl.execute('');
    }

    google.setOnLoadCallback(OnLoad);

</script>


<!-- google analytics -->
<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-18630139-1']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

</script>
</body>
</html>
