<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Registration Received | Human Connectome Project</title>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/header-includes.php'); ?>

</head>

<body>
<!-- site container -->
<div id="site_container">

    <div id="NIH_header2">
<span>
<a href="http://www.neuroscienceblueprint.nih.gov/connectome/" title="NIH Human Connectome Project" style="color:#b4cfff; text-decoration:none;">NIH Blueprint: The Human Connectome Project</a></span>
    </div>

    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/storefront-topnav.php'); ?>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/db-login.php'); ?>


    <!-- Breadcrumbs -->
    <div id="breadcrumbs">
        <p>&raquo; <a href="/courses">HCP Courses</a> &gt; registration confirmation</p></div>
    <!-- end breadcrumbs -->

    <!-- "Middle" area contains all content between the top navigation and the footer -->
    <div id="middle">
        <div id="content">

            <?php
            $orderId = $_POST['HCP-ORDERID'];
            if ($orderId) {
                ?>

                <?php

                /* check for successful transaction */
                $success = $_POST['respmessage'];
                if ($success == 'SUCCESS') {

                    // check for order type. If this is a custom order, forward results to the appropriate handling page
                    $q = "SELECT order_type,drive_recycle FROM orders WHERE id='".$orderId."';";
                    $r = mysqli_query($db,$q) or die($q);
                    $orderInfo = mysqli_fetch_array($r);

                    if ($orderInfo['order_type'] == 'custom') :
                        // build a form out of every variable in POST
                        ?>
                        <form action="/customcharge/process-custom-charge.php" method="post" id="redirect-form">
                            <?php foreach ($_POST as $key => $value): ?>
                                <input type="hidden" name="<?php echo $key ?>" value="<?php echo $value ?>" />
                            <?php endforeach; ?>
                        </form>

                        <?php
                        // submit that form
                        die('<p>Forwarding to Registration Form Handler</p><script>$("#redirect-form").submit();</script>');
                    endif;

                    ?>

                    <h2>HCP Course Registration: Success</h2>
                    <p>Thank you for your course registration. Your registration ID number is #<?php echo $orderId; ?> and your payment transaction ID is #<?php echo $_POST['tx'] ?>. Please save these for your records.</p>
                    <p>If any of this is incorrect, please contact us at <a href="mailto:orders@humanconnectome.org"><strong>orders@humanconnectome.org</strong></a> to correct it.</p>
                    <h3>Registration Summary</h3>
                    <?php
                    // set variables
                    $items = $_POST['itemcode'];


                    $q = "SELECT * FROM receipt,orders WHERE receipt.order_id='".$orderId."' AND orders.id='".$orderId."';";
                    $r = mysqli_query ($db,$q) or die($q);
                    $order = mysqli_fetch_array($r);

                    /* update order status in DB -- set status according to shipping country. Register discount code, if found */
                    $orderStatus = "open";
                    $discountApply = ($_POST['ref5val1'] && $_POST['ref5type1'] == 'HCP_DISCOUNT') ? ", registration_discount='".$_POST['ref5val1']."'" : "";
                    $accommodationApply = "";

                    /* if accommodations are bundled into the cost, disentangle them for display */
                    if ($_POST['ref1type2'] && $_POST['ref1type2'] == 'HCP-ACCOMMODATION') :
                        $accommodationApply = ", registration_accommodation='".$_POST['ref3val2']."'";
                        $accommodationFee = json_decode($order['other_cost'],true);

                        $q = "INSERT INTO accommodation_registration (order_id,course_id,accommodation_id,customer_name,email,status) VALUES ('".$order['id']."','".$order['course_registered']."','".$_POST['ref3val2']."','".$order['customer_name']."','".$order['customer_email']."','open');";
                        $r = mysqli_query($db,$q) or die($q);
                    endif;

                    $q = "UPDATE orders SET status='".$orderStatus."', transaction_id='".$_POST['tx']."'".$discountApply." ".$accommodationApply." WHERE id='".$orderId."';";
                    $r = mysqli_query($db,$q) or die($q);

                    // don't store discounts in the receipt, unless we reduced our price charged
                    $q = "SELECT * FROM receipt,orders WHERE receipt.order_id='".$orderId."' AND orders.id='".$orderId."';";
                    $r = mysqli_query ($db,$q) or die($q);
                    $order = mysqli_fetch_array($r);

                    // if a discount was registered, reduce the eligibility count by one.
                    if (isset($_POST['ref5val1']) && $_POST['ref5type1'] == 'HCP_DISCOUNT') :
                        $q = "UPDATE discount_codes SET total_applied = total_applied +1, remaining_eligible = remaining_eligible-1 WHERE code_id = '".$_POST['ref5val1']."';";
                        $r = mysqli_query($db,$q) or die($q);
                    endif;

                    // build array of courses to query against
                    $q = "SELECT course_id FROM courses";
                    $r = mysqli_query ($db,$q) or die($q);
                    $courses = array();
                    while ($course = mysqli_fetch_array($r)) :
                        $courses[] = $course[0];
                    endwhile;

                    $items=array();

                    for ($i=1; $i<=$_POST['itemcnt']; $i++) :
                        // check for item code, then check for quantity of each drive ordered.
                        // stop before you get to the last itemcode, which is for shipping
                        $courseId = $_POST['ref3val'.$i];
                        $qty = $_POST['qty'.$i];

                        if (in_array($courseId,$courses)) :

                            $q = "SELECT * FROM courses WHERE course_id='".$courseId."';";
                            $r = mysqli_fetch_array(mysqli_query($db,$q));
                            //	echo "<p>".$itemcode." found in [".implode(", ",$releases)."]";
                            // add to items array
                            $items[$i]=array('course_id' => $courseId, 'course_title' => $r['course_title'], 'cost' => $_POST['amount'.$i], 'regtype' => $_POST['ref2val'.$i]);
                        else :
                            echo "<p class='hidden'>".$courseId." not found in [".implode(", ",$courses)."]";
                        endif;
                    endfor;

                    ?>
                    <!-- report to customer -->
                    <table cellspacing="1" cellpadding="3" width="100%" class="sortable">
                        <thead>
                        <tr><th style="background-color:#06c;">Order ID: <?php echo $orderId ?></th>
                            <th>Registration Type</th></tr>
                        </thead>
                        <tbody>

                        <?php
                        foreach ($items as $key => $item) {
                            if (in_array($item['course_id'],$courses)) :
                                ?>
                                <tr><td><?php echo $item['course_title'] ?></td><td><?php echo $item['regtype'] ?></td></tr>
                                <?php
                            else :
                                echo "<p>$item not found in [".implode(", ",$courses)."]";
                            endif;
                        } // end item listing

                        ?>
                        <tr><th colspan = 2>Item costs</th></tr>
                        <tr><td>Total Registration Cost:</td><td align="right">$<?php echo number_format($order['registration_cost'],2) ?></td></tr>
                        <?php if ($_POST['ref1type2'] && $_POST['ref1type2'] == 'HCP-ACCOMMODATION') : ?>
                            <tr><td><?php echo trim($accommodationFee['charge_title']) ?>: </td><td align="right">$<?php echo number_format( floatval($accommodationFee['charge_amount']),2); ?></td> </tr>
                        <?php endif ?>
                        <?php if ($order['sales_tax']) : ?>
                            <tr><td>Sales Tax: </td><td align="right">$<?php echo number_format($order['sales_tax'],2); ?></td></tr>
                        <?php endif; ?>
                        <tr><td>Transaction Fee: </td><td align="right">$<?php echo number_format($order['cashnet_fee'],2); ?></td></tr>
                        <tr><td colspan="2" align="right" style="padding-top:10px;"><p><strong>Total cost: $<?php echo number_format($order['registration_cost'] + $order['cashnet_fee'] + floatval($accommodationFee['charge_amount']),2); ?></strong></p></td></tr>
                        <?php if ($order['customer_notes']) : ?>
                            <tr><td>Special Instructions: </td><td><em><?php echo $order['customer_notes']; ?></em></td></tr>
                        <?php endif; ?>


                        </tbody>
                    </table>


                    <?php
                    /* if order was unsuccessful */

                } else {

                    /* update order DB */
                    $q = "UPDATE orders SET status='failed',transaction_id='".$_POST['tx']."' WHERE id='".$orderId."';";
                    $r = mysqli_query($db,$q) or die($q);
                    ?>
                    <h2>Connectome In A Box: Problem With Your Order</h2>
                    <p>There was a problem with your order. Please contact <a href="mailto:orders@humanconnectome.org"><strong>orders@humanconnectome.org</strong></a> and inquire about order ID #<?php echo $_POST['HCP-ORDERID'] ?> to rectify it.</p>
                    <p><strong>Error Code <?php echo $_POST['result'] ?>:</strong> <span class="error"><?php echo $_POST['respmessage'] ?> </span></p>
                    <?php
                }
                ?>

                <div style="background-color:#f0f0f0; padding:6px; display:none;">
                    <?php

                    function printArray($array,$title){
                        echo "<h3>".$title."</h3>";
                        echo "<ul>";
                        foreach ($array as $key => $value){
                            echo "<li>$key => $value</li>";
                            if(is_array($value)){ //If $value is an array, print it as well!
                                printArray($value);
                            }
                        }
                        echo "</ul>";
                    }
                    printArray($order,"Data in Order table");
                    echo '<hr />';
                    printArray($_POST,"POST data received through form");

                    ?>
                </div>

                <p style="padding-bottom:6px; border-bottom: 1px #ccc solid">&nbsp;</p>
                <?php
                mysqli_close($db);
                ?>

                <?php
            } else {
                ?>
                <h2>Error</h2>
                <p>No order information. If you think you got to this page in error, please contact orders@humanconnectome.org</p>

                <div style="background-color:#f0f0f0; padding:6px; display:none;">
                    <?php

                    function printArray($array){
                        echo "<ul>";
                        foreach ($array as $key => $value){
                            echo "<li>$key => $value</li>";
                            if(is_array($value)){ //If $value is an array, print it as well!
                                printArray($value);
                            }
                        }
                    }
                    printArray($order);
                    echo '<hr />';
                    printArray($_POST);

                    ?>
                </div>
                <?php
            }
            ?>
        </div>
        <!-- /#Content -->

        <div id="sidebar-rt">

            <div id="sidebar-content">
                <?php include_once($_SERVER['DOCUMENT_ROOT'].'/incl/sidebar-2019-courseinfo.php'); ?>
            </div>
        </div>
        <!-- /sidebar -->


    </div> <!-- middle -->

    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/footer.php'); ?>

</div>
<!-- end container -->

<script src="//www.google.com/jsapi?key=AIzaSyA5m1Nc8ws2BbmPRwKu5gFradvD_hgq6G0" type="text/javascript"></script>
<script type="text/javascript">
    /*
     *  How to restrict a search to a Custom Search Engine
     *  http://www.google.com/cse/
     */

    google.load('search', '1');

    function OnLoad() {
        // Create a search control
        var searchControl = new google.search.SearchControl();

        // Add in a WebSearch
        var webSearch = new google.search.WebSearch();

        // Restrict our search to pages from our CSE
        webSearch.setSiteRestriction('000073750279216385221:vnupk6whx-c');

        // Add the searcher to the SearchControl
        searchControl.addSearcher(webSearch);

        // tell the searcher to draw itself and tell it where to attach
        searchControl.draw(document.getElementById("search"));

        // execute an inital search
        // searchControl.execute('');
    }

    google.setOnLoadCallback(OnLoad);

</script>


<!-- google analytics -->
<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-18630139-1']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

</script>
</body>
</html>
