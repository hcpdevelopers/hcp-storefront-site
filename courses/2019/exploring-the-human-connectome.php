<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>HCP Course 2019: Exploring the Human Connectome | Human Connectome Project</title>
    <meta http-equiv="Description" content="Course registration and curriculum for the 2019 HCP course: Exploring the Human Connectome, to be held July 8-12 in Portland, Oregon." />
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/header-includes.php'); ?>

    <style type="text/css">
        h2 { border-top: 1px solid #ccc !important; margin-top: 1em; padding-top: 1em; }
        #content li a { color: #444; border-bottom: 1px dashed #ccc; }
        #content li a:hover { color: #0168db; text-decoration: none; }
        .download-button { background-color: #0030a0; border: 1px solid #111; border-radius: 3px; box-shadow: 1px 2px #aaa; color: #fff; font: 16px Arial, sans-serif; font-weight:bold; margin-bottom: 1em; padding: 6px 10px; text-align: center; width: 100%; }
        .download-button span { font-size: 11px; }
        .download-button:hover { background-color: #0168db; box-shadow: none; cursor: pointer; }
        .download-button.disabled { background-color: #808080; }
        .download-button.disabled:hover { background-color: #909090; }
        .coursetime, .no-course { display:none; visibility: hidden;}
    </style>
</head>

<body>
<!-- site container -->
<div id="site_container">

    <div id="NIH_header2">
<span>
<a href="http://www.neuroscienceblueprint.nih.gov/connectome/" title="NIH Human Connectome Project" style="color:#b4cfff; text-decoration:none;">NIH Blueprint: The Human Connectome Project</a></span>
    </div>

    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/storefront-topnav.php'); ?>

    <!-- banner -->
    <div id="bannerOpen" class="data-banner" style="background: url('/img/courses/course-banner-HCP2019.jpg') left top no-repeat #fff;  background-size: 100%; height: 204px;"></div>

    <!-- Breadcrumbs -->
    <div id="breadcrumbs">
        <p>&raquo; <a href="/courses/">hcp course materials</a> &gt; Exploring the Human Connectome (2019)</p></div>
    <!-- end breadcrumbs -->

    <!-- "Middle" area contains all content between the top navigation and the footer -->
    <div id="middle">

        <div id="content-wide">
            <h1>Exploring the Human Connectome</h1>
            <a name="practical"></a>
            <h2>HCP Course Practical Data and Software</h2>
            <div style="display: inline-block; vertical-align: top; width: 600px; ">
                <p>Due to technical issues with sharing the large amount of data and virtual machine (VM) used at the course, we will not be distributing the 2019 course computer. We are working on changes to our course format for 2020 that will allow us to more easily distribute practical data and tools. </p>
            </div>

            <a name="schedule"></a>
            <div>
                <h2>Course Schedule</h2>
                <p><strong><a href="https://wustl.box.com/v/hcp-2019-all-lectures" target="_blank">Download All Lectures</a></strong> | <strong><a href="https://wustl.box.com/v/hcp-2019-all-practicals" target="_blank">Download All Practicals</a></strong></p>
                <p><strong>Day 1: Monday, July 8</strong></p>
                <table border="0" cellspacing="0" cellpadding="0" class="hcp-schedule" style="width: 100%">
                    <thead>
                    <tr>
                        <th>Type</th><th width="420">Course (Presenter)</th><th class="coursetime">Time</th><th class="resource">Resources</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr class="no-course">
                        <td colspan="2">Course Registration and Coffee</td><td class="coursetime" align="right">7:30 am</td><td class="resource">...</td>
                    </tr>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture 1</td><td>Intro and Overview (David Van Essen)</td><td class="coursetime" align="right">8:30 am</td>
                        <td class="resource"><a target="_blank" href="https://wustl.box.com/v/hcp-2019-lecture-01">https://wustl.box.com/v/hcp-2019-lecture-01</a></td>
                    </tr>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture 2</td><td>HCP Data Acquisition (Michael Harms)</td><td class="coursetime" align="right">9:30 am</td><td class="resource"><a target="_blank" href="https://wustl.box.com/v/hcp-2019-lecture-02">https://wustl.box.com/v/hcp-2019-lecture-02</a></td>
                    </tr>
                    <tr class="no-course">
                        <td colspan="2">Coffee / Tea Break</td><td class="coursetime" align="right">10:30 am</td><td class="resource">...</td>
                    </tr>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture 3</td><td>HCP Pipelines Orientation (Alan Anticevic)</td><td class="coursetime" align="right">11:00 am</td><td class="resource"><a target="_blank" href="https://wustl.box.com/v/hcp-2019-lecture-03">https://wustl.box.com/v/hcp-2019-lecture-03</a></td>
                    </tr>
                    <tr class="no-course">
                        <td colspan="2">Lunch <em>(not provided, make own arrangements)</em></td><td class="coursetime" align="right">11:45 am</td><td class="resource">...</td>
                    </tr>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture 4</td><td>Structural MRI: Precise Neuroanatomical Localization (Matt Glasser)</td><td class="coursetime" align="right">1:15 pm</td><td class="resource"><a target="_blank" href="https://wustl.box.com/v/hcp-2019-lecture-04">https://wustl.box.com/v/hcp-2019-lecture-04</a></td>
                    </tr>
                    <tr class="hcp-schedule-practical">
                        <td>Practical 1</td><td>Pipelines Under The Hood (Tim Brown)</td><td class="coursetime" align="right">2:15 pm</td><td class="resource"><a target="_blank" href="https://wustl.box.com/v/hcp-2019-practical-01a">https://wustl.box.com/v/hcp-2019-practical-01a</a><br><a target="_blank" href="https://wustl.box.com/v/hcp-2019-practical-01b">https://wustl.box.com/v/hcp-2019-practical-01b</a></td>
                    </tr>
                    <tr class="no-course">
                        <td colspan="2">Coffee / Tea Break</td><td class="coursetime" align="right">3:45 pm</td><td class="resource">...</td>
                    </tr>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture 5</td><td>Turnkey Pipelines in a Container (Alan Anticevic)</td><td class="coursetime" align="right">4:15 pm</td><td class="resource"><a target="_blank" href="https://wustl.box.com/v/hcp-2019-lecture-05">https://wustl.box.com/v/hcp-2019-lecture-05</a></td>
                    </tr>
                    <tr class="hcp-schedule-practical">
                        <td>Practical 2</td><td>Data Quality (Michael Harms)</td><td class="coursetime" align="right">5:00 pm</td><td class="resource"><a target="_blank" href="https://wustl.box.com/v/hcp-2019-practical-02a">https://wustl.box.com/v/hcp-2019-practical-02a</a><br><a target="_blank" href="https://wustl.box.com/v/hcp-2019-practical-02b">https://wustl.box.com/v/hcp-2019-practical-02b</a></td>
                    </tr>
                    <tr class="no-course">
                        <td colspan="2">Opening Reception (Location TBA)</td><td class="coursetime" align="right">6:30 - 8:30 pm</td><td class="resource">...</td>
                    </tr>
                    </tbody>
                </table>

                <p><strong>Day 2: Tuesday, July 9</strong></p>
                <table border="0" cellspacing="0" cellpadding="0" class="hcp-schedule" style="width: 100%">
                    <thead>
                    <tr>
                        <th>Type</th><th width="420">Course (Presenter)</th><th class="coursetime">Time</th><th class="resource">Resources</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture 6</td><td>Brain Parcellation (Matt Glasser)</td><td class="coursetime" align="right">8:30 am</td><td class="resource"><a target="_blank" href="https://wustl.box.com/v/hcp-2019-lecture-06">https://wustl.box.com/v/hcp-2019-lecture-06</a></td>
                    </tr>
                    <tr class="hcp-schedule-practical">
                        <td>Practical 3</td><td>Group Parcellation (Jenn Elam)</td><td class="coursetime" align="right">9:30 am</td><td class="resource"><a target="_blank" href="https://wustl.box.com/v/hcp-2019-practical-03">https://wustl.box.com/v/hcp-2019-practical-03</a></td>
                    </tr>
                    <tr class="no-course">
                        <td colspan="2">Coffee / Tea Break</td><td class="coursetime" align="right">10:15 am</td><td class="resource">...</td>
                    </tr>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture 7</td><td>Multimodal Classification of Areas in Individuals (Matt Glasser)</td><td class="coursetime" align="right">10:45 am</td><td class="resource"><a target="_blank" href="https://wustl.box.com/v/hcp-2019-lecture-07">https://wustl.box.com/v/hcp-2019-lecture-07</a></td>
                    </tr>
                    <tr class="hcp-schedule-practical">
                        <td>Practical 4</td><td>Individual Brain Parcellation (Jenn Elam)</td><td class="coursetime" align="right">11:45 am</td><td class="resource"><a target="_blank" href="https://wustl.box.com/v/hcp-2019-practical-04">https://wustl.box.com/v/hcp-2019-practical-04</a></td>
                    </tr>
                    <tr class="no-course">
                        <td colspan="2">Lunch <em>(not provided, make own arrangements)</em></td><td class="coursetime" align="right">12:15 pm</td><td class="resource">...</td>
                    </tr>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture 8</td><td>rfMRI Preprocessing, Spatial &amp; Temporal Denoising (Matt Glasser)</td><td class="coursetime" align="right">1:30 pm</td><td class="resource"><a target="_blank" href="https://wustl.box.com/v/hcp-2019-lecture-08">https://wustl.box.com/v/hcp-2019-lecture-08</a></td>
                    </tr>
                    <tr class="hcp-schedule-practical">
                        <td>Practical 5</td><td>rfMRI Preprocessing &amp; Denoising (Jenn Elam)</td><td class="coursetime" align="right">2:45 pm</td><td class="resource"><a target="_blank" href="https://wustl.box.com/v/hcp-2019-practical-05">https://wustl.box.com/v/hcp-2019-practical-05</a></td>
                    </tr>
                    <tr class="no-course">
                        <td colspan="2">Coffee / Tea Break</td><td class="coursetime" align="right">3:30 pm</td><td class="resource">...</td>
                    </tr>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture 9</td><td>rfMRI Network Analysis Strategies (Janine Bijsterbosch)</td><td class="coursetime" align="right">4:00 pm</td><td class="resource"><a target="_blank" href="https://wustl.box.com/v/hcp-2019-lecture-09">https://wustl.box.com/v/hcp-2019-lecture-09</a></td>
                    </tr>
                    <tr class="hcp-schedule-practical">
                        <td>Practical 6</td><td>Running HCP Pipelines on Lifespan HCP Data (Alan Anticevic)</td><td class="coursetime" align="right">5:00 pm</td><td class="resource"><a target="_blank" href="https://wustl.box.com/v/hcp-2019-practical-06">https://wustl.box.com/v/hcp-2019-practical-06</a></td>
                    </tr>
                    <tr class="no-course">
                        <td colspan="2">OPTIONAL: Troubleshooting Pipelines Café, Using Your Own Data (Snacks Provided)</td><td class="coursetime" align="right">6:00 - 7:00 pm</td><td class="resource">...</td>
                    </tr>
                    </tbody>
                </table>

                <p><strong>Day 3: Wednesday, July 10</strong></p>
                <table border="0" cellspacing="0" cellpadding="0" class="hcp-schedule" style="width: 100%">
                    <thead>
                    <tr>
                        <th>Type</th><th width="420">Course (Presenter)</th><th class="coursetime">Time</th><th class="resource">Resources</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture 10</td><td>rfMRI Netmats &amp; Dual Regression (Janine Bijsterbosch)</td><td class="coursetime" align="right">8:30 am</td><td class="resource"><a target="_blank" href="https://wustl.box.com/v/hcp-2019-lecture-10">https://wustl.box.com/v/hcp-2019-lecture-10</a></td>
                    </tr>
                    <tr class="hcp-schedule-practical">
                        <td>Practical 7</td><td>rfMRI Netmats &amp; Dual Regression (Janine Bijsterbosch)</td><td class="coursetime" align="right">9:00 am</td><td class="resource"><a target="_blank" href="https://wustl.box.com/v/hcp-2019-practical-07">https://wustl.box.com/v/hcp-2019-practical-07</a></td>
                    </tr>
                    <tr class="no-course">
                        <td colspan="2">Coffee / Tea Break</td><td class="coursetime" align="right">10:30 am</td><td class="resource">...</td>
                    </tr>
                    <tr class="hcp-schedule-practical">
                        <td>Practical 8</td><td>Intro to <strong>wb_command</strong> (Tim Coalson)</td><td class="coursetime" align="right">11:00 am</td><td class="resource"><a target="_blank" href="https://wustl.box.com/v/hcp-2019-practical-08">https://wustl.box.com/v/hcp-2019-practical-08</a></td>
                    </tr>
                    <tr class="no-course">
                        <td colspan="2">Lunch <em>(not provided, make own arrangements)</em></td><td class="coursetime" align="right">11:30 am</td><td class="resource">...</td>
                    </tr>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture 11</td><td>Diffusion MRI, Distortion Correction &amp; DTI (Jesper Andersson)</td><td class="coursetime" align="right">1:00 pm</td><td class="resource"><a target="_blank" href="https://wustl.box.com/v/hcp-2019-lecture-11">https://wustl.box.com/v/hcp-2019-lecture-11</a></td>
                    </tr>
                    <tr class="hcp-schedule-practical">
                        <td>Practical 9</td><td>Diffusion MRI, Distortion Correction &amp; DTI (Jesper Andersson)</td><td class="coursetime" align="right">2:00 pm</td><td class="resource"><a target="_blank" href="https://wustl.box.com/v/hcp-2019-practical-06">https://wustl.box.com/v/hcp-2019-practical-06</a></td>
                    </tr>
                    <tr class="no-course">
                        <td colspan="2">Coffee / Tea Break</td><td class="coursetime" align="right">3:00 pm</td><td class="resource">...</td>
                    </tr>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture 12</td><td>Fiber Orientation Models &amp; Tractography Anlayses (Michiel Cottaar)</td><td class="coursetime" align="right">3:30 pm</td><td class="resource"><a target="_blank" href="https://wustl.box.com/v/hcp-2019-lecture-12">https://wustl.box.com/v/hcp-2019-lecture-12</a></td>
                    </tr>
                    <tr class="hcp-schedule-practical">
                        <td>Practical 10</td><td>Tractography (Michiel Cottaar)</td><td class="coursetime" align="right">4:30 pm</td><td class="resource"><a target="_blank" href="https://wustl.box.com/v/hcp-2019-practical-10">https://wustl.box.com/v/hcp-2019-practical-10</a></td>
                    </tr>
                    <tr class="no-course">
                        <td colspan="2">OPTIONAL: Troubleshooting Pipelines Café, Using Your Own Data (Snacks Provided)</td><td class="coursetime" align="right">5:30 - 6:30 pm</td><td class="resource">...</td>
                    </tr>
                    </tbody>
                </table>

                <p><strong>Day 4: Thursday, July 11</strong></p>
                <table border="0" cellspacing="0" cellpadding="0" class="hcp-schedule" style="width: 100%">
                    <thead>
                    <tr>
                        <th>Type</th><th width="420">Course (Presenter)</th><th class="coursetime">Time</th><th class="resource">Resources</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture 13</td><td>Regression, Prediction &amp; Permutation Analyses (Anderson Winkler)</td><td class="coursetime" align="right">8:30 am</td><td class="resource"><a target="_blank" href="https://wustl.box.com/v/hcp-2019-lecture-13">https://wustl.box.com/v/hcp-2019-lecture-13</a></td>
                    </tr>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture 14</td><td>Family Structure &amp; Heritability (Anderson Winkler)</td><td class="coursetime" align="right">9:30 am</td><td class="resource"><a target="_blank" href="https://wustl.box.com/v/hcp-2019-lecture-14">https://wustl.box.com/v/hcp-2019-lecture-14</a></td>
                    </tr>
                    <tr class="no-course">
                        <td colspan="2">Coffee / Tea Break</td><td class="coursetime" align="right">10:30 am</td><td class="resource">...</td>
                    </tr>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture 15</td><td>Task fMRI and Behavioral Measure Analyses (Greg Burgess)</td><td class="coursetime" align="right">11:00 am</td><td class="resource"><a target="_blank" href="https://wustl.box.com/v/hcp-2019-lecture-15">https://wustl.box.com/v/hcp-2019-lecture-15</a></td>
                    </tr>
                    <tr class="hcp-schedule-practical">
                        <td>Practical 11</td><td>Task fMRI Analysis (Greg Burgess)</td><td class="coursetime" align="right">11:30 am</td><td class="resource"><a target="_blank" href="https://wustl.box.com/v/hcp-2019-practical-11">https://wustl.box.com/v/hcp-2019-practical-11</a></td>
                    </tr>
                    <tr class="no-course">
                        <td colspan="2">Lunch <em>(not provided, make own arrangements)</em></td><td class="coursetime" align="right">12:00 pm</td><td class="resource">...</td>
                    </tr>
                    <tr class="hcp-schedule-practical">
                        <td>Practical 11</td><td>tfMRI and PALM (Greg Burgess, Anderson Winkler)</td><td class="coursetime" align="right">1:30 pm</td><td class="resource"><a target="_blank" href="https://wustl.box.com/v/hcp-2019-practical-11">https://wustl.box.com/v/hcp-2019-practical-11</a></td>
                    </tr>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture 16</td><td>Lifespan HCP: Development and Aging (Leah Somerville)</td><td class="coursetime" align="right">2:30 pm</td><td class="resource"><a target="_blank" href="https://wustl.box.com/v/hcp-2019-lecture-16">https://wustl.box.com/v/hcp-2019-lecture-16</a></td>
                    </tr>
                    <tr class="no-course">
                        <td colspan="2">Coffee / Tea Break</td><td class="coursetime" align="right">3:30 pm</td><td class="resource">...</td>
                    </tr>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture 17</td><td>ConnectomeDB, CCF &amp; NIMH Data Archive (Tim Brown, Jenn Elam)</td><td class="coursetime" align="right">4:00 pm</td><td class="resource"><a target="_blank" href="https://wustl.box.com/v/hcp-2019-lecture-17">https://wustl.box.com/v/hcp-2019-lecture-17</a></td>
                    </tr>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture 18</td><td>Demo: Neuroimaging Cloud Basics and NDA (Tim Brown)</td><td class="coursetime" align="right">5:00 - 6:00 pm</td><td class="resource"><a target="_blank" href="https://wustl.box.com/v/hcp-2019-lecture-18">https://wustl.box.com/v/hcp-2019-lecture-18</a></td>
                    </tr>
                    </tbody>
                </table>

                <p><strong>Day 5: Friday, July 12</strong></p>
                <table border="0" cellspacing="0" cellpadding="0" class="hcp-schedule" style="width: 100%">
                    <thead>
                    <tr>
                        <th>Type</th><th width="420">Course (Presenter)</th><th class="coursetime">Time</th><th class="resource">Resources</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture 19</td><td>HCP-MEG Connectome and Analyses (Linda Larson-Prior)</td><td class="coursetime" align="right">8:30 am</td><td class="resource"><a target="_blank" href="https://wustl.box.com/v/hcp-2019-lecture-19">https://wustl.box.com/v/hcp-2019-lecture-19</a></td>
                    </tr>
                    <tr class="no-course">
                        <td colspan="2">Coffee / Tea Break</td><td class="coursetime" align="right">10:00 am</td><td class="resource">...</td>
                    </tr>
                    <tr class="hcp-schedule-practical">
                        <td>Practical 12</td><td>Building <strong>wb_view</strong> Scenes and Sharing on BALSA (Jenn Elam)</td><td class="coursetime" align="right">10:30 am</td><td class="resource"><a target="_blank" href="https://wustl.box.com/v/hcp-2019-practical-12">https://wustl.box.com/v/hcp-2019-practical-12</a></td>
                    </tr>
                    <tr class="no-course">
                        <td colspan="2">Lunch <em>(not provided, make own arrangements)</em></td><td class="coursetime" align="right">12:00 pm</td><td class="resource">...</td>
                    </tr>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture 20</td><td>Clinical Connectome: Application of HCP Methods to Neuropsychiatric Illness (Alan Anticevic)</td><td class="coursetime" align="right">1:30 pm</td><td class="resource"><a target="_blank" href="https://wustl.box.com/v/hcp-2019-lecture-20">https://wustl.box.com/v/hcp-2019-lecture-20</a></td>
                    </tr>
                    <tr class="hcp-schedule-practical">
                        <td>Practical 13</td><td>HCP Pipelines Outputs and Results (Tim Brown)</td><td class="coursetime" align="right">2:30 pm</td><td class="resource"><a target="_blank" href="https://wustl.box.com/v/hcp-2019-practical-13">https://wustl.box.com/v/hcp-2019-practical-13</a></td>
                    </tr>
                    <tr class="no-course">
                        <td colspan="2">Coffee / Tea Break</td><td class="coursetime" align="right">3:30 pm</td><td class="resource">...</td>
                    </tr>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture 21</td><td>HCP and Beyond: Looking Back and Forward (David Van Essen)</td><td class="coursetime" align="right">3:45 - 4:45 pm</td><td class="resource"><a target="_blank" href="https://wustl.box.com/v/hcp-2019-lecture-21">https://wustl.box.com/v/hcp-2019-lecture-21</a></td>
                    </tr>
                    </tbody>
                </table>

            </div>

            <h2>Course Description</h2>
            <div style="float:right; width: 230px; margin-left: 30px; margin-bottom: 30px; padding: 0 15px 15px; border-left: 1px dotted #ccc; font-size:10px;">
                <p><strong>On This Page:</strong></p>
                <ul>
                    <li><a href="#registration">Registration &amp; Accommodation</a></li>
                    <li><a href="#invitationletter">Invitation Letter</a></li>
                    <li><a href="#faculty">Faculty</a></li>
                    <li><a href="#schedule">Preliminary Course Schedule</a></li>
                    <li><a href="#cancellation">Cancellation Policy</a></li>
                    <li><a href="#contact">Contact Information</a></li>
                </ul>
                <p><img src="/img/courses/hcp-course-2019-uplace-hotel.png" alt="University Place Hotel" width="235" height="132" /></p>
            </div>
            <div style="width: 600px;">
                <p><span style="font-size:16px; line-height:1.2;"><strong>Learn about multimodal neuroimaging data, analysis, visualization, and sharing tools of the Human Connectome Project </strong></span> </p>
                <p>We are pleased to announce the <strong>2019 HCP Course: &quot;Exploring the Human Connectome&quot;</strong>, to be held <strong>July 8 &ndash; 12, 2019</strong> at the at the <a href="https://www.uplacehotel.com/" target="_blank">University Place Hotel and Conference Center</a> at Portland State University in Portland, Oregon, USA.</p>
                <p>This course is designed for investigators who are interested  in:</p>
                <ul>
                    <li>using HCP-style data currently available from the young adult HCP and HCP Lifespan (Development and Aging) projects</li>
                    <li>acquiring and analyzing HCP-style imaging and behavioral data at your own institution</li>
                    <li>processing your own non-HCP data (including legacy data) using HCP pipelines and methods</li>
                    <li>learning to use Connectome Workbench tools and share data using the BALSA imaging database</li>
                    <li>learning HCP multi-modal neuroimaging analysis methods, including those that combine MEG and MRI data </li>
                    <li>positioning yourself to capitalize on HCP-style data forthcoming from large-scale projects currently collecting data (e.g., Lifespan HCP development and aging and Connectomes Related to Human Disease projects)</li>
                    <li>learning how to obtain data from the NIMH Data Archive (NDA) and setup processing in an Amazon Web Services (AWS) environment</li>
                </ul>
                <p>This 5-day intensive course will provide training in the acquisition, analysis and visualization of freely-available data from the Human Connectome Project using methods and informatics tools developed by the WU-Minn-Oxford HCP consortium. Participants will learn how to acquire, analyze, visualize, and interpret data from four major MR modalities (structural MR, resting-state fMRI, diffusion imaging, task-evoked fMRI) plus magnetoencephalography (MEG) and extensive behavioral data.  Lectures and computer-based practicals will provide grounding in neurobiological as well as methodological issues involved in interpreting multimodal data, and will span the range from single-voxel/vertex to brain network analysis approaches. </p>
                <p>The course is open to graduate students, postdocs, faculty, non-profit and industry participants.  The course is aimed at both new and current users of HCP data, methods, and tools, and will cover both basic and advanced topics. Prior experience in human neuroimaging or in computational analysis of brain networks is desirable, preferably including familiarity with FSL and Freesurfer software. </p>
                <p>All lectures and printed material will be in English.</p>
                <p>All practicals will be conducted on Linux-based computers provided by the course with all necessary data and software. Attendees are welcome to bring their own laptops and other electronics, but we will not be able to support running course practicals on personal computers at the course.</p>
                <p>To get a sense of the material covered, check out the <a href="/courses/2017">2017</a> and <a href="/courses/2018">2018 course materials</a> (lecture slide PDFs, practical instructions, software and practical datasets in a virtual machine) currently available for download. Each year the course is updated with the latest developments and analysis recommendations from HCP.</p>
            </div>
            <a name="registration"></a>
            <a name="accommodations"></a>
            <div style="width: 600px;">
                <h2>Registration &amp; Accommodation Information</h2>
                <p>Registration (in US dollars) is $800 for PhD/MSc/undergrad students, $1300 for postdocs and other non-student attendees (including non-profit affiliated attendees), and $2300 for commercial attendees.</p>
                <p>Course registration includes lectures, computer practical sessions, coffee/tea breaks each day, a Monday evening kickoff reception, and a course book containing the lecture slides and practical instructions. </p>
                <p>Spaces are limited, registration will remain open until all spaces are filled. If registration fills, we will open a waiting list for those interested in being contacted if there are cancellations.</p>
                <p>The registration fee does not include accommodation (see below), or meals; there are many cafés and restaurants nearby in Portland and an onsite restaurant at the conference center.</p>
                <p>To register and pay, visit the <a href="/courses/course-registration.php">on-line registration page</a>. <strong>We are only able to take payment by credit card</strong> (sorry about this). Please note that your registration is not complete until you have finalized the payment (entering form information and then proceed to checkout and payment).</p>
                <a name="invitationletter"></a>
                <p>If you require an invitation letter as part of a travel visa application, please download <a href="HCP_Course_2019_Invitation_Letter.pdf" title="download invitation letter PDF">this letter</a> and fill in your contact information.</p>
            </div>
            <div style="float:right; width: 230px; margin-left: 30px; margin-bottom: 30px; padding: 0 15px 15px; border-left: 1px dotted #ccc; font-size:10px;">
                <p><img src="/img/courses/hcp-course-2019-broadway-hall.jpg" alt="Broadway Hall" width="235" height="157" /></p>
                <p><em>Typical dorm room at Broadway Residence Hall</em></p>
            </div>
            <div style="width: 600px">
                <h3>Accommodation Information</h3>
                <p>Due to the high cost and limited availability of accommodation in downtown Portland during the week of the course, we are offering two accommodation options to course attendees that we have blocked for 6 nights (check-in Sun July 7, check-out Sat July 13, 2019) at two locations: 1) onsite at the University Place Hotel and 2) at the Broadway Residence Hall on the Portland State campus, just 3 blocks away from the University Place course venue.</p>

                <h3>Accommodations Options</h3>
                <ol>
                    <li>
                        <strong><a href="https://www.uplacehotel.com/aboutus-amenities/" target="_blank" title="UPlace Hotel Amenities">University Place Hotel and Conference Center</a></strong>, 310 SW Lincoln Street, Portland, OR 97201 (<a href="https://goo.gl/maps/WwNsjG2vJzD2" target="_blank">map</a>): Standard King or Double-Double Rooms with hot breakfast included are available at the discounted rate of $128/night ($148/nt inclusive of tax). Book by June 15, 2019 to secure these rates. Two people can share these rooms but the reservation must be paid in one payment. Parking is $18/day additional. <br><br>
                        To make a reservation, call (503) 221-0140 Ext. 1 and tell reservations that you are with the "Human Connectome Project" room block. Or, go to our web page <a href="https://www.uplacehotel.com/" target="_blank">https://www.uplacehotel.com/</a>, select the booking dates then enter promotion code type in the code "BOOK NOW".
                    </li>
                    <li>
                        <strong><a href="https://www.pdx.edu/housing/broadway" title="pdx.edu/housing/broadway" target="_blank">Broadway Residence Hall at Portland State</a></strong>, 1948 SW Broadway, Portland, OR 97201 (<a href="https://goo.gl/maps/xqb7qmkpWuR2" target="_blank">map</a>): Double rooms with two twin beds and a private bathroom (towels and linens provided) are available at the discounted rate of $72/night, inclusive of tax ($432 for 6 nights). Two people can share these rooms but the reservation must be paid in one payment. If needed, parking ($15/day) and meals (Breakfast- $9/day, Lunch- $10.25/day, Dinner- $12.00/day) can be purchased as options when making your reservation.<br><br>
                        To make a reservation, go to the <a href="https://housingportal.pdx.edu/StarRezPortalConference/Go/Conferences/303" target="_blank">HCP 2019 Summer Housing and Conferences online portal</a>, and click “Sign Up".
                    </li>
                </ol>

                <p><strong>Note: Accommodation is in high demand in Portland in the summer. We recommend booking your lodging as soon as possible!</strong></p>

                <div class="column-third">
                    <a href="/courses/course-registration.php"><button>Register Now!</button></a>
                </div>
            </div>

            <a name="faculty"></a>
            <div style="width: 600px;">
                <h2>Faculty</h2>
                <p><strong>Speakers and practical tutors to include:</strong> David Van Essen, Matt Glasser, Michael Harms, Janine Bijsterbosch, Timothy Brown, Tim Coalson, Jesper Andersson, Michiel Cottaar, Anderson Winkler, Greg Burgess, Leah Somerville, Alan Anticevic, Linda Larson-Prior and Jennifer Elam.</p>
            </div>

            <a name="cancellation"></a>
            <div style="width: 600px;">
                <h2>Cancellation Policy</h2>
                <p>Please register with care. We have made commitments on costs that cannot be changed the closer we approach to the course date. Here is our refund policy:</p>
                <ul>
                    <li>Full refunds available on registration cancellations up to May 31, 2019</li>
                    <li>50% refunds available on registration cancellations from June 1 to June 14, 2019</li>
                    <li>No refunds of any kind for any cancellation on June 15, 2019 or beyond</li>
                </ul>
            </div>

            <a name="contact"></a>
            <div style="width: 600px;">
                <h2>Contact Information</h2>
                <p>If you have any questions, please contact us at: <a href="mailto:hcpcourse@humanconnectome.org">hcpcourse@humanconnectome.org</a></p>
                <p>We look forward to seeing you in Portland! </p>
            </div>

        </div>

        <!-- Primary page Content -->


    </div> <!-- middle -->

    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/footer.php'); ?>


</div>
<!-- end container -->

<script src="https://www.google.com/jsapi?key=AIzaSyA5m1Nc8ws2BbmPRwKu5gFradvD_hgq6G0" type="text/javascript"></script>

<!-- google analytics -->
<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-18630139-1']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

</script>
</body>
</html>
