<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>HCP Course 2018: Exploring the Human Connectome | Human Connectome Project</title>
    <meta http-equiv="Description" content="Course registration and curriculum for the 2018 HCP course: Exploring the Human Connectome, to be held June 19-23 in Vancouver." />
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/header-includes.php'); ?>

</head>

<body>
<!-- site container -->
<div id="site_container">

    <div id="NIH_header2">
<span>
<a href="http://www.neuroscienceblueprint.nih.gov/connectome/" title="NIH Human Connectome Project" style="color:#b4cfff; text-decoration:none;">NIH Blueprint: The Human Connectome Project</a></span>
    </div>

    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/storefront-topnav.php'); ?>

    <!-- banner -->
    <div id="bannerOpen" class="data-banner" style="background: url('/img/courses/course-banner-HCP2018.jpg') left bottom no-repeat #fff;  background-size: 100%; height: 204px;"></div>

    <!-- Breadcrumbs -->
    <div id="breadcrumbs">
        <p>&raquo; <a href="/courses/">hcp course materials</a> &gt; Exploring the Human Connectome (2018)</p></div>
    <!-- end breadcrumbs -->

    <!-- "Middle" area contains all content between the top navigation and the footer -->
    <div id="middle">

        <div id="content-wide">
            <style>
                h2 { border-top: 1px solid #ccc !important; margin-top: 1em; padding-top: 1em; }
                #content li a { color: #444; border-bottom: 1px dashed #ccc; }
                #content li a:hover { color: #0168db; text-decoration: none; }
                a.download-button { background-color: #0030a0; border: 1px solid #111; border-radius: 3px; box-shadow: 1px 2px #aaa; color: #fff; font: 16px Arial, sans-serif; font-weight:bold; margin-bottom: 1em; padding: 6px 10px; text-align: center; width: 100%; }
                a.download-button:hover { text-decoration: none;}
                a.download-button span { font-size: 11px; }
                a.download-button:hover { background-color: #0168db; box-shadow: none; cursor: pointer; }
                a.download-button.disabled { background-color: #808080; }
                a.download-button.disabled:hover { background-color: #909090; }

            </style>
            <h1>Exploring the Human Connectome</h1>
            <a name="practical"></a>
            <h2>HCP Course Practical Data and Software</h2>
            <div style="display: inline-block; vertical-align: top; width: 600px; ">
                <p>Unfortunately, due to University-wide network changes, the 2018 HCP Course Practical Data and Software is no longer available as a downloadable Virtual Machine (VM). We expect to be able to remedy this situation for distribution of the 2019 course VM. </p>
            </div>


            <a name="schedule"></a>
            <div>
                <h2>Course Schedule</h2>
                <p><strong>Day 1: Monday, Jun 25</strong></p>
                <table border="0" cellspacing="0" cellpadding="0" class="hcp-schedule" style="width: 100%">
                    <thead>
                    <tr>
                        <th>Type</th><th width="420">Course (Presenter)</th><th>Resources</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture</td><td>Intro and Overview (David Van Essen)</td><td>PDF: <a target="_blank" href="https://wustl.box.com/s/m4ai7fsgvs3im8rp7oy539kp75ubr395">https://wustl.box.com/s/m4ai7fsgvs3im8rp7oy539kp75ubr395</a> </td>
                    </tr>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture</td><td>HCP Data Acquisition (Michael Harms)</td><td>PDF: <a target="_blank" href="https://wustl.box.com/s/uif4k629eub2c7xf77hfwlhgals5xwoe">https://wustl.box.com/s/uif4k629eub2c7xf77hfwlhgals5xwoe</a></td>
                    </tr>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture</td><td>Structural MRI: Precise Neuroanatomical Localization (Matt Glasser)</td><td>PDF: <a target="_blank" href="https://wustl.box.com/s/jfajt9jfw0br17n4l05wsklk6n63p17a">https://wustl.box.com/s/jfajt9jfw0br17n4l05wsklk6n63p17a</a></td>
                    </tr>
                    <tr class="hcp-schedule-practical">
                        <td>Practical</td><td>Introduction to <strong>wb_view</strong> (Jenn Elam)</td><td>PDF: <a target="_blank" href="https://wustl.box.com/s/baq0i7xeqe9qblzfyrd3xo6ifawv8j58">https://wustl.box.com/s/baq0i7xeqe9qblzfyrd3xo6ifawv8j58</a></td>
                    </tr>
                    <tr class="hcp-schedule-practical">
                        <td>Practical</td><td>Data Quality (Michael Harms)</td><td>
                            Lecture PDF: <a target="_blank" href="https://wustl.box.com/s/krekn5svmg4rgig2ossqqhuhcbrgg8c6">https://wustl.box.com/s/krekn5svmg4rgig2ossqqhuhcbrgg8c6</a><br>
                            Practical PDF: <a target="_blank" href="https://wustl.box.com/s/2aju7zxxh7ed3nets2efos64buflxlqs">https://wustl.box.com/s/2aju7zxxh7ed3nets2efos64buflxlqs</a></td>
                    </tr>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture</td><td>Group Brain Parcellation (Matt Glasser)</td><td>PDF: <a target="_blank" href="https://wustl.box.com/s/k7hf1bv3vaaav04ge3hdhvlh92ikvwok">https://wustl.box.com/s/k7hf1bv3vaaav04ge3hdhvlh92ikvwok</a></td>
                    </tr>
                    <tr class="hcp-schedule-practical">
                        <td>Practical</td><td>Introduction to <strong>wb_command</strong> and Group Parcellation (Tim Coalson, Jenn Elam)</td><td>PDF: <a target="_blank" href="https://wustl.box.com/s/dhzvm7chueuavgv3ch5iqm5pu5xy1dd9">https://wustl.box.com/s/dhzvm7chueuavgv3ch5iqm5pu5xy1dd9</a></td>
                    </tr>
                    </tbody>
                </table>

                <p><strong>Day 2: Tuesday, Jun 26</strong></p>
                <table border="0" cellspacing="0" cellpadding="0" class="hcp-schedule" style="width: 100%">
                    <thead>
                    <tr>
                        <th>Type</th><th width="420">Course (Presenter)</th><th>Resources</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture</td><td>Multimodal Classification of Areas in Individuals and Parcellation Validation (Matt Glasser)</td><td>PDF: <a target="_blank" href="https://wustl.box.com/s/tfhu8rnsxm9xppfqkzkspxgq0t47e4bp">https://wustl.box.com/s/tfhu8rnsxm9xppfqkzkspxgq0t47e4bp</a></td>
                    </tr>
                    <tr class="hcp-schedule-practical">
                        <td>Practical</td><td>Individual Brain Parcellation (Matt Glasser, Jenn Elam)</td><td>PDF: <a target="_blank" href="https://wustl.box.com/s/2uwrsb36ewzdt4u9rrlpntbn7c0m0fjx">https://wustl.box.com/s/2uwrsb36ewzdt4u9rrlpntbn7c0m0fjx</a></td>
                    </tr>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture</td><td>rfMRI Background, Preprocessing, Denoising (Steve Smith)</td><td>PDF: <a target="_blank" href="https://wustl.box.com/s/pzh0247sxrvv2o25bokub2m1u5uhoh2x">https://wustl.box.com/s/pzh0247sxrvv2o25bokub2m1u5uhoh2x</a></td>
                    </tr>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture</td><td>rfMRI Temporal Noise (Matt Glasser)</td><td>PDF: <a target="_blank" href="https://wustl.box.com/s/n90b8cut123dxgy76djzspkvcz35hn3n">https://wustl.box.com/s/n90b8cut123dxgy76djzspkvcz35hn3n</a></td>
                    </tr>
                    <tr class="hcp-schedule-practical">
                        <td>Practical</td><td>rfMRI Preprocessing, Denoising; ICA-based Parcellations (Steve Smith, Jenn Elam)</td><td>PDF: <a target="_blank" href="https://wustl.box.com/s/xfs2506iz6pa6t7bfhhkno3baphnppvy">https://wustl.box.com/s/xfs2506iz6pa6t7bfhhkno3baphnppvy</a></td>
                    </tr>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture</td><td>rfMRI Network Analysis Strategies (Steve Smith)</td><td>PDF: <a target="_blank" href="https://wustl.box.com/s/g8x1ke7ftprd4v1s37zuy1ya9e5djnqp">https://wustl.box.com/s/g8x1ke7ftprd4v1s37zuy1ya9e5djnqp</a></td>
                    </tr>
                    <tr class="hcp-schedule-practical">
                        <td>Practical</td><td>rfMRI Netmats and Dual Regression (Steve Smith)</td><td>PDF: <a target="_blank" href="https://wustl.box.com/s/7lk2wwoklrqkusn4gm45ainzitmdsod7">https://wustl.box.com/s/7lk2wwoklrqkusn4gm45ainzitmdsod7</a></td>
                    </tr>
                    </tbody>
                </table>

                <p><strong>Day 3: Wednesday, Jun 27</strong></p>
                <table border="0" cellspacing="0" cellpadding="0" class="hcp-schedule" style="width: 100%">
                    <thead>
                    <tr>
                        <th>Type</th><th width="420">Course (Presenter)</th><th>Resources</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr class="hcp-schedule-practical">
                        <td>Practical</td><td>HCP Pipelines Practical (Tim Brown)</td><td>PDF: <a target="_blank" href="https://wustl.box.com/s/4ybn3bjd0v8idzz9ljxekvdgetdz3zsg">https://wustl.box.com/s/4ybn3bjd0v8idzz9ljxekvdgetdz3zsg</a></td>
                    </tr>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture</td><td>Diffusion MRI, Distortion Correction and DTI (Jesper Andersson)</td><td>PDF: <a target="_blank" href="https://wustl.box.com/s/60lk5jmxl6m78l7dusrmpiyjja4nssgi">https://wustl.box.com/s/60lk5jmxl6m78l7dusrmpiyjja4nssgi</a></td>
                    </tr>
                    <tr class="hcp-schedule-practical">
                        <td>Practical</td><td>Diffusion MRI, Distortion Correction and DTI Practical (Jesper Andersson)</td><td>PDF: <a target="_blank" href="https://wustl.box.com/s/6aox0ugssgnsn4lquizfgb5w06eyz8s2">https://wustl.box.com/s/6aox0ugssgnsn4lquizfgb5w06eyz8s2</a></td>
                    </tr>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture</td><td>Fibre Orientation Models and Tractography Analysis (Matteo Bastiani)</td><td>PDF: <a target="_blank" href="https://wustl.box.com/s/7xpu63yof3s68oicwulwh9d5nhvuq8en">https://wustl.box.com/s/7xpu63yof3s68oicwulwh9d5nhvuq8en</a></td>
                    </tr>
                    <tr class="hcp-schedule-practical">
                        <td>Practical</td><td>Fibre Orientation Models and Tractography Analysis Practical (Matteo Bastiani)</td><td>PDF: <a target="_blank" href="https://wustl.box.com/s/wna2cu94pqgt8zskg687mj8zlmfj1pq7">https://wustl.box.com/s/wna2cu94pqgt8zskg687mj8zlmfj1pq7</a></td>
                    </tr>
                    <tr class="hcp-schedule-practical">
                        <td>Practical</td><td>Tractography and Tracer Connectivity (Chad Donahue)</td><td>PDF: <a target="_blank" href="https://wustl.box.com/s/qr9f53xd1kedr03ozcxjj8fjh15mopac">https://wustl.box.com/s/qr9f53xd1kedr03ozcxjj8fjh15mopac</a></td>
                    </tr>

                    </tbody>
                </table>

                <p><strong>Day 4: Thursday, Jun 28</strong></p>
                <table border="0" cellspacing="0" cellpadding="0" class="hcp-schedule" style="width: 100%">
                    <thead>
                    <tr>
                        <th>Type</th><th width="420">Course (Presenter)</th><th>Resources</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture</td><td>Regression, Prediction, and Permutation Analyses (Thomas Nichols)</td><td>PDF: <a target="_blank" href="https://wustl.box.com/s/ivtho1n8e29vrbvdpqsrxr7n1elhqbyh">https://wustl.box.com/s/ivtho1n8e29vrbvdpqsrxr7n1elhqbyh</a></td>
                    </tr>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture</td><td>Family Structure and Heritability (Thomas Nichols)</td><td>PDF: <a target="_blank" href="https://wustl.box.com/s/seqjz60pxhtk5nquvgqxptm8uw6tu1v1">https://wustl.box.com/s/seqjz60pxhtk5nquvgqxptm8uw6tu1v1</a></td>
                    </tr>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture</td><td>Task fMRI and Behavioral Measure Analyses (Greg Burgess)</td><td>PDF: <a target="_blank" href="https://wustl.box.com/s/mtei9cdimm66fcbl5vxqp107kfe1vlmo">https://wustl.box.com/s/mtei9cdimm66fcbl5vxqp107kfe1vlmo</a></td>
                    </tr>
                    <tr class="hcp-schedule-practical">
                        <td>Practical</td><td>tfMRI and PALM (Greg Burgess, Anderson Winkler)</td><td>PDF: <a target="_blank" href="https://wustl.box.com/s/3b98a4mvgcwq0z0fhh4olvcqow1l5z95">https://wustl.box.com/s/3b98a4mvgcwq0z0fhh4olvcqow1l5z95</a></td>
                    </tr>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture</td><td>Lifespan HCP: Development and Aging (Leah Somerville)</td><td>PDF: <a target="_blank" href="https://wustl.box.com/s/1rxtybpup28xssm40mha1pbvo367va5w">https://wustl.box.com/s/1rxtybpup28xssm40mha1pbvo367va5w</a></td>
                    </tr>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture</td><td>HCP Informatics: Using ConnectomeDB and Amazon Web Services (Dan Marcus, Tim Brown)</td><td>PDF: <a target="_blank" href="https://wustl.box.com/s/cil1bnp62jooxw0g39fmjwdjuhk7mtci">https://wustl.box.com/s/cil1bnp62jooxw0g39fmjwdjuhk7mtci</a></td>
                    </tr>
                    <tr class="hcp-schedule-practical">
                        <td>Lecture</td><td>Cloud-based Processing Using HCP Pipelines and Amazon Web Services (Tim Brown)</td><td>PDF: <a target="_blank" href="https://wustl.box.com/s/cfhi0991kocgyi5isdrf0nm41otj91wc">https://wustl.box.com/s/cfhi0991kocgyi5isdrf0nm41otj91wc</a></td>
                    </tr>
                    </tbody>
                </table>

                <p><strong>Day 5: Friday, Jun 29</strong></p>
                <table border="0" cellspacing="0" cellpadding="0" class="hcp-schedule" style="width: 100%">
                    <thead>
                    <tr>
                        <th>Type</th><th width="420">Course (Presenter)</th><th>Resources</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture</td><td>Introduction to HCP-MEG Connectome (Linda Larson-Prior)</td><td>PDF: <a target="_blank" href="https://wustl.box.com/s/6u5wjv1km6ernoow5p0saaskyxlyox1t">https://wustl.box.com/s/6u5wjv1km6ernoow5p0saaskyxlyox1t</a></td>
                    </tr>
                    <tr class="hcp-schedule-practical">
                        <td>Practical</td><td>MEG Preprocessing, Channel and Source Analysis of tMEG and rMEG (Jan-Mathijs Schoffelen)</td><td>PDF: <a target="_blank" href="https://wustl.box.com/s/64afagzqmbr1gsosl3u7lca3beg70ksa">https://wustl.box.com/s/64afagzqmbr1gsosl3u7lca3beg70ksa</a></td>
                    </tr>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture</td><td>MEG Connectivity and Follow-up Analyses for HCP Data (Robert Oostenveld)</td><td>PDF: <a target="_blank" href="https://wustl.box.com/s/pi52p3grauc5t6qyps3t6gextsbft3om">https://wustl.box.com/s/pi52p3grauc5t6qyps3t6gextsbft3om</a></td>
                    </tr>
                    <tr class="hcp-schedule-practical">
                        <td>Practical</td><td>rMEG and tMEG Source-level Connectivity; Multimodal Integration (Jan-Mathijs Schoffelen)</td><td>PDF: <a target="_blank" href="https://wustl.box.com/s/q5mx6e95oe3ptimljfkgdzzcakmvf5m4">https://wustl.box.com/s/q5mx6e95oe3ptimljfkgdzzcakmvf5m4</a></td>
                    </tr>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture</td><td>Clinical Connectome: Application of HCP Methods to Neuropsychiatric Illness (Alan Anticevic)</td><td>PDF: <a target="_blank" href="https://wustl.box.com/s/7q7gwq0km3tax29y6wmvvbmu5g4hmau2">https://wustl.box.com/s/7q7gwq0km3tax29y6wmvvbmu5g4hmau2</a></td>
                    </tr>
                    <tr class="hcp-schedule-practical">
                        <td>Practical</td><td>Building <strong>wb_view</strong> scenes and sharing on BALSA (Jenn Elam, Chad Donahue)</td><td>PDF: <a target="_blank" href="https://wustl.box.com/s/r2livfxpsyfx9gxq9jloei6cbuywsm03">https://wustl.box.com/s/r2livfxpsyfx9gxq9jloei6cbuywsm03</a></td>
                    </tr>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture</td><td>HCP and Beyond: Looking Back and Forward (David Van Essen)</td><td>PDF: <a target="_blank" href="https://wustl.box.com/s/gvf67ezlze16k1yrcggyx0lhvdht1vur">https://wustl.box.com/s/gvf67ezlze16k1yrcggyx0lhvdht1vur</a></td>
                    </tr>
                    </tbody>
                </table>

            </div>

            <h2>Course Description</h2>
            <div style="float:right; width: 230px; margin-left: 30px; margin-bottom: 30px; padding: 0 15px 15px; border-left: 1px dotted #ccc; font-size:10px;">
                <p><strong>On This Page:</strong></p>
                <ul>
                    <li><a href="#registration">Registration &amp; Accommodation</a></li>
                    <li><a href="#invitationletter">Invitation Letter</a></li>
                    <li><a href="#faculty">Faculty</a></li>
                    <li><a href="#schedule">Course Schedule</a></li>
                    <li><a href="#cancellation">Cancellation Policy</a></li>
                    <li><a href="#contact">Contact Information</a></li>
                </ul>
                <p><img src="/img/courses/hcp-course-2018-blavatnik.jpg" alt="Blavatnik Center" width="235" height="235" /></p>
            </div>
            <div style="width: 600px;">
                <p><span style="font-size:16px; line-height:1.2;"><strong>Learn about multimodal neuroimaging data, analysis, visualization, and sharing tools of the Human Connectome Project </strong></span> </p>
                <p>We are pleased to announce the <strong>2018 HCP Course: &quot;Exploring the Human Connectome&quot;</strong>, to be held <strong>June 25 &ndash; 29, 2018</strong> at the <a href="https://www.bsg.ox.ac.uk/partner/contact-us" title="www.bsg.ox.ac.uk" target="_blank">Blavatnik School of Government</a>, at the University of Oxford, in Oxford, UK </p>
                <p>This course is designed for investigators who are interested  in:</p>
                <ul>
                    <li>using HCP-style data distributed by the Connectome Coordinating Facility (CCF) from the young adult (original) HCP and forthcoming projects</li>
                    <li>acquiring and analyzing HCP-style imaging and behavioral data at your own institution</li>
                    <li>processing your own non-HCP data using HCP pipelines and methods</li>
                    <li>learning to use Connectome Workbench tools and share data using the BALSA imaging database</li>
                    <li>learning HCP multi-modal neuroimaging analysis methods, including those that combine MEG and MRI data </li>
                    <li>positioning yourself to capitalize on HCP-style data forthcoming from large-scale projects currently collecting data (e.g., Lifespan HCP development and aging and Connectomes Related to Human Disease projects)</li>
                </ul>
                <p>This 5-day intensive course will provide training in the acquisition, analysis and visualization of freely-available data from the Human Connectome Project using methods and informatics tools developed by the WU-Minn-Oxford HCP consortium. Participants will learn how to acquire, analyze, visualize, and interpret data from four major MR modalities (structural MR, resting-state fMRI, diffusion imaging, task-evoked fMRI) plus magnetoencephalography (MEG) and extensive behavioral data.  Lectures and computer-based practicals will provide grounding in neurobiological as well as methodological issues involved in interpreting multimodal data, and will span the range from single-voxel/vertex to brain network analysis approaches. </p>
                <p>The course is open to graduate students, postdocs, faculty, and industry participants.  The course is aimed at both new and current users of HCP data, methods, and tools, and will cover both basic and advanced topics. Prior experience in human neuroimaging or in computational analysis of brain networks is desirable, preferably including familiarity with FSL and Freesurfer software. </p>
                <p>All lectures and printed material will be in English.</p>
                <p>To get a sense of the material covered, check out the <a href="/courses/2016">2016</a> and <a href="/courses/2017">2017 course materials</a> (lecture slide PDFs, practical instructions, software and practical datasets in a virtual machine) currently available for download. Each year the course is updated with the latest developments and analysis recommendations from HCP.</p>
            </div>
            <a name="registration"></a>
            <a name="accommodations"></a>
            <div style="width: 600px;">
                <h2>Registration &amp; Accommodation Information</h2>
                <p>Registration (in US dollars) is $800 for PhD/MSc/undergrad students, $1300 for postdocs and other non-student attendees, and $2400 for commercial attendees.</p>
                <p>Course registration includes lectures, computer practical sessions, coffee/tea breaks each day, an evening kickoff reception, and a course book containing the lecture slides and practical instructions. </p>
                <p>Spaces are limited, registration will remain open until all spaces are filled. If registration fills, we will open a waiting list for those interested in being contacted if there are cancellations.</p>
                <p>The Registration Only fee does not include accommodation (see below), or meals; there are many cafés and restaurants nearby in Oxford city.</p>
                <p>To register and pay, visit the on-line registration page. <strong>Please note that we are only able to take payment by credit card</strong> (sorry about this). Please note that your registration is not complete until you have finalized the payment (entering form information and then proceed to checkout and payment).</p>
                <a name="invitationletter"></a>
                <p>If you require an invitation letter as part of a travel visa application, please download <a href="HCP_Course_2018_Invitation_Letter.pdf" title="download invitation letter PDF">this letter</a> and fill in your contact information.</p>
            </div>
            <div style="float:right; width: 230px; margin-left: 30px; margin-bottom: 30px; padding: 0 15px 15px; border-left: 1px dotted #ccc; font-size:10px;">
                <img src="/img/courses/hcp-course-2018-worcester-dorm.jpg" width="230" height="140" alt="Worcester dorm room" /><br><br>
                <em>Typical Worcester dorm room</em>
            </div>
            <div style="width: 600px">
                <h3>New This Year: Registration With Accommodation</h3>
                <p>Due to the high cost and limited availability of accommodation in Oxford during the week of the course, we are offering a limited number of registrations with 6 nights of bed & breakfast accommodation (Sun June 24 – Fri June 29, 2018, check out Sat June 30), taxes included, for $675 additional. The rooms are single dormitory-style rooms with a private <em>en suite</em> bathroom at <a href="https://www.meetworcester.com/" title="www.meetworcester.com" target="_blank">Worcester College</a>, a short 5-10 minute walk from the course venue.</p>
                <p>Registration with Accommodation at Worcester (in US Dollars) is $1475 for PhD/MSc/undergrad students, $1975 for postdocs and other non-student attendees, and $3075 for commercial attendees.</p>
                <p><em><strong>Note:</strong> Accommodation is generally expensive and in high demand in Oxford at the end of June. We recommend booking your lodging as soon as possible!</em></p>

                <h3>Other Accommodations Options</h3>
                <ul>
                    <li>
                        <strong><a href="https://www.seh.ox.ac.uk/contact-us/maps" target="_blank" title="www.seh.ox.ac.uk">St. Edmund Hall</a> (15-20 min walk from Blavatnik School):</strong> 18 standard single rooms with shared bath facilities, breakfast and taxes included for £48/night (~$70 USD), are being held for HCP Course Attendees for the nights Sun June 24 – Fri June 29, 2018. Contact Susan McCarthy, for more information and to make a reservation: <a href="mailto:susan.mccarthy@seh.ox.ac.uk">susan.mccarthy@seh.ox.ac.uk</a>
                    </li>
                    <li>
                        <strong>Other dormitory-style options:</strong> <a href="http://www.oxfordrooms.co.uk" title="universityrooms.com" target="_blank">www.oxfordrooms.co.uk</a> allows searching for other dormitory-style rooms around Oxford.
                    </li>
                    <li>
                        <strong>AirBnB / Sublet options:</strong> <a href="http://oxfordapartment.co.uk/" title="Luxury short-term Oxford lets" target="_blank">oxfordapartment.co.uk</a> is a short term apartment rental site.
                    </li>
                </ul>
            </div>

            <a name="faculty"></a>
            <div style="width: 600px;">
                <h2>Faculty</h2>
                <p><strong>Speakers to include:</strong> David Van Essen, Matt Glasser, Michael Harms, Steve Smith, Tom Nichols, Timothy Brown, Jesper Andersson, Matteo Bastiani, Chad Donahue, Greg Burgess, Leah Somerville, Alan Anticevic, Linda Larson-Prior and Robert Oostenveld.</p>
                <p><strong>Practical Tutors:</strong> (in addition to speakers): Jenn Elam, Tim Coalson, Anderson Winkler, and Jan-Mathijs Schoffelen</p>
            </div>

            <a name="cancellation"></a>
            <div style="width: 600px;">
                <h2>Cancellation Policy</h2>
                <p>Please register with care. We have made commitments on costs that cannot be changed the closer we approach to the course date. Here is our refund policy:</p>
                <ul>
                    <li>Full refunds available on registration cancellations up to May 18, 2018</li>
                    <li>50% refunds available on registration cancellations from May 19 to May 31, 2018</li>
                    <li>No refunds of any kind for any cancellation on June 1, 2018 or beyond</li>
                </ul>
            </div>

            <a name="contact"></a>
            <div style="width: 600px;">
                <h2>Contact Information</h2>
                <p>If you have any questions, please contact us at: <a href="mailto:hcpcourse@humanconnectome.org">hcpcourse@humanconnectome.org</a></p>
                <p>We look forward to seeing you in Oxford! </p>
            </div>

        </div>

        <!-- Primary page Content -->


    </div> <!-- middle -->

    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/footer.php'); ?>


</div>
<!-- end container -->

<script src="https://www.google.com/jsapi?key=AIzaSyA5m1Nc8ws2BbmPRwKu5gFradvD_hgq6G0" type="text/javascript"></script>

<!-- google analytics -->
<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-18630139-1']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

</script>
</body>
</html>
