<!DOCTYPE html>
<html>
<head>
    <link href="/css/hcpv3.css" rel="stylesheet" type="text/css" />
    <link href="/css/sortable.css" rel="stylesheet" type="text/css" />
    <link href="/css/data.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="//code.jquery.com/jquery-1.8.3.min.js"></script></head>
<body style="background-color: #fff;">

<?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/db-login.php'); ?>

<?php
// create hash of current timestamp for a unique ID.
function base36converter() {
    $input = time();
    $base = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $output = '';

    while ($input > 0) {
        $output = $base[$input%36] . $output;
        $input = floor($input / 36);
    }

    return $output;
}
?>

<script type="text/javascript" src="/js/registration-validation.js"></script>
<div id="middle" style="width: inherit; padding: 0;">
    <div id="content" style="padding: 0; min-height: 250px;">
        <h3>Sign Up For The HCP Course Waiting List</h3>
        <?php

        if ($_POST) :
        // take post values and update order info
            $q = "INSERT INTO registration_waitlist (id,name,email,course_id) VALUES ('".base36converter()."','".$_POST['customer_name']."','".$_POST['customer_email']."','".$_POST['course_id']."');";
            $r = mysqli_query($db,$q) or die($q);

            echo "<p>Thank you, ".$_POST['customer_name'].". You have been added to the HCP Course Registration wait list. If a cancellation occurs or course capacity increases, you will be notified.</p>";

        else :

            $q = "SELECT * FROM registration_waitlist;";
            $r = mysqli_query($db,$q) or die ($q);
            $waitlist_members = mysqli_num_rows($r);
            ?>
            <p>Currently, there  <?php echo ($waitlist_members == 1) ? "is one person" : "are ".$waitlist_members." people" ?>  on the HCP Course registration waiting list. If you would like to add your name, please fill out the form below. </p>
            <form method="post" id="waitlist-form" action="waitlist-signup.php?courseId=<?php echo $_GET['courseId'] ?>" class="hcpForm">
                <input type="hidden" name="course_id" value="<?php echo $_GET['courseId'] ?>" />
                <div id="edit" class="hcpForm-wrapper address-panel">
                    <div class="hidden error-messages error-message-address error-message-general">
                        <p><strong>Errors Found:</strong></p>
                        <ul class="error-list">
                            <li>Things fall apart, the centre cannot hold.</li>
                        </ul>
                    </div>
                    <div>
                        <label for="customer_name">Name</label>
                        <input type="text" name="customer_name" style="width:485px" class="required" value="<?php echo $orderInfo["customer_name"]?>">
                    </div>
                    <div>
                        <label for="customer_email">Email Address</label>
                        <input type="text" name="customer_email" style="width:485px" class="required email" value="<?php echo $orderInfo["customer_email"]?>" />
                    </div>
                    <div class="hidden">
                        <label><input type="checkbox" value="sucker" name="dont_click_me" /> Don't Click Me</label>
                    </div>
                    <div style="margin-top: 1em;">
                        <input type="button" class="highlight" value="Add Name To Waiting List" onclick="validateMe('#waitlist-form',false,false,true)" /><br />
                        <a href="javascript:void" onclick="$('.address-panel').toggle();">Cancel</a>
                    </div>
                </div>
                <input type="hidden" id="itemcnt" value="1" />
            </form>
        <?php endif; ?>

    </div>
</div>

<?php
mysqli_close($db);
?>
</body>
</html>