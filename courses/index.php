<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>HCP Courses | Human Connectome Project</title>
    <meta http-equiv="Description" content="Register here for courses and training on working with Human Connectome data and research methods." />
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/header-includes.php'); ?>

</head>

<body>

<!-- site container -->
<div id="site_container">

    <div id="NIH_header2">
        <span><a href="http://www.neuroscienceblueprint.nih.gov/connectome/" title="NIH Human Connectome Project" style="color:#b4cfff; text-decoration:none;">NIH Blueprint: The Human Connectome Project</a></span>
    </div>

    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/storefront-topnav.php'); ?>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/db-login.php'); ?>

    <!-- banner -->
    <div id="bannerOpen" class="data-banner" style="background: url('/img/courses/course-banner-HCP2019.jpg') left top no-repeat #fff;  background-size: 100%; height: 204px;"></div>
    
    <!-- Breadcrumbs -->
    <div id="breadcrumbs">
        <p>&raquo; HCP Courses</p></div>
    <!-- end breadcrumbs -->

    <!-- "Middle" area contains all content between the top navigation and the footer -->
    <div id="middle">
        <div id="content">



            <h1>HCP Courses</h1>
            <p>The Human Connectome Project offers intensive training courses to the neuroscience community in the acquisition, analysis and visualization of data using methods and informatics tools developed by the WU-Minn HCP consortium. </p>

            <div id="courses">
                <div class="course-listing">
                    <h3>HCP Course 2019: Exploring the Human Connectome</h3>
                    <p><strong>Portland, Oregon. July 8-12, 2019.</strong></p>
                    <p><a href="/courses/2019/exploring-the-human-connectome.php"><button>View Course Info</button></a></p>
                </div>

                <div class="course-listing">
                    <h3>HCP Course 2018: Exploring the Human Connectome</h3>
                    <p><strong>Oxford, England. June 25-29, 2018.</strong></p>
                    <p><a href="/courses/2018/exploring-the-human-connectome.php"><button>View Course Info</button></a></p>
                </div>

                <div class="course-listing">
                    <h3>HCP Course 2017: Exploring the Human Connectome</h3>
                    <p><strong>Vancouver, BC, Canada. June 19-23, 2017.</strong></p>
                    <p><a href="/courses/2017/exploring-the-human-connectome.php"><button>View Course Info</button></a></p>
                </div>

                <div class="course-listing">
                    <h3>HCP Course 2016: Exploring the Human Connectome</h3>
                    <p><strong>Boston, MA. August 28-September 1, 2016.</strong></p>
                    <p><a href="/courses/2016/exploring-the-human-connectome.php"><button>View Course Info</button></a> </p>
                </div>

                <div class="course-listing">
                    <h3>HCP Course 2015: Exploring the Human Connectome</h3>
                    <p><strong>Honolulu, HI. June 8-12, 2015.</strong></p>

                    <p>The 2015 HCP course was the first offered and covered HCP-style MR and MEG acquisition, quality control, preprocessing pipelines, analysis, informatics, and database/visualization tools developed by HCP. The course provided attendees hands-on instruction on working with HCP data and data collection methods. All course materials including lecture slides, practical instructions, data, and a virtual machine of the course computer setup is available for download on <a href="2015/exploring-the-human-connectome.php">the 2015 course site</a>.</p>
                    <p><a href="/courses/2015/exploring-the-human-connectome.php"><button>View Course Info</button></a></p>

                </div>
            </div>

            <?php
            mysqli_close($db);
            ?>
        </div>
        <!-- /#Content -->

        <div id="sidebar-rt">

            <div id="sidebar-content">

            </div>
        </div>
        <!-- /sidebar -->


    </div> <!-- middle -->

    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/footer.php'); ?>

</div>
<!-- end container -->

<script src="//www.google.com/jsapi?key=AIzaSyA5m1Nc8ws2BbmPRwKu5gFradvD_hgq6G0" type="text/javascript"></script>
<script type="text/javascript">
    /*
     *  How to restrict a search to a Custom Search Engine
     *  http://www.google.com/cse/
     */

    google.load('search', '1');

    function OnLoad() {
        // Create a search control
        var searchControl = new google.search.SearchControl();

        // Add in a WebSearch
        var webSearch = new google.search.WebSearch();

        // Restrict our search to pages from our CSE
        webSearch.setSiteRestriction('000073750279216385221:vnupk6whx-c');

        // Add the searcher to the SearchControl
        searchControl.addSearcher(webSearch);

        // tell the searcher to draw itself and tell it where to attach
        searchControl.draw(document.getElementById("search"));

        // execute an inital search
        // searchControl.execute('');
    }

    google.setOnLoadCallback(OnLoad);

</script>


<!-- google analytics -->
<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-18630139-1']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

</script>
</body>
</html>
