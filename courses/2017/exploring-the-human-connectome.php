<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>HCP Course 2017: Exploring the Human Connectome | Human Connectome Project</title>
    <meta http-equiv="Description" content="Course registration and curriculum for the 2017 HCP course: Exploring the Human Connectome, to be held June 19-23 in Vancouver." />
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/header-includes.php'); ?>

</head>

<body>
<!-- site container -->
<div id="site_container">

    <div id="NIH_header2">
<span>
<a href="http://www.neuroscienceblueprint.nih.gov/connectome/" title="NIH Human Connectome Project" style="color:#b4cfff; text-decoration:none;">NIH Blueprint: The Human Connectome Project</a></span>
    </div>

    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/storefront-topnav.php'); ?>

    <!-- banner -->
    <div id="bannerOpen" class="data-banner" style="background: url('/img/courses/course-banner-HCP2017.jpg') left top no-repeat #fff !important; height: 204px"></div>

    <!-- Breadcrumbs -->
    <div id="breadcrumbs">
        <p>&raquo; <a href="/courses/">hcp course materials</a> &gt; Exploring the Human Connectome (2017)</p></div>
    <!-- end breadcrumbs -->

    <!-- "Middle" area contains all content between the top navigation and the footer -->
    <div id="middle">

        <div id="content-wide">
            <style>
                h2 { border-top: 1px solid #ccc !important; margin-top: 1em; padding-top: 1em; }
                #content li a { color: #444; border-bottom: 1px dashed #ccc; }
                #content li a:hover { color: #0168db; text-decoration: none; }
                .download-button { background-color: #0030a0; border: 1px solid #111; border-radius: 3px; box-shadow: 1px 2px #aaa; color: #fff; font: 16px Arial, sans-serif; font-weight:bold; margin-bottom: 1em; padding: 6px 10px; text-align: center; width: 100%; }
                .download-button span { font-size: 11px; }
                .download-button:hover { background-color: #0168db; box-shadow: none; cursor: pointer; }
                .download-button.disabled { background-color: #808080; }
                .download-button.disabled:hover { background-color: #909090; }

            </style>
            <h1>Exploring the Human Connectome</h1>
            <a name="schedule"></a>
            <div>
                <h2>Course Materials</h2>
                <p><strong>Day 1: Monday, Jun 19</strong></p>
                <table border="0" cellspacing="0" cellpadding="0" class="hcp-schedule" style="width: 100%">
                    <thead>
                    <tr>
                        <th>Type</th><th width="420">Course (Presenter)</th><th>Resources</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture</td><td>Intro and Overview (David Van Essen)</td><td>PDF: <a href="https://wustl.box.com/s/cwcumeq2ggigalnjznaw0bs7x3nvx4yv" target="_blank">https://wustl.box.com/s/cwcumeq2ggigalnjznaw0bs7x3nvx4yv</a> </td>
                    </tr>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture</td><td>HCP Data Acquisition (Michael Harms)</td><td>PDF: <a href="https://wustl.box.com/s/d8l3ed24697zkfbin3e196kc94rxcnrr" target="_blank">https://wustl.box.com/s/d8l3ed24697zkfbin3e196kc94rxcnrr</a></td>
                    </tr>
                    <tr class="hcp-schedule-practical">
                        <td>Practical</td><td>Introduction to <strong>wb_view</strong> (Jenn Elam)</td><td>PDF: <a href="https://wustl.box.com/s/50frhndpypnwywp0of9km13oyxaawi2k" target="_blank">https://wustl.box.com/s/50frhndpypnwywp0of9km13oyxaawi2k</a></td>
                    </tr>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture</td><td>Structural MRI: Precise Neuroanatomical Localization (Matt Glasser)</td><td>PDF: <a href="https://wustl.box.com/s/xhgja7gky9fd6rx70sxn45ntyftfagph" target="_blank">https://wustl.box.com/s/xhgja7gky9fd6rx70sxn45ntyftfagph</a></td>
                    </tr>
                    <tr class="hcp-schedule-practical">
                        <td>Practical</td><td>Data Quality (Michael Harms)</td><td>Lecture PDF: <a href="https://wustl.box.com/s/poi0pym8i3rzodbzbxz469yd4bjetm74" target="_blank">https://wustl.box.com/s/poi0pym8i3rzodbzbxz469yd4bjetm74</a><br>
                        Practical PDF: <a href="https://wustl.box.com/s/zs1cfsxqigsrtjkboksd7133vpi5dtvj" target="_blank">https://wustl.box.com/s/zs1cfsxqigsrtjkboksd7133vpi5dtvj</a></td>
                    </tr>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture</td><td>Group Brain Parcellation (Matt Glasser)</td><td>PDF: <a href="https://wustl.box.com/s/rpfm6a5sisbqg166z5wsoyfq31ic6hg6" target="_blank">https://wustl.box.com/s/rpfm6a5sisbqg166z5wsoyfq31ic6hg6</a></td>
                    </tr>
                    <tr class="hcp-schedule-practical">
                        <td>Practical</td><td>Introduction to <strong>wb_command</strong> and Group Parcellation (Tim Coalson, Jenn Elam)</td><td>PDF: <a href="https://wustl.box.com/s/ti46uqqlukqnh4u97aw74r53r7p8xu3c" target="_blank">https://wustl.box.com/s/ti46uqqlukqnh4u97aw74r53r7p8xu3c</a></td>
                    </tr>
                    </tbody>
                </table>

                <p><strong>Day 2: Tuesday, Jun 20</strong></p>
                <table border="0" cellspacing="0" cellpadding="0" class="hcp-schedule" style="width: 100%">
                    <thead>
                    <tr>
                        <th>Type</th><th width="420">Course (Presenter)</th><th>Time</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture</td><td>Multimodal Classification of Areas in Individuals and Parcellation Validation (Matt Glasser)</td><td>PDF: <a href="https://wustl.box.com/s/chaz76a0ppfmumv47jx5tj2goqpg93i9" target="_blank">https://wustl.box.com/s/chaz76a0ppfmumv47jx5tj2goqpg93i9</a> </td>
                    </tr>
                    <tr class="hcp-schedule-practical">
                        <td>Practical</td><td>Individual Brain Parcellation (Matt Glasser)</td><td>PDF: <a href="https://wustl.box.com/s/pbe2pdz54ba7jf2l2olr3d0fxx5o6slw" target="_blank">https://wustl.box.com/s/pbe2pdz54ba7jf2l2olr3d0fxx5o6slw</a></td>
                    </tr>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture</td><td>rfMRI Background, Preprocessing, Denoising (Steve Smith)</td><td>PDF: <a href="https://wustl.box.com/s/r7qm1ab1bctyawd2uvwwwt9pzvh3ttoo" target="_blank">https://wustl.box.com/s/r7qm1ab1bctyawd2uvwwwt9pzvh3ttoo</a></td>
                    </tr>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture</td><td>rfMRI Temporal Noise (Matt Glasser)</td><td>PDF: <a href="https://wustl.box.com/s/ohxkqkgpcrrpuxu8a8fa18f9p102s0qr" target="_blank">https://wustl.box.com/s/ohxkqkgpcrrpuxu8a8fa18f9p102s0qr</a></td>
                    </tr>
                    <tr class="hcp-schedule-practical">
                        <td>Practical</td><td>rfMRI Preprocessing, Denoising; ICA-based Parcellations (Steve Smith)</td><td>PDF: <a href="https://wustl.box.com/s/kns8ci5s53w4kjndbnqq6ohaha115w2t" target="_blank">https://wustl.box.com/s/kns8ci5s53w4kjndbnqq6ohaha115w2t</a></td>
                    </tr>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture</td><td>rfMRI Network Analysis Strategies (Steve Smith)</td><td>PDF: <a href="https://wustl.box.com/s/4h2w3hgjdlpskwtyz5hfamonoim8o1ex" target="_blank">https://wustl.box.com/s/4h2w3hgjdlpskwtyz5hfamonoim8o1ex</a></td>
                    </tr>
                    <tr class="hcp-schedule-practical">
                        <td>Practical</td><td>rfMRI Netmats and Dual Regression (Steve Smith)</td><td>PDF: <a href="https://wustl.box.com/s/ca5mo34evo2c2f8x041az5qwpeugc385" target="_blank">https://wustl.box.com/s/ca5mo34evo2c2f8x041az5qwpeugc385</a></td>
                    </tr>
                    </tbody>
                </table>

                <p><strong>Day 3: Wednesday, Jun 21</strong></p>
                <table border="0" cellspacing="0" cellpadding="0" class="hcp-schedule" style="width: 100%">
                    <thead>
                    <tr>
                        <th>Type</th><th width="420">Course (Presenter)</th><th>Time</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr class="hcp-schedule-practical">
                        <td>Practical</td><td>HCP Pipelines Practical (Tim Brown)</td><td>Lecture PDF: <a href="https://wustl.box.com/s/s0dtkeg15zqzds4if34s56p17poas780" target="_blank">https://wustl.box.com/s/s0dtkeg15zqzds4if34s56p17poas780</a><br>
                            Practical PDF: <a href="https://wustl.box.com/s/xw5kgz7gefd5sesgugucqp3atzihemfv" target="_blank">https://wustl.box.com/s/xw5kgz7gefd5sesgugucqp3atzihemfv</a></td>
                    </tr>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture</td><td>Diffusion MRI, Distortion Correction and DTI (Jesper Andersson)</td><td>PDF: <a href="https://wustl.box.com/s/gj40lz6q2jdynjgj4gxg3yucomobhx5w" target="_blank">https://wustl.box.com/s/gj40lz6q2jdynjgj4gxg3yucomobhx5w</a></td>
                    </tr>
                    <tr class="hcp-schedule-practical">
                        <td>Practical</td><td>Diffusion MRI, Distortion Correction and DTI Practical (Jesper Andersson)</td><td>PDF: <a href="https://wustl.box.com/s/5zoqwas78uv1l0whyekduk2st8j9cop0" target="_blank">https://wustl.box.com/s/5zoqwas78uv1l0whyekduk2st8j9cop0</a></td>
                    </tr>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture</td><td>Fibre Orientation Models and Tractography Analysis (Matteo Bastiani)</td><td>PDF: <a href="https://wustl.box.com/s/onkohrfdkcs4b7stbxpww4pz6746xn8w" target="_blank">https://wustl.box.com/s/onkohrfdkcs4b7stbxpww4pz6746xn8w</a></td>
                    </tr>
                    <tr class="hcp-schedule-practical">
                        <td>Practical</td><td>Fibre Orientation Models and Tractography Analysis Practical (Matteo Bastiani)</td><td>PDF: <a href="https://wustl.box.com/s/rd4q23kigh11kdrj0ndj9jip1tjk7id6" target="_blank">https://wustl.box.com/s/rd4q23kigh11kdrj0ndj9jip1tjk7id6</a></td>
                    </tr>
                    <tr class="hcp-schedule-practical">
                        <td>Practical</td><td>Tractography and Tracer Connectivity (Chad Donahue)</td><td>PDF: <a href="https://wustl.box.com/s/2dxp2us6n5j30odfgkzlmec5eal0lzmb" target="_blank">https://wustl.box.com/s/2dxp2us6n5j30odfgkzlmec5eal0lzmb</a></td>
                    </tr>

                    </tbody>
                </table>

                <p><strong>Day 4: Thursday, Jun 22</strong></p>
                <table border="0" cellspacing="0" cellpadding="0" class="hcp-schedule" style="width: 100%">
                    <thead>
                    <tr>
                        <th>Type</th><th width="420">Course (Presenter)</th><th>Time</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture</td><td>Regression, Prediction, and Permutation Analyses (Thomas Nichols)</td><td>PDF: <a href="https://wustl.box.com/s/afcsof3szemqhjhib63zr1o1mjkd6aqq" target="_blank">https://wustl.box.com/s/afcsof3szemqhjhib63zr1o1mjkd6aqq</a></td>
                    </tr>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture</td><td>Family Structure and Heritability (Thomas Nichols)</td><td>PDF: <a href="https://wustl.box.com/s/jiqbk50gbrcqadsp07cpp0a7dgmw8sgx" target="_blank">https://wustl.box.com/s/jiqbk50gbrcqadsp07cpp0a7dgmw8sgx</a></td>
                    </tr>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture</td><td>Task fMRI and Behavioral Measure Analyses (Greg Burgess)</td><td>PDF: <a href="https://wustl.box.com/s/gxir8lvp0k10zyd38lxkibdvv62634sx" target="_blank">https://wustl.box.com/s/gxir8lvp0k10zyd38lxkibdvv62634sx</a></td>
                    </tr>
                    <tr class="hcp-schedule-practical">
                        <td>Practical</td><td>tfMRI and PALM (Greg Burgess, Anderson Winkler)</td><td>PDF: <a href="https://wustl.box.com/s/lkdxqwj3qyrxgzsnzxfc685vns1msa9n" target="_blank">https://wustl.box.com/s/lkdxqwj3qyrxgzsnzxfc685vns1msa9n</a></td>
                    </tr>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture</td><td>Lifespan HCP: Development and Aging (Susan Bookheimer)</td><td>PDF: <a href="https://wustl.box.com/s/rs0w1u80cji32y69aik1xqil4cm8oekp" target="_blank">https://wustl.box.com/s/rs0w1u80cji32y69aik1xqil4cm8oekp</a></td>
                    </tr>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture</td><td>Clinical Connectome: Application of HCP Methods to Neuropsychiatric Illness (Alan Anticevic)</td><td>PDF: <a href="https://wustl.box.com/s/xnctn4gkpx7a64m68yrmogazhk5eje28" target="_blank">https://wustl.box.com/s/xnctn4gkpx7a64m68yrmogazhk5eje28</a></td>
                    </tr>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture</td><td>ConnectomeDB, Connectome Coordinating Facility, Amazon Web Services (Tim Brown)</td><td></td>
                    </tr>
                    <tr class="hcp-schedule-practical">
                        <td>Practical</td><td>Cloud-based Processing Using HCP Pipelines and Amazon Web Services (Tim Brown)</td><td>PDF: <a href="https://wustl.box.com/s/ebuxm8udvtsj2z5m6uuykwdo4ohiw6jh" target="_blank">https://wustl.box.com/s/ebuxm8udvtsj2z5m6uuykwdo4ohiw6jh</a></td>
                    </tr>
                    </tbody>
                </table>

                <p><strong>Day 5: Friday, Jun 23</strong></p>
                <table border="0" cellspacing="0" cellpadding="0" class="hcp-schedule" style="width: 100%">
                    <thead>
                    <tr>
                        <th>Type</th><th width="420">Course (Presenter)</th><th>Time</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture</td><td>Introduction to HCP-MEG Connectome (Linda Larson-Prior)</td><td>PDF: <a href="https://wustl.box.com/s/n1lzuzp5alr9tcsrftgah4g7uv4gyeih" target="_blank">https://wustl.box.com/s/n1lzuzp5alr9tcsrftgah4g7uv4gyeih</a></td>
                    </tr>
                    <tr class="hcp-schedule-practical">
                        <td>Practical</td><td>MEG Preprocessing, Channel and Source Analysis of tMEG and rMEG (Giorgios Michalareas)</td><td>PDF: <a href="https://wustl.box.com/s/m72sy2pk5e9h2javos4e9cius7mzp1sk" target="_blank">https://wustl.box.com/s/m72sy2pk5e9h2javos4e9cius7mzp1sk</a></td>
                    </tr>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture</td><td>MEG Connectivity and Follow-up Analyses for HCP Data (Robert Oostenveld)</td><td>PDF: <a href="https://wustl.box.com/s/01gv2bgvgin5f2aikqu4gbep04u3kfty" target="_blank">https://wustl.box.com/s/01gv2bgvgin5f2aikqu4gbep04u3kfty</a></td>
                    </tr>
                    <tr class="hcp-schedule-practical">
                        <td>Practical</td><td>rMEG and tMEG Source-level Connectivity; Multimodal Integration (Giorgios Michalareas)</td><td>PDF: <a href="https://wustl.box.com/s/6qtuuf0w6c4n9i4p6h5v7trpyz9sk03z" target="_blank">https://wustl.box.com/s/6qtuuf0w6c4n9i4p6h5v7trpyz9sk03z</a></td>
                    </tr>
                    <tr class="hcp-schedule-practical">
                        <td>Practical</td><td>Building <strong>wb_view</strong> scenes and sharing on BALSA (Jenn Elam, Chad Donahue)</td><td>PDF: <a href="https://wustl.box.com/s/d7nwru8vzfza9cp2gvkccxacjmaenztp" target="_blank">https://wustl.box.com/s/d7nwru8vzfza9cp2gvkccxacjmaenztp</a></td>
                    </tr>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture</td><td>HCP and Beyond: Looking Back and Forward (David Van Essen)</td><td>PDF: <a href="https://wustl.box.com/s/mcbx4j737ekg7j9udx3ohop4ioiws9t9" target="_blank">https://wustl.box.com/s/mcbx4j737ekg7j9udx3ohop4ioiws9t9</a></td>
                    </tr>
                    </tbody>
                </table>

            </div>

            <h2>Course Description</h2>
            <div style="float:right; width: 230px; margin-left: 30px; margin-bottom: 30px; padding: 0 15px 15px; border-left: 1px dotted #ccc; font-size:10px;">
                <p><strong>On This Page:</strong></p>
                <ul>
                    <li><a href="#registration">Registration</a></li>
                    <li><a href="#invitationletter">Invitation Letter</a></li>
                    <li><a href="#accommodations">Accommodations and Location</a></li>
                    <li><a href="#schedule">Course Schedule</a></li>
                    <li><a href="#faculty">Faculty</a></li>
                    <li><a href="#cancellation">Cancellation Policy</a></li>
                    <li><a href="#contact">Contact Information</a></li>
                </ul>
                <p>&nbsp;</p>
                <p><strong>Co-sponsored by: </strong></p>
                <p><img src="/img/courses/ubc-brain-centre-logo.jpg" alt="Djavad Mowafaghian Centre for Brain Health"/></p>
            </div>
            <div style="width: 600px;">
                <p><span style="font-size:16px; line-height:1.2;"><strong>Learn about multimodal neuroimaging data, analysis, visualization, and sharing tools of the Human Connectome Project </strong></span> </p>
                <p>We are pleased to announce the <strong>2017 HCP Course: &quot;Exploring the Human Connectome&quot;</strong>,  to be held <strong>June 19 &ndash; 23, 2017</strong> at the <a href="http://ubcconferences.com/" target="_blank"><strong>University of British Columbia</strong></a>, in  Vancouver, BC, Canada. This is the week before the <a href="http://www.humanbrainmapping.org/i4a/pages/index.cfm?pageID=3734">Organization of Human Brain Mapping (OHBM) annual meeting</a> to be held in downtown Vancouver (about 8 miles away from UBC) June 25-29.</p>
                <p>This course is designed for investigators who are interested  in:</p>
                <ul>
                    <li>using HCP-style data distributed by the Connectome Coordinating Facility (CCF) from the young adult (original) HCP and forthcoming projects</li>
                    <li>acquiring and analyzing HCP-style imaging and behavioral data at your own institution</li>
                    <li>processing your own non-HCP data using HCP pipelines and methods</li>
                    <li>learning to use Connectome Workbench tools and share data using the BALSA imaging database</li>
                    <li>learning HCP multi-modal neuroimaging analysis methods, including those that combine MEG and MRI data </li>
                    <li>positioning yourself to capitalize on HCP-style data forthcoming from large-scale projects currently collecting data (e.g., Lifespan HCP development and aging and Connectomes Related to Human Disease projects)</li>
                </ul>
                <p>This 5-day intensive course will provide training in the acquisition, analysis and visualization of freely-available data from the Human Connectome Project using methods and informatics tools developed by the WU-Minn-Oxford HCP consortium. Participants will learn how to acquire, analyze, visualize, and interpret data from four major MR modalities (structural MR, resting-state fMRI, diffusion imaging, task-evoked fMRI) plus magnetoencephalography (MEG) and extensive behavioral data.  Lectures and labs will provide grounding in neurobiological as well as methodological issues involved in interpreting multimodal data, and will span the range from single-voxel/vertex to brain network analysis approaches. </p>
                <p>The course is open to graduate students, postdocs, faculty, and industry participants.  The course is aimed at both new and current users of HCP data, methods, and tools, and will cover both basic and advanced topics. Prior experience in human neuroimaging or in computational analysis of brain networks is desirable, preferably including familiarity with FSL and Freesurfer software. </p>
                <p>To get a sense of the material covered, check out the <a href="/courses/2015">2015</a> and <a href="/courses/2016">2016 course materials</a> (lecture slide PDFs, practical instructions, software and practical datasets in a virtual machine) currently available for download. Each year the course is updated with the latest developments and analysis recommendations from HCP.</p>
                <p>All lectures and printed material will be in English.</p>
            </div>
            <a name="registration"></a>
            <div style="width: 600px;">
                <h2>Registration</h2>
                <p>Registration (in US dollars) is $750 for PhD/MSc/undergrad students, $1250 for postdocs and other non-student attendees, and $2200 for commercial attendees.</p>
                <p>Course registration includes lectures, computer practical sessions, a light continental breakfast and coffee/tea breaks each day, an evening kickoff reception, and a course book containing the lecture slides and practical instructions. </p>
                <p>Spaces are limited, registration will remain open until all spaces are filled or June 12 at the latest.</p>
                <p>The registration fee does not include accommodation (see below), or lunch and dinner; there are cafés and restaurants nearby on and just off the UBC campus.</p>
                <p>To register and pay, visit the <a href="/courses/course-registration.php">on-line registration page</a>. <strong>Please note that we are only able to take payment by credit card.</strong> (Sorry about this.) Please note also that your registration is not complete until you have finalized the payment (entering form information and then proceeding to checkout and payment).</p>
                <a name="invitationletter"></a>
                <p>If you require an invitation letter as part of a travel visa application, please download <a href="HCP_Course_2017_Invitation_Letter.pdf" title="download invitation letter PDF">this letter</a> and fill in your contact information.</p>
                <h3>UBC Faculty, Staff, and Student Registration</h3>
                <p>A limited number of UBC Faculty, Staff, and Students are eligible for a 25% discount from the HCP Course registration fee (provided as a refund for eligible registrants). To be eligible for this discount, UBC affiliates should use <a href="/courses/course-registration.php?discountCode=UBC2017" title="Apply the UBC discount code to your registration">this UBC registration page</a>.</p>
            </div>

            <a name="accommodations"></a>
            <div style="width: 600px;">
                <h2>Location </h2>
                <p><img src="/img/courses/hcp-course-2017-ubc-braincentre.jpg" alt="Djavad Mowafaghian Centre for Brain Health" /></p>
                <p>The HCP Course lectures and practicals will be held in the Rudy North Lecture Theatre at the <a href="http://www.brain.ubc.ca/" title="Djavad Mowafaghia Centre for Brain Health" target="_blank">Djavad Mowafaghian Centre for Brain Health</a>, our co-sponsor for this year’s course, on the University of British Columbia (UBC) campus, in Vancouver, BC, Canada.  Here’s a <a href="https://www.google.com/maps/place/Djavad+Mowafaghian+Centre+for+Brain+Health/@49.2645179,-123.2466185,17z/data=!3m1!4b1!4m5!3m4!1s0x548672c867a8ab7f:0x1456e5d6d3c1e60d!8m2!3d49.2645179!4d-123.2444298" target="_blank">map</a> of the location.</p>
            </div>
            <div style="width: 600px">
                <h2>Accommodations</h2>
                <p>Registrants are responsible for making their own accommodation arrangements. Organizing the course at UBC has allowed us to arrange a block of reasonably priced accommodation for all participants and staff in the <a title="Google Map: Walter Gage Residence Hall" href="https://www.google.com/maps/place/UBC+Conferences/@49.2645897,-123.2531846,15z/data=!4m8!1m2!2m1!1subc+near+Walter+Gage+Residence,+Student+Union+Boulevard,+Vancouver,+BC,+Canada!3m4!1s0x548672cae6d7614b:0xd80ee40e9d4893c9!8m2!3d49.269461!4d-123.2486437">Walter Gage residence hall</a>, a short 5-10 minute walk from the Centre for Brain Health. </p>
                <p>Most of the available rooms are Premium single private bedrooms in a 4 person shared apartment with shared washroom and shared lounge with TV. These will be offered for $52 CAD (~$40 USD) per night, plus applicable taxes.  Linens, two towels, soap, and in room coffee are provided. <em>Wired internet connection is provided, but you will need to bring your own Ethernet cable</em>.</p>
                <p>A limited number of Studio Suites (similar to a typical hotel room) with a double bed, private washroom, studio kitchen, and TV, will be offered for $139 CAD (~$105 USD) per night, plus applicable taxes. Linens are changed daily and in room coffee is provided.</p>
                <p>Registrants are responsible for making their own reservations through UBC Conferences and Accommodation using the <a href="https://reserve.ubcconferences.com/vancouver/availability.asp?hotelCode=%2A&startDate=06%2F16%2F2017&endDate=06%2F24%2F2017&adults=1&children=&rooms=1&requesttype=invBlockCode&code=G170618A" title="Book accommodations at UBC using the HCP Course block rate" target="_blank">HCP Course room block website</a>. </p>
                <p><strong>Note: May 17, 2017 is the cut off date to reserve UBC lodging under the HCP Course room block. </strong></p>
                <div class="column-third" style="width: 48%">
                    <a href="https://reserve.ubcconferences.com/vancouver/availability.asp?hotelCode=%2A&startDate=06%2F18%2F2017&endDate=06%2F23%2F2017&adults=1&children=&rooms=1&requesttype=invBlockCode&code=G170618A%20" title="Book accommodations at UBC using the HCP Course block rate" target="_blank"><button>Reserve Your Accommodations</button></a>
                </div>
            </div>

            <a name="faculty"></a>
            <div style="width: 600px;">
                <h2>Faculty</h2>
                <p><strong>Speakers to include:</strong> David Van Essen, Matt Glasser, Michael Harms, Steve Smith, Tom Nichols, Anderson Winkler, Greg Burgess, Susan Bookheimer, Alan Anticevic, Jesper Andersson, Matteo Bastiani, Chad Donahue, Linda Larson-Prior and Robert Oostenveld.</p>
                <p><strong>Practical Tutors:</strong> (in addition to speakers): Jenn Elam, Tim Brown, Tim Coalson, and Giorgos Michalareas</p>
            </div>

            <a name="cancellation"></a>
            <div style="width: 600px;">
                <h2>Cancellation Policy</h2>
                <p>Please register with care. We have made commitments on costs that cannot be changed the closer we approach to the course date. Here is our refund policy:</p>
                <ul>
                    <li>Full refunds available on registration cancellations up to May 12, 2017</li>
                    <li>50% refunds available on registration cancellations from May 13 to May 31, 2017</li>
                    <li>No refunds of any kind for any cancellation on June 1, 2017 or beyond</li>
                </ul>
            </div>

            <a name="contact"></a>
            <div style="width: 600px;">
                <h2>Contact Information</h2>
                <p>If you have any questions, please contact us at: <a href="mailto:hcpcourse@humanconnectome.org">hcpcourse@humanconnectome.org</a></p>
                <p>We look forward to seeing you in Vancouver! </p>
            </div>

        </div>

        <!-- Primary page Content -->


        <!-- modals -->
        <div id="page-mask" class="pm" onclick="javascript:modalClose();"></div>
        <div class="modal" id="download-course-vm" style="min-height:0px !important;">
            <div class="modal-title">Download Course VM <span class="modal-close" onclick="modalClose()"><img src="/img/close-dark.png" /></span></div>
            <div class="modal_content">
                <h1>Before you download the Course VM...</h1>
                <p><strong>Make sure you have at least 1TB of free disk space.</strong> The downloaded archive must be opened within VM virtualizer software software (VMware Player, VMware Fusion, or VirtualBox) where you will run the VM.  If your internal storage has more than 1TB of space free, this will generally be acceptably fast for the purpose. </p>
                <p><strong>If you are planning to buy a new external drive for this purpose</strong>, we recommend that you check the computer(s) you intend to use it with for USB 3, eSATA, or Thunderbolt ports, and get an external drive with whichever of those interfaces your computer(s) have. USB 2 will work, but at significantly reduced performance speed.</p>
                <p><strong>This download requires <a href="https://www.humanconnectome.org/documentation/connectomeDB/downloading/installing-aspera.html" title="Aspera installation instructions">the Aspera plugin</a></strong> and a free ConnectomeDB account. We recommend using Firefox to download as Aspera is not well supported in Chrome.</p>
                <p><strong>Remember to change the settings in the Aspera Connect plugin</strong> (via the “gear” icon in the Transfers window or through Aspera Connect Menu>Preferences>Transfers) to download to your desired location. The default in Aspera Connect is to download to your desktop (which you probably don’t want). </p>
                <p><button onclick="javascript:modalClose()">Cancel</button> <button title="begin download" onclick="javascript:window.location.assign('https://db.humanconnectome.org/app/action/ChooseDownloadResources?project=HCP_Resources&resource=CourseData&filePath=HCP_2015_Course_VM.zip')">Begin My Download</button></p>
            </div>
        </div> <!-- end "download course VM" modal -->

        <div class="modal" id="download-practicals" style="min-height:0px !important;">
            <div class="modal-title">Download Practical Data Only <span class="modal-close" onclick="modalClose()"><img src="/img/close-dark.png" /></span></div>
            <div class="modal_content">
                <h1>Before you download the Practical Data&hellip;</h1>
                <p><strong>Make sure you have at least 1TB of free disk space to download and extract the data.</strong> </p>
                <p><strong>If you are planning to buy a new external drive for this purpose</strong>, we recommend that use one compatible with USB 3, eSATA, or Thunderbolt connections. USB 2 will work, but at significantly reduced performance speed.</p>
                <p><strong>This download requires <a href="https://www.humanconnectome.org/documentation/connectomeDB/downloading/installing-aspera.html" title="Aspera installation instructions">the Aspera plugin</a></strong> and a free ConnectomeDB account. </p>
                <p><strong>Remember to change the settings in the Aspera Connect plugin</strong> (via the “gear” icon in the Transfers window or through Aspera Connect Menu>Preferences>Transfers) to download to your desired location. </p>
                <p class="alertBox">The paths provided throughout the HCP Course practical instructions, both for locating the software to run and for locating the data to process, will very likely not be correct for your particular configuration of software and data. You will be responsible for keeping track of these paths (where you installed the software and where you placed the data) and altering the commands provided in the course materials to reflect your chosen configuration. </p>
                <p>Additionally, the MEG practicals for Day 5 use precompiled MATLAB scripts that will only work on Linux systems. If you don’t have a Linux machine and want to run the MEG practicals, we recommend downloading the Course VM as in Option 1.</p>
                <p><button onclick="javascript:modalClose()">Cancel</button> <button title="begin download" onclick="javascript:window.location.assign('https://db.humanconnectome.org/app/action/ChooseDownloadResources?project=HCP_Resources&resource=CourseData&filePath=HCP_2015_Course_Data.zip')">Begin My Download</button></p>
            </div>
        </div> <!-- end "download course VM" modal -->

        <div class="modal" id="prerequisite-software" style="min-height:0px !important;">
            <div class="modal-title">Software Prerequisites for Practical Data <span class="modal-close" onclick="modalClose()"><img src="/img/close-dark.png" /></span></div>
            <div class="modal_content">
                <h1>Prerequisite software for running the 2015 HCP Course Practicals</h1>
                <ul>
                    <li><a href="http://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FslInstallation" target="_blank" title="Download FSL">FSL version 5.0.6</a></li>
                    <li><a href="https://www.humanconnectome.org/software/get-connectome-workbench.html" target="_blank" title="Get Connectome Workbench">Connectome Workbench v1.1.1</a></li>
                    <li><a href="https://www.humanconnectome.org/documentation/HCP-pipelines/meg-pipeline.html" target="_blank" title="Get MEG Connectome Pipelines">megconnectome v2.2 and fieldtrip-r9924</a></li>
                    <li><a href="http://www.mathworks.com/products/matlab/" target="_blank" title="Download MATLAB">MATLAB</a> with the official MATLAB toolboxes: Statistics, Bioinformatics, and Signal Processing OR <a href="http://www.gnu.org/software/octave/download.html" target="_blank" title="Download Octave">Octave v.3.8.0</a> or greater with toolboxes: control, general, signal, and statistics</li>
                    <li><a href="https://github.com/Washington-University/Pipelines/wiki/v3.4.0-Release-Notes,-Installation,-and-Usage#installation" target="_blank" title="Download HCP Pipeline Scripts">HCP Pipeline Scripts v3.4.0</a></li>
                </ul>
                <p>Requisite for running the HCP Pipelines: <a href="ftp://surfer.nmr.mgh.harvard.edu/pub/dist/freesurfer/5.3.0-HCP" target="_blank" title="Download Freesurfer">FreeSurfer version 5.3.0-HCP</a>.</p>
                <p>See this <a href="https://github.com/Washington-University/Pipelines/wiki/v3.4.0-Release-Notes,-Installation,-and-Usage#prerequisites" target="_blank" title="Installation Instructions">installation guidance</a> for using the prerequisite software in the HCP pipelines.</p>
                <p><button onclick="javascript:modalClose()">Close</button></p>
            </div>
        </div> <!-- end "download course VM" modal -->


    </div> <!-- middle -->

    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/footer.php'); ?>


</div>
<!-- end container -->

<script src="https://www.google.com/jsapi?key=AIzaSyA5m1Nc8ws2BbmPRwKu5gFradvD_hgq6G0" type="text/javascript"></script>

<!-- google analytics -->
<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-18630139-1']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

</script>
</body>
</html>
		