<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>HCP Course: Exploring the Human Connectome (Summer 2015) | Human Connectome Project</title>
    <meta http-equiv="Description" content="Course registration and curriculum for the 2016 HCP course: Exploring the Human Connectome" />
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/header-includes.php'); ?>

</head>

<body>
<!-- site container -->
<div id="site_container">

    <div id="NIH_header2">
<span>
<a href="http://www.neuroscienceblueprint.nih.gov/connectome/" title="NIH Human Connectome Project" style="color:#b4cfff; text-decoration:none;">NIH Blueprint: The Human Connectome Project</a></span>
    </div>

    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/storefront-topnav.php'); ?>

    <!-- banner -->
    <div id="bannerOpen" class="data-banner" style="background: url('/img/courses/course-banner-HCP2016.jpg') left top no-repeat #fff !important; height: 260px"></div>
    <!-- Breadcrumbs -->
    <div id="breadcrumbs">
        <p>&raquo; <a href="/courses/">hcp course materials</a> &gt; Exploring the Human Connectome (2016)</p></div>
    <!-- end breadcrumbs -->

    <!-- "Middle" area contains all content between the top navigation and the footer -->
    <div id="middle">

        <div id="content-wide">
            <style>
                h2 { border-top: 1px solid #ccc !important; margin-top: 1em; padding-top: 1em; }
                #content li a { color: #444; border-bottom: 1px dashed #ccc; }
                #content li a:hover { color: #0168db; text-decoration: none; }
                .download-button { background-color: #0030a0; border: 1px solid #111; border-radius: 3px; box-shadow: 1px 2px #aaa; color: #fff; font: 16px Arial, sans-serif; font-weight:bold; margin-bottom: 1em; padding: 6px 10px; text-align: center; width: 100%; }
                .download-button span { font-size: 11px; }
                .download-button:hover { background-color: #0168db; box-shadow: none; cursor: pointer; }
                .download-button.disabled { background-color: #808080; }
                .download-button.disabled:hover { background-color: #909090; }

            </style>
            <h1>Exploring the Human Connectome</h1>
            <a name="practical"></a>
            <h2>HCP Course Practical Data and Software</h2>
            <div style="display: inline-block; vertical-align: top; width: 600px; ">
                <p>Unfortunately, due to University-wide network changes, the 2016 HCP Course Practical Data and Software is no longer available as a downloadable Virtual Machine (VM). We expect to be able to remedy this situation for distribution of the 2019 course VM. </p>
            </div>

            <a name="schedule"></a>
            <div>
                <h2>Course Schedule</h2>
                <p><strong>Day 1: Sunday, Aug 28</strong></p>
                <table border="0" cellspacing="0" cellpadding="0" class="hcp-schedule" style="width: 100%">
                    <thead>
                    <tr>
                        <th>Type</th><th width="420">Course (Presenter)</th><th>Resources</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr class="hcp-schedule-lecture">
                        <td>Welcome</td><td>Intro and Overview (David Van Essen)</td><td>PDF: <a href="https://wustl.box.com/s/0edcj787ye9c2idc2ir95b766w8r3pqz">https://wustl.box.com/s/0edcj787ye9c2idc2ir95b766w8r3pqz</a></td>
                    </tr>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture</td><td>HCP Data Acquisition (Michael Harms)</td><td>PDF: <a href="https://wustl.box.com/s/sp4bcf9wjaao6lj3ysmlh495rjds9qsv">https://wustl.box.com/s/sp4bcf9wjaao6lj3ysmlh495rjds9qsv</a></td>
                    </tr>
                    <tr class="hcp-schedule-practical">
                        <td>Practical</td><td>Introduction to <strong>wb_view</strong> (Jenn Elam)</td><td>PDF: <a href="https://wustl.box.com/s/ndirdpom847h0f0ozrqleibxv9w4ssdw">https://wustl.box.com/s/ndirdpom847h0f0ozrqleibxv9w4ssdw</a></td>
                    </tr>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture</td><td>Structural MRI: Precise Neuroanatomical Localization (Matt Glasser)</td><td>PDF: <a href="https://wustl.box.com/s/d8rrs4cyhbbuoptvjywe1uqhpn3kd9fv">https://wustl.box.com/s/d8rrs4cyhbbuoptvjywe1uqhpn3kd9fv</a></td>
                    </tr>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture</td><td>Data Quality (Michael Harms)</td><td>PDF: <a href="https://wustl.box.com/s/72nudch8z8bponfmyvfaxj0421zc18fo">https://wustl.box.com/s/72nudch8z8bponfmyvfaxj0421zc18fo</a></td>
                    </tr>
                    <tr class="hcp-schedule-practical">
                        <td>Practical</td><td>Data Quality (Michael Harms)</td><td>PDF: <a href="https://wustl.box.com/s/0gpam933mvrstcoitm3yfl4p26n85gl2">https://wustl.box.com/s/0gpam933mvrstcoitm3yfl4p26n85gl2</a></td>
                    </tr>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture</td><td>Group Brain Parcellation (Matt Glasser)</td><td>PDF: <a href="https://wustl.box.com/s/eyot0msmq58kpaod785z3qqhgejk3oif">https://wustl.box.com/s/eyot0msmq58kpaod785z3qqhgejk3oif</a></td>
                    </tr>
                    <tr class="hcp-schedule-lecture">
                        <td>Practical</td><td>Introduction to <strong>wb_command</strong> and Group Parcellation (Tim Coalson, Jenn Elam)</td><td>PDF: <a href="https://wustl.box.com/s/k4ptm6mrxar1nlfwhzarmihsv5qwd970">https://wustl.box.com/s/k4ptm6mrxar1nlfwhzarmihsv5qwd970</a></td>
                    </tr>
                    </tbody>
                </table>

                <p><strong>Day 2: Monday, Aug 29</strong></p>
                <table border="0" cellspacing="0" cellpadding="0" class="hcp-schedule" style="width: 100%">
                    <thead>
                    <tr>
                        <th>Type</th><th width="420">Course (Presenter)</th><th>Resources</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture</td><td>Multimodal Classification of Areas in Individuals and Parcellation Validation (Matt Glasser)</td><td>PDF: <a href="https://wustl.box.com/s/i2mxsxjt6ac9d5xuqjct49hjnve75ck5">https://wustl.box.com/s/i2mxsxjt6ac9d5xuqjct49hjnve75ck5</a></td>
                    </tr>
                    <tr class="hcp-schedule-practical">
                        <td>Practical</td><td>Individual Brain Parcellation (Matt Glasser)</td><td>PDF: <a href="https://wustl.box.com/s/0ln9dtwouzhqxkuw5nh7x3bnr5tlsnjh">https://wustl.box.com/s/0ln9dtwouzhqxkuw5nh7x3bnr5tlsnjh</a></td>
                    </tr>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture</td><td>rfMRI background, Preprocessing, Denoising (Steve Smith)</td><td>PDF: <a href="https://wustl.box.com/s/4mo9frrozy5oprsdah7jecfvplkm3wiq">https://wustl.box.com/s/4mo9frrozy5oprsdah7jecfvplkm3wiq</a></td>
                    </tr>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture</td><td>rfMRI Temporal Noise (Steve Smith, Matt Glasser)</td><td>PDF: <a href="https://wustl.box.com/s/jbqr6k3oy4o92nxa62vonk0nhglce2ue">https://wustl.box.com/s/jbqr6k3oy4o92nxa62vonk0nhglce2ue</a></td>
                    </tr>
                    <tr class="hcp-schedule-practical">
                        <td>Practical</td><td>rfMRI Preprocessing, Denoising; ICA-based Parcellations (Steve Smith)</td><td>PDF: <a href="https://wustl.box.com/s/xl5oq5pvtg0v1jd719uygw4xdzp31y9t">https://wustl.box.com/s/xl5oq5pvtg0v1jd719uygw4xdzp31y9t</a></td>
                    </tr>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture</td><td>rfMRI Network Analysis Strategies (Steve Smith)</td><td>PDF: <a href="https://wustl.box.com/s/kxzlemlbosyn75pn61cpbxgbcypv49zn">https://wustl.box.com/s/kxzlemlbosyn75pn61cpbxgbcypv49zn</a></td>
                    </tr>
                    <tr class="hcp-schedule-practical">
                        <td>Practical</td><td>rfMRI Netmats and Dual Regression (Steve Smith)</td><td>PDF: <a href="https://wustl.box.com/s/se8fwaocighr3libgerd9uh34xvywtd6">https://wustl.box.com/s/se8fwaocighr3libgerd9uh34xvywtd6</a></td>
                    </tr>
                    </tbody>
                </table>

                <p><strong>Day 3: Tuesday, Aug 30</strong></p>
                <table border="0" cellspacing="0" cellpadding="0" class="hcp-schedule" style="width: 100%">
                    <thead>
                    <tr>
                        <th>Type</th><th width="420">Course (Presenter)</th><th>Resources</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture</td><td>HCP Pipelines (Tim Brown)</td><td>PDF: <a href="https://wustl.box.com/s/9ds0jaxliwebeoxqdvxel86x7g6z4sul">https://wustl.box.com/s/9ds0jaxliwebeoxqdvxel86x7g6z4sul</a></td>
                    </tr>
                    <tr class="hcp-schedule-practical">
                        <td>Practical</td><td>HCP Pipelines Practical (Tim Brown)</td><td>PDF: <a href="https://wustl.box.com/s/4u30jlz6cmsekpw2sxbcfq0wc33nze37">https://wustl.box.com/s/4u30jlz6cmsekpw2sxbcfq0wc33nze37</a></td>
                    </tr>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture</td><td>Regression, Prediction, and Permutation Analyses (Thomas Nichols, Anderson Winkler)</td><td>PDF: <a href="https://wustl.box.com/s/z8ienimugue86kvans6ks421v9ycmay2">https://wustl.box.com/s/z8ienimugue86kvans6ks421v9ycmay2</a></td>
                    </tr>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture</td><td>Family Structure and Heritability (Thomas Nichols)</td><td>PDF: <a href="https://wustl.box.com/s/lsab6irc5k2q1jrgrp0beiuo67v0y4hw">https://wustl.box.com/s/lsab6irc5k2q1jrgrp0beiuo67v0y4hw</a></td>
                    </tr>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture</td><td>Task fMRI and Behavioral Measure Analyses (Greg Burgess)</td><td>PDF: <a href="https://wustl.box.com/s/tbkv3u299zqupgsgzk402waqo3nw83ah">https://wustl.box.com/s/tbkv3u299zqupgsgzk402waqo3nw83ah</a></td>
                    </tr>
                    <tr class="hcp-schedule-practical">
                        <td>Practical</td><td>tfMRI and PALM (Greg Burgess, Anderson Winkler)</td><td>PDF: <a href="https://wustl.box.com/s/0xqeqhf84fuenmykjdfxbrydawpryn6v">https://wustl.box.com/s/0xqeqhf84fuenmykjdfxbrydawpryn6v</a></td>
                    </tr>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture</td><td>HCP Informatics (Daniel Marcus)</td><td>PDF: <a href="https://wustl.box.com/s/9xmofvj0nyjox7xp136j9joes1hbhn96">https://wustl.box.com/s/9xmofvj0nyjox7xp136j9joes1hbhn96</a></td>
                    </tr>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture</td><td>Understanding the Clinical Connectome: Application of HCP Methods to Neuropsychiatric Illness (Alan Anticevic)</td><td>PDF: <a href="https://wustl.box.com/s/kfvzywxr9pt4dqerc4i3bqep6oluzsqu">https://wustl.box.com/s/kfvzywxr9pt4dqerc4i3bqep6oluzsqu</a></td>
                    </tr>
                    </tbody>
                </table>

                <p><strong>Day 4: Wednesday, Aug 31</strong></p>
                <table border="0" cellspacing="0" cellpadding="0" class="hcp-schedule" style="width: 100%">
                    <thead>
                    <tr>
                        <th>Type</th><th width="420">Course (Presenter)</th><th>Resources</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture</td><td>Diffusion MRI, Distortion Correction and DTI (Jesper Andersson)</td><td>PDF: <a href="https://wustl.box.com/s/lpor0s3vt2qwaghwfaj4ed6hr8lwhnqu">https://wustl.box.com/s/lpor0s3vt2qwaghwfaj4ed6hr8lwhnqu</a></td>
                    </tr>
                    <tr class="hcp-schedule-practical">
                        <td>Practical</td><td>Diffusion MRI, Distortion Correction and DTI Practical (Jesper Andersson)</td><td>PDF: <a href="https://wustl.box.com/s/2m0ix0zkia16vb7a58agidiufq6g4ag6">https://wustl.box.com/s/2m0ix0zkia16vb7a58agidiufq6g4ag6</a></td>
                    </tr>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture</td><td>Fibre Orientation Models and Tractoghraphy Analysis (Michiel Cottar)</td><td>PDF: <a href="https://wustl.box.com/s/pzbdltem3jxjs0uxhz51p3mfpd9csdt5">https://wustl.box.com/s/pzbdltem3jxjs0uxhz51p3mfpd9csdt5</a></td>
                    </tr>
                    <tr class="hcp-schedule-practical">
                        <td>Practical</td><td>Fibre Orientation Models and Tractography Analysis Practical (Michiel Cottar)</td><td>PDF: <a href="https://wustl.box.com/s/35m28use7mv9lsdt3ixbqx4zp0fi4sgm">https://wustl.box.com/s/35m28use7mv9lsdt3ixbqx4zp0fi4sgm</a></td>
                    </tr>
                    <tr class="hcp-schedule-practical">
                        <td>Practical</td><td>Tractography and Tracer (Chad Donahue)</td><td>PDF: <a href="https://wustl.box.com/s/ovp9ev6h2wcsykc2kni4x9rshaet6fuq">https://wustl.box.com/s/ovp9ev6h2wcsykc2kni4x9rshaet6fuq</a></td>
                    </tr>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture</td><td>Cloud-based Processing (Daniel Marcus)</td><td>PDF: <a href="https://wustl.box.com/s/f08yp2k1o4ijyocpw1h4c9xlknhq2t07">https://wustl.box.com/s/f08yp2k1o4ijyocpw1h4c9xlknhq2t07</a></td>
                    </tr>
                    <tr class="hcp-schedule-practical">
                        <td>Practical</td><td>Cloud-based Processing Using HCP Pipelines and Amazon Web Services (Tim Brown)</td><td>PDF: <a href="https://wustl.box.com/s/xocq331xo761pnmj8kkbohwo21t6cchv">https://wustl.box.com/s/xocq331xo761pnmj8kkbohwo21t6cchv</a></td>
                    </tr>
                    </tbody>
                </table>

                <p><strong>Day 5: Thursday, Sep 1</strong></p>
                <table border="0" cellspacing="0" cellpadding="0" class="hcp-schedule" style="width: 100%">
                    <thead>
                    <tr>
                        <th>Type</th><th width="420">Course (Presenter)</th><th>Resources</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture</td><td>Introduction to HCP-MEG Connectome (Linda Larson-Prior)</td><td>PDF: <a href="https://wustl.box.com/s/baxn8g12s0qi0h9ju84typyocnk36u1x">https://wustl.box.com/s/baxn8g12s0qi0h9ju84typyocnk36u1x</a></td>
                    </tr>
                    <tr class="hcp-schedule-practical">
                        <td>Practical</td><td>MEG Preprocessing, Channel and Source Analysis of tMEG and rMEG (Giorgios Michalareas, Franceso di Pompeo)</td><td>PDF: <a href="https://wustl.box.com/s/1i5jhm94y9lsl9hby46085j3qbtajozy">https://wustl.box.com/s/1i5jhm94y9lsl9hby46085j3qbtajozy</a></td>
                    </tr>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture</td><td>MEG Connectivity and Follow-up Analyses for HCP MEG Data and Other Software Tools (Robert Oostenveld)</td><td>PDF: <a href="https://wustl.box.com/s/8ijsz9gahqf4g21cm0yrp2x98va4cn4v">https://wustl.box.com/s/8ijsz9gahqf4g21cm0yrp2x98va4cn4v</a></td>
                    </tr>
                    <tr class="hcp-schedule-practical">
                        <td>Practical</td><td>rMEG and tMEG Source-level Connectivity; Multimodal Integration (Giorgios Michalareas, Franceso di Pompeo)</td><td>PDF: <a href="https://wustl.box.com/s/39h0dcq63g7qy9erx4s2nk3e52nmiqw8">https://wustl.box.com/s/39h0dcq63g7qy9erx4s2nk3e52nmiqw8</a></td>
                    </tr>
                    <tr class="hcp-schedule-practical">
                        <td>Practical</td><td>Building <strong>wb_view</strong> scenes and sharing on BALSA (Jenn Elam)</td><td>PDF: <a href="https://wustl.box.com/s/6nv4ezqrcpsj86vtyx1ttotdet9p6yb3">https://wustl.box.com/s/6nv4ezqrcpsj86vtyx1ttotdet9p6yb3</a></td>
                    </tr>
                    <tr class="hcp-schedule-lecture">
                        <td>Lecture</td><td>HCP and Beyond: Looking Back and Forward (David Van Essen)</td><td>PDF: <a href="https://wustl.box.com/s/0tc4qef30czwhbc7tgh10qeisw7rj51m">https://wustl.box.com/s/0tc4qef30czwhbc7tgh10qeisw7rj51m</a></td>
                    </tr>
                    </tbody>
                </table>

            </div>

            <h2>Course Description</h2>
            <div style="float:right; width: 160px; margin-left: 30px; margin-bottom: 30px; padding: 0 15px 15px; border-left: 1px dotted #ccc; font-size:10px;">
                <p><strong>On This Page:</strong></p>
                <ul>
                    <li><a href="#practical">Practical</a></li>
                    <li><a href="#schedule">Schedule</a></li>
                    <li><a href="#faculty">Faculty and Curriculum Details</a></li>
                    <li><a href="#contact">Contact Information</a></li>
                </ul>
            </div>
            <div style="width: 600px;">
                <p><span style="font-size:16px; line-height:1.2;"><strong>Learn about multi-modal neuroimaging data, analysis, and visualization tools of the Human Connectome Project</strong></span> </p>
                <p>We are pleased to announce the <strong>2016 HCP Course: &quot;Exploring the Human Connectome&quot;</strong>,  to be held <strong>Aug 28 &ndash; Sep 1</strong> at the <a href="http://www.theconfcenter.hms.harvard.edu/" target="_blank"><strong>Joseph S Martin Conference Center at Harvard Medical Center</strong></a>, in  Boston, Massachusets, USA.</p>
                <p>This course is designed for investigators who are interested  in:</p>
                <ul>
                    <li>using data  being collected and distributed by HCP</li>
                    <li>acquiring  and analyzing HCP-style imaging and behavioral data at your own institution</li>
                    <li>processing your  own non-HCP data using HCP pipelines and methods</li>
                    <li>learning to use Connectome Workbench tools and the CIFTI connectivity data format </li>
                    <li>learning HCP  multi-modal neuroimaging analysis methods, including those that combine MEG and  MRI data </li>
                    <li>positioning yourself to capitalize on HCP-style data from forthcoming large-scale projects (e.g., Lifespan HCP and Connectomes Related to Human Disease)</li>
                </ul>
                <p>This 5-day intensive course will provide training in the  acquisition, analysis and visualization of freely-available data from the Human Connectome  Project using methods and informatics tools developed by the WU-Minn HCP consortium.  Participants will learn how to acquire, analyze, visualize, and interpret data  from four major MR modalities (structural MR, resting-state fMRI, diffusion  imaging, task-evoked fMRI) plus magnetoencephalography (MEG) and extensive  behavioral data.  Lectures and labs will  provide grounding in neurobiological as well as methodological issues involved in interpreting multimodal data, and will span the range from  single-voxel/vertex to brain network analysis approaches.  </p>
                <p>The course is open to graduate students, postdocs, faculty,  and industry participants.  The course is  aimed at both new and current users of HCP data, methods, and tools, and will  cover both basic and advanced topics. Prior experience in human neuroimaging or  in computational analysis of brain networks is desirable, preferably including  familiarity with FSL and Freesurfer software.  </p>
                <p>All lectures and printed material will be in English.</p>
            </div>
            <a name="faculty"></a>
            <div style="width: 600px;">
                <h2>Faculty</h2>
                <p><strong>Speakers:</strong> David Van Essen, Matt Glasser, Michael Harms, Steve Smith, Tom Nichols, Anderson Winkler, Greg Burgess, Dan Marcus, Alan Anticevic, Jesper Andersson, Michiel Cottaar, Chad Donahue, Robert Oostenveld, Linda Larson-Prior</p>
                <p><strong>Practical Tutors:</strong> (in addition to speakers): Jenn Elam, Gordon Xu, Tim Brown, Tim Coalson, Giorgos Michalareas, Francesco Di Pompeo</p>
            </div>

            <a name="contact"></a>
            <div style="width: 600px;">
                <h2>Contact Information</h2>
                <p>If you have any questions, please contact us at: <a href="mailto:hcpcourse@humanconnectome.org">hcpcourse@humanconnectome.org</a></p>
                <p>We look forward to seeing you in <em>Bah-ston</em>! </p>
            </div>

        </div>

        <!-- Primary page Content -->


        <!-- modals -->
        <div id="page-mask" class="pm" onclick="javascript:modalClose();"></div>
        <div class="modal" id="download-course-vm" style="min-height:0px !important;">
            <div class="modal-title">Download Course VM <span class="modal-close" onclick="modalClose()"><img src="/img/close-dark.png" /></span></div>
            <div class="modal_content">
                <h1>Before you download the Course VM...</h1>
                <p><strong>Make sure you have at least 1TB of free disk space.</strong> The downloaded archive must be opened within VM virtualizer software software (VMware Player, VMware Fusion, or VirtualBox) where you will run the VM.  If your internal storage has more than 1TB of space free, this will generally be acceptably fast for the purpose. </p>
                <p><strong>If you are planning to buy a new external drive for this purpose</strong>, we recommend that you check the computer(s) you intend to use it with for USB 3, eSATA, or Thunderbolt ports, and get an external drive with whichever of those interfaces your computer(s) have. USB 2 will work, but at significantly reduced performance speed.</p>
                <p><strong>This download requires <a href="https://www.humanconnectome.org/documentation/connectomeDB/downloading/installing-aspera.html" title="Aspera installation instructions">the Aspera plugin</a></strong> and a free ConnectomeDB account. We recommend using Firefox to download as Aspera is not well supported in Chrome.</p>
                <p><strong>Remember to change the settings in the Aspera Connect plugin</strong> (via the “gear” icon in the Transfers window or through Aspera Connect Menu>Preferences>Transfers) to download to your desired location. The default in Aspera Connect is to download to your desktop (which you probably don’t want). </p>
                <p><button onclick="javascript:modalClose()">Cancel</button> <button title="begin download" onclick="javascript:window.location.assign('https://db.humanconnectome.org/app/action/ChooseDownloadResources?project=HCP_Resources&resource=CourseData&filePath=HCP_2015_Course_VM.zip')">Begin My Download</button></p>
            </div>
        </div> <!-- end "download course VM" modal -->

        <div class="modal" id="download-practicals" style="min-height:0px !important;">
            <div class="modal-title">Download Practical Data Only <span class="modal-close" onclick="modalClose()"><img src="/img/close-dark.png" /></span></div>
            <div class="modal_content">
                <h1>Before you download the Practical Data&hellip;</h1>
                <p><strong>Make sure you have at least 1TB of free disk space to download and extract the data.</strong> </p>
                <p><strong>If you are planning to buy a new external drive for this purpose</strong>, we recommend that use one compatible with USB 3, eSATA, or Thunderbolt connections. USB 2 will work, but at significantly reduced performance speed.</p>
                <p><strong>This download requires <a href="https://www.humanconnectome.org/documentation/connectomeDB/downloading/installing-aspera.html" title="Aspera installation instructions">the Aspera plugin</a></strong> and a free ConnectomeDB account. </p>
                <p><strong>Remember to change the settings in the Aspera Connect plugin</strong> (via the “gear” icon in the Transfers window or through Aspera Connect Menu>Preferences>Transfers) to download to your desired location. </p>
                <p class="alertBox">The paths provided throughout the HCP Course practical instructions, both for locating the software to run and for locating the data to process, will very likely not be correct for your particular configuration of software and data. You will be responsible for keeping track of these paths (where you installed the software and where you placed the data) and altering the commands provided in the course materials to reflect your chosen configuration. </p>
                <p>Additionally, the MEG practicals for Day 5 use precompiled MATLAB scripts that will only work on Linux systems. If you don’t have a Linux machine and want to run the MEG practicals, we recommend downloading the Course VM as in Option 1.</p>
                <p><button onclick="javascript:modalClose()">Cancel</button> <button title="begin download" onclick="javascript:window.location.assign('https://db.humanconnectome.org/app/action/ChooseDownloadResources?project=HCP_Resources&resource=CourseData&filePath=HCP_2015_Course_Data.zip')">Begin My Download</button></p>
            </div>
        </div> <!-- end "download course VM" modal -->

        <div class="modal" id="prerequisite-software" style="min-height:0px !important;">
            <div class="modal-title">Software Prerequisites for Practical Data <span class="modal-close" onclick="modalClose()"><img src="/img/close-dark.png" /></span></div>
            <div class="modal_content">
                <h1>Prerequisite software for running the 2015 HCP Course Practicals</h1>
                <ul>
                    <li><a href="http://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FslInstallation" target="_blank" title="Download FSL">FSL version 5.0.6</a></li>
                    <li><a href="https://www.humanconnectome.org/software/get-connectome-workbench.html" target="_blank" title="Get Connectome Workbench">Connectome Workbench v1.1.1</a></li>
                    <li><a href="https://www.humanconnectome.org/documentation/HCP-pipelines/meg-pipeline.html" target="_blank" title="Get MEG Connectome Pipelines">megconnectome v2.2 and fieldtrip-r9924</a></li>
                    <li><a href="http://www.mathworks.com/products/matlab/" target="_blank" title="Download MATLAB">MATLAB</a> with the official MATLAB toolboxes: Statistics, Bioinformatics, and Signal Processing OR <a href="http://www.gnu.org/software/octave/download.html" target="_blank" title="Download Octave">Octave v.3.8.0</a> or greater with toolboxes: control, general, signal, and statistics</li>
                    <li><a href="https://github.com/Washington-University/Pipelines/wiki/v3.4.0-Release-Notes,-Installation,-and-Usage#installation" target="_blank" title="Download HCP Pipeline Scripts">HCP Pipeline Scripts v3.4.0</a></li>
                </ul>
                <p>Requisite for running the HCP Pipelines: <a href="ftp://surfer.nmr.mgh.harvard.edu/pub/dist/freesurfer/5.3.0-HCP" target="_blank" title="Download Freesurfer">FreeSurfer version 5.3.0-HCP</a>.</p>
                <p>See this <a href="https://github.com/Washington-University/Pipelines/wiki/v3.4.0-Release-Notes,-Installation,-and-Usage#prerequisites" target="_blank" title="Installation Instructions">installation guidance</a> for using the prerequisite software in the HCP pipelines.</p>
                <p><button onclick="javascript:modalClose()">Close</button></p>
            </div>
        </div> <!-- end "download course VM" modal -->


    </div> <!-- middle -->

    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/footer.php'); ?>


</div>
<!-- end container -->

<script src="https://www.google.com/jsapi?key=AIzaSyA5m1Nc8ws2BbmPRwKu5gFradvD_hgq6G0" type="text/javascript"></script>

<!-- google analytics -->
<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-18630139-1']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

</script>
</body>
</html>
