<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Register for HCP Courses | Human Connectome Project</title>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/header-includes.php'); ?>

</head>

<body>

<!-- site container -->
<div id="site_container">

    <div id="NIH_header2">
        <span><a href="http://www.neuroscienceblueprint.nih.gov/connectome/" title="NIH Human Connectome Project" style="color:#b4cfff; text-decoration:none;">NIH Blueprint: The Human Connectome Project</a></span>
    </div>

    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/storefront-topnav.php'); ?>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/db-login.php'); ?>

    <!-- Breadcrumbs -->
    <div id="breadcrumbs">
        <p>&raquo; <a href="/courses/">HCP Courses</a> &gt; course registration</p></div>
    <!-- end breadcrumbs -->

    <!-- "Middle" area contains all content between the top navigation and the footer -->
    <div id="middle">
        <div id="content">
            <noscript>
                <p style="color:#c00; clear:both;"><strong>Alert:</strong> You appear to have JavaScript turned off. Registration and access for ConnectomeDB requires JavaScript. Please enable it in your browser before proceeding.</p>
            </noscript>

            <h2 id="storeHead">HCP Course Registration Form</h2>

            <?php
            // check storefront status
            function checkRegStatus() {
                global $db;
                $q = "SELECT * FROM registration_status ORDER BY last_modified DESC LIMIT 1;"; // get the most recent storefront status only
                $r = mysqli_query($db,$q) or die("<p>Registration Ordering is currently undergoing maintenance.</p>");
                return mysqli_fetch_array($r);
            }
            $storefront = checkRegStatus();


            // check for custom order code
            if ($_GET['waitlistId']) :
                $q = "SELECT * FROM registration_waitlist WHERE id='".$_GET['waitlistId']."' LIMIT 1;";
                $r = mysqli_query($db,$q) or die ($q);
                if (mysqli_num_rows($r) > 0) {
                    $waitlistInfo = mysqli_fetch_assoc($r);
                    $customOrder = array('name' => $waitlistInfo['name'], 'type' => 'waitlistOrder');
                }
            endif;

            if ($_GET['customChargeId']) :
                $q = "SELECT * FROM custom_orders WHERE id='".$_GET['customChargeId']."' LIMIT 1;";
                $r = mysqli_query($db,$q) or die ($q);
                if (mysqli_num_rows($r) > 0) {
                    $customOrderInfo = mysqli_fetch_assoc($r);
                    $customOrder = array(
                        'name' => $customOrderInfo['customer_name'],
                        'email' => $customOrderInfo['customer_email'],
                        'order_id' => $customOrderInfo['original_order_id'],
                        'amount' => $customOrderInfo['amount'],
                        'charge_title' => $customOrderInfo['charge_title'],
                        'charge_description' => $customOrderInfo['charge_justification'],
                        'type' => 'customCharge'
                    );

                    // don't allow custom charges to be applied more than once.
                    if ($customOrderInfo['status']=='applied') die ('<p>Sorry, but there was a problem redeeming this charge code. Please contact orders@humanconnectome.org if you think you have received this message in error.</p>');

                    $q = "SELECT * FROM orders WHERE id='".$customOrder['order_id']."' LIMIT 1;";
                    $r = mysqli_query($db,$q) or die ($q);
                    $originalOrderInfo = mysqli_fetch_assoc($r);
                } else {
                    // don't try to apply unrecognized charge codes.
                    die('<p>Sorry, but there was a problem redeeming this charge code. Please contact orders@humanconnectome.org if you think you have received this message in error.</p>');
                }
            endif;


            if ($storefront['status'] !== 'open' && !isset($customOrder)) :
                ?>
                <script type="text/javascript">
                    // temporary "coming soon" form disabler

                    $(document).ready(function(){
                        $('select').prop('disabled',true);
                        $('input').prop('disabled','disabled');
                        $('textarea').prop('disabled','disabled');
                        $('.add-drive').hide();
                        $('#cinab-order').removeProp('action');
                        $('#storeHead').append(' - <a href="javascript:showModal(\'coming-soon\')">Coming Soon</a>');
                        showModal('coming-soon');
                    });


                </script>
            <?php endif; ?>
            <?php if (isset($customOrder['type']) && $customOrder['type']=='customCharge') : ?>
                <form id="hcp-course-reg" class="hcpForm" method="post" action="/courses/confirm-custom-charge.php">
            <?php else : ?>
                <form id="hcp-course-reg" class="hcpForm" method="post" action="/courses/confirm-registration.php">
            <?php endif ?>

                    <script type="text/javascript" src="/js/registration-validation.js"></script>

                    <h3>1: Registration Selection</h3>
                    <?php if (isset($customOrder['type']) && $customOrder['type']=='customCharge') : ?>
                        <input type="hidden" name="itemcnt" id="itemcnt" value="1" />
                        <!-- is this working? -->
                    <?php else : ?>
                        <input type="hidden" name="itemcnt" id="itemcnt" value="0" />
                    <?php endif; ?>

                    <div class="hcpForm-wrapper" id="product-info" style="overflow:auto;">
                        <div class="hidden error-messages error-message-product">
                            <p><strong>Errors Found:</strong></p>
                            <ul class="error-list">
                                <li>Things fall apart, the centre cannot hold.</li>
                            </ul>
                        </div>

                        <?php
                        /* DEFINE FUNCTION TO HANDLE PRODUCT DISPLAY.
                         * Requires an array that has single product information
                         */
                        $capacityExceeded = false;

                        function displayProducts($course) {
                            global $db, $capacityExceeded, $customOrder;
                            // if it exists, show discount info
                            if ($_GET['discountCode']) :
                                $discountCode = $_GET['discountCode'];
                                $q = "SELECT * FROM discount_codes WHERE code_id = '".$discountCode."';";
                                $r = mysqli_query($db,$q) or die ($q);

                                if (mysqli_num_rows($r) === 0) :
                                    // no match for code found, treat as invalid code
                                    ?>
                                    <div class="discount-banner invalid">You have entered an invalid discount code. Please contact <a href="mailto:orders@humanconnectome.org">orders@humanconnectome.org</a> if you think this message is incorrect.</div>
                                    <?php
                                else:
                                    $discountInfo = mysqli_fetch_assoc($r);
                                    if ($discountInfo['remaining_eligible'] < 1) :
                                        ?>
                                        <div class="discount-banner ineligible">Sorry, there are no eligible slots remaining for this discount code. You can register at full price, or email <a href="mailto:orders@humanconnectome.org">orders.humanconnectome.org</a> to join a waiting list to see if more discounted slots become available.</div>
                                    <?php else : ?>
                                        <div class="discount-banner eligible"><strong>Discount Qualified!</strong> <?php echo $discountInfo['discount_description'] ?><br><br><strong>Only <?php echo ($discountInfo['remaining_eligible'] > 1) ? $discountInfo['remaining_eligible']. " eligible spots remain." : "one eligible spot remains!"; ?></strong></div>
                                        <input type="hidden" name="discountCode" value="<?php echo $discountCode ?>" />
                                        <?php
                                    endif;
                                endif;

                            endif;
                            ?>
                            <div class="product-box" id="<?php echo $course['course_id']; ?>">
                                <?php
                                $q = "SELECT * FROM orders WHERE (status = 'open') and (order_type='course') and (course_registered='".$course['course_id']."');";
                                $r = mysqli_query($db,$q) or die($q);

                                $course['registrations'] = mysqli_num_rows($r);

                                if ($course['registrations'] > ($course['capacity'] * 0.8)) :
                                    if ($course['registrations'] < $course['capacity']) :
                                        ?>
                                        <div class="capacity-warning-message" style="background: #66f; color: #fff; padding: 10px;">
                                            <strong>Hurry, Limited Space Available!</strong> Only <?php echo $course['capacity'] - $course['registrations'] ?> spots remain.
                                        </div>
                                    <?php elseif ($customOrder) : ?>
                                        <div class="capacity-warning-message" style="background: #6c6; color: #fff; padding: 10px;">
                                            <strong>Welcome, <?php echo $customOrder['name'] ?>!</strong>
                                        </div>
                                    <?php else : ?>
                                        <div class="capacity-exceeded-message" style="background: #c33; color: #fff; padding: 10px;">
                                            <strong>This Course Is Full</strong>
                                        </div>
                                        <?php
                                        $capacityExceeded = true;
                                    endif;
                                endif;
                                ?>
                                <input type="hidden" name="<?php echo $course['cashnet_id'].'qty'; ?>" value="0" class="qty item-info" disabled="disabled" />
                                <input type="hidden" name="itemcode[]" value="<?php echo $course['cashnet_id'] ?>" class="itemcode item-info" disabled="disabled" />
                                <input type="hidden" name="regtype[]" value="" class="regtype item-info" disabled="disabled" />
                                <input type="hidden" name="courseid[]" value="<?php echo $course['course_id'] ?>" class="item-info" disabled="disabled" />
                                <input type="hidden" name="coursename[]" value="<?php echo $course['course_title'] ?>" class="item-info" disabled="disabled" />
                                <div class="product-description">
                                    <h3><?php echo $course['course_title']; ?> </h3>
                                    <p><?php echo $course['course_description']; ?></p>
                                    <p><strong>Course Dates: </strong><?php echo date ('D, M j, Y', strtotime($course['course_start_date']) ) ?> to <?php echo date ('D, M j, Y', strtotime($course['course_end_date']) ) ?></p>

                                    <?php if (!$capacityExceeded || $customOrder) : ?>
                                        <p>Select Registration Type (Required):
                                            <?php
                                            // parse JSON listing of registration fees into a PHP array
                                            $jsonString = $course['registration_fee'];
                                            // $jsonString = '{ "data": [{"role":"student","cost":"900.00"},{"role":"postdoc/academic","cost":"1500.00"},{"role":"commercial","cost":"2700"}]}';
                                            $feeScale = json_decode($jsonString,true);
                                            // var_dump($feeScale); echo $jsonString;
                                            ?>
                                            <select name="item-price[]" class="item-price-select">
                                                <option value="">Not Registered</option>
                                                <?php foreach ($feeScale as $k=>$array) : ?>
                                                    <?php foreach ($array as $key=>$fees) : ?>
                                                        <option class="<?php echo stripslashes($fees['role']) ?>" value="<?php echo $fees['cost'] ?>" data-regtype="<?php echo $fees['role'] ?>"><?php echo $fees['role'].": $".$fees['cost'] ?></option>
                                                    <?php endforeach; ?>
                                                <?php endforeach; ?>
                                            </select>
                                        </p>
                                    <?php endif; ?>

                                    <?php if ($course['accommodation_fee'] !== null) : ?>
                                        <?php
                                        // get accommodation info
                                        $q = "SELECT * FROM accommodations WHERE course_id='".$course['course_id']."';";
                                        $accommodations = mysqli_query($db,$q) or die ($q);

                                        $q = "SELECT sum(capacity) as total_capacity FROM accommodations WHERE course_id='".$course['course_id']."';";
                                        $r = mysqli_query($db,$q) or die($q);
                                        $availableCapacity = mysqli_fetch_assoc($r);

                                        $q = "SELECT count(status) as total_reg FROM accommodation_registration WHERE course_id='".$course['course_id']."' AND status='open';";
                                        $r = mysqli_query($db,$q) or die($q);
                                        $takenCapacity = mysqli_fetch_assoc($r);

                                        if ($availableCapacity['total_capacity'] > $takenCapacity['total_reg']) : ?>
                                        <p><strong>Accommodations Available:</strong> A limited amount of accommodations are available on-site and can be added to your registration. (<a href="/courses/2018/exploring-the-human-connectome.php#accommodations">Accommodation Information</a>) </p>
                                        <p>Select Accommodation Option (Optional):
                                            <select name="item-accommodation" class="item-accommodation-select">
                                                <option value="0">I will find accommodations off-campus</option>

                                                    <?php foreach ($accommodations as $fees) : ?>
                                                        <option value="<?php echo $fees['id'] ?>" data-regtype="<?php echo $fees['title'] ?>"><?php echo $fees['title'].":  add $".number_format($fees['cost'],2) ?></option>
                                                    <?php endforeach; ?>

                                            </select>
                                        </p>
                                        <?php else : ?>
                                            <p><em>On-site accommodations are sold out.</em></p>
                                        <?php endif; ?>

                                    <?php endif; ?>
                                    <p><em>Note: An additional 2.75% Cashnet fee will be assessed to each registration.</em></p>


                                    <div class="order-panel">
                                        <img src="/img/icon-HCP-course-small.png" />
                                    </div>
                                </div> <!-- /product description -->

                            </div>

                            <?
                        }
                        /* end PRODUCT DISPLAY function
                        */

                        // add custom display function for add-on charges
                        function displayCustomCharge() {
                            global $customOrder, $db, $originalOrderInfo;
                            ?>
                            <div class="product-box">

                                <div class="capacity-warning-message" style="background: #6c6; color: #fff; padding: 10px;">
                                    <strong>Welcome, <?php echo $customOrder['name'] ?>!</strong>
                                </div>

                                <input type="hidden" name="HCP-ADDONqty" value="1" />
                                <input type="hidden" name="itemcode" value="HCP-ADDON" />
                                <input type="hidden" name="item-price" value="<?php echo $customOrder['amount'] ?>" />
                                <input type="hidden" name="customChargeId" value="<?php echo $_GET['customChargeId'] ?>" />
                                <input type="hidden" name="customChargeTitle" value="<?php echo $customOrder['charge_title'] ?>" />
                                <div class="product-description">
                                    <h3>Custom Charge: <?php echo $customOrder['charge_title']; ?> </h3>
                                    <p>This custom charge was created at your request to add additional service to your existing order (#<?php echo $customOrder['order_id'] ?>). If you have a question regarding this charge, please contact <a href="mailto:orders@humanconnectome.org">orders@humanconnectome.org</a>.</p>
                                    <p><strong>Charge Amount:</strong> $ <?php echo number_format($customOrder['amount'],2) ?></p>
                                    <p><strong>Charge Description:</strong> <?php echo $customOrder['charge_description'] ?></p>
                                    <p><em>Note: An additional 2.25% Cashnet fee will be assessed to each transaction.</em></p>
                                    <div class="order-panel">
                                        <img src="/img/icon-HCP-course-small.png" />
                                    </div>
                                </div> <!-- /product description -->

                            </div>

                            <?
                        }

                        ?>

                        <?php
                        if (isset($customOrder['type']) && $customOrder['type']=='customCharge') :
                            displayCustomCharge();
                        else :
                            // get all CURRENT courses from DB and create a product picker for each
                            $courses = mysqli_query($db,'SELECT * FROM courses WHERE status="current" ORDER BY course_id ASC;');
                            $orderLimit = 1; // could be set in DB if desired
                            while ($course = mysqli_fetch_array($courses)) {
                                displayProducts($course);
                            } // end product loop for current releases
                            mysqli_free_result($courses);
                        endif;
                        ?>

                    </div> <!-- /wrapper -->

                    <?php if (!$capacityExceeded || $customOrder) : ?>

                        <h3>2: Course Registrant Information</h3>

                        <div class="hcpForm-wrapper" id="shipping-info">
                            <div class="hidden error-messages error-message-address error-message-country error-message-general">
                                <p><strong>Errors Found:</strong></p>
                                <ul class="error-list">
                                    <li>Things fall apart, the centre cannot hold.</li>
                                </ul>
                            </div>

                            <div>
                                <p>You will be prompted to fill out your billing address on the next page.</p><br>
                            </div>
                            <div>
                                <label for="customer_name">Registrant Name</label>
                                <input type="text" name="customer_name" style="width:485px" class="required" value="<?php if (isset($customOrder['name'])) echo $customOrder['name'] ?>">
                            </div>
                            <div>
                                <label for="customer_email">Registrant Email Address</label>
                                <input type="text" name="customer_email" style="width:485px" class="required email" value="<?php if (isset($customOrder['email'])) echo $customOrder['email'] ?>" />
                            </div>
                            <div>
                                <label for="customer_institution">Institution</label>
                                <input type="text" name="customer_institution" style="width:485px" value="<?php if (isset($originalOrderInfo['customer_institution'])) echo $originalOrderInfo['customer_institution'] ?>" >
                            </div>
                            <div>
                                <label for="customer_pi">Name of PI / Faculty Mentor (Optional)</label>
                                <input type="text" name="customer_pi" style="width: 485px;" value="<?php if (isset($originalOrderInfo['customer_pi'])) echo $originalOrderInfo['customer_pi'] ?>">
                            </div>
                            <div>
                                <label for="shipping_phone">Contact Phone Number (optional)</label>
                                <input type="text" name="shipping_phone" style="width:485px" class="" value="<?php if (isset($originalOrderInfo['shipping_phone'])) echo $originalOrderInfo['shipping_phone'] ?>" />
                            </div>

                        </div>

                        <h3>3: Billing Information</h3>

                        <div class="hcpForm-wrapper" style="overflow:auto;">
                            <div>
                                <label for="HCP-COUNTRY">Billing Country</label>
                                <select name="HCP-COUNTRY" id="country-select" class="required">
                                    <option value=""></option>
                                    <option value="United States">United States</option>
                                    <option value="Afghanistan">Afghanistan</option>
                                    <option value="Albania">Albania</option>
                                    <option value="Algeria">Algeria</option>
                                    <option value="American Samoa">American Samoa</option>
                                    <option value="Andorra">Andorra</option>
                                    <option value="Anguilla">Anguilla</option>
                                    <option value="Antarctica">Antarctica</option>
                                    <option value="Antigua And Barbuda">Antigua And Barbuda</option>
                                    <option value="Argentina">Argentina</option>
                                    <option value="Armenia">Armenia</option>
                                    <option value="Aruba">Aruba</option>
                                    <option value="Australia">Australia</option>
                                    <option value="Austria">Austria</option>
                                    <option value="Azerbaijan">Azerbaijan</option>
                                    <option value="Bahamas">Bahamas</option>
                                    <option value="Bahrain">Bahrain</option>
                                    <option value="Bangladesh">Bangladesh</option>
                                    <option value="Barbados">Barbados</option>
                                    <option value="Belarus">Belarus</option>
                                    <option value="Belgium">Belgium</option>
                                    <option value="Belize">Belize</option>
                                    <option value="Benin">Benin</option>
                                    <option value="Bermuda">Bermuda</option>
                                    <option value="Bhutan">Bhutan</option>
                                    <option value="Bolivia">Bolivia</option>
                                    <option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
                                    <option value="Botswana">Botswana</option>
                                    <option value="Bouvet Island">Bouvet Island</option>
                                    <option value="Brazil">Brazil</option>
                                    <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
                                    <option value="Brunei Darussalam">Brunei Darussalam</option>
                                    <option value="Bulgaria">Bulgaria</option>
                                    <option value="Burkina Faso">Burkina Faso</option>
                                    <option value="Burundi">Burundi</option>
                                    <option value="Cambodia">Cambodia</option>
                                    <option value="Cameroon">Cameroon</option>
                                    <option value="Canada">Canada</option>
                                    <option value="Cape Verde">Cape Verde</option>
                                    <option value="Cayman Islands">Cayman Islands</option>
                                    <option value="Central African Republic">Central African Republic</option>
                                    <option value="Chad">Chad</option>
                                    <option value="Chile">Chile</option>
                                    <option value="China">China</option>
                                    <option value="Christmas Island">Christmas Island</option>
                                    <option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
                                    <option value="Colombia">Colombia</option>
                                    <option value="Comoros">Comoros</option>
                                    <option value="Congo">Congo</option>
                                    <option value="Congo, the Democratic Republic of the">Congo, the Democratic Republic of the</option>
                                    <option value="Cook Islands">Cook Islands</option>
                                    <option value="Costa Rica">Costa Rica</option>
                                    <option value="Cote d'Ivoire">Cote d'Ivoire</option>
                                    <option value="Croatia">Croatia</option>
                                    <option value="Cyprus">Cyprus</option>
                                    <option value="Czech Republic">Czech Republic</option>
                                    <option value="Denmark">Denmark</option>
                                    <option value="Djibouti">Djibouti</option>
                                    <option value="Dominica">Dominica</option>
                                    <option value="Dominican Republic">Dominican Republic</option>
                                    <option value="East Timor">East Timor</option>
                                    <option value="Ecuador">Ecuador</option>
                                    <option value="Egypt">Egypt</option>
                                    <option value="El Salvador">El Salvador</option>
                                    <option value="England">England</option>
                                    <option value="Equatorial Guinea">Equatorial Guinea</option>
                                    <option value="Eritrea">Eritrea</option>
                                    <option value="Espana">Espana</option>
                                    <option value="Estonia">Estonia</option>
                                    <option value="Ethiopia">Ethiopia</option>
                                    <option value="Falkland Islands">Falkland Islands</option>
                                    <option value="Faroe Islands">Faroe Islands</option>
                                    <option value="Fiji">Fiji</option>
                                    <option value="Finland">Finland</option>
                                    <option value="France">France</option>
                                    <option value="French Guiana">French Guiana</option>
                                    <option value="French Polynesia">French Polynesia</option>
                                    <option value="French Southern Territories">French Southern Territories</option>
                                    <option value="Gabon">Gabon</option>
                                    <option value="Gambia">Gambia</option>
                                    <option value="Georgia">Georgia</option>
                                    <option value="Germany">Germany</option>
                                    <option value="Ghana">Ghana</option>
                                    <option value="Gibraltar">Gibraltar</option>
                                    <option value="Great Britain">Great Britain</option>
                                    <option value="Greece">Greece</option>
                                    <option value="Greenland">Greenland</option>
                                    <option value="Grenada">Grenada</option>
                                    <option value="Guadeloupe">Guadeloupe</option>
                                    <option value="Guam">Guam</option>
                                    <option value="Guatemala">Guatemala</option>
                                    <option value="Guinea">Guinea</option>
                                    <option value="Guinea-Bissau">Guinea-Bissau</option>
                                    <option value="Guyana">Guyana</option>
                                    <option value="Haiti">Haiti</option>
                                    <option value="Heard and Mc Donald Islands">Heard and Mc Donald Islands</option>
                                    <option value="Honduras">Honduras</option>
                                    <option value="Hong Kong">Hong Kong</option>
                                    <option value="Hungary">Hungary</option>
                                    <option value="Iceland">Iceland</option>
                                    <option value="India">India</option>
                                    <option value="Indonesia">Indonesia</option>
                                    <option value="IRAN">IRAN</option>
                                    <option value="Iraq">Iraq</option>
                                    <option value="Ireland">Ireland</option>
                                    <option value="Israel">Israel</option>
                                    <option value="Italy">Italy</option>
                                    <option value="Jamaica">Jamaica</option>
                                    <option value="Japan">Japan</option>
                                    <option value="Jordan">Jordan</option>
                                    <option value="Kazakhstan">Kazakhstan</option>
                                    <option value="Kenya">Kenya</option>
                                    <option value="Kiribati">Kiribati</option>
                                    <option value="KOREA, DEMOCRATIC PEOPLE'S REPUBLIC OF">KOREA, DEMOCRATIC PEOPLE'S REPUBLIC OF</option>
                                    <option value="Korea, Republic of">Korea, Republic of</option>
                                    <option value="Kuwait">Kuwait</option>
                                    <option value="Kyrgyzstan">Kyrgyzstan</option>
                                    <option value="Lao People's Democratic Republic">Lao People's Democratic Republic</option>
                                    <option value="Latvia">Latvia</option>
                                    <option value="Lebanon">Lebanon</option>
                                    <option value="Lesotho">Lesotho</option>
                                    <option value="Liberia">Liberia</option>
                                    <option value="Libya">Libya</option>
                                    <option value="Liechtenstein">Liechtenstein</option>
                                    <option value="Lithuania">Lithuania</option>
                                    <option value="Luxembourg">Luxembourg</option>
                                    <option value="MACAO">MACAO</option>
                                    <option value="Macedonia">Macedonia</option>
                                    <option value="Madagascar">Madagascar</option>
                                    <option value="Malawi">Malawi</option>
                                    <option value="Malaysia">Malaysia</option>
                                    <option value="Maldives">Maldives</option>
                                    <option value="Mali">Mali</option>
                                    <option value="Malta">Malta</option>
                                    <option value="Marshall Islands">Marshall Islands</option>
                                    <option value="Martinique">Martinique</option>
                                    <option value="Mauritania">Mauritania</option>
                                    <option value="Mauritius">Mauritius</option>
                                    <option value="Mayotte">Mayotte</option>
                                    <option value="Mexico">Mexico</option>
                                    <option value="Micronesia, Federated States of">Micronesia, Federated States of</option>
                                    <option value="Moldova, Republic of">Moldova, Republic of</option>
                                    <option value="Monaco">Monaco</option>
                                    <option value="Mongolia">Mongolia</option>
                                    <option value="Montserrat">Montserrat</option>
                                    <option value="Morocco">Morocco</option>
                                    <option value="Mozambique">Mozambique</option>
                                    <option value="Myanmar">Myanmar</option>
                                    <option value="Namibia">Namibia</option>
                                    <option value="Nauru">Nauru</option>
                                    <option value="Nepal">Nepal</option>
                                    <option value="Netherlands">Netherlands</option>
                                    <option value="Netherlands Antilles">Netherlands Antilles</option>
                                    <option value="New Caledonia">New Caledonia</option>
                                    <option value="New Zealand">New Zealand</option>
                                    <option value="Nicaragua">Nicaragua</option>
                                    <option value="Niger">Niger</option>
                                    <option value="Nigeria">Nigeria</option>
                                    <option value="Niue">Niue</option>
                                    <option value="Norfolk Island">Norfolk Island</option>
                                    <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                                    <option value="Norway">Norway</option>
                                    <option value="Oman">Oman</option>
                                    <option value="Pakistan">Pakistan</option>
                                    <option value="Palau">Palau</option>
                                    <option value="Panama">Panama</option>
                                    <option value="Papua New Guinea">Papua New Guinea</option>
                                    <option value="Paraguay">Paraguay</option>
                                    <option value="Peru">Peru</option>
                                    <option value="Philippines">Philippines</option>
                                    <option value="Pitcairn">Pitcairn</option>
                                    <option value="Poland">Poland</option>
                                    <option value="Portugal">Portugal</option>
                                    <option value="Puerto Rico">Puerto Rico</option>
                                    <option value="Qatar">Qatar</option>
                                    <option value="Reunion">Reunion</option>
                                    <option value="Romania">Romania</option>
                                    <option value="Russia">Russia</option>
                                    <option value="Russian Federation">Russian Federation</option>
                                    <option value="Rwanda">Rwanda</option>
                                    <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                                    <option value="Saint Lucia">Saint Lucia</option>
                                    <option value="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option>
                                    <option value="Samoa (Independent)">Samoa (Independent)</option>
                                    <option value="San Marino">San Marino</option>
                                    <option value="Sao Tome and Principe">Sao Tome and Principe</option>
                                    <option value="Saudi Arabia">Saudi Arabia</option>
                                    <option value="Scotland">Scotland</option>
                                    <option value="Senegal">Senegal</option>
                                    <option value="Serbia and Montenegro">Serbia and Montenegro</option>
                                    <option value="Seychelles">Seychelles</option>
                                    <option value="Sierra Leone">Sierra Leone</option>
                                    <option value="Singapore">Singapore</option>
                                    <option value="Slovakia">Slovakia</option>
                                    <option value="Slovenia">Slovenia</option>
                                    <option value="Solomon Islands">Solomon Islands</option>
                                    <option value="Somalia">Somalia</option>
                                    <option value="South Africa">South Africa</option>
                                    <option value="South Georgia and the South Sandwich Islands">South Georgia and the South Sandwich Islands</option>
                                    <option value="Spain">Spain</option>
                                    <option value="Sri Lanka">Sri Lanka</option>
                                    <option value="St. Helena">St. Helena</option>
                                    <option value="St. Pierre and Miquelon">St. Pierre and Miquelon</option>
                                    <option value="Suriname">Suriname</option>
                                    <option value="Svalbard and Jan Mayen Islands">Svalbard and Jan Mayen Islands</option>
                                    <option value="Swaziland">Swaziland</option>
                                    <option value="Sweden">Sweden</option>
                                    <option value="Switzerland">Switzerland</option>
                                    <option value="Taiwan">Taiwan</option>
                                    <option value="Tajikistan">Tajikistan</option>
                                    <option value="Tanzania">Tanzania</option>
                                    <option value="Thailand">Thailand</option>
                                    <option value="Togo">Togo</option>
                                    <option value="Tokelau">Tokelau</option>
                                    <option value="Tonga">Tonga</option>
                                    <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                                    <option value="Tunisia">Tunisia</option>
                                    <option value="Turkey">Turkey</option>
                                    <option value="Turkmenistan">Turkmenistan</option>
                                    <option value="Turks and Caicos Islands">Turks and Caicos Islands</option>
                                    <option value="Tuvalu">Tuvalu</option>
                                    <option value="Uganda">Uganda</option>
                                    <option value="Ukraine">Ukraine</option>
                                    <option value="United Arab Emirates">United Arab Emirates</option>
                                    <option value="United Kingdom">United Kingdom</option>
                                    <option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
                                    <option value="Uruguay">Uruguay</option>
                                    <option value="Uzbekistan">Uzbekistan</option>
                                    <option value="Vanuatu">Vanuatu</option>
                                    <option value="Vatican City State (Holy See)">Vatican City State (Holy See)</option>
                                    <option value="Venezuela">Venezuela</option>
                                    <option value="Viet Nam">Viet Nam</option>
                                    <option value="Virgin Islands (British)">Virgin Islands (British)</option>
                                    <option value="Virgin Islands (U.S.)">Virgin Islands (U.S.)</option>
                                    <option value="Wales">Wales</option>
                                    <option value="Wallis and Futuna Islands">Wallis and Futuna Islands</option>
                                    <option value="Western Sahara">Western Sahara</option>
                                    <option value="Yemen">Yemen</option>
                                    <option value="Zambia">Zambia</option>
                                    <option value="Zimbabwe">Zimbabwe</option>
                                </select>

                                <?php if (isset($originalOrderInfo['billing_country'])) : ?>
                                    <script>
                                        $(document).ready(function(){
                                            $('#country-select').find('option[value="<?php echo $originalOrderInfo['billing_country'] ?>"]').prop('selected','selected');
                                        });
                                    </script>
                                <?php endif; ?>
                            </div>
                            <div>
                                <label for="HCP-NAME_G">Name of Billing Contact</label>
                                <input type="text" name="HCP-NAME_G" style="width:485px" class="required" value="<?php if (isset($originalOrderInfo['billing_contact'])) echo $originalOrderInfo['billing_contact'] ?>" />
                            </div>
                            <div>
                                <label for="HCP-INSTITUTION">Institution</label>
                                <input type="text" name="HCP-INSTITUTION" style="width:485px"  />
                            </div>
                            <div>
                                <label for="HCP-ADDR_G">Street Address</label>
                                <input type="text" name="HCP-ADDR_G" style="width:485px" class="required" value="<?php if (isset($originalOrderInfo['billing_address'])) echo $originalOrderInfo['billing_address'] ?>" />
                            </div>
                            <div>
                                <label for="billing_office">Office / Suite / Etc</label>
                                <textarea name="billing_office" style="width:485px" rows="3"></textarea>
                            </div>
                            <div>
                                <label for="HCP-CITY_G">City</label>
                                <input type="text" name="HCP-CITY_G" style="width:290px" class="required" value="<?php if (isset($originalOrderInfo['billing_city'])) echo $originalOrderInfo['billing_city'] ?>" />
                            </div>
                            <div class="state-US">
                                <label for="shipping_state">State</label>
                                <select name="billing_state" class="required">
                                    <option selected="selected"></option>
                                    <option value="AL">Alabama</option>
                                    <option value="AK">Alaska</option>
                                    <option value="AZ">Arizona</option>
                                    <option value="AR">Arkansas</option>
                                    <option value="CA">California</option>
                                    <option value="CO">Colorado</option>
                                    <option value="CT">Connecticut</option>
                                    <option value="DC">District Of Columbia</option>
                                    <option value="DE">Delaware</option>
                                    <option value="FL">Florida</option>
                                    <option value="GA">Georgia</option>
                                    <option value="HI">Hawaii</option>
                                    <option value="ID">Idaho</option>
                                    <option value="IL">Illinois</option>
                                    <option value="IN">Indiana</option>
                                    <option value="IA">Iowa</option>
                                    <option value="KS">Kansas</option>
                                    <option value="KY">Kentucky</option>
                                    <option value="LA">Louisiana</option>
                                    <option value="ME">Maine</option>
                                    <option value="MD">Maryland</option>
                                    <option value="MA">Massachusetts</option>
                                    <option value="MI">Michigan</option>
                                    <option value="MN">Minnesota</option>
                                    <option value="MS">Mississippi</option>
                                    <option value="MO">Missouri</option>
                                    <option value="MT">Montana</option>
                                    <option value="NE">Nebraska</option>
                                    <option value="NV">Nevada</option>
                                    <option value="NH">New Hampshire</option>
                                    <option value="NJ">New Jersey</option>
                                    <option value="NM">New Mexico</option>
                                    <option value="NY">New York</option>
                                    <option value="NC">North Carolina</option>
                                    <option value="ND">North Dakota</option>
                                    <option value="OH">Ohio</option>
                                    <option value="OK">Oklahoma</option>
                                    <option value="OR">Oregon</option>
                                    <option value="PA">Pennsylvania</option>
                                    <option value="RI">Rhode Island</option>
                                    <option value="SC">South Carolina</option>
                                    <option value="SD">South Dakota</option>
                                    <option value="TN">Tennessee</option>
                                    <option value="TX">Texas</option>
                                    <option value="UT">Utah</option>
                                    <option value="VT">Vermont</option>
                                    <option value="VA">Virginia</option>
                                    <option value="WA">Washington</option>
                                    <option value="WV">West Virginia</option>
                                    <option value="WI">Wisconsin</option>
                                    <option value="WY">Wyoming</option>
                                </select>

                                <?php if (isset($originalOrderInfo['billing_country']) && $originalOrderInfo['billing_country'] == 'United States') : ?>
                                    <script>
                                        $(document).ready(function(){
                                            $('select[name=billing_state]').find('option[value="<?php echo $originalOrderInfo['billing_state'] ?>"]').prop('selected','selected');
                                        });
                                    </script>
                                <?php endif; ?>
                            </div>
                            <div class="state-int hidden">
                                <label for="shipping_state">State / Province / Region</label>
                                <input type="text" name="billing_state" style="width:150px" disabled="disabled" />

                                <?php if (isset($originalOrderInfo['billing_country']) && $originalOrderInfo['billing_country'] !== 'United States') : ?>
                                    <script>
                                        $(document).ready(function(){
                                            $('.state-US').addClass('hidden').find('select').prop('disabled','disabled').removeClass('required');
                                            $('.state-int').removeClass('hidden').find('input').val('<?php echo $originalOrderInfo['billing_state'] ?>').prop('disabled',false);
                                        });
                                    </script>
                                <?php endif; ?>
                            </div>
                            <div>
                                <label for="HCP-ZIP_G">ZIP/Postal Code</label>
                                <input type="text" name="HCP-ZIP_G" style="width:90px" class="required" value="<?php if (isset($originalOrderInfo['billing_postal_code'])) echo $originalOrderInfo['billing_postal_code'] ?>" />
                            </div>

                        </div>

                        <h3>4: Custom Requirements or Requests</h3>
                        <div class="hcpForm-wrapper" style="overflow:auto">
                            <div>
                                <p>If you have any special dietary restrictions or special needs accommodations that you would like us to know about, please let us know in this space. </p><br>
                            </div>
                            <div>
                                <label for="customer_notes">Custom Order Requests</label>
                                <textarea name="customer_notes" style="width:485px" rows="3"></textarea>
                            </div>
                        </div>

                        <ul>
                            <li>Only credit card payments can be accepted at this time. Payments are securely processed by an eTransact storefront set up by Washington University in St Louis. The Human Connectome Project website does not store or track credit card information in its database. </li>
                        </ul>
                        <div class="" id="order-cost">
                            <div class="g-recaptcha" data-sitekey="6LcXMzEUAAAAAHwC1l4GRLw-AsCp2q-_PAQtzZyh"></div>
                            <br>
                            <?php if (!$_GET['noValidation']) : ?>
                                <!-- validates order -->
                                <p style="margin-bottom:0;"><input type="button" class="highlight" value="Proceed to Checkout" onclick="validateMe('#hcp-course-reg')" /></p>
                            <?php else : ?>
                                <p style="margin-bottom:0;"><input type="submit" class="highlight" value="Proceed to Checkout" /></p>
                                <input type="hidden" name="dut" value="true" />
                            <?php endif; ?>
                        </div>

                    <?php else : ?>
                        <iframe src="waitlist-signup.php?courseId=HCP-REG-3" frameborder="0" height="450" width="560"></iframe>
                    <?php endif; ?>

                </form>

                <!-- modal overlays -->
                <div id="page-mask" class="pm" onclick="javascript:modalClose();"></div>

                <div class="modal" id="coming-soon" style="min-height:0px !important;">
                    <div class="modal-title">HCP Course Registration Form Closed <span class="modal-close" onclick="modalClose()"><img src="/img/close-dark.png" /></span></div>
                    <div class="modal_content">
                        <h1><img src="/img/icon-HCP-course.png" align="right" style="margin:0 0 0 30px" /><?php echo $storefront['message_title'] ?></h1>
                        <?php echo $storefront['message_text'] ?>
                        <p><button onclick="javascript:modalClose()">Okay Thanks</button> </p>
                    </div>
                </div> <!-- end "coming soon" modal -->


                <?php
                mysqli_close($db);
                ?>
        </div>
        <!-- /#Content -->

        <div id="sidebar-rt">

            <div id="sidebar-content">
                <?php include_once($_SERVER['DOCUMENT_ROOT'].'/incl/sidebar-2019-courseinfo.php'); ?>
            </div>
        </div>
        <!-- /sidebar -->


    </div> <!-- middle -->

    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/footer.php'); ?>


</div>
<!-- end container -->

<script src="//www.google.com/jsapi?key=AIzaSyA5m1Nc8ws2BbmPRwKu5gFradvD_hgq6G0" type="text/javascript"></script>
<script type="text/javascript">
    /*
     *  How to restrict a search to a Custom Search Engine
     *  http://www.google.com/cse/
     */

    google.load('search', '1');

    function OnLoad() {
        // Create a search control
        var searchControl = new google.search.SearchControl();

        // Add in a WebSearch
        var webSearch = new google.search.WebSearch();

        // Restrict our search to pages from our CSE
        webSearch.setSiteRestriction('000073750279216385221:vnupk6whx-c');

        // Add the searcher to the SearchControl
        searchControl.addSearcher(webSearch);

        // tell the searcher to draw itself and tell it where to attach
        searchControl.draw(document.getElementById("search"));

        // execute an inital search
        // searchControl.execute('');
    }

    google.setOnLoadCallback(OnLoad);

</script>

<!-- google analytics -->
<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-18630139-1']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

</script>
</body>
</html>
