<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Error | Human Connectome Project</title>
    <meta http-equiv="Description" content="Public access to HCP data. Currently, Phase 1 Pilot Data is available." />
    <meta http-equiv="Keywords" content="Public data release, Human Connectome Project, pilot data, brain connectivity data" />
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/header-includes.php'); ?>

</head>

<body>

<!-- site container -->
<div id="site_container">

    <div id="NIH_header2">
        <span><a href="http://www.neuroscienceblueprint.nih.gov/connectome/" title="NIH Human Connectome Project" style="color:#b4cfff; text-decoration:none;">NIH Blueprint: The Human Connectome Project</a></span>
    </div>

    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/storefront-topnav.php'); ?>

    <!-- Breadcrumbs -->
    <div id="breadcrumbs">
        <p>&raquo; <a href="https://www.humanconnectome.org">HCP Home</a> &gt; Storefront</p></div>
    <!-- end breadcrumbs -->

    <!-- "Middle" area contains all content between the top navigation and the footer -->
    <div id="middle">
        <div id="content-wide">
            <?php if (preg_match('/\/courses\/2019%20(.*)/',parse_url($_SERVER['REQUEST_URI'],PHP_URL_PATH))) : ?>
                <h1>Error: Malformed URL Detected</h1>
                <p>Redirecting you to the 2019 course page...</p>
                <script>
                    setTimeout(function(){
                        window.location.assign('/courses/2019/exploring-the-human-connectome.php')
                    },2000);
                </script>
            <?php else : ?>
                <h1>Error - That link didn't go anywhere.</h1>
                <p>Oops. Sorry about that.</p>
            <?php endif; ?>


        </div>
        <!-- /#Content -->

    </div> <!-- middle -->

    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/footer.php'); ?>

</div>
<!-- end container -->

<script src="//www.google.com/jsapi?key=AIzaSyA5m1Nc8ws2BbmPRwKu5gFradvD_hgq6G0" type="text/javascript"></script>
<script type="text/javascript">
    /*
     *  How to restrict a search to a Custom Search Engine
     *  http://www.google.com/cse/
     */

    google.load('search', '1');

    function OnLoad() {
        // Create a search control
        var searchControl = new google.search.SearchControl();

        // Add in a WebSearch
        var webSearch = new google.search.WebSearch();

        // Restrict our search to pages from our CSE
        webSearch.setSiteRestriction('000073750279216385221:vnupk6whx-c');

        // Add the searcher to the SearchControl
        searchControl.addSearcher(webSearch);

        // tell the searcher to draw itself and tell it where to attach
        searchControl.draw(document.getElementById("search"));

        // execute an inital search
        // searchControl.execute('');
    }

    google.setOnLoadCallback(OnLoad);

</script>


<!-- google analytics -->
<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-18630139-1']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

</script>
</body>
</html>
