<div class="sidebox databox">
    <h2>Sign up for Open Access Data</h2>
    <p><img src="/img/icon-signature.png" width="256" height="205" alt="Sign terms" style="margin-left:-10px" /></p>
    <p> Create a free account at <strong><a href="https://db.humanconnectome.org">db.humanconnectome.org</a></strong></p>
    <p style="font-size:11px !important">Terms updated Apr 26, 2013. <br />
        <a href="/data/data-use-terms/open-access.php">View online</a> | <a href="/data/data-use-terms/DataUseTerms-HCP-Open-Access-26Apr2013.pdf">Download PDF</a></p>
</div>