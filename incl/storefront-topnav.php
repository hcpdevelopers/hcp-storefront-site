<?php
// top nav
?>
<!-- Header -->
<div id="header"><img src="/img/header-bg.png" border="0" usemap="#Header" width="1024" height="130" />
    <map name="Header" id="Header">
        <area shape="rect" coords="14,7,400,123" href="/" alt="Human Connectome Project Logo" />
    </map>
</div>
<!-- end Header -->

<!-- top level navigation -->
<div id="topnav">
    <ul id="nav">
        <li><a href="/">Home</a></li>
        <li><a href="/courses/">HCP Courses</a>
            <ul>
                <li><a href="/courses/course-registration.php">Course Registration</a></li>
                <li><a href="/courses/2019">2019 Course Info &amp; Materials</a></li>
                <li><a href="/courses/2018">2018 Course Info &amp; Materials</a></li>
                <li><a href="/courses/">All Courses</a></li>
            </ul>
        </li>
        <li><a href="/support/">Customer Support</a></li>
        <li><a href="https://humanconnectome.org/">HCP Project Site</a></li>
    </ul>
</div>
<div id="search">
    <!--   If Javascript is not enabled, nothing will display here -->
    <script>
        (function() {
            var cx = '000073750279216385221:vnupk6whx-c';
            var gcse = document.createElement('script');
            gcse.type = 'text/javascript';
            gcse.async = true;
            gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
                '//www.google.com/cse/cse.js?cx=' + cx;
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(gcse, s);
        })();
    </script>
    <gcse:search></gcse:search>
</div> <!-- Search box -->
<link rel="stylesheet" href="/css/google-cse-customizations.css" />
<!-- End Nav -->
