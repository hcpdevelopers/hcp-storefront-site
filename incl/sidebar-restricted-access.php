<div class="sidebox databox">
    <h2>Apply for Restricted Data</h2>
    <p><img src="/img/icon-signature-red.png" width="256" height="205" alt="Sign terms" style="margin-left:-10px"/></p>
    <p><strong>Download the Restricted Data Use Terms and application.</strong></p>
    <p style="font-size:11px !important">Terms updated Nov 30, 2017<br/>
        <a href="/data/data-use-terms/restricted-access-overview.php">Usage overview</a> | <a
            href="/data/data-use-terms/restricted-access.php">View terms online</a><a
            href="/data/data-use-terms/DataUseTerms_HCP_RestrictedAccess_30Nov2017.pdf"><br/>
            Download PDF Application</a></p>
</div>