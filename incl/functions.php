<?php
function print_date($dateString,$dateFormat = 'M j, Y') {
    $timestamp = strtotime($dateString);
    return date($dateFormat,$timestamp);
}

function base36converter() {
    $input = time();
    $base = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $output = '';

    while ($input > 0) {
        $output = $base[$input%36] . $output;
        $input = floor($input / 36);
    }

    return $output;
}

?>