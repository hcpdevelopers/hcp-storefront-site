<div class="sidebox databox">
    <h2>HCP Course Details</h2>
    <p><img src="/img/courses/hcp-course-2019-uplace-hotel.png" alt="Uplace Hotel" width="235" height="132" /></p>
    <p>The 2019 HCP Course is designed for investigators who are interested in:</p>
    <ul>
        <li>using HCP-style data currently available from the young adult HCP and HCP Lifespan (Development and Aging) projects</li>
        <li>acquiring and analyzing HCP-style imaging and behavioral data at your own institution</li>
        <li>processing your own non-HCP data (including legacy data) using HCP pipelines and methods</li>
        <li>learning to use Connectome Workbench tools and share data using the BALSA imaging database</li>
        <li>learning HCP multi-modal neuroimaging analysis methods, including those that combine MEG and  MRI data </li>
        <li>positioning yourself to capitalize on HCP-style data forthcoming from large-scale projects currently collecting data (e.g., Lifespan HCP development and aging and Connectomes Related to Human Disease projects)</li>
        <li>learning how to obtain data from the NIMH Data Archive (NDA) and setup processing in an Amazon Web Services (AWS) environment</li>
    </ul>
    <p>For more details and accommodation info, see the <a href="/courses/2019">HCP Course Website</a></p>
</div>
<!-- /sidebox -->