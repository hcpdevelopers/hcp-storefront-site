<div class="sidebox databox">
    <h2>HCP Course Details</h2>
    <p><img src="/img/courses/hcp-course-2016-boston.jpg" alt="Exploring the Human Connectome" /></p>
    <p>The 2016 HCP Course is designed for investigators who are interested in:</p>
    <ul>
        <li>using data being collected and distributed by HCP</li>
        <li>acquiring and analyzing HCP-style imaging and behavioral data at your own institution</li>
        <li>processing your own non-HCP data using HCP pipelines and methods</li>
        <li>learning to use Connectome Workbench tools and the CIFTI connectivity data format</li>
        <li>learning HCP multi-modal neuroimaging analysis methods, including those that combine MEG and MRI data</li>
    </ul>
    <p>&nbsp;</p>
    <p>For more details, see the <a href="/courses/2016/exploring-the-human-connectome.php">HCP Course Website</a></p>
</div>
<!-- /sidebox -->