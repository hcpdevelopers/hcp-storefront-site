<?php
// header includes
?>

<link href="/css/hcpv3.css" rel="stylesheet" type="text/css" />
<link href="/css/data.css" rel="stylesheet" type="text/css" />
<link href="/css/sortable.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="//code.jquery.com/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="/js/jquery.easing.1.2.js"></script>
<script src="//code.jquery.com/ui/1.10.1/jquery-ui.js"></script>
<script type="text/javascript" src="/js/global.js"></script>
<script type="text/javascript" src="/js/data-access.js"></script>
<script type="text/javascript" src="/js/dut-controls.js"></script>
<script src="https://www.google.com/recaptcha/api.js"></script>

<?php require_once($_SERVER['DOCUMENT_ROOT'].'/incl/functions.php'); ?>